-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: races
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `sum` decimal(10,1) NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `fk_user_id_idx` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (30,4,'2017-05-29',100.0),(31,3,'2017-05-29',100.0),(32,6,'2017-05-29',100.0);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `races`.`account_AFTER_INSERT` AFTER INSERT ON `account` FOR EACH ROW
BEGIN
	UPDATE user SET user.money = user.money + NEW.sum
    WHERE user.id = NEW.user_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `bet`
--

DROP TABLE IF EXISTS `bet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `bet_type` enum('Победитель','Первая тройка') NOT NULL,
  `sum` decimal(10,1) NOT NULL,
  `bet_date` datetime NOT NULL,
  `result` enum('Победа','Проигрыш','Отмена') DEFAULT NULL,
  `horse_race_name` varchar(45) DEFAULT NULL,
  `horse_race_date` datetime DEFAULT NULL,
  `horse_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_id_idx` (`user_id`),
  KEY `fk_race_name_idx` (`horse_race_name`),
  KEY `fk_bet_horse_id_idx` (`horse_id`),
  KEY `fk_bet_user_id_idx` (`user_id`),
  KEY `fk_bet_race_name_idx` (`horse_race_name`),
  CONSTRAINT `fk_bet_horse_id` FOREIGN KEY (`horse_id`) REFERENCES `horse_in_race` (`horse_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_bet_race_name` FOREIGN KEY (`horse_race_name`) REFERENCES `race` (`race_name`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_bet_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bet`
--

LOCK TABLES `bet` WRITE;
/*!40000 ALTER TABLE `bet` DISABLE KEYS */;
INSERT INTO `bet` VALUES (5,3,'Победитель',15.0,'2017-05-30 06:52:27','Победа',NULL,'2017-05-20 06:00:00',NULL),(6,3,'Победитель',5.0,'2017-05-30 06:52:34','Отмена',NULL,'2017-05-24 12:55:00',5),(7,3,'Победитель',3.0,'2017-05-30 06:52:34','Отмена',NULL,'2017-05-24 12:55:00',8),(8,3,'Победитель',11.0,'2017-05-30 06:52:42','Победа','Саузвелл','2017-05-23 03:25:00',4),(9,3,'Первая тройка',2.0,'2017-05-30 06:52:42','Победа','Саузвелл','2017-05-23 03:25:00',8),(10,3,'Первая тройка',5.0,'2017-05-30 06:52:53','Победа','Нью-Маркет','2017-05-22 08:30:00',NULL),(11,3,'Победитель',3.0,'2017-05-30 06:52:53','Проигрыш','Нью-Маркет','2017-05-22 08:30:00',NULL),(12,3,'Первая тройка',1.0,'2017-05-30 06:53:06','Победа','Мейдан','2017-05-21 07:15:00',4),(13,3,'Победитель',2.0,'2017-05-30 06:53:06','Победа','Мейдан','2017-05-21 07:15:00',7),(14,6,'Победитель',7.0,'2017-05-30 06:53:40','Победа',NULL,'2017-05-20 06:00:00',NULL),(15,6,'Первая тройка',9.0,'2017-05-30 06:53:40','Победа',NULL,'2017-05-20 06:00:00',4),(16,6,'Победитель',5.0,'2017-05-30 06:53:51','Отмена',NULL,'2017-05-24 12:55:00',5),(17,6,'Первая тройка',15.0,'2017-05-30 06:53:51','Отмена',NULL,'2017-05-24 12:55:00',8),(18,6,'Победитель',5.0,'2017-05-30 06:53:55','Проигрыш','Саузвелл','2017-05-23 03:25:00',5),(19,6,'Первая тройка',3.0,'2017-05-30 06:54:01','Проигрыш','Нью-Маркет','2017-05-22 08:30:00',NULL),(20,6,'Первая тройка',20.0,'2017-05-30 06:54:08','Победа','Мейдан','2017-05-21 07:15:00',4),(21,4,'Первая тройка',22.0,'2017-05-30 06:54:24','Отмена',NULL,'2017-05-24 12:55:00',8),(22,4,'Победитель',11.0,'2017-05-30 06:54:32','Отмена',NULL,'2017-05-24 12:55:00',5),(23,4,'Первая тройка',3.0,'2017-05-30 06:54:32','Отмена',NULL,'2017-05-24 12:55:00',12),(24,4,'Победитель',5.0,'2017-05-30 06:54:39','Победа','Саузвелл','2017-05-23 03:25:00',4),(25,4,'Победитель',2.0,'2017-05-30 06:54:39','Проигрыш','Саузвелл','2017-05-23 03:25:00',8),(26,4,'Первая тройка',1.0,'2017-05-30 06:54:46','Проигрыш','Нью-Маркет','2017-05-22 08:30:00',NULL),(27,4,'Победитель',3.0,'2017-05-30 06:54:46','Проигрыш','Нью-Маркет','2017-05-22 08:30:00',6),(28,4,'Победитель',15.0,'2017-05-30 06:54:56','Проигрыш','Мейдан','2017-05-21 07:15:00',NULL),(29,4,'Победитель',2.0,'2017-05-30 06:54:56','Проигрыш','Мейдан','2017-05-21 07:15:00',4);
/*!40000 ALTER TABLE `bet` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `races`.`bet_AFTER_INSERT` AFTER INSERT ON `bet` FOR EACH ROW
BEGIN
	UPDATE user SET user.money = user.money - NEW.sum
    WHERE user.id = NEW.user_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `races`.`bet_AFTER_UPDATE` AFTER UPDATE ON `bet` FOR EACH ROW
BEGIN
	DECLARE temp_coeff DECIMAL(10,1);
    DECLARE prev_sum DECIMAL(10,1);
    
    IF (NEW.result IS NOT NULL) THEN
		IF (NEW.result = 1 AND OLD.bet_type = 1) THEN
		SELECT coeff INTO temp_coeff FROM horse_in_race
					WHERE OLD.horse_race_name = horse_in_race.horse_race_name AND
						OLD.horse_race_date = horse_in_race.horse_race_date AND
						OLD.horse_id = horse_in_race.horse_id;
						
		SELECT money INTO prev_sum FROM user
		WHERE user.id = old.user_id;
		
		UPDATE user 
		SET user.money = user.money + (OLD.sum * temp_coeff)
		WHERE user.id = OLD.user_id;
		END IF;
		
		IF (NEW.result = 1 AND OLD.bet_type = 2) THEN
		SELECT coeff INTO temp_coeff FROM horse_in_race
					WHERE OLD.horse_race_name = horse_in_race.horse_race_name AND
						OLD.horse_race_date = horse_in_race.horse_race_date AND
						OLD.horse_id = horse_in_race.horse_id;
						
		SELECT money INTO prev_sum FROM user
		WHERE user.id = old.user_id;
		
		UPDATE user 
		SET user.money = user.money + (OLD.sum * temp_coeff / 3)
		WHERE user.id = OLD.user_id;
		END IF;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `conversation`
--

DROP TABLE IF EXISTS `conversation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation` (
  `conv_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_one` int(11) DEFAULT NULL,
  `user_two` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT 'Дата создания диалога',
  PRIMARY KEY (`conv_id`),
  KEY `user_one_fk_idx` (`user_one`),
  KEY `user_two_fk_idx` (`user_two`),
  CONSTRAINT `user_one_fk` FOREIGN KEY (`user_one`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_two_fk` FOREIGN KEY (`user_two`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation`
--

LOCK TABLES `conversation` WRITE;
/*!40000 ALTER TABLE `conversation` DISABLE KEYS */;
INSERT INTO `conversation` VALUES (5,1,2,'2017-05-15 15:55:59'),(6,1,1,'2017-05-15 15:56:26'),(7,3,1,'2017-05-15 16:01:18'),(8,3,2,'2017-05-15 16:02:13');
/*!40000 ALTER TABLE `conversation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation_reply`
--

DROP TABLE IF EXISTS `conversation_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversation_reply` (
  `conv_repl_id` int(11) NOT NULL AUTO_INCREMENT,
  `reply` text,
  `user_id_fk` int(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `conv_id_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`conv_repl_id`),
  KEY `user_id_fk_idx` (`user_id_fk`),
  KEY `conv_id_fk_idx` (`conv_id_fk`),
  CONSTRAINT `conv_id_fk` FOREIGN KEY (`conv_id_fk`) REFERENCES `conversation` (`conv_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id_fk` FOREIGN KEY (`user_id_fk`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation_reply`
--

LOCK TABLES `conversation_reply` WRITE;
/*!40000 ALTER TABLE `conversation_reply` DISABLE KEYS */;
INSERT INTO `conversation_reply` VALUES (12,'Привет, Иван, не забудь отправить мне сообщение когда выставишь коэффициенты на скачку от 4 числа',1,'2017-05-15 15:55:59',5),(13,'Приятно поговорить с умным человеком...',1,'2017-05-15 15:56:27',6),(14,'Привет, Админ. Коэффициенты выставил, можно делать ставки, отписываю тебе',2,'2017-05-15 15:57:40',5),(15,'Здравствуйте, администратор. Меня невозможно найти, сложно потерять и легко забыть, вот такая я странная и спешу сообщить вам об этом',3,'2017-05-15 16:01:18',7),(16,'Разведка сообщила мне, что вы являетесь букмекером. Скажите, как можно заработать много денег на ставках?',3,'2017-05-15 16:02:13',8);
/*!40000 ALTER TABLE `conversation_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hippodrom`
--

DROP TABLE IF EXISTS `hippodrom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hippodrom` (
  `name` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `country` varchar(45) NOT NULL,
  `length` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hippodrom`
--

LOCK TABLES `hippodrom` WRITE;
/*!40000 ALTER TABLE `hippodrom` DISABLE KEYS */;
INSERT INTO `hippodrom` VALUES ('Лоншан','Париж','Франция',1001),('Мейдан','Кейптаун','ЮАР',2500),('Нью-Маркет','Лондон','Англия',1800),('Саузвелл','Ноттингемпшир','Англия',1407);
/*!40000 ALTER TABLE `hippodrom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horse`
--

DROP TABLE IF EXISTS `horse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `color` varchar(45) NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horse`
--

LOCK TABLES `horse` WRITE;
/*!40000 ALTER TABLE `horse` DISABLE KEYS */;
INSERT INTO `horse` VALUES (1,'Немо','Темно-синий',300),(2,'Агат','Коричневый',311),(3,'Невил','Бело-коричневый',350),(4,'Ральф','Черный',339),(5,'Лаки','Черно-белый',325),(6,'Нико','Темно-коричневый',316),(7,'Альма','Черный',305),(8,'Эльдорадо','Красный',299),(9,'Кристина','Коричневый',309),(10,'Альфред','Темно-коричневый',302),(11,'Мални','Светло-серый',322),(12,'Кромка','Серо-коричневый',313),(13,'Хлоя','Белый',281);
/*!40000 ALTER TABLE `horse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horse_in_race`
--

DROP TABLE IF EXISTS `horse_in_race`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horse_in_race` (
  `horse_race_date` datetime NOT NULL,
  `horse_race_name` varchar(45) NOT NULL,
  `horse_id` int(11) NOT NULL,
  `status` enum('Open','Decline') NOT NULL DEFAULT 'Open',
  `coeff` decimal(10,1) DEFAULT NULL,
  `place` int(2) DEFAULT NULL,
  PRIMARY KEY (`horse_race_date`,`horse_race_name`,`horse_id`),
  KEY `fk_horse_id_idx` (`horse_id`),
  KEY `fk_in_race_name_idx` (`horse_race_name`),
  CONSTRAINT `fk_horse` FOREIGN KEY (`horse_id`) REFERENCES `horse` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_in_race_name` FOREIGN KEY (`horse_race_name`) REFERENCES `hippodrom` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horse_in_race`
--

LOCK TABLES `horse_in_race` WRITE;
/*!40000 ALTER TABLE `horse_in_race` DISABLE KEYS */;
INSERT INTO `horse_in_race` VALUES ('2017-05-20 06:00:00','Лоншан',1,'Open',1.5,4),('2017-05-20 06:00:00','Лоншан',2,'Open',2.2,1),('2017-05-20 06:00:00','Лоншан',3,'Open',2.6,3),('2017-05-20 06:00:00','Лоншан',4,'Open',3.1,2),('2017-05-21 07:15:00','Мейдан',1,'Open',4.4,3),('2017-05-21 07:15:00','Мейдан',3,'Open',3.2,4),('2017-05-21 07:15:00','Мейдан',4,'Open',2.5,2),('2017-05-21 07:15:00','Мейдан',7,'Open',1.9,1),('2017-05-22 08:30:00','Нью-Маркет',1,'Open',1.5,1),('2017-05-22 08:30:00','Нью-Маркет',2,'Open',5.2,4),('2017-05-22 08:30:00','Нью-Маркет',6,'Open',1.2,2),('2017-05-22 08:30:00','Нью-Маркет',12,'Open',3.5,3),('2017-05-23 03:25:00','Саузвелл',4,'Open',2.3,1),('2017-05-23 03:25:00','Саузвелл',5,'Open',3.4,2),('2017-05-23 03:25:00','Саузвелл',8,'Open',2.5,3),('2017-05-23 03:25:00','Саузвелл',10,'Open',2.8,4),('2017-05-24 12:55:00','Лоншан',5,'Decline',2.3,NULL),('2017-05-24 12:55:00','Лоншан',8,'Decline',3.4,NULL),('2017-05-24 12:55:00','Лоншан',11,'Decline',1.5,NULL),('2017-05-24 12:55:00','Лоншан',12,'Decline',2.4,NULL),('2017-05-25 20:30:00','Мейдан',3,'Open',2.3,NULL),('2017-05-25 20:30:00','Мейдан',5,'Open',3.4,NULL),('2017-05-25 20:30:00','Мейдан',6,'Open',1.7,NULL),('2017-05-25 20:30:00','Мейдан',9,'Open',2.7,NULL),('2017-05-26 08:40:00','Нью-Маркет',3,'Open',2.1,NULL),('2017-05-26 08:40:00','Нью-Маркет',5,'Open',2.4,NULL),('2017-05-26 08:40:00','Нью-Маркет',6,'Open',3.4,NULL),('2017-05-26 08:40:00','Нью-Маркет',10,'Open',1.9,NULL),('2017-05-27 11:20:00','Саузвелл',2,'Open',3.2,NULL),('2017-05-27 11:20:00','Саузвелл',4,'Open',2.3,NULL),('2017-05-27 11:20:00','Саузвелл',6,'Open',4.5,NULL),('2017-05-27 11:20:00','Саузвелл',8,'Open',1.8,NULL),('2017-05-27 17:20:00','Лоншан',1,'Open',2.3,NULL),('2017-05-27 17:20:00','Лоншан',6,'Open',2.5,NULL),('2017-05-27 17:20:00','Лоншан',11,'Open',1.1,NULL),('2017-05-27 17:20:00','Лоншан',13,'Open',3.6,NULL);
/*!40000 ALTER TABLE `horse_in_race` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `races`.`horse_in_race_AFTER_UPDATE` AFTER UPDATE ON `horse_in_race` FOR EACH ROW
BEGIN

IF(NEW.place IS NOT NULL) THEN
	IF(NEW.place = 1) THEN
    
		UPDATE bet 
		SET bet.result = 1
		WHERE (bet.bet_type = 1 OR bet.bet_type = 2) AND
        bet.horse_race_name = OLD.horse_race_name AND
        bet.horse_race_date = OLD.horse_race_date AND
        bet.horse_id = OLD.horse_id;
        
        ELSE IF(NEW.place < 4) THEN
        
		begin
		UPDATE bet 
		SET bet.result = 1
		WHERE bet.bet_type = 2 AND
		bet.horse_race_name = OLD.horse_race_name AND
		bet.horse_race_date = OLD.horse_race_date AND
		bet.horse_id = OLD.horse_id;

		UPDATE bet 
		SET bet.result = 2
		WHERE bet.bet_type = 1 AND
		bet.horse_race_name = OLD.horse_race_name AND
		bet.horse_race_date = OLD.horse_race_date AND
		bet.horse_id = OLD.horse_id;
		end;
		ELSE 

		UPDATE bet 
		SET bet.result = 2
		WHERE bet.horse_race_name = OLD.horse_race_name AND
		bet.horse_race_date = OLD.horse_race_date AND
		bet.horse_id = OLD.horse_id;


		END IF;
        
	END IF;
END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `race`
--

DROP TABLE IF EXISTS `race`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `race` (
  `race_name` varchar(45) NOT NULL,
  `date` datetime NOT NULL,
  `status` enum('Открыта','Закрыта','Отменена','Проведена') NOT NULL DEFAULT 'Закрыта',
  PRIMARY KEY (`race_name`,`date`),
  CONSTRAINT `fk_race_name` FOREIGN KEY (`race_name`) REFERENCES `hippodrom` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `race`
--

LOCK TABLES `race` WRITE;
/*!40000 ALTER TABLE `race` DISABLE KEYS */;
INSERT INTO `race` VALUES ('Лоншан','2017-05-20 06:00:00','Проведена'),('Лоншан','2017-05-24 12:55:00','Отменена'),('Лоншан','2017-05-27 17:20:00','Открыта'),('Мейдан','2017-05-21 07:15:00','Проведена'),('Мейдан','2017-05-25 20:30:00','Открыта'),('Нью-Маркет','2017-05-22 08:30:00','Проведена'),('Нью-Маркет','2017-05-26 08:40:00','Открыта'),('Саузвелл','2017-05-23 03:25:00','Проведена'),('Саузвелл','2017-05-27 11:20:00','Открыта');
/*!40000 ALTER TABLE `race` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `support`
--

DROP TABLE IF EXISTS `support`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `support` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `type` enum('Ставки','Скачки','Платежи','Вывод средств','Другое') NOT NULL,
  `date` datetime NOT NULL,
  `text` varchar(350) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `support`
--

LOCK TABLES `support` WRITE;
/*!40000 ALTER TABLE `support` DISABLE KEYS */;
INSERT INTO `support` VALUES (1,'testemail@mail.ru','Скачки','2017-05-01 05:06:52','Здравствуйте, как часто происходят скачки? Хочу выйграть много денег, но не знаю как часто они проходят. Заранее спасибо за ответ.'),(2,'lovethedesign@gmail.com','Вывод средств','2017-05-01 07:40:24','Здравствуйте. Ваш сайт великолепен. Нет ничего лучше чем он. Можете ли вы дать контактные данные вашего дизайнера? Хочу чтобы он сделал мне такой же классный сайт.');
/*!40000 ALTER TABLE `support` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `secondname` varchar(45) NOT NULL,
  `role` enum('Admin','Bookmaker','Client') DEFAULT 'Client',
  `money` decimal(10,1) unsigned DEFAULT '0.0',
  `status` enum('Активен','Забанен') NOT NULL DEFAULT 'Активен',
  `avatarpath` varchar(60) DEFAULT '/avatars/default.png',
  `reg_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Administrator','5f6595f53c7db74993120b2a329e4a70','admin@gmail.com','Alexey','Горелик','Admin',NULL,'Активен','/avatars/34920833-c6dc-4cd7-a350-723b3db882a9.png','2017-04-11 11:30:00'),(2,'Ivan22','5f6595f53c7db74993120b2a329e4a70','ivan@mail.ru','Иван','Иванов','Bookmaker',NULL,'Активен','/avatars/50_cent.png','2017-02-23 09:30:00'),(3,'orangeMood','5f6595f53c7db74993120b2a329e4a70','orange@mood.com','Снежана','Фаренгейт','Client',125.1,'Активен','/avatars/532fad80-7e85-41c9-9e63-f4a985806a20.png','2017-04-01 22:36:24'),(4,'viktor','5f6595f53c7db74993120b2a329e4a70','lisovViktor@gmail.com','Виктор','Лысов','Client',83.5,'Активен','/avatars/031ebf17-f825-4145-b381-be25f37eb2ed.png','2017-03-28 19:44:03'),(6,'angelinaxxx','5f6595f53c7db74993120b2a329e4a70','angelinaxxx@gmail.com','Angelina','XXX','Client',97.4,'Активен','/avatars/495bc100-0f00-433c-a31e-9dc0f3ec05f1.jpg','2017-04-22 18:58:45');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `races`.`user_BEFORE_INSERT` BEFORE INSERT ON `user` FOR EACH ROW
BEGIN
	SET NEW.password = MD5(NEW.password);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping events for database 'races'
--

--
-- Dumping routines for database 'races'
--
/*!50003 DROP PROCEDURE IF EXISTS `changePassword` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `changePassword`(newEmail VARCHAR(45), inputPassword VARCHAR(45))
BEGIN
	UPDATE user SET user.password = MD5(inputPassword) 
	WHERE user.name = newEmail;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `checkAuthentication` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `checkAuthentication`(nickname VARCHAR(45), inputPassword VARCHAR(45), OUT resultLogin BOOLEAN, OUT resultPassword BOOLEAN)
BEGIN
DECLARE tempLogin VARCHAR(45);
DECLARE tempPassword VARCHAR(45);

SELECT user.name
INTO tempLogin
FROM user
WHERE user.name = nickname;

IF templogin IS NOT NULL THEN
SET resultLogin = TRUE;
ELSE
SET resultLogin = FALSE;
END IF;


SELECT user.password
INTO tempPassword
FROM user
WHERE user.name = nickname;
IF tempPassword = MD5(inputPassword) THEN
SET resultPassword = TRUE;
ELSE
SET resultPassword = FALSE;
END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `checkPassword` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `checkPassword`(nickname VARCHAR(45), inputPassword VARCHAR(45), OUT result BOOLEAN)
BEGIN
	DECLARE tempPassword VARCHAR(45);
	SELECT 
	user.password
	INTO tempPassword FROM
	user
	WHERE
	user.name = nickname;
	IF tempPassword = MD5(inputPassword) THEN
	set result = TRUE;
	ELSE
	set result = FALSE;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `checkRegistration` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `checkRegistration`(nickname VARCHAR(45), email VARCHAR(45), OUT resultLogin BOOLEAN, OUT resultEmail BOOLEAN)
BEGIN
DECLARE tempLogin VARCHAR(45) DEFAULT NULL;
DECLARE tempEmail VARCHAR(45) DEFAULT NULL;

SELECT user.name
INTO tempLogin
FROM user
WHERE user.name = nickname;

IF tempLogin IS NOT NULL THEN
SET resultLogin = FALSE;
ELSE
SET resultLogin = TRUE;
END IF;

SELECT user.email 
INTO tempEmail FROM user
WHERE user.email = email;

IF tempEmail IS NOT NULL THEN
SET resultEmail = FALSE;
ELSE
SET resultEmail = TRUE;
END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `decline_horse` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `decline_horse`(new_race_name VARCHAR(45), new_race_date DATETIME, new_horse_id INT(11))
begin 
/* переменные куда мы извлекаем данные */ 
Declare temp_user_id INT(11); 
Declare temp_sum DECIMAL(10,1); 
/* переменная hadler - a*/ 
Declare done integer default 0; 
/*Объявление курсора*/ 
Declare temp_table Cursor for SELECT bet.user_id, bet.sum FROM races.bet 
WHERE bet.horse_race_name = new_race_name 
AND bet.horse_race_date = new_race_date 
AND bet.horse_id = new_horse_id; 
/*HANDLER*/ 
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done=1; 
/* открытие курсора */ 
Open temp_table; 
/*извлекаем данные */ 
WHILE done = 0 DO 
FETCH temp_table INTO temp_user_id,temp_sum; 

UPDATE user 
SET 
user.money = user.money + temp_sum 
WHERE 
user.id = temp_user_id; 

UPDATE bet 
SET 
bet.result = 'Отмена' 
WHERE 
bet.horse_race_date = new_race_date 
AND bet.horse_race_name = new_race_name
AND bet.horse_id = new_horse_id;

UPDATE horse_in_race
SET horse_in_race.status = 2
WHERE horse_in_race.horse_race_date = new_race_date 
AND horse_in_race.horse_race_name = new_race_name
AND horse_in_race.horse_id = new_horse_id;

END WHILE; 
UPDATE user 
SET 
user.money = user.money - temp_sum 
WHERE 
user.id = temp_user_id; 
/*закрытие курсора */ 
Close temp_table; 
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `decline_race` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `decline_race`(new_race_name VARCHAR(45), new_race_date DATETIME)
begin
/* переменные куда мы извлекаем данные */
Declare temp_user_id INT(11);
Declare temp_sum DECIMAL(10,1);
/* переменная hadler - a*/
Declare done integer default 0;
/*Объявление курсора*/
Declare temp_table Cursor for SELECT bet.user_id, bet.sum FROM races.bet 
WHERE bet.horse_race_name = new_race_name
AND bet.horse_race_date = new_race_date; 
/*HANDLER*/
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done=1;
/* открытие курсора */
Open temp_table;
/*извлекаем данные */
WHILE done = 0 DO 
FETCH temp_table INTO temp_user_id,temp_sum;

UPDATE user 
SET 
user.money = user.money + temp_sum
WHERE
user.id = temp_user_id;

UPDATE bet SET bet.result = 'Отмена'
WHERE bet.horse_race_date = new_race_date AND
bet.horse_race_name = new_race_name;

END WHILE;
UPDATE user 
SET 
user.money = user.money - temp_sum
WHERE
user.id = temp_user_id;
/*закрытие курсора */
Close temp_table; 

UPDATE horse_in_race
SET horse_in_race.status = 2 
WHERE horse_in_race.horse_race_date = new_race_date AND
horse_in_race.horse_race_name = new_race_name;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-30 16:52:09
