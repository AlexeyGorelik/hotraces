function checkAction(f) {
    // Если поставлен флажок, снимаем блокирование кнопки
    if (f.checked)
    {
    	document.getElementsByName(f.name + "-id")[0].disabled = 0;
    	document.getElementsByName(f.name + "-name")[0].disabled = 0;
    }
    else
    {
    	document.getElementsByName(f.name + "-id")[0].disabled = 1;
    	document.getElementsByName(f.name + "-name")[0].disabled = 1;
    }

}