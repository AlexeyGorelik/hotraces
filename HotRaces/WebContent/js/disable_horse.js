function agreeForm(f) {
    if (f.checked)
    {
    	document.getElementsByName(f.id + "-id")[0].disabled = 0;
        document.getElementsByName(f.id + "-name")[0].disabled = 0;
        document.getElementsByName(f.id + "-status")[0].disabled = 0;
    } 
    else 
    {
    	document.getElementsByName(f.id + "-id")[0].disabled = 1;
        document.getElementsByName(f.id + "-name")[0].disabled = 1;
        document.getElementsByName(f.id + "-status")[0].disabled = 1;
    }

   }