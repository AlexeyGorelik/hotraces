function determineUserButton(obj) {
  var BAN_BUTTON = "ban";
  var UNBAN_BUTTON = "unban";

  var BAN_COMMAND = "banUser";
  var UNBAN_COMMAND = "unbanUser";

  if(obj.name == BAN_BUTTON)
  {
    document.getElementById('userCommand').value = BAN_COMMAND;
  }
  else if(obj.name == UNBAN_BUTTON)
  {
    document.getElementById('userCommand').value = UNBAN_COMMAND;
  }

  document.forms["changeUserStatus"].submit();

}

function determineRaceButton(obj) {
  var CREATE_BUTTON = "toCreateRace";
  var CARRY_OUT_BUTTON = "carryOutRace";
  var DELETE_BUTTON = "deleteRace";
  var EDIT_BUTTON = "toEditRace";

  var CREATE_COMMAND = "toCreateRace";
  var CARRY_OUT_COMMAND = "carryOutRace";
  var DELETE_COMMAND = "deleteRace";
  var EDIT_COMMAND = "toEditRace";

  if(obj.name == CREATE_BUTTON)
  {
    document.getElementById('raceCommand').value = CREATE_COMMAND;
  }
  else if(obj.name == CARRY_OUT_BUTTON)
  {
    document.getElementById('raceCommand').value = CARRY_OUT_COMMAND;
  }
  else if(obj.name == DELETE_BUTTON)
  {
    document.getElementById('raceCommand').value = DELETE_COMMAND;
  }
  else if(obj.name == EDIT_BUTTON)
  {
    document.getElementById('raceCommand').value = EDIT_COMMAND;
  }

  document.forms["adminRaceCommand"].submit();
}