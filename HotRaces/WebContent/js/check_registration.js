function validateForm() {
	console.log("registration completed.");
	var result = true; // результат, используем, чтобы проверять сразу все поля ввода
// Константы
		var EMPTY_FIELD = "*заполните поле",
			PSWRD_NOT_EQUAL = "*пароли не совпадают",
			BAD_DATA = "*данные введены неверно";
// Находим ссылки на элементы сообщений об ошибках
		/*var errLogin = document.getElementById("err-login"),
			errPswrd1 =  document.getElementById("err-pswrd1"),
			errPswrd2 =  document.getElementById("err-pswrd2");
			errAge = document.getElementById("err-age");
			errEmail = document.getElementById("err-email");*/
		var	errMsg = document.getElementById("error-message");
// Предварительно очищаем сообщения об ошибках
		errMsg.innerHTML = "";
// Читаем значения из полей формы
		var firstName = document.forms[0]["firstName"].value,
			secondName = document.forms[0]["secondName"].value,
			login = document.forms[0]["login"].value,    // или document.testForm.usrname.value
			pswrd1 = document.forms[0]["pswrd1"].value,      // или document.testForm.pwd1.value
			pswrd2 = document.forms[0]["pswrd2"].value;      // или document.testForm.pwd2.value
			email = document.forms[0]["email"].value;
// Проверка поля "Пользователь"
		if (!firstName) {
			errMsg.innerHTML = EMPTY_FIELD;
			result = false;
		}  // обязательное заполнение
		if (firstName && firstName.search(/[а-яА-Яa-zA-Z_]{3,16}/i) !== 0) {
			errMsg.innerHTML = BAD_DATA;
			result = false;
		}
		if (!secondName) {
			errMsg.innerHTML = EMPTY_FIELD;
			result = false;
		}  // обязательное заполнение
		if (secondName && secondName.search(/[а-яА-Яa-zA-Z_]{3,16}/i) !== 0) {
			errMsg.innerHTML = BAD_DATA;
			result = false;
		}
		if (!login) {
			errMsg.innerHTML = EMPTY_FIELD;
			result = false;
		}  // обязательное заполнение
		if (login && login.search(/[a-zA-Z\d_]{3,16}/i) !== 0) {
			errMsg.innerHTML = BAD_DATA;
			result = false;
		}  // Первый символ - латинская буква в любом регистре
// Проверка паролей
		if (!pswrd1) {
			errMsg.innerHTML = EMPTY_FIELD; 
			result = false;
		}  // обязательное заполнение 
		if (pswrd1 && pswrd1.search(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[\w]{6,}$/i) !== 0) {
			errMsg.innerHTML = BAD_DATA;
			result = false;
		} 
		if (!pswrd2) {
			errMsg.innerHTML = EMPTY_FIELD; 
			result = false;
		}  // обязательное заполнение
		if (pswrd2 && pswrd2.search(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[\w]{6,}$/i) !== 0) {
			errMsg.innerHTML = BAD_DATA;
			result = false;
		} 
		if (pswrd1 && pswrd2 && pswrd1 !== pswrd2) {
			errMsg.innerHTML = PSWRD_NOT_EQUAL; 
            errMsg.innerHTML = PSWRD_NOT_EQUAL;
            document.forms[0]["pswrd1"].value = "";   // сброс 
            document.forms[0]["pswrd2"].value = "";   // сброс
			result = false;
		}
		if (!email) {
			errMsg.innerHTML = EMPTY_FIELD; 
			result = false;
		}
		if (email && email.search(/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/i) !== 0) {
			errMsg.innerHTML = BAD_DATA;
			result = false;
		} 
// возвращаем итог проверки		
		return result;
	}