var i = 0;
var CColor = "#000000"; //Цвет стрелок
var CBackground = "#f6a426"; //Цвет фона
var CSeconds = "#f00"; //Цвет секундной стрелки
var CSize = 190; //Размер поля
var CCenter = CSize / 2; //Радиус круга
var CTSize = CCenter - 10; //Расстояние от центра где рисуются отметки минут 
var CMSize = CTSize * 0.7; //Длинна минутной стрелки
var CSSize = CTSize * 0.8; //Длинна секундной стрелки
var CHSize = CTSize * 0.6; //Длинна часовой стрелки
var example;

function ctxlineL(x1,y1,len,angle,color,wid){//Функция рисования линии под углом
	var x2 = (CCenter + (len * Math.cos(angle)));
	var y2 = (CCenter + (len * Math.sin(angle)));
	ctxL.beginPath();
	ctxL.strokeStyle = color;
	ctxL.lineWidth = wid; 
	ctxL.moveTo(x1,y1);
	ctxL.lineTo(x2,y2);
	ctxL.stroke();
}

function ctxcircleL(x,y,rd,color){//Функция рисования круга
	ctxL.beginPath();
	ctxL.arc(x, y, rd, 0, 2*Math.PI, false);
	ctxL.fillStyle = color;
	ctxL.fill();
	ctxL.lineWidth = 1;
	ctxL.strokeStyle = color;
	ctxL.stroke();
}

function tickLondon(){ //Функция рисования стрелок 
	//Стираем предыдущие стрелки
	ctxcircleL(CCenter,CCenter ,CSSize,CBackground);
	 var tm1=new Date(Date.now());
	 var h1=tm1.getUTCHours();
	 var m1=tm1.getUTCMinutes();
	 var s1=tm1.getUTCSeconds();
	 var ml1 = tm1.getUTCMilliseconds();
	//Вычисляем поворот
	i = 360/3600 * ((m1*60)+s1);
	//Рисуем стрелку
	ctxlineL(CCenter,CCenter,CMSize,((i-90) / 180 * Math.PI),CColor,4);//Минутная

	i = 360/720*((h1*60)+ m1);
	ctxlineL(CCenter,CCenter,CHSize,((i-90) / 180 * Math.PI),CColor,5);// Часовая

	ctxcircleL(CCenter,CCenter,9,CColor);//Круг от стрелки

	i = 360/(60*1000)* ((s1*1000)+ ml1);
	ctxlineL(CCenter,CCenter,CSSize,((i-90) / 180 * Math.PI),CSeconds,3);//Секундная

	ctxcircleL(CCenter,CCenter,6,CSeconds);//Круг от секундной стрелки 
}
function startLondonClock() {
	example= document.getElementById("london_clock"), ctxL = example.getContext('2d');
	ctxL.fillStyle = CBackground; 
	ctxL.fillRect(0, 0,example.width,example.height);

	for(iv=0;iv<12;iv++){// Рисуем часовые метки
		i = 360/12*iv;
		ctxcircleL((CCenter + (CTSize * Math.cos((i-90) / 180 * Math.PI))),(CCenter + (CTSize * Math.sin((i-90) / 180 * Math.PI))),4,CColor);
	}

	for(iv=0;iv<60;iv++){// Рисуем минутные метки
		i = 360/60*iv;
		ctxcircleL((CCenter + (CTSize * Math.cos((i-90) / 180 * Math.PI))),(CCenter + (CTSize * Math.sin((i-90) / 180 * Math.PI))),2,CColor);
	}

	setInterval('tickLondon(); ',10);
}

startLondonClock();