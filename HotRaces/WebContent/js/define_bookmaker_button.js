function determineRaceButton(obj) {
  var DELETE_BUTTON = "deleteRace";
  var SET_COEFF_BUTTON = "setCoeff";

  var DELETE_COMMAND = "deleteRaceBookmaker";
  var SET_COEFF_COMMAND = "toSetCoeff";

  if(obj.name == DELETE_BUTTON)
  {
    document.getElementById('raceCommand').value = DELETE_COMMAND;
  }
  else if(obj.name == SET_COEFF_BUTTON)
  {
    document.getElementById('raceCommand').value = SET_COEFF_COMMAND;
  }

  document.forms["bookmakerRaceComand"].submit();
}