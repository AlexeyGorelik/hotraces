<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="header">
	<div class="authorization">
		<div class="languages-main">
			<div class="languages">
				<ul>
					<li><a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=locale&locale=ru_RU"><img src="${ pageContext.servletContext.contextPath }/pictures/flags/ru_BIG.png" alt="RU"></a></li>
					<li><a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=locale&locale=en_US"><img src="${ pageContext.servletContext.contextPath }/pictures/flags/gb.png" alt="USA"></a></li>
				</ul>
			</div>
		</div>
		<c:if test="${ not empty user }">
			<div class='sign-in-user'>
				<div class='side'>
					<ul class='menu-drop-registr'>
						<li class='menu-list'>
							<div class='registration-nickname'>
								<p>${ user.login }</p>
							</div>
							<div class='registration-img'>
								<img style="width: 50px; height: 50px;" src="${ pageContext.request.contextPath }${ user.getAvatarPath() }"/>
							</div>
							<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="GET">
								<ul class='menu-drop'>
						        	<li><a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toProfile"><fmt:message key='header.user.myProfile' bundle='${ rb }'/></a></li>
						        	<c:set var="status" value="Client" scope="page"/>
						        	<c:if test = "${ user.role eq status }">
							       		<li><a href='${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toPayment'><fmt:message key='header.user.myPayment' bundle='${ rb }'/></a></li>
							        </c:if>
							        <li><a href='${ pageContext.servletContext.contextPath }/HotRacesServlet?command=LogOut'><fmt:message key='header.user.exit' bundle='${ rb }'/></a></li>
						        </ul>
					        </form>
	        			</li>
	        		</ul>
	       		</div>﻿
	        </div>
		</c:if>
		<c:if test="${ empty user }">
			<div class='sign-in-main'>
				<div class='sign-in'>
					<a class='sign-in-ref' href='/HotRaces/HotRacesServlet?command=tologin'><p><fmt:message key='header.signIn' bundle='${ rb }'/></p></a>
				</div>
				<div class='registration'>
					<a class='registration-ref' href='/HotRaces/HotRacesServlet?command=toregistration'><p><fmt:message key='header.registration' bundle='${ rb }'/></p></a>
				</div>
			</div>
		</c:if>
	</div>
	<div class="contacts-main">
		<div class="logo-and-social">
			<div class="header-info">
				<div class="logo">
					<img src="${ pageContext.servletContext.contextPath }/pictures/logoBig.png" alt="Hot races">
				</div>
				<div class="info">
					<div class="picture-info">
						<a href="https://vk.com/durov"><img src="${ pageContext.servletContext.contextPath }/pictures/header/vk.png" alt="VK"></a>
					</div>
					<div class="picture-info">
						<a href="https://https://www.facebook.com/zuck"><img src="${ pageContext.servletContext.contextPath }/pictures/header/fb.png" alt="FB"></a>
					</div>
					<div class="picture-info">
						<a href="https://twitter.com/realdonaldtrump"><img src="${ pageContext.servletContext.contextPath }/pictures/header/twitter.png" alt="Call"></a>
					</div>
					<div class="picture-info">
						<a href="https://www.instagram.com/epamsystems/"><img src="${ pageContext.servletContext.contextPath }/pictures/header/inst.png" alt="TW"></a>
					</div>
					<div class="picture-info" style="width: 70px;">
						<a href="https://www.youtube.com/user/EPAMpress"><img src="${ pageContext.servletContext.contextPath }/pictures/header/youtube.png" alt="YT"></a>
					</div>
					<div class="picture-info" style="margin: 0;">
						<a href="https://ok.ru/piratetreasures"><img src="${ pageContext.servletContext.contextPath }/pictures/header/class.png" alt="OK"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="clocks-main">
			<div class="clocks">
				
				<div class="time">
					<canvas height="190" width="190" id="moscow_clock">
						<fmt:message key="header.canvas" bundle="${ rb }"/>
					</canvas>
					<script src="${ pageContext.servletContext.contextPath }/js/moscow_clock.js"></script>
					<div class="city">
						<p><fmt:message key="header.city.moscow" bundle="${ rb }"/></p>
					</div>
				</div>
				<div class="time">
					<canvas height="190" width="190" id="london_clock">
						<fmt:message key="header.canvas" bundle="${ rb }"/>
					</canvas>
					<script src="${ pageContext.servletContext.contextPath }/js/london_clock.js"></script>
					<div class="city">
						<p><fmt:message key="header.city.london" bundle="${ rb }"/></p>
					</div>
				</div>
			</div>
		</div>
		<div class="small-info">
			<div class="small-help">
				<div class="picture">
					<img src="${ pageContext.servletContext.contextPath }/pictures/header/quest.png" alt="Call">
				</div>
				<div class="contact">
					<a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toHowToPlay"><p><fmt:message key="header.howToPlay" bundle="${ rb }"/></p></a>
				</div>
			</div>
			<div class="small-help">
				<div class="picture">
					<img src="${ pageContext.servletContext.contextPath }/pictures/header/dollar.png" alt="Call">
				</div>
				<div class="contact">
					<a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toPaymentSystem"><p><fmt:message key="header.payments" bundle="${ rb }"/></p></a>
				</div>
			</div>
			<div class="small-help">
				<div class="picture">
					<img src="${ pageContext.servletContext.contextPath }/pictures/header/phone.png" alt="Call">
				</div>
				<div class="contact">
					<a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toSupport"><p><fmt:message key="header.support" bundle="${ rb }"/></p></a>
				</div>
			</div>
		</div>
	</div>
	<div class="navigation-main">
		<div class="nagivation">
			<ul>
				<li class="navigation-item">
					<a id="toMain" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toMain"><fmt:message key="header.navig.main" bundle="${ rb }"/></a></li>
				<li class="navigation-item">
					<a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toBets"><fmt:message key="header.navig.bets" bundle="${ rb }"/></a></li>
				<li class="navigation-item">
					<a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toLive"><fmt:message key="header.navig.live" bundle="${ rb }"/></a></li>
				<li class="navigation-item">
					<a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toAboutUs"><fmt:message key="header.navig.aboutUs" bundle="${ rb }"/></a></li>
				<li class="navigation-item">
					<a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toContact"><fmt:message key="header.navig.contacs" bundle="${ rb }"/></a></li>
			</ul>
		</div>
	</div> 
</div>
