<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="footer">
	<hr class="footer-hr"/>
	<div class="footer-navigation">
		<ul>
			<li class="footer-ul-head"><fmt:message key="footer.navigation" bundle="${ rb }"/></li>
			<li><a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toMain"><fmt:message key="footer.main" bundle="${ rb }"/></a></li>
			<li><a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toBets"><fmt:message key="footer.bets" bundle="${ rb }"/></a></li>
			<li><a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toLive"><fmt:message key="footer.live" bundle="${ rb }"/></a></li>
		</ul>
	</div>
	<div class="footer-faq">
		<ul>
			<li class="footer-ul-head">F.A.Q.</li>
			<li><a href="/HotRaces/HotRacesServlet?command=toFaq&faqName=whatIsRace"><fmt:message key="footer.whatIsIt" bundle="${ rb }"/></a></li>
			<li><a href="/HotRaces/HotRacesServlet?command=toFaq&faqName=raceTypes"><fmt:message key="footer.raceType" bundle="${ rb }"/></a></li>
			<li><a href="/HotRaces/HotRacesServlet?command=toFaq&faqName=betsOnRace"><fmt:message key="footer.betOnRace" bundle="${ rb }"/></a></li>
			<li><a href="/HotRaces/HotRacesServlet?command=toFaq&faqName=betsTypes"><fmt:message key="footer.betType" bundle="${ rb }"/></a></li>
			<li><a href="/HotRaces/HotRacesServlet?command=toFaq&faqName=betsStrat"><fmt:message key="footer.strategy" bundle="${ rb }"/></a></li>
		</ul>
	</div>
	<div class="footer-help">
		<ul>
			<li class="footer-ul-head"><fmt:message key="footer.additional" bundle="${ rb }"/></li>
			<li><a href="/HotRaces/HotRacesServlet?command=toregistration"><fmt:message key="footer.registration" bundle="${ rb }"/></a></li>
			<li><a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toHowToPlay"><fmt:message key="footer.howToPlay" bundle="${ rb }"/></a></li>
			<li><a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toPaymentSystem"><fmt:message key="footer.payment" bundle="${ rb }"/></a></li>
			<li><a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toContact"><fmt:message key="footer.help" bundle="${ rb }"/></a></li>
		</ul>
	</div>
	<div class="footer-about-us">
		<ul>
			<li class="footer-ul-head"><fmt:message key="footer.contactUs" bundle="${ rb }"/></li>
			<li><a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toAboutUs"><fmt:message key="footer.aboutUs" bundle="${ rb }"/></a></li>
			<li><a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toContact"><fmt:message key="footer.contacts" bundle="${ rb }"/></a></li>
		</ul>
	</div>
	<div class="footer-payment">
		<img src="${ pageContext.servletContext.contextPath }/pictures/payment/visa.png">
		<img src="${ pageContext.servletContext.contextPath }/pictures/payment/mastercard.png">
		<img src="${ pageContext.servletContext.contextPath }/pictures/payment/maestro.png">
	</div>
	<div class="footer-medals">
		<img src="${ pageContext.servletContext.contextPath }/pictures/footer/small_medal.png" title="<fmt:message key="footer.medalOne" bundle="${ rb }"/>">
		<img src="${ pageContext.servletContext.contextPath }/pictures/footer/small_medal.png" title="<fmt:message key="footer.medalTwo" bundle="${ rb }"/>">
		<img src="${ pageContext.servletContext.contextPath }/pictures/footer/small_medal.png" title="<fmt:message key="footer.medalThree" bundle="${ rb }"/>">
	</div>
	<hr class="footer-hr"/>
	<div class="footer-copyright">
		<div class="rights">
			<p><fmt:message key="footer.company" bundle="${ rb }"/></p>
			<p><fmt:message key="footer.copyright" bundle="${ rb }"/> &#169; 1994-2017</p>
		</div>
		
		<div class="pegi">
			<img src="${ pageContext.servletContext.contextPath }/pictures/footer/pegi.png" alt="" class="pegi">
			<p><fmt:message key="footer.pegi" bundle="${ rb }"/></p>
		</div>
	</div>
	
</div>