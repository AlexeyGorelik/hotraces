<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<div class="left-info-menu">
	<div class="faq-menu">
		<div class="left-menu-header">
			<p>F.A.Q.</p>
		</div>
		<div class="menu-item">
			<a href="/HotRaces/HotRacesServlet?command=toFaq&faqName=whatIsRace"><p><fmt:message key="body.leftMenu.whatIsIt" bundle="${ rb }"/></p></a>
		</div>
		<div class="menu-item">
			<a href="/HotRaces/HotRacesServlet?command=toFaq&faqName=raceTypes"><p><fmt:message key="body.leftMenu.raceType" bundle="${ rb }"/></p></a>
		</div>
		<div class="menu-item">
			<a href="/HotRaces/HotRacesServlet?command=toFaq&faqName=betsOnRace"><p><fmt:message key="body.leftMenu.betOnRace" bundle="${ rb }"/></p></a>
		</div>
		<div class="menu-item">
			<a href="/HotRaces/HotRacesServlet?command=toFaq&faqName=betsTypes"><p><fmt:message key="body.leftMenu.betType" bundle="${ rb }"/></p></a>
		</div>
		<div class="menu-item">
			<a href="/HotRaces/HotRacesServlet?command=toFaq&faqName=betsStrat"><p><fmt:message key="body.leftMenu.strategy" bundle="${ rb }"/></p></a>
		</div>
	</div>


	<div class="hippodroms-menu">
		<div class="left-menu-header">
			<p><fmt:message key="body.leftMenu.hippodroms" bundle="${ rb }"/></p>
		</div>
		<div class="menu-item">
			<div class="menu-name-country"><a href="/HotRaces/HotRacesServlet?command=toHippodromDescription&hippName=meydan"><p><fmt:message key="body.leftMenu.hippodroms.meidan" bundle="${ rb }"/></p></a></div>
			<div class="menu-flag-country"><img src="${ pageContext.servletContext.contextPath }/pictures/flags/menu/OAE_menu.png"></div>
		</div>
		<div class="menu-item">
			<div class="menu-name-country"><a href="/HotRaces/HotRacesServlet?command=toHippodromDescription&hippName=southwell"><p><fmt:message key="body.leftMenu.hippodroms.southwell" bundle="${ rb }"/></p></a></div>
			<div class="menu-flag-country"><img src="${ pageContext.servletContext.contextPath }/pictures/flags/menu/EN_menu.png"></div>
		</div>
		<div class="menu-item">
			<div class="menu-name-country"><a href="/HotRaces/HotRacesServlet?command=toHippodromDescription&hippName=lonshan"><p><fmt:message key="body.leftMenu.hippodroms.lonshan" bundle="${ rb }"/></p></a></div>
			<div class="menu-flag-country"><img src="${ pageContext.servletContext.contextPath }/pictures/flags/menu/FRA_menu.png"></div>
		</div>
		<div class="menu-item">
			<div class="menu-name-country"><a href="/HotRaces/HotRacesServlet?command=toHippodromDescription&hippName=newMarket"><p><fmt:message key="body.leftMenu.hippodroms.newMarket" bundle="${ rb }"/></p></a></div>
			<div class="menu-flag-country"><img src="${ pageContext.servletContext.contextPath }/pictures/flags/menu/EN_menu.png"></div>
		</div>
	</div>
</div>

