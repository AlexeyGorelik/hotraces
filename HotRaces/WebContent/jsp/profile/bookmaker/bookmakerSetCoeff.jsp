<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="locDate" uri="localeDate" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="bookmaker.setcoeff.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/profile.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/bookmaker_set_coeff_table.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<%@ include file="../../pageParts/header/header.jsp"%>
	<div class="main-body">
		<div class="add-set-coeff-main">
			<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
				<c:if test="${ not empty error }">
					<p class="error-message">${ error }</p>
				</c:if>
				<h2 class="add-race-h2"><fmt:message key="bookmaker.setcoeff.table.name" bundle="${ rb }"/></h2>
				<p class="input-description"><fmt:message key="bookmaker.setcoeff.table.raceName" bundle="${ rb }"/>: ${ raceName }</p>
				<input name="race-name" type="hidden" value="${ raceName }">
				<p class="input-description"><fmt:message key="bookmaker.setcoeff.table.raceDate" bundle="${ rb }"/>: <locDate:localeDate locale="${ locale }" date="${ raceDate }"/></p>
				<input name="race-date" type="hidden" maxlength="19" value="<locDate:localeDate locale="${ locale }" date="${ raceDate }"/>">
				
				<table id="data-table" class="data-table">
				
				<thead>
					<tr>
						<th width="300px"><fmt:message key="bookmaker.setcoeff.table.horseId" bundle="${ rb }"/></th>
						<th width="300px"><fmt:message key="bookmaker.setcoeff.table.horseName" bundle="${ rb }"/></th>
						<th width="300px"><fmt:message key="bookmaker.setcoeff.table.coeff" bundle="${ rb }"/></th>
					</tr>
				</thead>
				
				<tbody id="body-table">
				
				
				<c:forEach var="data" items="${ setCoeffList }" varStatus="status">
					<tr>
						<input type="hidden" name="horse-id-${ status.count }" value="${ data.getHorseId() }"/>
						<td name="horse-id-${ status.count }">${ data.getHorseId() }</td>
						
						<input type="hidden" name="horse-name-${ status.count }" value="${ data.getHorseName() }"/>
						<td name="horse-name-${ status.count }">${ data.getHorseName() }</td>
						
						<td class="set-coeff"><input type="text" class="input-coeff" name="coeff-${ status.count }" value="${ data.getCoeff() }" pattern ="^\d+(?:\.\d{1,2})?$" title="<fmt:message key="bookmaker.table.coeffTitle" bundle="${ rb }"/>"/></td>
					</tr>
				
				</c:forEach>
				
				
				</tbody>
				</table>
				<br>
				<input type="submit" class="button" value="<fmt:message key="bookmaker.setcoeff.table.accept" bundle="${ rb }"/>"/>
				<input type="hidden" name="horseCount" value="${ horseCount }"/>
				<input type="hidden" name="command" value="setCoeff"/>
				</form>
			</div>
	</div>
<%@ include file="../../pageParts/footer/footer.jsp"%>
</body>
</html>