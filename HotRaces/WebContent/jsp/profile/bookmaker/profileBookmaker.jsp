<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="locDate" uri="localeDate" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="profile.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/profile.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/bookmaker_race_table.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/messages_table.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/message.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/pagination.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-3.2.1.min.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-ui.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery.table.hpaging.js"></script>
	<%@ include file="../../pageParts/header/header.jsp"%>
	<div class="main-body">

			<div class="profile">
				<div class="user-info">
					<div class="profile-img-and-settings">
						<div class="user-img">
							<img src="${ pageContext.request.contextPath }${ user.getAvatarPath() }" title="<fmt:message key="profile.recomSize" bundle="${ rb }"/>"/>
						</div>

						<div class="controls">
							<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=uploadAvatar" method="POST" enctype="multipart/form-data">
								<input class="change-avatar-button" type="button" value="Change avatar" id="nofile"/>
								<input name="file" type="file" onchange="this.form.submit()" id="fileuploads" runat="server" style="display: none;" />
								<input type="submit" hidden>
							</form>
							<c:if test="${ uplAvatar }">
								<p style="color: red;"> Can't upload avatar</p>
							</c:if>
						</div>
					</div>
					<div class="user-about">
						<div class="user-data">
							<p class="user-data-value">${ user.getFirstName() } ${ user.getSecondName() }</p><br>
							<p class="user-data-value"> ${ user.getEmail() }</p><br>
							<p class="user-data-value"><fmt:message key="profile.status" bundle="${ rb }"/>: ${ user.getStatus() }</p><br>
						</div>
						<div class="user-buttons">
							<div class="profile-settings-button">
								<div class="edit-button">
									<a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toChangeProfile"><p><fmt:message key="profile.changeProfile" bundle="${ rb }"/></p></a>
								</div>
							</div>
							<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
								<input type="hidden" name="command" value="toCoeffTable" />
								<input class="coeff-button" type="submit" value="<fmt:message key="bookmaker.profile.coeffBtn" bundle="${ rb }"/>"/>
							</form>
							<div class="profile-messages-button">
								<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
									<input type="hidden" name="command" value="profileMessages" />
									<input class="messages-button" type="submit" value="<fmt:message key="profile.messagesBtn" bundle="${ rb }"/>"/>
								</form>
							</div>
						</div>
					</div>
				</div>
				<c:if test="${ not empty bookmakerError }">
					<p class="error-message">${ bookmakerError }</p>
				</c:if>
				<c:if test="${ currentRaces }">
				<div class="set-coeff-main ">
					<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST" name="bookmakerRaceComand">
						<h2 class="add-race-h2"><fmt:message key="bookmaker.table.name" bundle="${ rb }"/></h2>
						<input type="button" class="set-coeff-button" onclick="determineRaceButton(this)" name="setCoeff" value="<fmt:message key="bookmaker.table.setCoeffBtn" bundle="${ rb }"/>">
						<input type="button" class="decline-rac-button" onclick="determineRaceButton(this)" name="deleteRace" value="<fmt:message key="bookmaker.table.declineRaceBtn" bundle="${ rb }"/>">
						<input type="hidden" id="raceCommand" name="command" value="undef">
						<table id="data-table" class="data-table">
						
						<thead>
							<tr>
								<th width="240px"><fmt:message key="bookmaker.table.raceName" bundle="${ rb }"/></th>
								<th width="240px"><fmt:message key="bookmaker.table.raceDate" bundle="${ rb }"/></th>
								<th width="240px"><fmt:message key="bookmaker.table.raceStatus" bundle="${ rb }"/></th>
								<th width="240px"><fmt:message key="bookmaker.table.raceCity" bundle="${ rb }"/></th>
								<th width="240px"><fmt:message key="bookmaker.table.raceCountry" bundle="${ rb }"/></th>
							</tr>
						</thead>
						
						<tbody id="body-table">
						
						<c:forEach var="race" items="${ raceList }" varStatus="status">
							<tr>
								<td>${ race.getName() }</td>
								<td><locDate:localeDate locale="${ locale }" date="${ race.getDate() }"/></td>
								<td>${ race.getStatus() }</td>
								<td>${ race.getCity() }</td>
								<td>${ race.getCountry() }</td>
							</tr>
						</c:forEach>
						
						</tbody>
						</table>
						<input type="hidden" name="selected-row-value" id="selected-row"/>
						
						<br>
						<input type="hidden" name="command" value="toSetCoeff"/>
						</form>
					</div>
				</c:if>
				<c:if test="${ not empty profileError }">
					<p class="error-bookmaker-message">${ profileError }</p>
				</c:if>
				<c:if test="${ noMessagesBefore }">
					<p class="no-messages-before"><fmt:message key="profile.table.noBefore" bundle="${ rb }"/></p>
				</c:if>
				<c:if test="${ showMessagesList }">
					<%@ include file="../message/messagesList.jsp"%>
				</c:if>
				<c:if test="${ messageWithUser }">
					<%@ include file="../message/messageBookmaker.jsp"%>
				</c:if>
				<c:if test="${ newMessage }">
					<%@ include file="../message/newMessage.jsp"%>
				</c:if>
			</div>
		</div>
<%@ include file="../../pageParts/footer/footer.jsp"%>
<script src="${ pageContext.servletContext.contextPath }/js/define_bookmaker_button.js"></script>
<script src="${ pageContext.servletContext.contextPath }/js/select_race_row_bookmaker.js"></script>
<script src="${ pageContext.servletContext.contextPath }/js/pagination.js"></script>
<script src="${ pageContext.servletContext.contextPath }/js/messagepagination.js"></script>
<script src="${ pageContext.servletContext.contextPath }/js/upload_avatar.js"></script>
</body>
</html>