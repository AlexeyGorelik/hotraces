<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<p class="error-in-message">${ messageError }</p>
<div class="messages-info">
	<table id="messages-table" class="messages-table">
		<caption><fmt:message key="messages.list.table.name" bundle="${ rb }"/></caption>
		<thead>
			<tr>
				<th><fmt:message key="messages.list.table.last" bundle="${ rb }"/></th>
				<th><fmt:message key="messages.list.table.author" bundle="${ rb }"/></th>
				<th><fmt:message key="messages.list.table.text" bundle="${ rb }"/></th>
			</tr>
		</thead>
		<tbody id="body-table">
			<c:forEach var="message" items="${ messagesList }" varStatus="status">
			<tr>
				<td width="230px">${ message.getDate() }</td>
				<td width="150px">${ message.getNameFrom() }</td>
				<td>${ message.getCutText() }</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="message-table-action">
		<c:if test="${ not empty messagesList}">
		<div class="action-with-message">
			<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="GET">
				<input type="submit" class="select-dailog-button" value="<fmt:message key="messages.list.table.selectdialog" bundle="${ rb }"/>">
				<input type="hidden" name="user-to" id="userTo"/>
				<input type="hidden" name="command" value="toWriteMessage"/>
			</form>
		</div>
		</c:if>
		<div class="action-with-message">
			<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
				<input type="submit" class="new-user-message-button" value="<fmt:message key="messages.list.table.newmessage" bundle="${ rb }"/>">
				<input type="hidden" name="command" value="toNewMessage"/>
			</form>
		</div>
	</div>
</div>
<script src="${ pageContext.servletContext.contextPath }/js/select_row_messages.js"></script>