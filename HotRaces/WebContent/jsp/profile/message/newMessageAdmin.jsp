<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<p class="error-in-message">${ messageError }</p>
<div class="new-message-admin-main">
	<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
		<p class="new-dialog-with"><fmt:message key="messages.new.nickname" bundle="${ rb }"/>: </p>
		<select class="user-list" name="userToName">
			<c:forEach var="user" items="${ allUserList }" varStatus="status">
				<option value="${ user }">${ user }</option>
			</c:forEach>
		</select>
		<div class="send-message">
			<textarea name="message-text" placeholder="<fmt:message key="messages.new.entermessage" bundle="${ rb }"/>"></textarea>
		</div>
		<input type="hidden" name="command" value="sendNewMessage" />
		<input class="send-message-to-user-button" type="submit" value="<fmt:message key="messages.new.send" bundle="${ rb }"/>"/>
	</form>
</div>