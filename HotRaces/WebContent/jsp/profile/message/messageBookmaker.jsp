<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<p class="error-in-message">${ messageError }</p>
<div class="message-bookmaker-main">
	<p id="history-with" class="dialog-with"><fmt:message key="messages.history" bundle="${ rb }"/>: ${ userToName }</p>
	
	<div id="dialog" class="dialog">
	
		<c:forEach var="message" items="${ messageList }" >
		<div class="message">
		
			<c:if test="${ message.getIdFrom() == user.getUserId()  }">
			<div class="avatar">
				<img class="message-avatar" src="${ pageContext.request.contextPath }${ user.getAvatarPath() }" alt="">
			</div>
			<div class="people-data">
				${ user.getLogin() }, ${ message.getDate() }
			</div>
				${ message.getText() }
			</c:if>
			
			<c:if test="${ message.getIdFrom() != user.getUserId()  }">
			<div class="avatar">
				<img class="message-avatar" src="${ pageContext.request.contextPath }${ message.getAvatarPath() }" alt="">
			</div>
			<div class="people-data">
				${ message.getNameFrom() }, ${ message.getDate() }
			</div>
				${ message.getText() }
			</c:if>
			
		</div>
		</c:forEach>
	</div>
	<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
		<div class="send-message">
			<textarea name="message-text"> </textarea>
		</div>
		<input type="hidden" name="command" value="sendMessage" />
		<input type="hidden" name="userToName" value="${ userToName }"/>
		<input class="send-message-to-user-button" type="submit" value="<fmt:message key="messages.send" bundle="${ rb }"/>"/>
	</form>
</div>
<script src="${ pageContext.servletContext.contextPath }/js/scroll_dialog.js"></script>