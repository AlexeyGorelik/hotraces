<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/profile-change.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<%@ include file="../../pageParts/header/header.jsp"%>
	<div class="change-data-block">
			<div class="shadow">
				<div class="change-profile-form">
					<div class="form">
					      <div class="tab-content">
					        
					        <div id="login">   
					          <h1><fmt:message key="profilechange.header" bundle="${ rb }"/></h1>
					          <c:if test="${ not empty changeError }">
							  	<p class="error-profile-change">${ changeError }</p>
							  </c:if>
					          <form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST" enctype="multipart/form-data">
					          <div class="field-wrap-container">
						          <div class="field-wrap-label"><p><fmt:message key="profilechange.firstName" bundle="${ rb }"/></p></div>
						          <div class="field-wrap">
						            <input type="text" name="changeFirstName" pattern="[а-яА-Яa-zA-Z\d_]{3,16}"  autofocus autocomplete="off" value="${ user.getFirstName() }"/>
						          </div>
					          </div>
					          <div class="field-wrap-container">
						          <div class="field-wrap-label"><p><fmt:message key="profilechange.secondName" bundle="${ rb }"/></p></div>
						          <div class="field-wrap">
						            <input type="text" name="changeSecondName" pattern="[а-яА-Яa-zA-Z\d_]{3,16}"  autocomplete="off" value="${ user.getSecondName() }"/>
						          </div>
					          </div>
					          <div class="field-wrap-container">
						          <div class="field-wrap-label"><p><fmt:message key="profilechange.nickname" bundle="${ rb }"/></p></div>
						          <div class="field-wrap">
						            <input type="text" name="changeNickname" pattern="[a-zA-Z\d_]{3,16}" autocomplete="off" value="${ user.login }"/>
						          </div>
					          </div>
					          <div class="field-wrap-container">
						          <div class="field-wrap-label"><p><fmt:message key="profilechange.email" bundle="${ rb }"/></p></div>
						          <div class="field-wrap">
						            <input type="email" name="changeEmail" pattern="^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$" autocomplete="off" value="${ user.email }"/>
						          </div>
					          </div>
					          <input type="hidden" name="command" value="changeProfile"/>
					          <input type="submit" class="button button-confirm" value="<fmt:message key="profilechange.confirm" bundle="${ rb }"/>"/>
					          
					          </form>

					        </div>
					        
					      </div>
					</div> 
					
				</div>
			</div>
		</div>
<%@ include file="../../pageParts/footer/footer.jsp"%>
<script src="${ pageContext.servletContext.contextPath }/js/change_profile.js"></script>
</body>
</html>