<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><fmt:message key="payment.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/payment.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<div class="main-page">
		<%@ include file="../../pageParts/header/header.jsp"%>
	</div>
	<div class="main-body">
		<div class="registration-block">
				<div class="shadow">
				<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
					<c:if test="${ not empty errorMessage }">
						<p class="payment-error-message">${ errorMessage }</p>
					</c:if>
					<div class="card-main">
						<div class="card-picture">
							<div class="card-logo visa"></div>
							<div class="card-logo master-card"></div>
							<div class="card-logo maestro"></div>
						</div>

						<div class="card-number">
							<div class="code">
								<p><fmt:message key="payment.cardnumber" bundle="${ rb }"/></p><br>
								<input class="card-field" type="text" name="card-num1" maxlength="4" autofocus pattern="^[0-9]{4}$" title="<fmt:message key="payment.cardNumber" bundle="${ rb }"/>">
								<input class="card-field" type="text" name="card-num2" maxlength="4" pattern="^[0-9]{4}$" title="<fmt:message key="payment.cardNumber" bundle="${ rb }"/>">
								<input class="card-field" type="text" name="card-num3" maxlength="4" pattern="^[0-9]{4}$" title="<fmt:message key="payment.cardNumber" bundle="${ rb }"/>">
								<input class="card-field" type="text" name="card-num4" maxlength="4" pattern="^[0-9]{4}$" title="<fmt:message key="payment.cardNumber" bundle="${ rb }"/>">
							</div>
							<div class="code">
								<br><p><fmt:message key="payment.validityPerioud" bundle="${ rb }"/></p><br>
								<input class="card-field-date" type="text" name="card-month" maxlength="2" pattern="^[0-9]{2}$" title="<fmt:message key="payment.monthTitle" bundle="${ rb }"/>">
								<input class="card-field-date" type="text" name="card-year" maxlength="2" pattern="^[0-9]{2}$" title="<fmt:message key="payment.yearTitle" bundle="${ rb }"/>">
								<p>&nbsp;&nbsp;<fmt:message key="payment.month" bundle="${ rb }"/> / <fmt:message key="payment.year" bundle="${ rb }"/></p>
							</div>
							<div class="payment-sum">
								<br><p><fmt:message key="payment.sum" bundle="${ rb }"/></p><br>
								<input class="field-sum" type="text" name="sum" pattern="^[-+]?[0-9]*[.,]?[0-9](?:[eE][-+]?[0-9]+)?$" title="<fmt:message key="payment.sumTitle" bundle="${ rb }"/>">
							</div>
						</div>


					</div>
					<div class="card-additional">
						<div class="black-background"></div>
							<p>CVC2 / CVV2</p>
						<div class="back-code"><input class="card-field-back" name="cvv" type="text" maxlength="3" pattern="^[0-9]{3}$" title="<fmt:message key="payment.cvv" bundle="${ rb }"/>"></div>
					</div>
					<input class="confirm-pay" type="submit" value="<fmt:message key="payment.pay" bundle="${ rb }"/>">
					<input type="hidden" name="command" value="payment">
				</form>
				</div>
			</div>
	</div>
<%@ include file="../../pageParts/footer/footer.jsp"%>
</body>
</html>