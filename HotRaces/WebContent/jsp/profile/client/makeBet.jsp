<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="locDate" uri="localeDate" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="makebet.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/left_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/make_bet.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<%@ include file="../../pageParts/header/header.jsp"%>
	
	<div class="main-body">
		<%@ include file="../../pageParts/left-menu/leftmenu.jsp"%>
				
		<div class="make-bet">
		<p class="error-message">${ makeBetError }</p>
			<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
				<p class="error-message"> ${ error }</p>
				<h2 class="add-race-h2"><fmt:message key="makebet.table.name" bundle="${ rb }"/></h2>
				<p class="bet-data-description">${ raceName }, <locDate:localeDate locale="${ locale }" date="${ raceDate }"/></p>
				<input name="race-name" type="hidden" value="${ raceName }">
				<input name="race-date" type="hidden" value="<locDate:localeDate locale="${ locale }" date="${ raceDate }"/>">
				
				<table id="data-table" class="data-table">
				
				<thead>
					<tr>
						<th width="185px"><fmt:message key="makebet.table.horseId" bundle="${ rb }"/></th>
						<th width="185px"><fmt:message key="makebet.table.horseName" bundle="${ rb }"/></th>
						<th width="185px"><fmt:message key="makebet.table.coeff" bundle="${ rb }"/></th>
						<th width="120px"><fmt:message key="makebet.table.betType" bundle="${ rb }"/></th>
						<th width="225px"><fmt:message key="makebet.table.sum" bundle="${ rb }"/></th>
					</tr>
				</thead>
				
				<tbody id="body-table">
				
				
				<c:forEach var="data" items="${ betList }" varStatus="status">
					<tr>
						<input type="hidden" name="horse-id-${ status.count }" value="${ data.getHorseId() }"/>
						<td name="horse-id-${ status.count }">${ data.getHorseId() }</td>
						
						<input type="hidden" name="horse-name-${ status.count }" value="${ data.getHorseName() }"/>
						<td name="horse-name-${ status.count }">${ data.getHorseName() }</td>
						
						<input type="hidden" name="horse-coeff-${ status.count }" value="${ data.getCoeff() }"/>
						<td name="horse-coeff-${ status.count }">${ data.getCoeff() }</td>
						
						<td style="background-color: #212121;">
							<select name="bet-type-${ status.count }" onchange="changeValue(this)">
								<option value="1"><fmt:message key="makebet.table.bettype.winner" bundle="${ rb }"/></option>
								<option value="2"><fmt:message key="makebet.table.bettype.firstThree" bundle="${ rb }"/></option>
							</select>
						</td>
						
						<td class="set-sum"><input type="text" class="input-coeff" name="bet-${ status.count }" pattern ="^\d+(?:\.\d{1,2})?$"/></td>
					</tr>
				
				</c:forEach>
				
				
				</tbody>
				</table>
				
				<br>
				<input type="submit" class="button" value="<fmt:message key="makebet.table.button.makebet" bundle="${ rb }"/>"/>
				<input type="hidden" name="betCount" value="${ betCount }"/>
				<input type="hidden" name="command" value="makeBet"/>
			</form>
		</div>
	</div>
<%@ include file="../../pageParts/footer/footer.jsp"%>
</body>
</html>