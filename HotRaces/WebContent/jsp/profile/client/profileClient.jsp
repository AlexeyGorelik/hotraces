<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="locDate" uri="localeDate" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="profile.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/profile.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/messages_table.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/message.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/pagination.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/table_by_page.css">
</head>
<body>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-3.2.1.min.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-ui.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery.table.hpaging.js"></script>
	<%@ include file="../../pageParts/header/header.jsp"%>
	<div class="main-body">

			<div class="profile">
				<div class="user-info">
					<div class="profile-img-and-settings">
						<div class="user-img">
							<img src="${ pageContext.request.contextPath }${ user.getAvatarPath() }" title="Рекомендуемый размер изображения 250x250 px"/>
						</div>
						<div class="controls">
							<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=uploadAvatar" method="POST" enctype="multipart/form-data">
								<input class="change-avatar-button" type="button" value="Change avatar" id="nofile"/>
								<input name="file" type="file" onchange="this.form.submit()" id="fileuploads" runat="server" style="display: none;" />
								<input type="submit" hidden="true">
							</form>
							<c:if test="${ uplAvatar }">
								<p class="error-message">Can't upload avatar.</p>
							</c:if>
						</div>
					</div>
					<div class="user-about">
						<div class="user-data">
							<p class="user-data-value">${ user.getFirstName() } ${ user.getSecondName() }</p><br>
							<p class="user-data-value"> ${ user.getEmail() }</p><br>
							<p class="user-data-value"><fmt:message key="profile.status" bundle="${ rb }"/>: ${ user.getStatus() }</p><br>
							<p class="user-data-value"><fmt:message key="profile.onAccaount" bundle="${ rb }"/>: ${ user.getMoney().toString() }$</p><br>
						</div>
						<div class="user-buttons">
							<div class="profile-settings-button">
								<div class="edit-button">
									<a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toChangeProfile"><p><fmt:message key="profile.changeProfile" bundle="${ rb }"/></p></a>
								</div>
							</div>
							<div class="profile-account-button">
								<div class="edit-button">
									<a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toPayment"><p><fmt:message key="profile.addMoney" bundle="${ rb }"/></p></a>
								</div>
							</div>
							<div class="profile-prev-bets-button">
								<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
									<input type="hidden" name="command" value="profilePreviousBet" />
									<input class="previous-bets-button" type="submit" value="<fmt:message key="profile.prevBets" bundle="${ rb }"/>">
								</form>
							</div>
							<div class="profile-messages-button">
								<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
									<input type="hidden" name="command" value="profileMessages" />
									<input class="messages-button" type="submit" value="<fmt:message key="profile.messagesBtn" bundle="${ rb }"/>"/>
								</form>
							</div>
						</div>
					</div>
				</div>
				<c:if test="${ noBetsBefore }">
					<p class="no-bets-before"><fmt:message key="profile.table.noBefore" bundle="${ rb }"/></p>
				</c:if>
				<c:if test="${ not empty prevBetList }">
				<div class="bets-info">
					<table id="previous-bets-table" class="previous-bets-table">
						<caption><fmt:message key="profile.table.previousBets" bundle="${ rb }"/></caption>
						<thead>
							<tr>
								<th><fmt:message key="profile.table.betNumber" bundle="${ rb }"/></th>
								<th><fmt:message key="profile.table.raceName" bundle="${ rb }"/></th>
								<th><fmt:message key="profile.table.horse" bundle="${ rb }"/></th>
								<th><fmt:message key="profile.table.raceDate" bundle="${ rb }"/></th>
								<th><fmt:message key="profile.table.sum" bundle="${ rb }"/></th>
								<th><fmt:message key="profile.table.result" bundle="${ rb }"/></th>
								<th><fmt:message key="profile.table.winSum" bundle="${ rb }"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="bet" items="${prevBetList}" varStatus="status">
								<tr>
									<td><c:out value="${ bet.id }" /></td>
									<td><c:out value="${ bet.getRaceName() }" /></td>
									<td><c:out value="${ bet.getHorseName() }" /></td>
									<td><locDate:localeDate locale="${ locale }" date="${ bet.getRaceDate() }"/></td>
									
									<td><c:out value="${ bet.getSum() }" /></td>
									<td><c:out value="${ bet.getResult() }" /></td>
									<td><c:out value="${ bet.getSumWin() }" /></td>
								</tr>
							</c:forEach>
					
						</tbody>
						</table>
					</div>
				</c:if>
				<c:if test="${ not empty profileError }">
					<p class="error-client-message">${ profileError }</p>
				</c:if>
				<c:if test="${ noMessagesBefore }">
					<p class="no-messages-before"><fmt:message key="profile.table.noBefore" bundle="${ rb }"/></p>
				</c:if>
				<c:if test="${ showMessagesList }">
					<%@ include file="../message/messagesList.jsp"%>
				</c:if>
				<c:if test="${ messageWithUser }">
					<%@ include file="../message/messageClient.jsp"%>
				</c:if>
				<c:if test="${ newMessage }">
					<%@ include file="../message/newMessage.jsp"%>
				</c:if>
			</div>
		</div>
<%@ include file="../../pageParts/footer/footer.jsp"%>
<script src="${ pageContext.servletContext.contextPath }/js/upload_avatar.js" type="text/javascript"></script>
<script src="${ pageContext.servletContext.contextPath }/js/clientpagination.js"></script>
</body>
</html>