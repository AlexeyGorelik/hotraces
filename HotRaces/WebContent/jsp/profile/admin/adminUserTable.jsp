<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="admin.profile.user.title" bundle="${ rb }"/></title>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/admin_profile.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/admin_table.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/pagination.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-3.2.1.min.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-ui.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery.table.hpaging.js"></script>
	<%@ include file="../../pageParts/header/header.jsp"%>
	<div class="main-body">
		<div class="profile">

				<div class="user-info">
					<div class="left-admin-menu">
						<ul class="user-navig-panel">
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/userOrange.png"></div>
								<a class="navig-user" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=adminUserTable"><fmt:message key="admin.profile.panel.menu.users" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/race.png" ></div>
								<a class="navig-race" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=adminRaceTable"><fmt:message key="admin.profile.panel.menu.races" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/horse.png" ></div>
								<a class="navig-horse" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toHorseTable"><fmt:message key="admin.profile.panel.menu.horses" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/statistic.png" ></div>
								<a class="navig-statistic" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toStatistic"><fmt:message key="admin.profile.panel.menu.stat" bundle="${ rb }"/></a></li>
						</ul>
					</div>
					<c:if test="${ not empty userTableError }">
						<p class="error-message">${ userTableError }</p>
					</c:if>
					<c:if test="${ userListExist }">
					<div class="table-info">
					<form name="changeUserStatus" action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
						<input type="button" class="button" onclick="determineUserButton(this)" name="ban" value="<fmt:message key="admin.profile.user.table.banUser" bundle="${ rb }"/>">
						<input type="button" class="button" onclick="determineUserButton(this)" name="unban" value="<fmt:message key="admin.profile.user.table.unbanUser" bundle="${ rb }"/>">
						<input type="hidden" id="userCommand" name="command" value="undef">
						<script src="${ pageContext.servletContext.contextPath }/js/define_button.js"></script>
						<br>
						<table id="data-table" class="data-table">
							<caption><fmt:message key="admin.profile.user.table.name" bundle="${ rb }"/></caption>
							<thead>
								<tr>
									<th width="130px"><fmt:message key="admin.profile.user.table.userId" bundle="${ rb }"/></th>
									<th width="120px"><fmt:message key="admin.profile.user.table.nickname" bundle="${ rb }"/></th>
									<th width="120px"><fmt:message key="admin.profile.user.table.email" bundle="${ rb }"/></th>
									<th width="120px"><fmt:message key="admin.profile.user.table.userFirstName" bundle="${ rb }"/></th>
									<th width="120px"><fmt:message key="admin.profile.user.table.userSecond" bundle="${ rb }"/></th>
									<th width="120px"><fmt:message key="admin.profile.user.table.status" bundle="${ rb }"/></th>
								</tr>
							</thead>
							<tbody id="body-table" class="body-table">
								<c:forEach var="user" items="${ userList }" varStatus="status">
									<tr>
										<td><c:out value="${ user.getUserId() }" /></td>
										<td><c:out value="${ user.getLogin() }" /></td>
										<td><c:out value="${ user.getEmail() }" /></td>
										<td><c:out value="${ user.getFirstName() }" /></td>
										<td><c:out value="${ user.getSecondName() }" /></td>
										<td><c:out value="${ user.getStatus() }" /></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<input type="hidden" name="selected-row-value" id="selected-row"/>
					</form>
					</div>
					
				</c:if>
				</div>
			</div>
		</div>
<%@ include file="../../pageParts/footer/footer.jsp"%>
<script src="${ pageContext.servletContext.contextPath }/js/pagination.js"></script>
<script src="${ pageContext.servletContext.contextPath }/js/select_row.js"></script>
</body>
</html>