<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="locDate" uri="localeDate" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="admin.profile.editRace.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/admin_profile.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/admin_change_race.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<%@ include file="../../pageParts/header/header.jsp"%>
	<div class="main-body">
			<div class="profile">
				<div class="user-info">
					<div class="left-admin-menu">
						<ul class="user-navig-panel">
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/userOrange.png"></div>
								<a class="navig-user" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=adminUserTable"><fmt:message key="admin.profile.panel.menu.users" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/race.png" ></div>
								<a class="navig-race" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=adminRaceTable"><fmt:message key="admin.profile.panel.menu.races" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/horse.png" ></div>
								<a class="navig-horse" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toHorseTable"><fmt:message key="admin.profile.panel.menu.horses" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/statistic.png" ></div>
								<a class="navig-statistic" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toStatistic"><fmt:message key="admin.profile.panel.menu.stat" bundle="${ rb }"/></a></li>
						</ul>
					</div>

					<div class="change-race-main">
					<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
						<h2 class="add-race-h2"><fmt:message key="admin.profile.editRace.table.name" bundle="${ rb }"/></h2>
						<div class="race-edit-descr">
						<p class="input-description"><fmt:message key="admin.profile.editRace.table.hippodrome" bundle="${ rb }"/>:</p>
						<p class="input-race-data">${ raceName }</p>
						<input type="hidden" name="race-name" value="${ raceName }">
						
						<p class="input-description"><fmt:message key="admin.profile.editRace.table.date" bundle="${ rb }"/>:</p>
						<p class="input-race-data"><locDate:localeDate locale="${ locale }" date="${ raceDate }"/></p>
						<input type="hidden" name="race-date" value="<locDate:localeDate locale="${ locale }" date="${ raceDate }"/>"/>
						</div>
						<table id="data-table" class="data-table">
						<thead>
							<tr>
								<th width="110px"><fmt:message key="admin.profile.editRace.table.delete" bundle="${ rb }"/></th>
								<th width="100px"><fmt:message key="admin.profile.editRace.table.add" bundle="${ rb }"/></th>
								<th width="156px"><fmt:message key="admin.profile.editRace.table.horseId" bundle="${ rb }"/></th>
								<th width="206px"><fmt:message key="admin.profile.editRace.table.horseName" bundle="${ rb }"/></th>
							</tr>
						</thead>
						<tbody id="body-table">
						<c:forEach var="horse" items="${ horseList }" varStatus="status">
							<tr>
								<td>
									<div class="checkbox-wrap">
									<label>
									    <input class="checkbox" type="checkbox" name="horse-${status.count}" value="delete"/>
									    <span class="checkbox-custom"></span>
									</label>
									</div>
								</td>
								<td>
								</td>
								<td>
									<select class="horse-list" name="horse-${status.count}-id" onchange="changeValue(this)">
										<option value="${ horse.getId() }" selected>${ horse.getId() }</option>
										<%-- <c:forEach var="horseItem" items="${ horseCollList }" varStatus="tempStatus">
											<c:if test="${ horse.getId() == horseItem.getId() }">
												<option value="${ horseItem.getId() }" selected>${ horseItem.getId() }</option>
											</c:if>
											<c:if test="${ horse.getId() != horseItem.getId() }">
												<option value="${ horseItem.getId() }" disabled>${ horseItem.getId() }</option>
											</c:if>
										</c:forEach>--%>
									</select>
								</td>
								<td>
									<select class="horse-list" name="horse-${status.count}-name" onchange="changeValue(this)">
										<option value="${ horse.getName() }" selected>${ horse.getName() }</option>
										<%-- <c:forEach var="horseItem" items="${ horseCollList }" varStatus="tempStatus">
											<c:if test="${ horse.getName() == horseItem.getName() }">
												<option value="${ horseItem.getName() }" selected>${ horseItem.getName() }</option>
											</c:if>
											<c:if test="${ horse.getName() != horseItem.getName() }">
												<option value="${ horseItem.getName() }" disabled>${ horseItem.getName() }</option>
											</c:if>
										</c:forEach> --%>
									</select>
								</td>
							</tr>
						</c:forEach>
						
						<c:forEach var="loop" begin="${ minHorseCount }" end="${ maxHorseCount }" >
							<tr>
								<td></td>
								<td>
									<div class="checkbox-wrap">
									<label>
									    <input class="checkbox" type="checkbox" name="horse-${loop}" value="add" onClick="checkAction(this)"/>
									    <span class="checkbox-custom"></span>
									</label>
									</div>
								</td>
								<td>
									<select class="horse-list" name="horse-${loop}-id" disabled onchange="changeValue(this)">
										<c:forEach var="horseItem" items="${ horseCollList }" varStatus="status">
											<option value="${ horseItem.getId() }">${ horseItem.getId() }</option>
										</c:forEach>
									</select>
								</td>
								<td>
									<select class="horse-list" name="horse-${loop}-name" disabled onchange="changeValue(this)">
										<c:forEach var="horseItem" items="${ horseCollList }" varStatus="status">
											<option value="${ horseItem.getName() }">${ horseItem.getName() }</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							
						</c:forEach>
						</tbody>
						</table>
						<br>
						<input type="submit" class="save-button" value="<fmt:message key="admin.profile.editRace.table.save" bundle="${ rb }"/>"/>
						<input type="hidden" name="command" value="editRace"/>
						
						</form>
					</div>
				</div>
			</div>
		</div>
<%@ include file="../../pageParts/footer/footer.jsp"%>
<script src="${ pageContext.servletContext.contextPath }/js/edit_race_action_with_horse.js"></script>
<script src="${ pageContext.servletContext.contextPath }/js/change_select_value.js"></script>
</body>
</html>