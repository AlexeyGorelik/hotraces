<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="locDate" uri="localeDate" %>
<%@ taglib prefix="rtb" uri="raceTableBody" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="admin.profile.race.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/admin_profile.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/admin_table.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/pagination.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-3.2.1.min.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-ui.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery.table.hpaging.js"></script>
	<%@ include file="../../pageParts/header/header.jsp"%>
	<div class="main-body">
		<div class="profile">

				<div class="user-info">
					<div class="left-admin-menu">
						<ul class="user-navig-panel">
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/userOrange.png"></div>
								<a class="navig-user" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=adminUserTable"><fmt:message key="admin.profile.panel.menu.users" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/race.png" ></div>
								<a class="navig-race" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=adminRaceTable"><fmt:message key="admin.profile.panel.menu.races" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/horse.png" ></div>
								<a class="navig-horse" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toHorseTable"><fmt:message key="admin.profile.panel.menu.horses" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/statistic.png" ></div>
								<a class="navig-statistic" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toStatistic"><fmt:message key="admin.profile.panel.menu.stat" bundle="${ rb }"/></a></li>
						</ul>
					</div>
					<c:if test="${ not empty raceError }">
						<p class="error-message">${ raceError }</p>
					</c:if>
					<c:if test="${ raceListExist }">
					<div class="table-info">
					<form name="adminRaceCommand" action="${ pageContext.servletContext.contextPath }/HotRacesServlet">
						<input type="button" class="button" onclick="determineRaceButton(this)" name="toCreateRace" value="<fmt:message key="admin.profile.race.table.createRace" bundle="${ rb }"/>">
						<input type="button" class="button" onclick="determineRaceButton(this)" name="toEditRace" value="<fmt:message key="admin.profile.race.table.editRace" bundle="${ rb }"/>">
						<input type="button" class="button" onclick="determineRaceButton(this)" name="carryOutRace" value="<fmt:message key="admin.profile.race.table.carryOutRace" bundle="${ rb }"/>">
						<input type="button" class="button" onclick="determineRaceButton(this)" name="deleteRace" value="<fmt:message key="admin.profile.race.table.deleteRace" bundle="${ rb }"/>">
						<input type="hidden" id="raceCommand" name="command" value="undef">
						<script src="${ pageContext.servletContext.contextPath }/js/define_button.js"></script>
						<br>
						<table id="data-table" class="data-table">
							<caption><fmt:message key="admin.profile.race.table.name" bundle="${ rb }"/></caption>
							<thead>
								<tr>
									<th width="130px"><fmt:message key="admin.profile.race.table.raceName" bundle="${ rb }"/></th>
									<th width="160px"><fmt:message key="admin.profile.race.table.date" bundle="${ rb }"/></th>
									<th width="120px"><fmt:message key="admin.profile.race.table.status" bundle="${ rb }"/></th>
									<th width="120px"><fmt:message key="admin.profile.race.table.city" bundle="${ rb }"/></th>
									<th width="120px"><fmt:message key="admin.profile.race.table.country" bundle="${ rb }"/></th>
									<th width="80px"><fmt:message key="admin.profile.race.table.length" bundle="${ rb }"/></th>
								</tr>
							</thead>
							<rtb:dataTable data="${ raceList }" locale="${ locale }"></rtb:dataTable>
						</table>
							<input type="hidden" name="selected-row-value" id="selected-row"/>
						</form>
					</div>
					
				</c:if>

				</div>
			</div>
		</div>
<script src="${ pageContext.servletContext.contextPath }/js/pagination.js"></script>
<%@ include file="../../pageParts/footer/footer.jsp"%>
<script src="${ pageContext.servletContext.contextPath }/js/select_race_row.js"></script>
</body>
</html>