<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="admin.profile.horse.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/admin_profile.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/admin_table.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/pagination.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>  
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-3.2.1.min.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-ui.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery.table.hpaging.js"></script>
	<%@ include file="../../pageParts/header/header.jsp"%>
	<div class="main-body">
		<div class="profile">

				<div class="user-info">
					<div class="left-admin-menu">
						<ul class="user-navig-panel">
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/userOrange.png"></div>
								<a class="navig-user" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=adminUserTable"><fmt:message key="admin.profile.panel.menu.users" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/race.png" ></div>
								<a class="navig-race" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=adminRaceTable"><fmt:message key="admin.profile.panel.menu.races" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/horse.png" ></div>
								<a class="navig-horse" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toHorseTable"><fmt:message key="admin.profile.panel.menu.horses" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/statistic.png" ></div>
								<a class="navig-statistic" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toStatistic"><fmt:message key="admin.profile.panel.menu.stat" bundle="${ rb }"/></a></li>
						</ul>
					</div>
					<c:if test="${ not empty horseError }">
						<p class="error-message">${ horseError }</p>
					</c:if>
					<c:if test="${ horseListExist }">
					<div class="table-horse-info">
						<table id="data-table" class="data-table">
								
								<caption><fmt:message key="admin.profile.horse.table.name" bundle="${ rb }"/></caption>
								<thead>
									<tr>
										<th width="100px"><fmt:message key="admin.profile.horse.table.horseId" bundle="${ rb }"/></th>
										<th width="250px"><fmt:message key="admin.profile.horse.table.horseName" bundle="${ rb }"/></th>
										<th width="200px"><fmt:message key="admin.profile.horse.table.color" bundle="${ rb }"/></th>
										<th width="100px"><fmt:message key="admin.profile.horse.table.weight" bundle="${ rb }"/></th>
									</tr>
								</thead>
								<tbody id="body-table">
									<c:forEach var="horse" items="${ horseList }" varStatus="status">
										<tr class="data-horse">
											<td><c:out value="${ horse.getId() }" /></td>
											<td><c:out value="${ horse.getName() }" /></td>
											<td><c:out value="${ horse.getColor() }" /></td>
											<td><c:out value="${ horse.getWeight() }" /></td>
											
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<input type="hidden" name="selected-row-value" id="selected-row"/>
					</div>
				</c:if>

				</div>
			</div>
		</div>
<script src="${ pageContext.servletContext.contextPath }/js/pagination.js"></script>
<%@ include file="../../pageParts/footer/footer.jsp"%>
</body>
</html>