<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="profile.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/admin_profile.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/messages_table.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/message.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/pagination.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-3.2.1.min.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-ui.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery.table.hpaging.js"></script>
	<%@ include file="../../pageParts/header/header.jsp"%>
	<div class="main-body">
		<div class="profile">

				<div class="user-info">
					<div class="left-admin-menu">
						<ul class="user-navig-panel">
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/userOrange.png"></div>
								<a class="navig-user" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=adminUserTable"><fmt:message key="admin.profile.panel.menu.users" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/race.png" ></div>
								<a class="navig-race" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=adminRaceTable"><fmt:message key="admin.profile.panel.menu.races" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/horse.png" ></div>
								<a class="navig-horse" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toHorseTable"><fmt:message key="admin.profile.panel.menu.horses" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/statistic.png" ></div>
								<a class="navig-statistic" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toStatistic"><fmt:message key="admin.profile.panel.menu.stat" bundle="${ rb }"/></a></li>
						</ul>
					</div>
					<c:if test="${ not empty statisticError }">
						<p class="error-admin-message">${ statisticError }</p>
					</c:if>
					<div class="admin-statistic">
					<div class="bets-statistic">
						<p class="bets-stat-descr"><fmt:message key="admin.profile.stat.bets" bundle="${ rb }"/></p>
						<img src="${ pageContext.servletContext.contextPath }/pictures/stat/bets-statistic.png" alt="">
						<p class="bets-stat-today"><fmt:message key="admin.profile.stat.bets.today" bundle="${ rb }"/>:</p>
						<p class="bets-stat-today-count">${ betsToday }</p>
						<p class="bets-stat-month"><fmt:message key="admin.profile.stat.bets.thisMonth" bundle="${ rb }"/>:</p>
						<p class="bets-stat-month-count">${ betsMonth }</p>
					</div>
					<div class="user-statistic">
						<img src="${ pageContext.servletContext.contextPath }/pictures/stat/new_user.png" alt="">
						<p class="user-number">${ userCount }</p>
						<p class="user-bumber-descr"><fmt:message key="admin.profile.stat.people.registrate" bundle="${ rb }"/></p>
					</div>
					<div class="income-statistic">
						<p class="bets-stat-descr"><fmt:message key="admin.profile.stat.income" bundle="${ rb }"/></p>
						<img src="${ pageContext.servletContext.contextPath }/pictures/stat/income.png" alt="">
						<p class="bets-stat-today"><fmt:message key="admin.profile.stat.race.today" bundle="${ rb }"/>:</p>
						<p class="bets-stat-today-count">${ incomeToday }$</p>
						<p class="bets-stat-month"><fmt:message key="admin.profile.stat.race.month" bundle="${ rb }"/>:</p>
						<p class="bets-stat-month-count">${ incomeMonth }$</p>
					</div>
					<div class="race-statistic">
						<p class="bets-stat-descr"><fmt:message key="admin.profile.stat.race" bundle="${ rb }"/></p>
						<img src="${ pageContext.servletContext.contextPath }/pictures/stat/race.png" alt="">
						<p class="bets-stat-today"><fmt:message key="admin.profile.stat.race.today" bundle="${ rb }"/>:</p>
						<p class="bets-stat-today-count">${ raceToday }</p>
						<p class="bets-stat-month"><fmt:message key="admin.profile.stat.race.month" bundle="${ rb }"/>:</p>
						<p class="bets-stat-month-count">${ raceMonth }</p>
					</div>
				</div>
				
				</div>
		</div>
	</div>
<%@ include file="../../pageParts/footer/footer.jsp"%>
</body>
</html>