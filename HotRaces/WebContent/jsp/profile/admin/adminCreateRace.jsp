<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="locDate" uri="localeDate" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="admin.profile.createRace.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/admin_profile.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/admin_add_race.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<%@ include file="../../pageParts/header/header.jsp"%>
	<div class="main-body">
			<div class="profile">
				<div class="user-info">
					<div class="left-admin-menu">
						<ul class="user-navig-panel">
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/userOrange.png"></div>
								<a class="navig-user" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=adminUserTable"><fmt:message key="admin.profile.panel.menu.users" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/race.png" ></div>
								<a class="navig-race" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=adminRaceTable"><fmt:message key="admin.profile.panel.menu.races" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/horse.png" ></div>
								<a class="navig-horse" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toHorseTable"><fmt:message key="admin.profile.panel.menu.horses" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/statistic.png" ></div>
								<a class="navig-statistic" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toStatistic"><fmt:message key="admin.profile.panel.menu.stat" bundle="${ rb }"/></a></li>
						</ul>
					</div>

					<div class="add-race-main">
					<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
						<h2 class="add-race-h2"><fmt:message key="admin.profile.createRace.table.title" bundle="${ rb }"/></h2>
						<p class="input-description"><fmt:message key="admin.profile.createRace.table.name" bundle="${ rb }"/></p>
						<select class="hipp-list" name="add-race-name">
							<c:forEach var="hippItem" items="${ hippList }" varStatus="status">
								<option value="${ hippItem.getName() }">${ hippItem.getName() }</option>
							</c:forEach>
						</select>
						<p class="input-description"><fmt:message key="admin.profile.createRace.table.date" bundle="${ rb }"/></p><input name="add-race-date" class="input-date" type="datetime-local"/>
						<table id="data-table" class="data-table">
						<thead>
							<tr>
								<th style="width: 110px;"><fmt:message key="admin.profile.createRace.table.add" bundle="${ rb }"/></th>
								<th style="width: 234px;"><fmt:message key="admin.profile.createRace.table.horseId" bundle="${ rb }"/></th>
								<th style="width: 234px;"><fmt:message key="admin.profile.createRace.table.horseName" bundle="${ rb }"/></th>
							</tr>
						</thead>
						<tbody id="body-table">
						<c:forEach begin="${ minHorseCount }" end="${ maxHorseCount }" varStatus="loop">
							<tr>
								<td>
									<div class="checkbox-wrap">
									<label>
									    <input class="checkbox" type="checkbox" name="checkbox-horse" id="horse-${loop.index}" checked onclick="agreeForm(this)" value="horse-${loop.index}"/>
									    <span class="checkbox-custom"></span>
									</label>
									</div>
								</td>
								<td>
									<select class="horse-list" name="horse-${loop.index}-id" onchange="changeValue(this)">
										<c:forEach var="horseItem" items="${ horseList }" varStatus="status">
											<option value="${ horseItem.getId() }">${ horseItem.getId() }</option>
										</c:forEach>
									</select>
								</td>
								<td>
									<select class="horse-list" name="horse-${loop.index}-name" onchange="changeValue(this)">
										<c:forEach var="horseItem" items="${ horseList }" varStatus="status">
											<option value="${ horseItem.getName() }">${ horseItem.getName() }</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							
						</c:forEach>
							
						</tbody>
						</table>
						
						
						<br>
						<input type="submit" class="button" value="<fmt:message key="admin.profile.createRace.table.createRace" bundle="${ rb }"/>"/>
						<input type="hidden" name="command" value="createRace"/>
						</form>
					</div>
				</div>
			</div>
		</div>
<%@ include file="../../pageParts/footer/footer.jsp"%>
<script src="${ pageContext.servletContext.contextPath }/js/disable_horse.js"></script>
<script src="${ pageContext.servletContext.contextPath }/js/change_select_value.js"></script>
</body>
</html>