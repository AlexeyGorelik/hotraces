<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><fmt:message key="profile.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/admin_profile.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/messages_table.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/message.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/pagination.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-3.2.1.min.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-ui.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery.table.hpaging.js"></script>
	<%@ include file="../../pageParts/header/header.jsp"%>
	<div class="main-body">
		<div class="profile">

				<div class="user-info">
					<div class="left-admin-menu">
						<ul class="user-navig-panel">
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/userOrange.png"></div>
								<a class="navig-user" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=adminUserTable"><fmt:message key="admin.profile.panel.menu.users" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/race.png" ></div>
								<a class="navig-race" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=adminRaceTable"><fmt:message key="admin.profile.panel.menu.races" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/horse.png" ></div>
								<a class="navig-horse" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toHorseTable"><fmt:message key="admin.profile.panel.menu.horses" bundle="${ rb }"/></a></li>
							<li><div class="navig-pictrue"><img src="${ pageContext.servletContext.contextPath }/pictures/admin/navigation/statistic.png" ></div>
								<a class="navig-statistic" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toStatistic"><fmt:message key="admin.profile.panel.menu.stat" bundle="${ rb }"/></a></li>
						</ul>
					</div>
					<div class="profile-img-and-settings">
						<div class="user-img">
							<img src="${ pageContext.request.contextPath }${ user.getAvatarPath() }" title="<fmt:message key="profile.recomSize" bundle="${ rb }"/>"/>
						</div>

						<div class="controls">
						
							<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=uploadAvatar" method="POST" enctype="multipart/form-data">
								<input class="change-avatar-button" type="button" value="<fmt:message key="profile.uploadAvatar" bundle="${ rb }"/>" id="nofile"/>
								<input name="file" type="file" onchange="this.form.submit()" id="fileuploads" runat="server" style="display: none;" />
								<input type="submit" hidden="true">
							</form>
						</div>
					</div>
					<div class="user-about">
						<div class="user-data">
							<p class="user-data-value">${ user.getFirstName() } ${ user.getSecondName() }</p><br>
							<p class="user-data-value">${ user.getEmail() }</p><br>
							<p class="user-data-value"><fmt:message key="profile.status" bundle="${ rb }"/>: ${ user.getStatus() }</p><br>
						</div>
					</div>
					<div class="user-buttons">
						<div class="profile-messages-button">
							<div class="profile-settings-button">
								<div class="edit-button">
									<a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toChangeProfile"><p><fmt:message key="profile.changeProfile" bundle="${ rb }"/></p></a>
								</div>
							</div>
							<form class="admin-prof-form" action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
								<input type="hidden" name="command" value="profileMessages" />
								<input class="messages-button" type="submit" value="<fmt:message key="profile.messagesBtn" bundle="${ rb }"/>"/>
							</form>
							<form class="admin-prof-form" action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="GET">
								<input type="hidden" name="command" value="toAdminSupport" />
								<input class="messages-button" type="submit" value="<fmt:message key="admin.profile.button.support" bundle="${ rb }"/>"/>
							</form>
						</div>
					</div>
					
				</div>
				<c:if test="${ not empty profileError }">
					<p class="error-admin-message">${ profileError }</p>
				</c:if>
				<c:if test="${ showMessagesList }">
					<%@ include file="../message/messagesAdminList.jsp"%>
				</c:if>
				<c:if test="${ messageWithUser }">
					<%@ include file="../message/messageAdmin.jsp"%>
				</c:if>
				<c:if test="${ newMessage }">
					<%@ include file="../message/newMessageAdmin.jsp"%>
				</c:if>
		</div>
	</div>
<%@ include file="../../pageParts/footer/footer.jsp"%>
<script src="${ pageContext.servletContext.contextPath }/js/upload_avatar.js"></script>
<script src="${ pageContext.servletContext.contextPath }/js/messagepagination.js"></script>
</body>
</html>