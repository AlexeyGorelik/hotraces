<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><fmt:message key="forgPassword.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/left_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/forgot_password.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<div class="main-page">
		<%@ include file="../pageParts/header/header.jsp"%>
	</div>
	<div class="main-body">
		<%@ include file="../pageParts/left-menu/leftmenu.jsp"%>
		<c:if test="${ not empty forgPasswError }">
			<p class="error-paswrd-message">${ forgPasswError }</p>
		</c:if>
		<c:if test="${ not empty newPassword }">
			<p class="new-password"><fmt:message key="forgPassword.pass.descr" bundle="${ rb }"/> ${ newPassword }</p>
		</c:if>
		
		<div class="forgot-password-main">
			<h2><fmt:message key="forgPassword.header" bundle="${ rb }"/></h2>
			<p><fmt:message key="forgPassword.writeEmail" bundle="${ rb }"/></p>
			<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
				<input type="text" name="email" class="forg-password-email" placeholder="<fmt:message key="forgPassword.placeholder" bundle="${ rb }"/>" pattern="^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$" title="<fmt:message key="forgPassword.email.title" bundle="${ rb }"/>"/>
				<input type="submit" class="forg-password-submit" value="<fmt:message key="forgPassword.send" bundle="${ rb }"/>"/>
				<input type="hidden" name="command" value="changeUserPassword"/>
			</form>
		</div>
	</div>
<%@ include file="../pageParts/footer/footer.jsp"%>
</body>
</html>