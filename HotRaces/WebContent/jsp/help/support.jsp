<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><fmt:message key="support.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/left_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/support.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<div class="main-page">
		<%@ include file="../pageParts/header/header.jsp"%>
	</div>
	<div class="main-body">
		<%@ include file="../pageParts/left-menu/leftmenu.jsp"%>
		<c:if test="${ not empty forgPasswError }">
			<p class="error-paswrd-message">${ forgPasswError }</p>
		</c:if>
		<c:if test="${ not empty newPassword }">
			<p class="new-password"><fmt:message key="forgPassword.pass.descr" bundle="${ rb }"/> ${ newPassword }</p>
		</c:if>
		
		<div class="support">
				<h1 id="supp-header" class="supp-header"><fmt:message key="support.header" bundle="${ rb }"/></h1>
				<div class="question">
					<form action="HotRacesServlet" method="POST">
						<div class="question-email-wrapper">
							<p class="question-input-email"><fmt:message key="support.input.email" bundle="${ rb }"/></p>
							<input type="text" name="email" class="question-email" pattern="^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$" title='<fmt:message key="support.email.title" bundle="${ rb }"/>'>
						</div>
						<div class="question-topic-wrapper">
							<p class="question-select-topic"><fmt:message key="support.topic" bundle="${ rb }"/></p>
							<select name='quest-type'>
								<option value='Ставки'><fmt:message key="support.topic.bets" bundle="${ rb }"/></option>
								<option value='Скачки'><fmt:message key="support.topic.races" bundle="${ rb }"/></option>
								<option value='Платежи'><fmt:message key="support.topic.payments" bundle="${ rb }"/></option>
								<option value='Вывод средств'><fmt:message key="support.topic.withdrowmoney" bundle="${ rb }"/></option>
								<option value='Другое'><fmt:message key="support.topic.other" bundle="${ rb }"/></option>
							</select>
						</div>
						<div class="question-text-wrapper">
							<p><fmt:message key="support.input.question" bundle="${ rb }"/></p>
							<textarea name="text-question" id="" cols="35" rows="10" maxlength="350" placeholder="<fmt:message key="support.question.title" bundle="${ rb }"/>"></textarea>
						</div>
						<input class="submit" type="submit" value="<fmt:message key="support.button.send" bundle="${ rb }"/>">
						<input type="hidden" name="command" value="sendQuestion">
					</form>
				</div>
				<c:if test="${ success }">
					<p class='successfull'><fmt:message key="support.success" bundle="${ rb }"/></p>
				</c:if>
				<c:if test="${ error }">
					<p class='error'><fmt:message key="support.error" bundle="${ rb }"/></p>
				</c:if>
			</div>
	</div>
<%@ include file="../pageParts/footer/footer.jsp"%>
</body>
</html>