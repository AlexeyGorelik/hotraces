<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><fmt:message key="payment.system.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/left_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/payment_system.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<div class="main-page">
		<%@ include file="../pageParts/header/header.jsp"%>
	</div>
	<div class="main-body">
		<%@ include file="../pageParts/left-menu/leftmenu.jsp"%>
		<div class="payment-sysytems-main">
			<h1><fmt:message key="payment.system.header" bundle="${ rb }"/></h1>
			<p><fmt:message key="payment.system.sent1" bundle="${ rb }"/>:</p><br>
			<p><fmt:message key="payment.system.sent1.1" bundle="${ rb }"/></p><br>
			<p><fmt:message key="payment.system.sent1.2" bundle="${ rb }"/></p><br>
			<p><fmt:message key="payment.system.sent1.3" bundle="${ rb }"/></p><br><br>
			<p><fmt:message key="payment.system.sent2" bundle="${ rb }"/>:</p><br>
			<p><fmt:message key="payment.system.sent2.1" bundle="${ rb }"/></p><br>
			<p><fmt:message key="payment.system.sent2.2" bundle="${ rb }"/></p><br>
			<p><fmt:message key="payment.system.sent2.3" bundle="${ rb }"/></p><br><br>
			<p><fmt:message key="payment.system.sent3.1" bundle="${ rb }"/> <a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toHowToPlay">
				<fmt:message key="payment.system.sent3.2" bundle="${ rb }"/></a> <fmt:message key="payment.system.sent3.3" bundle="${ rb }"/>.</p><br>
			<p><fmt:message key="payment.system.sent4.1" bundle="${ rb }"/> <a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toContact">
				<fmt:message key="payment.system.sent4.2" bundle="${ rb }"/></a> <fmt:message key="payment.system.sent4.3" bundle="${ rb }"/>.</p>
		</div>
	</div>
<%@ include file="../pageParts/footer/footer.jsp"%>
</body>
</html>