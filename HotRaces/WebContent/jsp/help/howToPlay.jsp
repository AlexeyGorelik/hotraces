<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><fmt:message key="howToPlay.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/left_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/how_to_play.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<div class="main-page">
		<%@ include file="../pageParts/header/header.jsp"%>
	</div>
	<div class="main-body">
		<%@ include file="../pageParts/left-menu/leftmenu.jsp"%>
		<div class="how-to-play-main">
			<h1><fmt:message key="howToPlay.header" bundle="${ rb }"/></h1>
			<h2><fmt:message key="howToPlay.step1.sent1" bundle="${ rb }"/></h2>
			<p><fmt:message key="howToPlay.step1.sent2" bundle="${ rb }"/></p><br>
			<p><fmt:message key="howToPlay.step1.sent3" bundle="${ rb }"/></p><br>
			<p><fmt:message key="howToPlay.step1.sent4" bundle="${ rb }"/></p><br>
			<img src="${ pageContext.servletContext.contextPath }/pictures/how_to_play/auth_${ locale }.png"/><br>
			<p><fmt:message key="howToPlay.step1.sent5" bundle="${ rb }"/></p><br>
			<img src="${ pageContext.servletContext.contextPath }/pictures/how_to_play/registration_${ locale }.png"/><br>
			<p><fmt:message key="howToPlay.step1.sent5" bundle="${ rb }"/></p><br>
			<p><fmt:message key="howToPlay.step1.sent6" bundle="${ rb }"/></p><br>
			<h2><fmt:message key="howToPlay.step2.sent1" bundle="${ rb }"/></h2>
			<p><fmt:message key="howToPlay.step2.sent2" bundle="${ rb }"/></p><br>
			<p><fmt:message key="howToPlay.step2.sent3" bundle="${ rb }"/></p><br>
			<img src="${ pageContext.servletContext.contextPath }/pictures/how_to_play/log_in_${ locale }.png"/><br>
			<p><fmt:message key="howToPlay.step2.sent4" bundle="${ rb }"/></p><br>
			<img src="${ pageContext.servletContext.contextPath }/pictures/how_to_play/suc_log_in_${ locale }.png"/><br>
			<h2><fmt:message key="howToPlay.step3.sent1" bundle="${ rb }"/></h2>
			<p><fmt:message key="howToPlay.step3.sent2" bundle="${ rb }"/></p><br>
			<p><fmt:message key="howToPlay.step3.sent3" bundle="${ rb }"/></p><br>
			<img src="${ pageContext.servletContext.contextPath }/pictures/how_to_play/my_payment_${ locale }.png"/><br>
			<p><fmt:message key="howToPlay.step3.sent4" bundle="${ rb }"/></p><br>
			<img src="${ pageContext.servletContext.contextPath }/pictures/how_to_play/payment_${ locale }.png"/><br>
			<p><fmt:message key="howToPlay.step3.sent5" bundle="${ rb }"/></p><br>
			<h2><fmt:message key="howToPlay.step4.sent1" bundle="${ rb }"/></h2>
			<p><fmt:message key="howToPlay.step4.sent2" bundle="${ rb }"/></p><br>
			<img src="${ pageContext.servletContext.contextPath }/pictures/how_to_play/bets_li_${ locale }.png"/><br>
			<p><fmt:message key="howToPlay.step4.sent3" bundle="${ rb }"/></p><br>
			<img src="${ pageContext.servletContext.contextPath }/pictures/how_to_play/select_race_${ locale }.png"/><br>
			<p><fmt:message key="howToPlay.step4.sent4" bundle="${ rb }"/></p><br>
			<img src="${ pageContext.servletContext.contextPath }/pictures/how_to_play/make_bet_${ locale }.png"/><br><br>
			<p><fmt:message key="howToPlay.step4.sent5" bundle="${ rb }"/></p><br>
			<p><fmt:message key="howToPlay.step4.sent6" bundle="${ rb }"/></p><br>
			<p><fmt:message key="howToPlay.step4.sent7" bundle="${ rb }"/></p><br>
			<p><fmt:message key="howToPlay.step4.sent8" bundle="${ rb }"/></p><br>
			<p><fmt:message key="howToPlay.step4.sent9" bundle="${ rb }"/></p><br>
			<p><fmt:message key="howToPlay.step4.sent10" bundle="${ rb }"/></p><br><br>
		</div>
	</div>
<%@ include file="../pageParts/footer/footer.jsp"%>
</body>
</html>