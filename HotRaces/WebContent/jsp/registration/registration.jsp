<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><fmt:message key="registration.title" bundle="${ rb }"/></title>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/registration.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
	<script src="${ pageContext.servletContext.contextPath }/js/check_registration.js"></script>
</head>
<body>
	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<div class="main-page">
		<%@ include file="../pageParts/header/header.jsp"%>
		
		<div id="registr-ancor" class="registration-block">
			<div class="shadow">
				<div class="registration-form">
					<div class="form">
					    <div class="tab-content">
					        <div id="signup">  
					       	 	<c:if test="${ not empty registrationError }">
									<p class="error-registration-message">${ registrationError }</p>
								</c:if> 
					        	<h1><fmt:message key="registration.signUp" bundle="${ rb }"/></h1>
					            <p id="registration-message" class="registration-message"> ${ registrationMsg } </p>
					        	<form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" onsubmit="return validateForm()" method="POST">
						          	<input type="hidden" name="command" value="registration" />
							        <div class="top-row">
							            <div class="field-wrap">
							            	<label>
							                <fmt:message key="registration.firstName" bundle="${ rb }"/><span class="req">*</span>
							            	</label>
							            	<input type="text" name="firstName" required title='<fmt:message key="registration.name" bundle="${ rb }"/>' autofocus pattern="[а-яА-Яa-zA-Z_]{3,16}" autocomplete="off" />
							            </div>
							        
							            <div class="field-wrap">
							            	<label>
							                <fmt:message key="registration.lastName" bundle="${ rb }"/><span class="req">*</span>
							            	</label>
							            	<input type="text" name="secondName" required title='<fmt:message key="registration.name" bundle="${ rb }"/>' pattern="[а-яА-Яa-zA-Z_]{3,16}" autocomplete="off"/>
							        	</div>
							        </div>

									<div class="field-wrap">
							        	<label>
							              <fmt:message key="registration.login" bundle="${ rb }"/><span class="req">*</span>
							        	</label>
							        	<input type="text" name="login" required title='<fmt:message key="registration.loginTitle" bundle="${ rb }"/>' pattern="[a-zA-Z\d_]{3,16}" autocomplete="off"/>
							        </div>

							        <div class="field-wrap">
							        	<label>
							              <fmt:message key="registration.email" bundle="${ rb }"/><span class="req">*</span>
							        	</label>
							        	<input type="email" name="email" required title="<fmt:message key="registration.emailTitle" bundle="${ rb }"/>" pattern="^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$" autocomplete="off"/>
							        </div>

						        	<div class="field-wrap">
							        	<label>
							              <fmt:message key="registration.setPassword" bundle="${ rb }"/><span class="req">*</span>
							        	</label>
							        	<input type="password" name="pswrd1" required title="<fmt:message key="registration.passwordTitle" bundle="${ rb }"/>" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[\w]{6,}$" autocomplete="off"/>
							        </div>
							        
							        <div class="field-wrap">
							        	<label>
							              <fmt:message key="registration.repeatPassword" bundle="${ rb }"/><span class="req">*</span>
							        	</label>
							        	<input type="password" name="pswrd2" required title="<fmt:message key="registration.passwordTitle" bundle="${ rb }"/>" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[\w]{6,}$" autocomplete="off"/>
							        </div>
							        <a class="to-main-registration" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toMain"><fmt:message key="registration.toMain" bundle="${ rb }"/></a>  
							        <button type="submit" class="registration-button"><fmt:message key="registration.button.registration" bundle="${ rb }"/></button>
							        
						        </form>
					        </div>
					        
					      </div>
					</div> 
				</div>
			</div>
		</div>
	</div>
<%@ include file="../pageParts/footer/footer.jsp"%>
<script src="${ pageContext.servletContext.contextPath }/js/sign_in.js"></script>
</body>
</html>