<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><fmt:message key="faq.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/left_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/main.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/faq.css">
</head>
<body>
	<div class="main-page">
		<%@ include file="../pageParts/header/header.jsp"%>
	</div>
	<div class="main-body">
		<%@ include file="../pageParts/left-menu/leftmenu.jsp"%>

		<div class="faq">
				<h2 id="whatIsRace"><fmt:message key="faq.sent1" bundle="${ rb }"/></h2>
				<p><fmt:message key="faq.sent2" bundle="${ rb }"/><br><br></p>
				<br><br>

				<h2 id="raceTypes"><fmt:message key="faq.sent3" bundle="${ rb }"/></h2>
				<p><fmt:message key="faq.sent4" bundle="${ rb }"/><br><br><fmt:message key="faq.sent4.1" bundle="${ rb }"/></p>
				<br><br>

				<h2 id="betsOnRace"><fmt:message key="faq.sent5" bundle="${ rb }"/></h2>
				<p><fmt:message key="faq.sent6" bundle="${ rb }"/><br><br><fmt:message key="faq.sent6.1" bundle="${ rb }"/></p>
				<br><br>
				
				<h2 id="betsTypes"><fmt:message key="faq.sent7" bundle="${ rb }"/>Виды ставок</h2>
				<p><fmt:message key="faq.sent8" bundle="${ rb }"/><br><br><fmt:message key="faq.sent8.1" bundle="${ rb }"/><br><br>
				<fmt:message key="faq.sent8.2" bundle="${ rb }"/></p><br><br>
				
				<h2 id="betsStrat"><fmt:message key="faq.sent9" bundle="${ rb }"/>Стратегии ставок</h2>
				<p><fmt:message key="faq.sent10" bundle="${ rb }"/><br><br><fmt:message key="faq.sent10.1" bundle="${ rb }"/><br><br>
				<fmt:message key="faq.sent10.2" bundle="${ rb }"/></p><br><br>

			</div>
	</div>
<%@ include file="../pageParts/footer/footer.jsp"%>
</body>
</html>
