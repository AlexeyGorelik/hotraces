<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><fmt:message key="aboutus.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/left_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/contact.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<div class="main-page">
		<%@ include file="../pageParts/header/header.jsp"%>
	</div>
	<div class="main-body">
		<%@ include file="../pageParts/left-menu/leftmenu.jsp"%>
		<div class="contact-content">
				<h1 class="contacts-header"><fmt:message key="aboutus.header" bundle="${ rb }"/></h1>
				<p class="contact-description"><fmt:message key="aboutus.sent1" bundle="${ rb }"/></p>
				<p class="contact-description"><fmt:message key="aboutus.sent2" bundle="${ rb }"/></p>
				<p class="contact-description"><fmt:message key="aboutus.sent3" bundle="${ rb }"/></p>
				<p class="contact-description"><fmt:message key="aboutus.sent4" bundle="${ rb }"/></p>
				<p class="contact-description"><fmt:message key="aboutus.sent5" bundle="${ rb }"/></p>
				<p class="contact-description"><fmt:message key="aboutus.sent6" bundle="${ rb }"/></p>
				<p class="contact-description"><fmt:message key="aboutus.sent7" bundle="${ rb }"/></p>
				<p class="contact-description"><fmt:message key="aboutus.sent8" bundle="${ rb }"/></p>
				<p class="contact-description"><fmt:message key="aboutus.sent9" bundle="${ rb }"/></p>
				<h2 class="contact-name"><fmt:message key="aboutus.sent10" bundle="${ rb }"/></h2>
				<p class="contact-description"><fmt:message key="aboutus.sent11" bundle="${ rb }"/></p>
				<p class="contact-description"><fmt:message key="aboutus.sent12" bundle="${ rb }"/></p>

			</div>
	</div>
<%@ include file="../pageParts/footer/footer.jsp"%>
</body>
</html>