<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><fmt:message key="hippDescr.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/left_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/main.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/hippodrom_description.css">
</head>
<body>
	<div class="main-page">
		<%@ include file="../pageParts/header/header.jsp"%>
	</div>
	<div class="main-body">
		<%@ include file="../pageParts/left-menu/leftmenu.jsp"%>

		<div class="hipp-description-page">
			<h2 id="meydan"><fmt:message key="hippDescr.meydan" bundle="${ rb }"/></h2>
			<p><fmt:message key="hippDescr.meydan.sent1" bundle="${ rb }"/>
			<br><br>
			<fmt:message key="hippDescr.meydan.sent2" bundle="${ rb }"/></p><br>
			<img src="${ pageContext.servletContext.contextPath }/pictures/hipp_descr/meydan_1.png">
			<img src="${ pageContext.servletContext.contextPath }/pictures/hipp_descr/meydan_2.png">
			<br><br>
			<h2 id="southwell"><fmt:message key="hippDescr.southwell" bundle="${ rb }"/></h2>
			<p><fmt:message key="hippDescr.southwell.sent1" bundle="${ rb }"/>
			<br><br>
			<fmt:message key="hippDescr.southwell.sent2" bundle="${ rb }"/></p><br>
			<img src="${ pageContext.servletContext.contextPath }/pictures/hipp_descr/southwell_1.png">
			<img src="${ pageContext.servletContext.contextPath }/pictures/hipp_descr/southwell_2.png">
			<br>
			<h2 id="lonshan"><fmt:message key="hippDescr.lonshan" bundle="${ rb }"/></h2>
			<p><fmt:message key="hippDescr.lonshan.sent1" bundle="${ rb }"/>
			<br><br>
			<fmt:message key="hippDescr.lonshan.sent2" bundle="${ rb }"/></p><br>
			<img src="${ pageContext.servletContext.contextPath }/pictures/hipp_descr/lonshan_1.png">
			<img src="${ pageContext.servletContext.contextPath }/pictures/hipp_descr/lonshan_2.png">
			<br>
			 
			<h2 id="newMarket"><fmt:message key="hippDescr.newMarket" bundle="${ rb }"/></h2>
			<p><fmt:message key="hippDescr.newMarket.sent1" bundle="${ rb }"/>
			<br><br>
			<fmt:message key="hippDescr.newMarket.sent2" bundle="${ rb }"/></p><br>
			<img src="${ pageContext.servletContext.contextPath }/pictures/hipp_descr/new_market_1.png">
			<img src="${ pageContext.servletContext.contextPath }/pictures/hipp_descr/new_market_2.png">
			<br>
		</div>
	</div>
<%@ include file="../pageParts/footer/footer.jsp"%>
</body>
</html>
