<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><fmt:message key="login.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/sign_in.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<%@ include file="../pageParts/header/header.jsp"%>
	<div class="main-page">
		
		<div id="login-ancor" class="registration-block">
			<div class="shadow">
				<div class="registration-form">
					 <div class="form">
					      <div class="tab-content">
					        <div id="login">   
					          <p class="error-login-message"> ${ authenticationError } </p>
					          <h1><fmt:message key="login.signIn" bundle="${ rb }"/></h1>
					          
					          <form action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="POST">
					          <input type="hidden" name="command" value="login" />
					            <div class="field-wrap">
					            <label>
					              <fmt:message key="login.login" bundle="${ rb }"/>
					            </label>
					            <input type="text" name="login" required autofocus pattern="[a-zA-Z\d_]{3,16}" title='<fmt:message key="login.loginTitle" bundle="${ rb }"/>' autocomplete="off"/>
					          </div>
					          <div class="field-wrap">
					            <label>
					              <fmt:message key="login.password" bundle="${ rb }"/>
					            </label>
					            <input type="password" name="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[\w]{6,}$" title="<fmt:message key="login.passwordTitle" bundle="${ rb }"/>" autocomplete="off"/>
					          </div>
					          <div class="registr-block"><p class="registr"><a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toRegistration"><fmt:message key="login.registration" bundle="${ rb }"/></a></p></div>
					          <div class="forgot-block"><p class="forgot"><a href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toForgotPassword"><fmt:message key="login.forgot" bundle="${ rb }"/></a></p></div>
					          <a class="to-main-sign-in" href="${ pageContext.servletContext.contextPath }/HotRacesServlet?command=toMain"><fmt:message key="login.toMain" bundle="${ rb }"/></a>
					          <input type="submit" class="sign-in-button" value="<fmt:message key="login.button.logIn" bundle="${ rb }"/>"/>
					          </form>
					        </div>
					      </div>
					</div> 
				</div>
			</div>
		</div>
	</div>
<%@ include file="../pageParts/footer/footer.jsp"%>
<script src="${ pageContext.servletContext.contextPath }/js/sign_in.js"></script>
</body>
</html>