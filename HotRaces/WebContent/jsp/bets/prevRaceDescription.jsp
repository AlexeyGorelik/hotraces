<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8"/>
	<title><fmt:message key="prevRaceDescr.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/left_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/bets.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/prev_race_descr.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<div class="main-page">
		<%@ include file="../pageParts/header/header.jsp"%>
	</div>
	<div class="main-body">
		<%@ include file="../pageParts/left-menu/leftmenu.jsp"%>
		<c:if test="${ empty horseList }">
			<p class="no-prev-race"><fmt:message key="prevRaceDescr.noPrevRaceErr" bundle="${ rb }"/></p>
		</c:if>
		<div class="previous-races">
			<div class="race-description"><p class="description"><fmt:message key="prevRaceDescr.raceName" bundle="${ rb }"/>: ${ raceName }</p><p class="description"><fmt:message key="prevRaceDescr.raceDate" bundle="${ rb }"/>: ${ raceDate }</p></div>
				<table id="prev-race-table" class="prev-race-table">
						
						<caption><fmt:message key="prevRaceDescr.table.name" bundle="${ rb }"/></caption>
						<thead>
							<tr>
								<th><fmt:message key="prevRaceDescr.table.horseName" bundle="${ rb }"/></th>
								<th width="150px"><fmt:message key="prevRaceDescr.table.horseId" bundle="${ rb }"/></th>
								<th width="150px"><fmt:message key="prevRaceDescr.table.coeff" bundle="${ rb }"/></th>
								<th width="150px"><fmt:message key="prevRaceDescr.table.place" bundle="${ rb }"/></th>
							</tr>
						</thead>
						<tbody id="body-table">
							<c:forEach var="horse" items="${ horseList }" varStatus="status">
								<tr>
									<td>${ horse.getName() }</td>
									<td>${ horse.getHorseId() }</td>
									<td>${ horse.getStringCoeff() }</td>
									<td>${ horse.getStringPlace() }</td>
								</tr>
							</c:forEach>
						</tbody>
				</table>
			</div>
			
		</div>
<%@ include file="../pageParts/footer/footer.jsp"%>
</body>
</html>