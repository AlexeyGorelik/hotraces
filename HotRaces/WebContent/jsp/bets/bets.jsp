<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="locDate" uri="localeDate" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title><fmt:message key="bets.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/left_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/bets.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/prev_race.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/pagination.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-3.2.1.min.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery-ui.js"></script>
	<script src="${ pageContext.servletContext.contextPath }/js/jquery.table.hpaging.js"></script>
	<div class="main-page">
		<%@ include file="../pageParts/header/header.jsp"%>
	</div>
	<div class="main-body">
		<%@ include file="../pageParts/left-menu/leftmenu.jsp"%>
		<div class="bets">
			<c:if test="${ not empty betError }">
				<p class="error-bets-message">${ betError }</p>
			</c:if>
			<div class="hipp-wrapper">
			
				<div class="hippodrom">
					<h2 class="hippodrom-name"><fmt:message key='bets.hipp.lonshan' bundle='${ rb }'/></h2>
					<img src="${ pageContext.servletContext.contextPath }/pictures/hippodroms/lonshan.png"/>
					<div class="table-info">
					
					<form name="changeUserStatus" action="${ pageContext.servletContext.contextPath }/HotRacesServlet"  method="GET">
						<br>
						
						<table id="data-table" class="data-table">
							<tbody id="body-table"  class="body-table">
							<c:forEach var="betRace" items="${ LonshanList }" varStatus="status">
								<tr name="lonshan">
									<td><locDate:localeDate locale="${ locale }" date="${ betRace.getDate() }"/></td>
									<td><c:out value="${ betRace.getStatus() }" /></td>
								</tr>
							</c:forEach>
							</tbody>
						</table>
						<input type="hidden" name="selected-row-value" id="selected-row-lonshan"/>
						<input type="hidden" name="hippodrom" value="Лоншан"/>
						<input type="hidden" name="command" value="toMakeBet"/>
						<input type="submit" class="button" value="<fmt:message key='bets.button.makeBet' bundle='${ rb }'/>">
					</form>
					</div>
				</div>
				
				<div class="hippodrom">
					<h2 class="hippodrom-name"><fmt:message key='bets.hipp.meydan' bundle='${ rb }'/></h2>
					<img src="${ pageContext.servletContext.contextPath }/pictures/hippodroms/meydan.jpg"/>
					<div class="table-info">
					<form name="changeUserStatus" action="${ pageContext.servletContext.contextPath }/HotRacesServlet"  method="GET">
						<br>
						
						<table id="data-table" class="data-table">
							<tbody id="body-table"  class="body-table">
							<c:forEach var="betRace" items="${ MeydanList }" varStatus="status">
								<tr name="meydan">
									<td><locDate:localeDate locale="${ locale }" date="${ betRace.getDate() }"/></td>
									<td><c:out value="${ betRace.getStatus() }" /></td>
								</tr>
							</c:forEach>
							</tbody>
						</table>
						<input type="hidden" name="selected-row-value" id="selected-row-meydan"/>
						<input type="hidden" name="hippodrom" value="Мейдан"/>
						<input type="hidden" name="command" value="toMakeBet"/>
						<input type="submit" class="button" value="<fmt:message key='bets.button.makeBet' bundle='${ rb }'/>">
					</form>
					</div>
				</div>
				
				</div>
				<div class="hipp-wrapper">
					<div class="hippodrom">
						<h2 class="hippodrom-name"><fmt:message key='bets.hipp.southwell' bundle='${ rb }'/></h2>
						<img src="${ pageContext.servletContext.contextPath }/pictures/hippodroms/southwell.png"/>
						<div class="table-info">
						<form name="changeUserStatus" action="${ pageContext.servletContext.contextPath }/HotRacesServlet"  method="GET">
							<br>
							
							<table id="data-table" class="data-table">
								<tbody id="body-table"  class="body-table">
										<c:forEach var="betRace" items="${ SouthwellList }" varStatus="status">
											<tr name="southwell">
												<td><locDate:localeDate locale="${ locale }" date="${ betRace.getDate() }"/></td>
												<td><c:out value="${ betRace.getStatus() }" /></td>
											</tr>
								</c:forEach>
								</tbody>
							</table>
							<input type="hidden" name="selected-row-value" id="selected-row-southwell"/>
							<input type="hidden" name="hippodrom" value="Саузвелл"/>
							<input type="hidden" name="command" value="toMakeBet"/>
							<input type="submit" class="button" value="<fmt:message key='bets.button.makeBet' bundle='${ rb }'/>">
						</form>
						</div>
					</div>
				
				<div class="hippodrom">
					<h2 class="hippodrom-name"><fmt:message key='bets.hipp.newMarket' bundle='${ rb }'/></h2>
					<img src="${ pageContext.servletContext.contextPath }/pictures/hippodroms/newmarket.png"/>
					<div class="table-info">
					<form name="changeUserStatus" action="${ pageContext.servletContext.contextPath }/HotRacesServlet"  method="GET">
						<br>
						
						<table id="data-table" class="data-table">
							<tbody id="body-table"  class="body-table">
							<c:forEach var="betRace" items="${ NewMarketList }" varStatus="status">
								<tr name="newmarket">
									<td><locDate:localeDate locale="${ locale }" date="${ betRace.getDate() }"/></td>
									<td><c:out value="${ betRace.getStatus() }" /></td>
								</tr>
							</c:forEach>
							</tbody>
						</table>
						<input type="hidden" name="selected-row-value" id="selected-row-newmarket"/>
						<input type="hidden" name="hippodrom" value="Нью-Маркет"/>
						<input type="hidden" name="command" value="toMakeBet"/>
						<input type="submit" class="button" value="<fmt:message key='bets.button.makeBet' bundle='${ rb }'/>">
					</form>
					</div>
				</div>
				
				</div>
			</div>
			<div class="previous-races">
			
				<form name="changeUserStatus" action="${ pageContext.servletContext.contextPath }/HotRacesServlet" method="GET"">
					<br>
					<table id="prev-race-table" class="prev-race-table">
							
						<caption><fmt:message key='bets.prevRaceTable.title' bundle='${ rb }'/></caption>
						<thead>
							<tr>
								<th width="330px"><fmt:message key='bets.prevRaceTable.name' bundle='${ rb }'/></th>
								<th width="300px"><fmt:message key='bets.prevRaceTable.date' bundle='${ rb }'/></th>
								<th width="200px"><fmt:message key='bets.prevRaceTable.status' bundle='${ rb }'/></th>
							</tr>
						</thead>
						<tbody id="body-table">
							<c:if test="${ empty prevRaceList }">
								<p class="no-prev-race"><fmt:message key='bets.prevRaceTable.noBefore' bundle='${ rb }'/></p>
							</c:if>
							<c:forEach var="prevRace" items="${ prevRaceList }" varStatus="status">
								<tr>
									<td>${ prevRace.getName() }</td>
									<td><locDate:localeDate locale="${ locale }" date="${ prevRace.getDate() }"/></td>
									<td>${ prevRace.getStatus() }</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<input type="submit" class="race-descr-button" value="<fmt:message key='bets.prevRaceTable.detail' bundle='${ rb }'/>">
					<input type="hidden" id="userCommand" name="command" value="toPrevRaceDescription">
					<input type="hidden" name="selected-prev-race-row" id="selected-prev-race-row"/>
				</form>
			
			</div>
		</div>
<%@ include file="../pageParts/footer/footer.jsp"%>
<script src="${ pageContext.servletContext.contextPath }/js/betspagination.js"></script>
<script src="${ pageContext.servletContext.contextPath }/js/select_prev_race_row.js"></script>
<script src="${ pageContext.servletContext.contextPath }/js/select_row.js"></script>
</body>
</html>