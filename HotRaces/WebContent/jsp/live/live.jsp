<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${ locale }" scope="session" /> 
<fmt:setBundle basename="prop.locale" var="rb" />
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><fmt:message key="live.title" bundle="${ rb }"/></title>
	<link href="${ pageContext.servletContext.contextPath }/pictures/TitleLogo.png" rel="shortcut icon"/>
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/header_css.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/drop_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/left_menu.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/live.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/css/footer.css">
</head>
<body>
	<div class="main-page">
		<%@ include file="../pageParts/header/header.jsp"%>
	</div>
	<div class="main-body">
		<%@ include file="../pageParts/left-menu/leftmenu.jsp"%>
		<div class="live-main">
	
		<h2 class="hippodrom-description"><fmt:message key="live.meydan" bundle="${ rb }"/></h2>
		<iframe class="video" src="https://www.youtube.com/embed/dcduwLtvIWE" frameborder="0" allowfullscreen></iframe>
		<h2 class="hippodrom-description"><fmt:message key="live.southwell" bundle="${ rb }"/></h2>
		<iframe class="video" src="https://www.youtube.com/embed/1UbLqwdccdM" frameborder="0" allowfullscreen></iframe>
		<h2 class="hippodrom-description"><fmt:message key="live.lonshan" bundle="${ rb }"/></h2>
		<iframe class="video" src="https://www.youtube.com/embed/c76HoF1owdA" frameborder="0" allowfullscreen></iframe>
		<h2 class="hippodrom-description"><fmt:message key="live.newMarket" bundle="${ rb }"/></h2>
		<iframe class="video" src="https://www.youtube.com/embed/17g3AZyrkkE" frameborder="0" allowfullscreen></iframe>

	</div>
	</div>
	
<%@ include file="../pageParts/footer/footer.jsp"%>
</body>
</html>