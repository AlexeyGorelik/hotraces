package com.gorelik.hotraces.listener;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.PropertyConfigurator;

import com.gorelik.hotraces.connection.ConnectionPool;

/**
 * The listener interface for receiving context events. The class that is
 * interested in processing a context event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addContextListener<code> method. When the context event
 * occurs, that object's appropriate method is invoked.
 *
 * @see ContextEvent
 */
@WebListener("application context listener")
public class ContextListener implements ServletContextListener {

	/**
	 * Initialize log4j when the application is being started.
	 *
	 * @param event
	 *            the event
	 */
	@Override
	public void contextInitialized(ServletContextEvent event) {
		ServletContext context = event.getServletContext();
		String log4jConfigFile = context.getInitParameter("log4j-config-location");
		String fullPath = context.getRealPath("") + File.separator + log4jConfigFile;
		PropertyConfigurator.configure(fullPath);

		ConnectionPool.getInstance();
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		ConnectionPool.realClosePool();
	}
}