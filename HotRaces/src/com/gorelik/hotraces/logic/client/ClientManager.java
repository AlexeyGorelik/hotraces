package com.gorelik.hotraces.logic.client;

import java.util.List;

import com.gorelik.hotraces.dao.RaceDAO;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.race.PrevRaceData;

/**
 * The Class RaceManager. Class that executes clients actions.
 */
public class ClientManager {
	/**
	 * Find previous race list.
	 *
	 * @return the list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<PrevRaceData> findPrevRaceList() throws ManagerException {
		try (RaceDAO raceDao = new RaceDAO()) {
			return raceDao.findPrevRaceList();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}