package com.gorelik.hotraces.logic.login;

import com.gorelik.hotraces.dao.UserDAO;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class LoginManager. Class that executes some action with login.
 */
public class LoginManager {
	/**
	 * Login.
	 *
	 * @param loginData
	 *            the login data
	 * @return the user
	 * @throws ManagerException
	 *             the manager exception
	 */
	public User login(LoginData loginData) throws ManagerException {
		if (loginData.getValidation() == null) {
			throw new ManagerException(HotRacesErrorCode.AUTH_VALID_CLASS_IS_NULL);
		} else if (!loginData.getValidation()) {
			throw new ManagerException(loginData.getErrorMessage());
		}

		try (UserDAO userDAO = new UserDAO()) {

			User user = userDAO.login(loginData);
			if (user != null) {
				return user;
			} else {
				throw new ManagerException(HotRacesErrorCode.AUTH_WRONG_LOGIN_OR_PASS);
			}
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}
