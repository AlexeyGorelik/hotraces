package com.gorelik.hotraces.logic.login;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class LoginData. Class that contains login data.
 */
public class LoginData {

	/** The login key value for data map. */
	private final String LOGIN = "login";

	/** The password key value for data map. */
	private final String PASSWORD = "password";

	/** The validation status. */
	private Boolean validation;

	/** The error message. */
	private String errorMessage = "";

	/** The data map. */
	protected Map<String, String> dataMap = new HashMap<String, String>();

	/**
	 * Instantiates a new login data.
	 *
	 * @param login
	 *            the login
	 * @param password
	 *            the password
	 */
	public LoginData(String login, String password) {
		dataMap.put(LOGIN, login);
		dataMap.put(PASSWORD, password);
	}

	/**
	 * Gets the login.
	 *
	 * @return the login
	 */
	public String getLogin() {
		return dataMap.get(LOGIN);
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return dataMap.get(PASSWORD);
	}

	/**
	 * Gets the validation.
	 *
	 * @return the validation
	 */
	public Boolean getValidation() {
		return this.validation;
	}

	/**
	 * Sets the validation.
	 *
	 * @param valid
	 *            the new validation
	 */
	public void setValidation(boolean valid) {
		validation = (validation != null) ? valid : new Boolean(valid);
	}

	/**
	 * Sets the error message.
	 *
	 * @param msg
	 *            the new error message
	 */
	public void setErrorMessage(String msg) {
		this.errorMessage = msg;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return this.errorMessage;
	}
}
