package com.gorelik.hotraces.logic.bookmaker;

import java.math.BigDecimal;

/**
 * The Class BookmakerSetCoeffData. Class that contains information about set
 * coefficient.
 */
public class BookmakerSetCoeffData {

	/** The horse id. */
	private int horseId;

	/** The horse name. */
	private String horseName;

	/** The coefficient of bet. */
	private BigDecimal coeff;

	/**
	 * Instantiates a new bookmaker set coefficient data.
	 *
	 * @param horseId
	 *            the horse id
	 * @param horseName
	 *            the horse name
	 * @param coeff
	 *            the coefficient
	 */
	public BookmakerSetCoeffData(int horseId, String horseName, BigDecimal coeff) {
		this.horseId = horseId;
		this.horseName = horseName;
		this.coeff = coeff;
	}

	/**
	 * Gets the horse id.
	 *
	 * @return the horse id
	 */
	public int getHorseId() {
		return this.horseId;
	}

	/**
	 * Gets the horse name.
	 *
	 * @return the horse name
	 */
	public String getHorseName() {
		return this.horseName;
	}

	/**
	 * Gets the coeff.
	 *
	 * @return the coeff
	 */
	public BigDecimal getCoeff() {
		return this.coeff;
	}
}
