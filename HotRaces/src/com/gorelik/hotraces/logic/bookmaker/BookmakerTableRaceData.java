package com.gorelik.hotraces.logic.bookmaker;

/**
 * The Class BookmakerTableRaceData. Class that contains information about race
 * for bookmaker.
 */
public class BookmakerTableRaceData {

	/** The name. */
	private String name;

	/** The date. */
	private String date;

	/** The race city. */
	private String city;

	/** The race country. */
	private String country;

	/** The race status. */
	private String status;

	/**
	 * Instantiates a new bookmaker table race data.
	 *
	 * @param name
	 *            the name
	 * @param date
	 *            the date
	 * @param status
	 *            the status
	 * @param city
	 *            the city
	 * @param country
	 *            the country
	 */
	public BookmakerTableRaceData(String name, String date, String status, String city, String country) {
		this.name = name;
		this.date = date;
		this.city = city;
		this.country = country;
		this.status = status;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public String getDate() {
		return this.date;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return this.city;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return this.country;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return this.status;
	}
}
