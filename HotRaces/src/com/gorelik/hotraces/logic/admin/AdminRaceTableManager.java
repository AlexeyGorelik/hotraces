package com.gorelik.hotraces.logic.admin;

import java.util.List;

import com.gorelik.hotraces.dao.RaceDAO;
import com.gorelik.hotraces.entity.Race;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;

/**
 * The Class AdminRaceTableManager. Class that works with race table for
 * administrator.
 */
public class AdminRaceTableManager {
	/**
	 * Creates the race list.
	 *
	 * @return the list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<Race> createAdminRaceTable() throws ManagerException {
		try (RaceDAO raceDao = new RaceDAO()) {
			return raceDao.createAdminRaceTable();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}
