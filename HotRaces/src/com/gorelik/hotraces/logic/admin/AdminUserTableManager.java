package com.gorelik.hotraces.logic.admin;

import java.util.List;

import com.gorelik.hotraces.dao.UserDAO;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;

/**
 * The Class AdminUserTableManager. Class that works with user table for
 * administrator.
 */
public class AdminUserTableManager {
	/**
	 * Creates the user list.
	 *
	 * @return the list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<User> createUserList() throws ManagerException {
		try (UserDAO userDao = new UserDAO()) {
			return userDao.createAdminUserTable();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}

	}
}
