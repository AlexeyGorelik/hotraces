package com.gorelik.hotraces.logic.message;

import java.util.List;

import com.gorelik.hotraces.dao.MessageDAO;
import com.gorelik.hotraces.dao.UserDAO;
import com.gorelik.hotraces.entity.Message;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class MessageManager. Class that executes actions with messages.
 */
public class MessageManager {

	/**
	 * Find messages list.
	 *
	 * @param userId
	 *            the user id
	 * @return the list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<Message> findMessagesList(int userId) throws ManagerException {
		List<Message> list = null;
		try (MessageDAO messageDao = new MessageDAO()) {
			list = messageDao.findMessageList(userId);
			return list;
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Find conversation.
	 *
	 * @param userId
	 *            the user id
	 * @param userTo
	 *            the user to
	 * @return the list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<Message> findConversation(int userId, String userTo) throws ManagerException {
		if (userTo.isEmpty()) {
			throw new ManagerException(HotRacesErrorCode.EMPTY_USER);
		}
		List<Message> list = null;
		try (MessageDAO messageDao = new MessageDAO()) {
			list = messageDao.findConversation(userId, userTo);
			return list;
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Send message.
	 *
	 * @param userIdFrom
	 *            the user id from
	 * @param userToName
	 *            the user to name
	 * @param text
	 *            the text
	 * @throws ManagerException
	 *             the manager exception
	 */
	public void sendMessage(int userIdFrom, String userToName, String text) throws ManagerException {
		if (text.trim().isEmpty()) {
			throw new ManagerException(HotRacesErrorCode.MESSAGE_IS_EMPTY);
		}
		try (MessageDAO messageDao = new MessageDAO()) {
			messageDao.sendMessage(userIdFrom, userToName, text);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Send new message.
	 *
	 * @param userIdFrom
	 *            the user id from
	 * @param userToName
	 *            the user to name
	 * @param text
	 *            the text
	 * @throws ManagerException
	 *             the manager exception
	 */
	public void sendNewMessage(int userIdFrom, String userToName, String text) throws ManagerException {
		if (text.trim().isEmpty()) {
			throw new ManagerException(HotRacesErrorCode.MESSAGE_IS_EMPTY);
		}
		try (MessageDAO messageDao = new MessageDAO()) {
			messageDao.sendNewMessage(userIdFrom, userToName, text);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Find user list.
	 *
	 * @return the list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<String> findUserList() throws ManagerException {
		try (UserDAO userDao = new UserDAO()) {
			return userDao.findUserList();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}
