package com.gorelik.hotraces.logic.registration;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class RegistrationData. Class contains information about registration
 * data.
 */
public class RegistrationData {

	/** The user login. */
	private final String LOGIN = "login";

	/** The user password. */
	private final String PASSWORD = "password";

	/** The repeat password. */
	private final String REPEAT_PASSWORD = "repeatPassword";

	/** The user email. */
	private final String EMAIL = "email";

	/** The user first name. */
	private final String FIRST_NAME = "firstName";

	/** The user second name. */
	private final String SECOND_NAME = "secondName";

	/** The validation result. */
	private Boolean validation;

	/** The error message. */
	private String errorMessage = "";

	/** The data map. */
	protected Map<String, String> dataMap = new HashMap<String, String>();

	/**
	 * Instantiates a new registration data.
	 *
	 * @param login
	 *            the login
	 * @param password
	 *            the password
	 * @param repeatPassword
	 *            the repeat password
	 * @param email
	 *            the email
	 * @param firstName
	 *            the first name
	 * @param secondName
	 *            the second name
	 */
	public RegistrationData(String login, String password, String repeatPassword, String email, String firstName,
			String secondName) {
		dataMap.put(LOGIN, login);
		dataMap.put(PASSWORD, password);
		dataMap.put(REPEAT_PASSWORD, repeatPassword);
		dataMap.put(EMAIL, email);
		dataMap.put(FIRST_NAME, firstName);
		dataMap.put(SECOND_NAME, secondName);
	}

	/**
	 * Gets the login.
	 *
	 * @return the login
	 */
	public String getLogin() {
		return dataMap.get(LOGIN);
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return dataMap.get(PASSWORD);
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return dataMap.get(EMAIL);
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return dataMap.get(FIRST_NAME);
	}

	/**
	 * Gets the second name.
	 *
	 * @return the second name
	 */
	public String getSecondName() {
		return dataMap.get(SECOND_NAME);
	}

	/**
	 * Gets the validation.
	 *
	 * @return the validation
	 */
	public Boolean getValidation() {
		return this.validation;
	}

	/**
	 * Sets the validation.
	 *
	 * @param valid
	 *            the new validation
	 */
	public void setValidation(boolean valid) {
		validation = (validation != null) ? valid : new Boolean(valid);
	}

	/**
	 * Sets the error message.
	 *
	 * @param msg
	 *            the new error message
	 */
	public void setErrorMessage(String msg) {
		this.errorMessage = msg;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return this.errorMessage;
	}

	/**
	 * Gets the repeat password.
	 *
	 * @return the repeat password
	 */
	public String getRepeatPassword() {
		return dataMap.get(REPEAT_PASSWORD);
	}
}
