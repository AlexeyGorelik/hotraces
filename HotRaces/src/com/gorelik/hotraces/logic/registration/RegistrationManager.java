package com.gorelik.hotraces.logic.registration;

import com.gorelik.hotraces.dao.UserDAO;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class RegistrationManager. Class that executes some actions with
 * registration.
 */
public class RegistrationManager {

	/**
	 * Check registration.
	 *
	 * @param regData
	 *            the registration data
	 * @throws ManagerException
	 *             the manager exception
	 */
	public void registrateUser(RegistrationData regData) throws ManagerException {
		if (regData.getValidation() == null) {
			throw new ManagerException(HotRacesErrorCode.REGISTR_VALID_CLASS_IS_NULL);
		} else if (!regData.getValidation()) {
			throw new ManagerException(regData.getErrorMessage());
		}

		try (UserDAO userDAO = new UserDAO()) {
			userDAO.registrateUser(regData);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}
