package com.gorelik.hotraces.logic.payment;

import java.math.BigDecimal;

/**
 * The Class PaymentData. Class that contains information about user payment.
 */
public class PaymentData {

	/** The card code. */
	private String cardCode[] = new String[4];

	/** The card cvv. */
	private String cvv;

	/** The card month. */
	private String month;

	/** The card year. */
	private String year;

	/** The sum of payment. */
	private String sum;

	/** The error message. */
	private String errorMessage = "";

	/** The validation result. */
	private Boolean validation;

	/**
	 * Instantiates a new payment data.
	 *
	 * @param cvv
	 *            the cvv
	 * @param sum
	 *            the sum
	 * @param month
	 *            the month
	 * @param year
	 *            the year
	 * @param cardCode
	 *            the card code
	 */
	public PaymentData(String cvv, String sum, String month, String year, String... cardCode) {
		this.cvv = cvv;
		this.cardCode = cardCode;
		this.month = month;
		this.year = year;
		this.sum = sum;
	}

	/**
	 * Gets the card code array.
	 *
	 * @return the card code array
	 */
	public String[] getCardCodeArray() {
		return this.cardCode;
	}

	/**
	 * Gets the card code string.
	 *
	 * @return the card code string
	 */
	public String getCardCodeString() {
		String temp = null;
		for (int i = 0; i < cardCode.length - 1; i++) {
			temp += this.cardCode[i];
			temp += '-';
		}
		temp += this.cardCode[cardCode.length - 1];
		return temp;
	}

	/**
	 * Gets the cvv.
	 *
	 * @return the cvv
	 */
	public String getCvv() {
		return this.cvv;
	}

	/**
	 * Gets the sum.
	 *
	 * @return the sum
	 */
	public BigDecimal getSum() {
		return BigDecimal.valueOf(Double.valueOf(this.sum));
	}

	/**
	 * Gets the string sum.
	 *
	 * @return the string sum
	 */
	public String getStringSum() {
		return this.sum;
	}

	/**
	 * Gets the month.
	 *
	 * @return the month
	 */
	public String getMonth() {
		return this.month;
	}

	/**
	 * Gets the year.
	 *
	 * @return the year
	 */
	public String getYear() {
		return this.year;
	}

	/**
	 * Sets the error message.
	 *
	 * @param msg
	 *            the new error message
	 */
	public void setErrorMessage(String msg) {
		this.errorMessage = msg;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return this.errorMessage;
	}

	/**
	 * Sets the validation.
	 *
	 * @param valid
	 *            the new validation
	 */
	public void setValidation(boolean valid) {
		this.validation = valid;
	}

	/**
	 * Gets the validation.
	 *
	 * @return the validation
	 */
	public Boolean getValidation() {
		return this.validation;
	}
}
