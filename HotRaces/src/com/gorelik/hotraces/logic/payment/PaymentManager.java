package com.gorelik.hotraces.logic.payment;

import com.gorelik.hotraces.dao.PaymentDAO;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class PaymentManager. Class that executes operations with payment.
 */
public class PaymentManager {

	/**
	 * Pay.
	 *
	 * @param payData
	 *            the pay data
	 * @param user
	 *            the user
	 * @return the user
	 * @throws ManagerException
	 *             the manager exception
	 */
	public User pay(PaymentData payData, User user) throws ManagerException {
		if (payData.getValidation() == null) {
			throw new ManagerException(HotRacesErrorCode.PAYMENT_VALID_CLASS_IS_NULL);

		} else if (!payData.getValidation()) {
			throw new ManagerException(payData.getErrorMessage());
		}

		try (PaymentDAO payDao = new PaymentDAO()) {
			return payDao.makePayment(payData, user);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

}
