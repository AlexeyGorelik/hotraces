package com.gorelik.hotraces.logic.profile;

import com.gorelik.hotraces.dao.UserDAO;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class ProfileChangeManager. Class executes some actions with profile.
 */
public class ProfileChangeManager {

	/**
	 * Check changing data.
	 *
	 * @param user
	 *            the user
	 * @param profChangeData
	 *            the prof change data
	 * @return the user
	 * @throws ManagerException
	 *             the manager exception
	 */
	public User changeUserData(User user, ProfileChangeData profChangeData) throws ManagerException {
		if (profChangeData.getValidation() == null) {
			throw new ManagerException(HotRacesErrorCode.PROFILE_CHANGE_VALID_CLASS_IS_NULL);
		} else if (!profChangeData.getValidation()) {
			throw new ManagerException(profChangeData.getErrorMessage());
		}

		try (UserDAO userDAO = new UserDAO()) {
			userDAO.changeData(user, profChangeData);
			return user;
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}
