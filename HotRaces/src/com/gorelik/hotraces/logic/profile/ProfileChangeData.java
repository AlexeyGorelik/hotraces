package com.gorelik.hotraces.logic.profile;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class ProfileChangeData. Class contains information about profile change
 * values.
 */
public class ProfileChangeData {

	/** The login. */
	private final String LOGIN = "login";

	/** The email. */
	private final String EMAIL = "email";

	/** The first name. */
	private final String FIRST_NAME = "firstName";

	/** The second name. */
	private final String SECOND_NAME = "secondName";

	/** The validation result. */
	private Boolean validation;

	/** The error message. */
	private String errorMessage = "";

	/** The data map. */
	protected Map<String, String> dataMap = new HashMap<String, String>();

	/**
	 * Instantiates a new profile change data.
	 *
	 * @param firstName
	 *            the first name
	 * @param secondName
	 *            the second name
	 * @param login
	 *            the login
	 * @param email
	 *            the email
	 */
	public ProfileChangeData(String firstName, String secondName, String login, String email) {
		dataMap.put(LOGIN, login);
		dataMap.put(EMAIL, email);
		dataMap.put(FIRST_NAME, firstName);
		dataMap.put(SECOND_NAME, secondName);
	}

	/**
	 * Gets the login.
	 *
	 * @return the login
	 */
	public String getLogin() {
		return dataMap.get(LOGIN);
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return dataMap.get(EMAIL);
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return dataMap.get(FIRST_NAME);
	}

	/**
	 * Gets the second name.
	 *
	 * @return the second name
	 */
	public String getSecondName() {
		return dataMap.get(SECOND_NAME);
	}

	/**
	 * Gets the validation.
	 *
	 * @return the validation
	 */
	public Boolean getValidation() {
		return this.validation;
	}

	/**
	 * Sets the validation.
	 *
	 * @param valid
	 *            the new validation
	 */
	public void setValidation(boolean valid) {
		validation = (validation != null) ? valid : new Boolean(valid);
	}

	/**
	 * Sets the error message.
	 *
	 * @param msg
	 *            the new error message
	 */
	public void setErrorMessage(String msg) {
		this.errorMessage = msg;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return this.errorMessage;
	}
}
