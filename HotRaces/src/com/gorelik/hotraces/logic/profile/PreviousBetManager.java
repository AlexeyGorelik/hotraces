package com.gorelik.hotraces.logic.profile;

import java.util.List;

import com.gorelik.hotraces.dao.BetDAO;
import com.gorelik.hotraces.entity.Bet;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;

/**
 * The Class PreviousBetManager. Class executes some actions with previous bets.
 */
public class PreviousBetManager {

	/**
	 * Gets the previous bets.
	 *
	 * @param user
	 *            the user
	 * @return the previous bets
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<Bet> getPreviousBets(User user) throws ManagerException {
		try (BetDAO betDao = new BetDAO()) {
			return betDao.findUserResult(user);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}
