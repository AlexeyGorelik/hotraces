package com.gorelik.hotraces.logic.bets;

import java.math.BigDecimal;

/**
 * The Class MakeBetData. Class that contains information about data.
 */
public class MakeBetData {
	/** The horse id. */
	private int horseId;

	/** The horse name. */
	private String horseName;

	/** The coefficient of bet. */
	private BigDecimal coeff;

	/** The sum of bet. */
	private BigDecimal sum;

	/** The bet type. */
	private int betType;

	/**
	 * Instantiates a new make bet data.
	 *
	 * @param horseId
	 *            the horse id
	 * @param horseName
	 *            the horse name
	 * @param coeff
	 *            the coeff
	 */
	public MakeBetData(int horseId, String horseName, BigDecimal coeff) {
		this.horseId = horseId;
		this.horseName = horseName;
		this.coeff = coeff;
	}

	/**
	 * Instantiates a new make bet data.
	 *
	 * @param horseId
	 *            the horse id
	 * @param horseName
	 *            the horse name
	 * @param coeff
	 *            the coeff
	 * @param sum
	 *            the sum
	 * @param betType
	 *            the bet type
	 */
	public MakeBetData(int horseId, String horseName, BigDecimal coeff, BigDecimal sum, int betType) {
		this.horseId = horseId;
		this.horseName = horseName;
		this.coeff = coeff;
		this.sum = sum;
		this.betType = betType;
	}

	/**
	 * Gets the horse id.
	 *
	 * @return the horse id
	 */
	public int getHorseId() {
		return this.horseId;
	}

	/**
	 * Gets the horse name.
	 *
	 * @return the horse name
	 */
	public String getHorseName() {
		return this.horseName;
	}

	/**
	 * Gets the coeff.
	 *
	 * @return the coeff
	 */
	public BigDecimal getCoeff() {
		return this.coeff;
	}

	/**
	 * Gets the string coeff.
	 *
	 * @return the string coeff
	 */
	public String getStringCoeff() {
		return this.coeff.toString();
	}

	/**
	 * Gets the sum.
	 *
	 * @return the sum
	 */
	public BigDecimal getSum() {
		return sum;
	}

	/**
	 * Sets the sum.
	 *
	 * @param sum
	 *            the new sum
	 */
	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	/**
	 * Gets the bet type.
	 *
	 * @return the bet type
	 */
	public int getBetType() {
		return betType;
	}

	/**
	 * Sets the bet type.
	 *
	 * @param betType
	 *            the new bet type
	 */
	public void setBetType(int betType) {
		this.betType = betType;
	}

}
