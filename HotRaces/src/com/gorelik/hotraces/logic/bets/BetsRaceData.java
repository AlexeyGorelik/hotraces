package com.gorelik.hotraces.logic.bets;

/**
 * The Class BetsRaceData. Class that contains data about race and bet.
 */
public class BetsRaceData {

	/** The race date. */
	private String raceDate;

	/** The race status. */
	private String raceStatus;

	/**
	 * Instantiates a new bets race data.
	 *
	 * @param date
	 *            the date
	 * @param status
	 *            the status
	 */
	public BetsRaceData(String date, String status) {
		this.raceDate = date;
		this.raceStatus = status;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public String getDate() {
		return this.raceDate;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return this.raceStatus;
	}
}
