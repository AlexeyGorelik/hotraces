package com.gorelik.hotraces.logic.bets;

import java.math.BigDecimal;
import java.time.format.DateTimeParseException;
import java.util.List;

import com.gorelik.hotraces.dao.BetDAO;
import com.gorelik.hotraces.dao.RaceDAO;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.race.RaceData;
import com.gorelik.hotraces.logic.race.RaceEnum;
import com.gorelik.hotraces.logic.user.UserStatusEnum;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.PageManager;

/**
 * The Class BetsManager. Class that works with best.
 */
public class BetsManager {

	/**
	 * Gets the race list.
	 *
	 * @param race
	 *            the race
	 * @return the horse list with coefficients
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<BetsRaceData> getRacesToBets(RaceEnum race) throws ManagerException {
		try (RaceDAO raceDao = new RaceDAO()) {
			return raceDao.findRaceToBets(race);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Gets the horse in current bet list.
	 *
	 * @param raceName
	 *            the race name
	 * @param raceDate
	 *            the race date
	 * @param locale
	 *            the locale
	 * @return the horse in current bet list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<MakeBetData> findMakeBetData(String raceName, String raceDate, String locale) throws ManagerException {
		if (raceName.isEmpty()) {
			throw new ManagerException(HotRacesErrorCode.MAKE_BET_EMPTY_RACE);
		}

		RaceData raceData = new RaceData(raceName, raceDate);

		try (BetDAO betDao = new BetDAO()) {
			return betDao.findMakeBetList(raceData);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Make bet.
	 *
	 * @param raceData
	 *            the race data
	 * @param locale
	 *            the locale
	 * @param list
	 *            the list with bets data
	 * @param user
	 *            the current user
	 * @return the updated user
	 * @throws ManagerException
	 *             the manager exception
	 */
	public User makeBet(RaceData raceData, String locale, List<MakeBetData> list, User user) throws ManagerException {
		if (user.getStatus().equals(UserStatusEnum.BAN)) {
			throw new ManagerException(HotRacesErrorCode.BAN_STATUS_ACTION);
		}

		BigDecimal currentSum = new BigDecimal(0);
		for (MakeBetData data : list) {
			currentSum = currentSum.add(data.getSum());
		}
		if (user.getMoney().compareTo(currentSum) < 0) {
			throw new ManagerException(HotRacesErrorCode.MAKE_BET_NOT_ENOUGHT_MONEY);
		}

		try {
			raceData.setDate(PageManager.formatDate(raceData.getDate(), locale));
		} catch (DateTimeParseException e) {
			throw new ManagerException(HotRacesErrorCode.WRONG_DATE);
		}

		try (BetDAO betDao = new BetDAO()) {
			betDao.makeBet(raceData, list, user);
			user.setMoney(user.getMoney().subtract(currentSum));
			return user;
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}