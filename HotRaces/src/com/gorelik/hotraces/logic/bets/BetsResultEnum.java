package com.gorelik.hotraces.logic.bets;

/**
 * The Enum BetsResultEnum.
 */
public enum BetsResultEnum {

	/** The win. */
	WIN("������"),

	/** The lose. */
	LOSE("��������"),

	/** The decline. */
	DECLINE("������");

	/**
	 * Instantiates a new bets result enum.
	 *
	 * @param type
	 *            the type
	 */
	BetsResultEnum(String type) {
		this.type = type;
	}

	/** The string value of type. */
	private String type;

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return this.type;
	}
}
