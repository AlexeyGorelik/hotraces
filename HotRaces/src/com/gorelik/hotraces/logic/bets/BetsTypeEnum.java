package com.gorelik.hotraces.logic.bets;

/**
 * The Enum BetsTypeEnum. Enumeration that contains all bet types.
 */
public enum BetsTypeEnum {

	/** The win. */
	WIN("����������"),

	/** The first three. */
	FIRST_THREE("������ ������");

	/**
	 * Instantiates a new bets type enum.
	 *
	 * @param type
	 *            the type
	 */
	BetsTypeEnum(String type) {
		this.type = type;
	}

	/** The string value of bet type. */
	private String type;

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return this.type;
	}
}
