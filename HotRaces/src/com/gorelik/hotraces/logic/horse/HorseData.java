package com.gorelik.hotraces.logic.horse;

import java.math.BigDecimal;

/**
 * The Class HorseData. Class that contains information about horse and bet.
 */
public class HorseData {
	private static final String EMPTY_PLACE = "-";

	private static final int ZERO_PLACE = 0;
	/** The horse name. */
	private String name;

	/** The horse identifier */
	private int horseId;

	/** The coefficient */
	private BigDecimal coeff;

	/** The place */
	private int place;

	/**
	 * Instantiates a new horse data.
	 *
	 * @param name
	 *            the name
	 * @param horseId
	 *            the horse id
	 * @param coeff
	 *            the coeff
	 * @param place
	 *            the place
	 */
	public HorseData(String name, int horseId, BigDecimal coeff, int place) {
		this.name = name;
		this.horseId = horseId;
		this.coeff = coeff;
		this.place = place;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the horse id.
	 *
	 * @return the horse id
	 */
	public int getHorseId() {
		return this.horseId;
	}

	/**
	 * Gets the coeff.
	 *
	 * @return the coeff
	 */
	public BigDecimal getCoeff() {
		return this.coeff;
	}

	/**
	 * Gets the string coeff.
	 *
	 * @return the string coeff
	 */
	public String getStringCoeff() {

		if (coeff != null && !coeff.toString().isEmpty()) {
			return this.coeff.toString();
		} else {
			return "-";
		}
	}

	/**
	 * Gets the place.
	 *
	 * @return the place
	 */
	public int getPlace() {
		return this.place;
	}

	/**
	 * Gets the string value of place. If place is "0", then returns "-".
	 *
	 * @return the place
	 */
	public String getStringPlace() {
		return (place == ZERO_PLACE) ? EMPTY_PLACE : String.valueOf(place);
	}
}
