package com.gorelik.hotraces.logic.horse;

import java.time.format.DateTimeParseException;
import java.util.List;

import com.gorelik.hotraces.dao.HorseDAO;
import com.gorelik.hotraces.entity.Horse;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.race.RaceData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.PageManager;

/**
 * The Class HorseManager. Class that executes some actions woth horse and bets.
 */
public class HorseManager {
	/**
	 * Find horses data in previous race.
	 *
	 * @param raceData
	 *            the race data
	 * @param locale
	 *            the locale
	 * @return the list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<HorseData> findPrevRaceData(RaceData raceData, String locale) throws ManagerException {
		if (raceData.getName().isEmpty()) {
			throw new ManagerException(HotRacesErrorCode.ADMIN_RACE_NAME_IS_EMPTY);
		}
		if (raceData.getDate().isEmpty()) {
			throw new ManagerException(HotRacesErrorCode.ADMIN_RACE_DATE_IS_EMPTY);
		}
		try {
			raceData.setDate(PageManager.formatDate(raceData.getDate(), locale));
		} catch (DateTimeParseException e) {
			throw new ManagerException(HotRacesErrorCode.WRONG_DATE);
		}

		try (HorseDAO horseDao = new HorseDAO()) {
			return horseDao.findHorsesDataInPrevRace(raceData);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Gets the horse list.
	 *
	 * @return the horse list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<Horse> findAllHorsesList() throws ManagerException {
		try (HorseDAO horseDao = new HorseDAO()) {
			return horseDao.findAllHorsesList();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}
