package com.gorelik.hotraces.logic.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import javax.servlet.http.Part;

import com.gorelik.hotraces.exception.UploadAvatarException;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class FileManager. Class that executes information with avatar.
 */
public class FileManager {

	/** The Constant JPG_EXPANSION. Image expansion. */
	private static final String JPG_EXPANSION = ".jpg";

	/** The Constant PNG_EXPANSION. Image expansion. */
	private static final String PNG_EXPANSION = ".png";

	/**
	 * Creates the file.
	 *
	 * @param part
	 *            the part
	 * @param realPath
	 *            the real path for picture
	 * @return the path to picture
	 * @throws UploadAvatarException
	 *             the upload avatar exception
	 */
	public String createFile(Part part, String realPath) throws UploadAvatarException {
		Part filePart = part;
		String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
		String expansion = fileName.subSequence(fileName.length() - 4, fileName.length()).toString();

		if (!expansion.equals(JPG_EXPANSION) && !expansion.equals(PNG_EXPANSION)) {
			throw new UploadAvatarException(HotRacesErrorCode.WRONG_EXPANSION);
		}

		String pathToProject = realPath;
		File file = new File(pathToProject + "\\avatars", fileName);

		while (file.exists()) {
			fileName = UUID.randomUUID().toString() + expansion;
			file = new File(pathToProject + "\\avatars", fileName);
		}
		try (InputStream input = filePart.getInputStream()) {
			Files.copy(input, file.toPath());
			return "/avatars/" + fileName;
		} catch (IOException e) {
			throw new UploadAvatarException(HotRacesErrorCode.UPLOAD_AVATAR_ERROR);
		}
	}
}
