package com.gorelik.hotraces.logic.upload;

import javax.servlet.http.Part;

import com.gorelik.hotraces.dao.UserDAO;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.exception.UploadAvatarException;
import com.gorelik.hotraces.logic.file.FileManager;

/**
 * The Class UploadManager. Class that executes some actions with user avatars.
 */
public class UploadManager {

	/** The Constant DEFAULT_PATH. Path to default avatar. */
	private static final String DEFAULT_PATH = "/avatars/default.png";

	/**
	 * Upload avatar.
	 *
	 * @param part
	 *            the part
	 * @param user
	 *            the user
	 * @param realPath
	 *            the real path
	 * @return the user
	 * @throws ManagerException
	 *             the manager exception
	 */
	public User uploadAvatar(Part part, User user, String realPath) throws ManagerException {
		FileManager fileManager = new FileManager();
		String pathToAvatar = DEFAULT_PATH;
		try {
			pathToAvatar = fileManager.createFile(part, realPath);
			user.setAvatarPath(pathToAvatar);
		} catch (UploadAvatarException e) {
			throw new ManagerException(e.getMessage());
		}

		try (UserDAO userDao = new UserDAO()) {
			return userDao.changeAvatar(user, pathToAvatar);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}
