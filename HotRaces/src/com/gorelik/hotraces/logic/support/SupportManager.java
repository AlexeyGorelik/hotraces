package com.gorelik.hotraces.logic.support;

import java.util.List;

import com.gorelik.hotraces.dao.SupportDAO;
import com.gorelik.hotraces.entity.SupportMessage;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;

/**
 * The Class SupportManager. Class that executes some actions with support
 * messages.
 */
public class SupportManager {

	/**
	 * Send question.
	 *
	 * @param suppMsg
	 *            the support message
	 * @throws ManagerException
	 *             the manager exception
	 */
	public void sendQuestion(SupportMessage suppMsg) throws ManagerException {
		try (SupportDAO messageDao = new SupportDAO()) {
			messageDao.sendQuestion(suppMsg);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Find support message list.
	 *
	 * @return the list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<SupportMessage> findSuppMessageList() throws ManagerException {
		try (SupportDAO messageDao = new SupportDAO()) {
			return messageDao.findSuppMessageList();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}
