package com.gorelik.hotraces.logic.user;

/**
 * The Enum UserStatusEnum. Enum that contains all types of user status.
 */
public enum UserStatusEnum {

	/** The active. */
	ACTIVE("�������"),

	/** The ban. */
	BAN("�������");

	/** The value. */
	private String value;

	/**
	 * Instantiates a new user status enum.
	 *
	 * @param value
	 *            the value
	 */
	private UserStatusEnum(String value) {
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return this.value;
	}
}
