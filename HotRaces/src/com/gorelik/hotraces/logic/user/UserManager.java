package com.gorelik.hotraces.logic.user;

import com.gorelik.hotraces.dao.UserDAO;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class UserManager. Class that executes some actions with user data.
 */
public class UserManager {
	/** The Constant BAN_COMMAND. Ban command value. */
	private static final String BAN_COMMAND = "banUser";

	/** The Constant UNBAN_COMMAND. Unban command value. */
	private static final String UNBAN_COMMAND = "unbanUser";

	/** The Constant BAN_VALUE. Ban value in DB. */
	private static final int BAN_VALUE = 2;

	/** The Constant UNBAN_VALUE. Unban value in DB. */
	private static final int UNBAN_VALUE = 1;

	/**
	 * Change user status.
	 *
	 * @param userId
	 *            the user id
	 * @param command
	 *            the command (ban or unban)
	 * @throws ManagerException
	 *             the manager exception
	 */
	public void changeUserStatus(String userId, String command) throws ManagerException {
		Integer commandType = null;

		if (BAN_COMMAND.equals(command)) {
			commandType = BAN_VALUE;
		} else if (UNBAN_COMMAND.equals(command)) {
			commandType = UNBAN_VALUE;
		}

		if (commandType == null) {
			throw new ManagerException(HotRacesErrorCode.ADMIN_CHANGE_STATUS_CLASS_IS_NULL);
		}
		if (userId.isEmpty()) {
			throw new ManagerException(HotRacesErrorCode.ADMIN_CHANGE_STATUS_USER_IS_EMPTY);
		}

		try (UserDAO userDao = new UserDAO()) {
			userDao.changeUserStatus(userId, commandType);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Change password.
	 *
	 * @param email
	 *            the email
	 * @return the string value of new password
	 * @throws ManagerException
	 *             the manager exception
	 */
	public String changePassword(String email) throws ManagerException {
		try (UserDAO userDao = new UserDAO()) {
			return userDao.changePassword(email);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}
