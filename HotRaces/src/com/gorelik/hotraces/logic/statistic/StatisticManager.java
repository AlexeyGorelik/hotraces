package com.gorelik.hotraces.logic.statistic;

import java.math.BigDecimal;

import com.gorelik.hotraces.dao.BetDAO;
import com.gorelik.hotraces.dao.RaceDAO;
import com.gorelik.hotraces.dao.UserDAO;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;

/**
 * The Class StatisticManager. Class work with application statistic data.
 */
public class StatisticManager {
	/**
	 * Find today race count.
	 *
	 * @return the int number of how many races has carried out today
	 * @throws ManagerException
	 *             the manager exception
	 */
	public int findTodayRaceCount() throws ManagerException {
		try (RaceDAO raceDao = new RaceDAO()) {
			return raceDao.findTodayRaceCount();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Find month race count.
	 *
	 * @return the int number of how many races has carried out this month
	 * @throws ManagerException
	 *             the manager exception
	 */
	public int findMonthRaceCount() throws ManagerException {
		try (RaceDAO raceDao = new RaceDAO()) {
			return raceDao.findMonthRaceCount();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Find today bets count.
	 *
	 * @return the int
	 * @throws ManagerException
	 *             the manager exception
	 */
	public int findTodayBetsCount() throws ManagerException {
		try (BetDAO betDao = new BetDAO()) {
			return betDao.findTodayBetsCount();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Find month bets count.
	 *
	 * @return the int
	 * @throws ManagerException
	 *             the manager exception
	 */
	public int findMonthBetsCount() throws ManagerException {
		try (BetDAO betDao = new BetDAO()) {
			return betDao.findMonthBetsCount();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Find today income.
	 *
	 * @return the big decimal
	 * @throws ManagerException
	 *             the manager exception
	 */
	public BigDecimal findTodayIncome() throws ManagerException {
		try (BetDAO betDao = new BetDAO()) {
			return betDao.findTodayIncome();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Find month income.
	 *
	 * @return the big decimal
	 * @throws ManagerException
	 *             the manager exception
	 */
	public BigDecimal findMonthIncome() throws ManagerException {
		try (BetDAO betDao = new BetDAO()) {
			return betDao.findMonthIncome();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Find user this month count.
	 *
	 * @return the number of users which have registered this month
	 * @throws ManagerException
	 *             the manager exception
	 */
	public int findUserThisMonthCount() throws ManagerException {
		try (UserDAO userDao = new UserDAO()) {
			return userDao.findUserThisMonthCount();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}
