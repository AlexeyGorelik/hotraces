package com.gorelik.hotraces.logic.race;

import java.time.format.DateTimeParseException;
import java.util.List;

import com.gorelik.hotraces.dao.RaceDAO;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.bookmaker.BookmakerSetCoeffData;
import com.gorelik.hotraces.logic.bookmaker.BookmakerTableRaceData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.PageManager;

/**
 * The Class AdminRaceManager. Class lets bookmaker to work with race(set
 * coefficient, edit coefficients, etc.).
 */
public class BookmakerRaceManager {
	/**
	 * Gets the race to set coefficient.
	 *
	 * @param raceData
	 *            the race data
	 * @return the race to set coefficient
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<BookmakerSetCoeffData> findRacesToSetCoeff(RaceData raceData) throws ManagerException {
		try (RaceDAO raceDao = new RaceDAO()) {
			return raceDao.findHorsesToSetCoeff(raceData);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Sets the coefficient in race.
	 *
	 * @param raceData
	 *            the race data
	 * @param locale
	 *            the locale
	 * @param dataList
	 *            the data list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public void setCoeffInRace(RaceData raceData, String locale, List<BookmakerSetCoeffData> dataList)
			throws ManagerException {
		try {
			raceData.setDate(PageManager.formatDate(raceData.getDate(), locale));
		} catch (DateTimeParseException e) {
			throw new ManagerException(HotRacesErrorCode.WRONG_DATE);
		}

		try (RaceDAO raceDao = new RaceDAO()) {
			raceDao.setCoeffInRace(raceData, dataList);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Creates the bookmaker table.
	 *
	 * @return the list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<BookmakerTableRaceData> createBookmakerRaceTable() throws ManagerException {
		try (RaceDAO raceDao = new RaceDAO()) {
			return raceDao.findBookmakerTable();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Delete race as bookmaker.
	 *
	 * @param raceData
	 *            the race data
	 * @param locale
	 *            the locale
	 * @throws ManagerException
	 *             the manager exception
	 */
	public void deleteRace(RaceData raceData, String locale) throws ManagerException {
		if (raceData == null) {
			throw new ManagerException(HotRacesErrorCode.EMPTY_SELECTED_ROW);
		}
		if (raceData.getStatus() == null || !raceData.getStatus().equals(RaceStatusEnum.OPEN.getType())) {
			throw new ManagerException(HotRacesErrorCode.BOOKMAKER_WRONG_RACE_TYPE);
		}
		if (raceData.getName().isEmpty() || raceData.getDate().isEmpty() || raceData.getStatus().isEmpty()) {
			throw new ManagerException(HotRacesErrorCode.EMPTY_SELECTED_ROW);
		}
		try {
			raceData.setDate(PageManager.formatDate(raceData.getDate(), locale));
		} catch (DateTimeParseException e) {
			throw new ManagerException(HotRacesErrorCode.WRONG_DATE);
		}

		try (RaceDAO raceDao = new RaceDAO()) {
			raceDao.deleteRaceBookmaker(raceData);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}
