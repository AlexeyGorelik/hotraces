package com.gorelik.hotraces.logic.race;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class RaceData. Class that contains information about race.
 */
public class RaceData {

	/** The race name. */
	private final String RACE_NAME = "name";

	/** The race date. */
	private final String RACE_DATE = "date";

	/** The race status. */
	private final String RACE_STATUS = "status";

	/** The data map. */
	protected Map<String, String> dataMap = new HashMap<String, String>();

	/**
	 * Instantiates a new race data.
	 *
	 * @param name
	 *            the name
	 */
	public RaceData(String name) {
		dataMap.put(RACE_NAME, name);
	}

	/**
	 * Instantiates a new race data.
	 *
	 * @param name
	 *            the name
	 * @param date
	 *            the date
	 */
	public RaceData(String name, String date) {
		dataMap.put(RACE_NAME, name);
		dataMap.put(RACE_DATE, date);
	}

	/**
	 * Instantiates a new race data.
	 *
	 * @param race
	 *            the race
	 */
	public RaceData(String[] race) {
		dataMap.put(RACE_NAME, race[0]);
		dataMap.put(RACE_DATE, race[1]);
		dataMap.put(RACE_STATUS, race[2]);
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return dataMap.get(RACE_NAME);
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public String getDate() {
		return dataMap.get(RACE_DATE);
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return dataMap.get(RACE_STATUS);
	}

	/**
	 * Sets the date.
	 *
	 * @param date
	 *            the new date
	 */
	public void setDate(String date) {
		dataMap.put(RACE_DATE, date);
	}
}
