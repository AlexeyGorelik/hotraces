package com.gorelik.hotraces.logic.race;

/**
 * The Class EditRaceData. Class that contains information about edit race.
 */
public class EditRaceData {

	/** The horse id. */
	private String horseId;

	/** The horse name. */
	private String horseName;

	/** The action (delete, add, etc.). */
	private String action;

	/**
	 * Instantiates a new edits the race data.
	 *
	 * @param horseId
	 *            the horse id
	 * @param horseName
	 *            the horse name
	 * @param action
	 *            the action
	 */
	public EditRaceData(String horseId, String horseName, String action) {
		this.horseId = horseId;
		this.horseName = horseName;
		this.action = action;
	}

	/**
	 * Gets the horse id.
	 *
	 * @return the horse id
	 */
	public String getHorseId() {
		return horseId;
	}

	/**
	 * Gets the horse name.
	 *
	 * @return the horse name
	 */
	public String getHorseName() {
		return horseName;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
}
