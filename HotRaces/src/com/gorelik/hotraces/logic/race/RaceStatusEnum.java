package com.gorelik.hotraces.logic.race;

/**
 * The Enum RaceStatusEnum. Enum that contains all race status.
 */
public enum RaceStatusEnum {
	/** The open status. */
	OPEN("�������"),
	/** The close status. */
	CLOSE("�������"),
	/** The decline status. */
	DECLINE("��������"),
	/** The carried out status. */
	CARRIED_OUT("���������");

	/** The type. */
	private String type;

	/**
	 * Instantiates a new race status enum.
	 *
	 * @param type
	 *            the type
	 */
	private RaceStatusEnum(String type) {
		this.type = type;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return this.type;
	}
}