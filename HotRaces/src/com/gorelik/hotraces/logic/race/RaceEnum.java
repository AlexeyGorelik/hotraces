package com.gorelik.hotraces.logic.race;

/**
 * The Enum RaceEnum. Enum that contains all races in DB.
 */
public enum RaceEnum {

	/** The lonshan. */
	LONSHAN("������", "Lonshan"),

	/** The newmarket. */
	NEWMARKET("���-������", "NewMarket"),

	/** The meydan. */
	MEYDAN("������", "Meydan"),

	/** The southwell. */
	SOUTHWELL("��������", "Southwell");

	/**
	 * Instantiates a new race enum.
	 *
	 * @param value
	 *            the value
	 * @param engValue
	 *            the english value
	 */
	RaceEnum(String value, String engValue) {
		this.value = value;
		this.engValue = engValue;
	}

	/** The value. */
	private String value;

	/** The english value. */
	private String engValue;

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * Gets the english value.
	 *
	 * @return the english value
	 */
	public String getEngValue() {
		return this.engValue;
	}
}
