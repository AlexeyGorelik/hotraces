package com.gorelik.hotraces.logic.race;

/**
 * The Class PrevRaceData. Class that contains information about previous race.
 */
public class PrevRaceData {

	/** The name. */
	private String name;

	/** The date. */
	private String date;

	/** The status. */
	private String status;

	/**
	 * Instantiates a new previous race data.
	 *
	 * @param name
	 *            the name
	 * @param date
	 *            the date
	 * @param status
	 *            the status
	 */
	public PrevRaceData(String name, String date, String status) {
		this.name = name;
		this.date = date;
		this.status = status;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public String getDate() {
		return this.date;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return this.status;
	}
}
