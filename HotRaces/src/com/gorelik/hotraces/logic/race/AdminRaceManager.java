package com.gorelik.hotraces.logic.race;

import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gorelik.hotraces.dao.HorseDAO;
import com.gorelik.hotraces.dao.RaceDAO;
import com.gorelik.hotraces.entity.Horse;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.validation.DateValidator;

/**
 * The Class AdminRaceManager. Class lets administrator to work with
 * race(create, edit, etc.).
 */
public class AdminRaceManager {
	/**
	 * The Constant RIGHT_DATE_LENGTH. Common date length (example:
	 * "YYYY-MM-DD HH-mm-ss").
	 */
	private static final int RIGHT_DATE_LENGTH = 16;

	/** The Constant DEFAULT_DATE_DELIMETER. Default delimeter in html-tag. */
	private static final String DEFAULT_DATE_DELIMETER = "T";

	/** The Constant SPACE_DELIMETER. Delimeter to database. */
	private static final String SPACE_DELIMETER = " ";

	/** The Constant SECONDS. Seconds format to database. */
	private static final String SECONDS = ":00";
	/**
	 * The Constant MIN_HORSE_IN_RACE_COUNT. Minimum number of horses to edit
	 * race.
	 */
	private static final int MIN_HORSE_IN_RACE_COUNT = 2;

	/**
	 * The Constant DELETE_ACTION. Action with horse to count final horse number
	 * (it has to be >= 2).
	 */
	private static final String DELETE_ACTION = "delete";

	/**
	 * Creates the horse list.
	 *
	 * @param raceData
	 *            the race data
	 * @return the list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<Horse> createEditHorseList(RaceData raceData) throws ManagerException {
		if (!(RaceStatusEnum.OPEN.getType().equals(raceData.getStatus())
				|| RaceStatusEnum.CLOSE.getType().equals(raceData.getStatus()))) {
			throw new ManagerException(HotRacesErrorCode.EDIT_RACE_WRONG_STATUS);
		}
		try (RaceDAO raceDao = new RaceDAO()) {
			return raceDao.findEditHorseList(raceData);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Edits the race.
	 *
	 * @param editData
	 *            the edit data
	 * @param raceData
	 *            the race data
	 * @param locale
	 *            the locale
	 * @param horseBeforeEdit
	 *            the horse before edit
	 * @throws ManagerException
	 *             the manager exception
	 */
	public void editRace(List<EditRaceData> editData, RaceData raceData, String locale, int horseBeforeEdit)
			throws ManagerException {
		if (raceData.getName().isEmpty()) {
			throw new ManagerException(HotRacesErrorCode.ADMIN_RACE_NAME_IS_EMPTY);
		}
		if (raceData.getDate().isEmpty()) {
			throw new ManagerException(HotRacesErrorCode.ADMIN_RACE_DATE_IS_EMPTY);
		}

		for (int i = 0; i < editData.size(); i++) {
			if (DELETE_ACTION.equals(editData.get(i).getAction())) {
				horseBeforeEdit--;
			}
		}

		if (horseBeforeEdit < MIN_HORSE_IN_RACE_COUNT) {
			throw new ManagerException(HotRacesErrorCode.ADMIN_CANT_EDIT_ONE_HORSE);
		}

		try {
			raceData.setDate(PageManager.formatDate(raceData.getDate(), locale));
		} catch (DateTimeParseException e) {
			throw new ManagerException(HotRacesErrorCode.WRONG_DATE);
		}

		try (RaceDAO raceDao = new RaceDAO()) {
			raceDao.editRace(editData, raceData);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Carry out race.
	 *
	 * @param raceData
	 *            the race data
	 * @param locale
	 *            the locale
	 * @throws ManagerException
	 *             the manager exception
	 */
	public void carryOutRace(String[] raceDataArr, String locale) throws ManagerException {
		RaceData raceData = null;
		try {
			raceData = new RaceData(raceDataArr);
		} catch (IndexOutOfBoundsException e) {
			throw new ManagerException(HotRacesErrorCode.EMPTY_SELECTED_ROW);
		}
		if (RaceStatusEnum.CARRIED_OUT.getType().equals(raceData.getStatus())) {
			throw new ManagerException(HotRacesErrorCode.ADMIN_RACE_ALREADY_CARRIED_OUT);
		}
		System.out.println(RaceStatusEnum.CLOSE.getType().equals(raceData.getStatus()));
		System.out.println(RaceStatusEnum.OPEN.getType().equals(raceData.getStatus()));
		if (!(RaceStatusEnum.CLOSE.getType().equals(raceData.getStatus())
				|| RaceStatusEnum.OPEN.getType().equals(raceData.getStatus()))) {
			throw new ManagerException(HotRacesErrorCode.ADMIN_WRONG_CARRY_OUT_STATUS);
		}
		try {
			raceData.setDate(PageManager.formatDate(raceData.getDate(), locale));
		} catch (DateTimeParseException e) {
			throw new ManagerException(HotRacesErrorCode.WRONG_DATE);
		}

		try (RaceDAO raceDao = new RaceDAO()) {
			raceDao.carryOutRace(raceData);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Delete race.
	 *
	 * @param raceData
	 *            the race data
	 * @param locale
	 *            the locale
	 * @throws ManagerException
	 *             the manager exception
	 */
	public void deleteRace(String[] raceDataArr, String locale) throws ManagerException {
		RaceData raceData = null;
		try {
			raceData = new RaceData(raceDataArr);
		} catch (IndexOutOfBoundsException e) {
			throw new ManagerException(HotRacesErrorCode.EMPTY_SELECTED_ROW);
		}
		if (raceData.getStatus() == null || !(RaceStatusEnum.OPEN.getType().equals(raceData.getStatus())
				|| RaceStatusEnum.CLOSE.getType().equals(raceData.getStatus()))) {
			throw new ManagerException(HotRacesErrorCode.EMPTY_DELETE_RACE_SELECT_VALUE);
		}
		if (raceData.getName().isEmpty() || raceData.getDate().isEmpty() || raceData.getStatus().isEmpty()) {
			throw new ManagerException(HotRacesErrorCode.EMPTY_SELECTED_ROW);
		}
		try {
			raceData.setDate(PageManager.formatDate(raceData.getDate(), locale));
		} catch (DateTimeParseException e) {
			throw new ManagerException(HotRacesErrorCode.WRONG_DATE);
		}

		try (RaceDAO raceDao = new RaceDAO()) {
			raceDao.deleteRace(raceData);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Creates the race.
	 *
	 * @param horseId
	 *            the horse id
	 * @param horseName
	 *            the horse name
	 * @param raceData
	 *            the race data
	 * @param locale
	 *            the locale
	 * @throws ManagerException
	 *             the manager exception
	 */
	public void createRace(String[] horseId, String[] horseName, RaceData raceData, String locale)
			throws ManagerException {
		if (horseId.length != horseName.length) {
			throw new ManagerException(HotRacesErrorCode.ADMIN_RACE_NOT_EQUALS_COUNT);
		}
		if (horseId.length == 1) {
			throw new ManagerException(HotRacesErrorCode.ADMIN_CREATE_RACE_ONE_HORSE);
		}
		if (raceData.getName().isEmpty()) {
			throw new ManagerException(HotRacesErrorCode.ADMIN_RACE_NAME_IS_EMPTY);
		}
		if (raceData.getDate().length() != RIGHT_DATE_LENGTH) {
			throw new ManagerException(HotRacesErrorCode.ADMIN_WRONG_RACE_DATE);
		}
		raceData.setDate(raceData.getDate().replace(DEFAULT_DATE_DELIMETER, SPACE_DELIMETER).concat(SECONDS));
		if (!new DateValidator().checkFullDate(raceData.getDate())) {
			throw new ManagerException(HotRacesErrorCode.WRONG_DATE);
		}

		Map<String, String> horseMap = new HashMap<String, String>(horseId.length);
		for (int i = 0; i < horseId.length; i++) {
			if (!horseMap.containsKey(horseId[i])) {
				horseMap.put(horseId[i], horseName[i]);
			} else {
				throw new ManagerException(HotRacesErrorCode.EQUALS_HORSES_IN_RACE);
			}
		}

		try (RaceDAO raceDao = new RaceDAO()) {
			raceDao.createRace(raceData, horseMap);
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Find race data.
	 *
	 * @return the list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<RaceData> findAllHippodromes() throws ManagerException {
		try (RaceDAO raceDao = new RaceDAO()) {
			return raceDao.findAllHippodromesList();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}

	/**
	 * Find horse list.
	 *
	 * @return the list
	 * @throws ManagerException
	 *             the manager exception
	 */
	public List<Horse> findAllHorses() throws ManagerException {
		try (HorseDAO raceDao = new HorseDAO()) {
			return raceDao.findAllHorsesList();
		} catch (DAOException e) {
			throw new ManagerException(e.getMessage());
		}
	}
}
