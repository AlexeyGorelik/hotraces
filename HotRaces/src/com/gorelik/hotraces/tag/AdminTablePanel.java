package com.gorelik.hotraces.tag;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.gorelik.hotraces.entity.Race;

/**
 * The Class AdminTablePanel. Class that displays admin table on jsp.
 */
public class AdminTablePanel extends TagSupport {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8687108728866163826L;

	/** The list of races. */
	private List<Race> list;

	/** The current row. Counter for row. */
	private int currentRow = 0;

	/** The max table size. */
	private int maxSize;

	/** The locale. */
	private String locale;

	/**
	 * Sets the locale to determine how to display date.
	 *
	 * @param locale
	 *            the new locale
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * Sets the data.
	 *
	 * @param list
	 *            the new data
	 */
	public void setData(List<Race> list) {
		this.list = (List<Race>) list;
		maxSize = list.size();
	}

	@Override
	public int doStartTag() throws JspTagException {
		currentRow = 0;
		try {
			JspWriter out = pageContext.getOut();
			out.write("<tbody id='body-table'>");
			while (maxSize-- > 0) {
				try {
					pageContext.getOut().write("<tr>");
					pageContext.getOut().write("<td>" + list.get(currentRow).getName() + "</td>");
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
					LocalDateTime dateTime = LocalDateTime.parse(list.get(currentRow).getStringDate(), formatter);

					DateTimeFormatter formatterRu = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
					DateTimeFormatter formatterEn = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm");
					switch (locale) {
					case "ru_RU":
						pageContext.getOut().write("<td>" + dateTime.format(formatterRu).toString() + "</td>");
						break;
					case "en_US":
						pageContext.getOut().write("<td>" + dateTime.format(formatterEn).toString() + "</td>");
						break;
					default:
						pageContext.getOut().write("<td>" + dateTime.format(formatterRu).toString() + "</td>");
						break;
					}
					pageContext.getOut().write("<td>" + list.get(currentRow).getStatus() + "</td>");
					pageContext.getOut().write("<td>" + list.get(currentRow).getCity() + "</td>");
					pageContext.getOut().write("<td>" + list.get(currentRow).getCountry() + "</td>");
					pageContext.getOut().write("<td>" + list.get(currentRow).getLength() + "</td>");
					pageContext.getOut().write("</tr>");
				} catch (IOException e) {
					throw new JspTagException(e.getMessage());
				}
				currentRow++;
			}
			pageContext.getOut().write("</tbody>");
		} catch (IOException e) {
			throw new JspTagException(e.getMessage());
		}
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() {
		return EVAL_PAGE;
	}
}