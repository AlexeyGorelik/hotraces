package com.gorelik.hotraces.tag;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * The Class LocaleDate. Class that displays data on jsp.
 */
@SuppressWarnings("serial")
public class LocaleDate extends TagSupport {

	/** The locale. */
	private String locale;

	/** The date. */
	private String date;

	/**
	 * Sets the date.
	 *
	 * @param date
	 *            the new date
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Sets the locale.
	 *
	 * @param locale
	 *            the new locale
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
	 */
	@Override
	public int doStartTag() throws JspTagException {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTime = LocalDateTime.parse(date, formatter);

			DateTimeFormatter formatterRu = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
			DateTimeFormatter formatterEn = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm");
			JspWriter out = pageContext.getOut();
			switch (locale) {
			case "ru_RU":
				out.write(dateTime.format(formatterRu).toString());
				break;
			case "en_US":
				out.write(dateTime.format(formatterEn).toString());
				break;
			default:
				out.write(dateTime.format(formatterRu).toString());
				break;
			}

		} catch (IOException e) {
			throw new JspTagException(e.getMessage());
		}
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() {
		return EVAL_PAGE;
	}
}