package com.gorelik.hotraces.exception;

/**
 * The Class ManagerException. Class which handles exception in Manager.
 */
public class ManagerException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4908502484061688242L;

	/**
	 * Instantiates a new manager exception.
	 */
	public ManagerException() {

	}

	/**
	 * Instantiates a new manager exception.
	 *
	 * @param message
	 *            the message
	 */
	public ManagerException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new manager exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public ManagerException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new manager exception.
	 *
	 * @param cause
	 *            the cause
	 */
	public ManagerException(Throwable cause) {
		super(cause);
	}
}