package com.gorelik.hotraces.exception;

/**
 * The Class WrapperConnectionException. Class which handles exception in
 * Connection.
 */
public class WrapperConnectionException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2013021304036514514L;

	/**
	 * Instantiates a new wrapper connection exception.
	 */
	public WrapperConnectionException() {

	}

	/**
	 * Instantiates a new wrapper connection exception.
	 *
	 * @param message
	 *            the message
	 */
	public WrapperConnectionException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new wrapper connection exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public WrapperConnectionException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new wrapper connection exception.
	 *
	 * @param cause
	 *            the cause
	 */
	public WrapperConnectionException(Throwable cause) {
		super(cause);
	}
}