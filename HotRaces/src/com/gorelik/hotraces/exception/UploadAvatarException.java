package com.gorelik.hotraces.exception;

/**
 * The Class UploadAvatarException. Class which handles exception in upload
 * avatar.
 */
public class UploadAvatarException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8320796692690793360L;

	/**
	 * Instantiates a new upload avatar exception.
	 */
	public UploadAvatarException() {

	}

	/**
	 * Instantiates a new upload avatar exception.
	 *
	 * @param message
	 *            the message
	 */
	public UploadAvatarException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new upload avatar exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public UploadAvatarException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new upload avatar exception.
	 *
	 * @param cause
	 *            the cause
	 */
	public UploadAvatarException(Throwable cause) {
		super(cause);
	}
}