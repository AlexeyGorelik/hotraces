package com.gorelik.hotraces.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.gorelik.hotraces.logic.bets.BetsResultEnum;
import com.gorelik.hotraces.logic.bets.BetsTypeEnum;

/**
 * The Class Bet. Class which contains information about user's bet.
 */
public class Bet {
	/**
	 * The Constant FIRST_THREE_DIVIDER_COEFF. On this digit divides sum of win
	 * if bet type is "First three".
	 */
	private static final String FIRST_THREE_DIVIDER_COEFF = "3.0";

	/**
	 * The Constant EMPTY_RESULT. Character to show result if bet has lost or
	 * hasn't determined yet.
	 */
	private static final String EMPTY_RESULT = "-";

	/** The id. Unique bet identifier. */
	private int id;

	/** The user id. User identifier which made this bet. */
	private int userId;

	/**
	 * The bet type. Type of current bet.
	 * 
	 * @see BetsTypeEnum
	 */
	private String betType;

	/** The sum. Win sum of bet (if exists). */
	private BigDecimal sum;

	/** The date. Date when user has created current bet. */
	private String date;

	/**
	 * The result. The result of current bet.
	 * 
	 * @see BetsResultEnum
	 */
	private String result;

	/** The race name. Name of race on which race user has created the bet. */
	private String raceName;

	/** The horse name. Name of horse on which user has created bet. */
	private String horseName;

	/** The horse id. Identifier of horse on which user has created bet. */
	private int horseId;

	/** The race date. Date when the race will be carried out. */
	private String raceDate;

	/** The coeff. Winning horse coefficient. */
	private BigDecimal coeff;

	/**
	 * Instantiates a new bet.
	 *
	 * @param id
	 *            the unique identifier of current bet
	 * @param raceName
	 *            the race name
	 * @param raceDate
	 *            the race date
	 * @param horseName
	 *            the horse name
	 * @param betType
	 *            the bet type
	 * @param sum
	 *            the sum
	 * @param date
	 *            the date when user has created bet
	 * @param horseId
	 *            the horse id
	 * @param userId
	 *            the user id
	 */
	public Bet(int id, String raceName, String raceDate, String horseName, String betType, BigDecimal sum, String date,
			int horseId, int userId) {
		this.id = id;
		this.betType = betType;
		this.sum = sum;
		this.date = date;
		this.raceName = raceName;
		this.horseName = horseName;
		this.raceDate = raceDate;
		this.horseId = horseId;
		this.userId = userId;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * Gets the bet type.
	 *
	 * @return the bet type
	 */
	public String getBetType() {
		return betType;
	}

	/**
	 * Sets the bet type.
	 *
	 * @param betType
	 *            the new bet type
	 */
	public void setBetType(String betType) {
		this.betType = betType;
	}

	/**
	 * Gets the sum.
	 *
	 * @return the sum
	 */
	public BigDecimal getSum() {
		return sum;
	}

	/**
	 * Sets the sum.
	 *
	 * @param sum
	 *            the new sum
	 */
	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	/**
	 * Gets the race name.
	 *
	 * @return the race name
	 */
	public String getRaceName() {
		return raceName;
	}

	/**
	 * Sets the race name.
	 *
	 * @param raceName
	 *            the new race name
	 */
	public void setRaceName(String raceName) {
		this.raceName = raceName;
	}

	/**
	 * Gets the horse name.
	 *
	 * @return the horse name
	 */
	public String getHorseName() {
		return horseName;
	}

	/**
	 * Sets the horse name.
	 *
	 * @param horseName
	 *            the new horse name
	 */
	public void setHorseName(String horseName) {
		this.horseName = horseName;
	}

	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	public String getResult() {
		return result == null || result.isEmpty() ? EMPTY_RESULT : this.result;
	}

	/**
	 * Sets the result.
	 *
	 * @param result
	 *            the new result
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * Gets the race date.
	 *
	 * @return the race date
	 */
	public String getRaceDate() {
		return raceDate;
	}

	/**
	 * Sets the race date.
	 *
	 * @param raceDate
	 *            the new race date
	 */
	public void setRaceDate(String raceDate) {
		this.raceDate = raceDate;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 *
	 * @param date
	 *            the new date
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Gets the coeff.
	 *
	 * @return the coeff
	 */
	public BigDecimal getCoeff() {
		return coeff;
	}

	/**
	 * Sets the coeff.
	 *
	 * @param coeff
	 *            the new coeff
	 */
	public void setCoeff(BigDecimal coeff) {
		this.coeff = coeff;
	}

	/**
	 * Gets the horse id.
	 *
	 * @return the horse id
	 */
	public int getHorseId() {
		return horseId;
	}

	/**
	 * Sets the horse id.
	 *
	 * @param horseId
	 *            the new horse id
	 */
	public void setHorseId(int horseId) {
		this.horseId = horseId;
	}

	/**
	 * Gets the sum win.
	 *
	 * @return the sum win
	 */
	public String getSumWin() {
		if (result == null || coeff == null) {
			return EMPTY_RESULT;
		}
		if (result.equals(BetsResultEnum.WIN.getValue()) && betType.equals(BetsTypeEnum.WIN.getValue())) {
			return sum.multiply(coeff).setScale(1, RoundingMode.HALF_UP).toString();
		} else if (result.equals(BetsResultEnum.WIN.getValue())
				&& betType.equals(BetsTypeEnum.FIRST_THREE.getValue())) {
			BigDecimal temp = sum.multiply(coeff);
			BigDecimal dividor = new BigDecimal(FIRST_THREE_DIVIDER_COEFF);
			return temp.divide(dividor, 1, RoundingMode.HALF_UP).toString();
		} else {
			return EMPTY_RESULT;
		}
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;

		if (o == this)
			return true;

		if (o.getClass() != this.getClass())
			return false;

		Bet temp = (Bet) o;

		if (this.id != temp.getId())
			return false;

		if (this.userId != temp.getUserId())
			return false;

		if (!this.betType.equals(temp.getBetType()))
			return false;

		if (!this.sum.equals(temp.getSum()))
			return false;

		if (!this.date.equals(temp.getDate()))
			return false;

		if (!this.raceName.equals(temp.getRaceName()))
			return false;

		if (!this.horseName.equals(temp.getHorseName()))
			return false;

		if (this.horseId != temp.getHorseId())
			return false;

		if (!this.raceDate.equals(temp.getRaceDate()))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return this.id;
	}
}