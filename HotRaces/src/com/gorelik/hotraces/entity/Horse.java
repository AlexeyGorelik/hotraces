package com.gorelik.hotraces.entity;

/**
 * The Class Horse. Class contains information about horse.
 */
public class Horse {

	/** The id. Unique horse identifier. */
	private int id;

	/** The name. Horse name. */
	private String name;

	/** The color. Horse color. */
	private String color;

	/** The weight. Horse weight in kilograms. */
	private int weight;

	/**
	 * Instantiates a new horse.
	 *
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 * @param color
	 *            the color
	 * @param weigth
	 *            the weigth
	 */
	public Horse(int id, String name, String color, int weight) {
		this.id = id;
		this.name = name;
		this.color = color;
		this.weight = weight;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the color.
	 *
	 * @return the color
	 */
	public String getColor() {
		return this.color;
	}

	/**
	 * Gets the weigth.
	 *
	 * @return the weigth
	 */
	public int getWeight() {
		return this.weight;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;

		if (o == this)
			return true;

		if (o.getClass() != this.getClass())
			return false;

		Horse temp = (Horse) o;

		if (this.id != temp.getId())
			return false;

		if (!this.name.equals(temp.getName()))
			return false;

		if (!this.color.equals(temp.getColor()))
			return false;

		if (this.weight != temp.getWeight())
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return this.id;
	}
}
