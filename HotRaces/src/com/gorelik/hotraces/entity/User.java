package com.gorelik.hotraces.entity;

import java.math.BigDecimal;

/**
 * The Class User. Class contains information about user.
 */
public class User {

	/** The user id. Unique user identifier. */
	private int userId;

	/** The login. User login (nickname). */
	private String login;

	/** The first name. User first name. */
	private String firstName;

	/** The second name. User second name. */
	private String secondName;

	/** The role. User role. */
	private String role;

	/** The email. User email. */
	private String email;

	/** The status. User profile status. */
	private String status;

	/** The money. Money on account. */
	private BigDecimal money;

	/** The avatar path. User avatar path. */
	private String avatarPath;

	/**
	 * Instantiates a new user.
	 *
	 * @param id
	 *            the unique identifier
	 * @param login
	 *            the login
	 * @param firstName
	 *            the first name
	 * @param secondName
	 *            the second name
	 * @param email
	 *            the email
	 * @param role
	 *            the user role
	 * @param status
	 *            the account status
	 */
	public User(int id, String login, String firstName, String secondName, String email, String role, String status) {
		this.userId = id;
		this.login = login;
		this.firstName = firstName;
		this.secondName = secondName;
		this.email = email;
		this.role = role;
		this.status = status;
	}

	/**
	 * Instantiates a new user.
	 *
	 * @param id
	 *            the unique identifier
	 * @param login
	 *            the login
	 * @param firstName
	 *            the first name
	 * @param secondName
	 *            the second name
	 * @param email
	 *            the email
	 * @param role
	 *            the user role
	 * @param status
	 *            the account status
	 * @param money
	 *            the money
	 */
	public User(int id, String login, String firstName, String secondName, String email, String role, String status,
			BigDecimal money) {
		this.userId = id;
		this.login = login;
		this.firstName = firstName;
		this.secondName = secondName;
		this.email = email;
		this.role = role;
		this.status = status;
		this.money = money;
		avatarPath = "/avatars/default.png";
	}

	/**
	 * Sets the login.
	 *
	 * @param login
	 *            the new login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Gets the login.
	 *
	 * @return the login
	 */
	public String getLogin() {
		return this.login;
	}

	/**
	 * Sets the second name.
	 *
	 * @param second
	 *            the new second name
	 */
	public void setSecondName(String second) {
		this.secondName = second;
	}

	/**
	 * Gets the second name.
	 *
	 * @return the second name
	 */
	public String getSecondName() {
		return secondName;
	}

	/**
	 * Gets the money.
	 *
	 * @return the money
	 */
	public BigDecimal getMoney() {
		return money;
	}

	/**
	 * Sets the money.
	 *
	 * @param money
	 *            the new money
	 */
	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	/**
	 * Sets the first name.
	 *
	 * @param first
	 *            the new first name
	 */
	public void setFirstName(String first) {
		this.firstName = first;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the avatar path.
	 *
	 * @param path
	 *            the new avatar path
	 */
	public void setAvatarPath(String path) {
		this.avatarPath = path;
	}

	/**
	 * Gets the avatar path.
	 *
	 * @return the avatar path
	 */
	public String getAvatarPath() {
		return this.avatarPath;
	}

	/**
	 * Sets the user id.
	 *
	 * @param id
	 *            the new user id
	 */
	public void setUserId(int id) {
		this.userId = id;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public int getUserId() {
		return this.userId;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;

		if (o == this)
			return true;

		if (o.getClass() != this.getClass())
			return false;

		User temp = (User) o;

		if (this.userId != temp.getUserId())
			return false;

		if (!this.login.equals(temp.getLogin()))
			return false;

		if (!this.firstName.equals(temp.getFirstName()))
			return false;

		if (!this.secondName.equals(temp.getSecondName()))
			return false;

		if (!this.role.equals(temp.getRole()))
			return false;

		if (!this.email.equals(temp.getEmail()))
			return false;

		if (!this.status.equals(temp.getStatus()))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return this.userId;
	}
}
