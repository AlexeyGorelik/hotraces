package com.gorelik.hotraces.entity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * The Class SupportMessage.
 */
public class SupportMessage {

	/**
	 * The Constant MILLISECONDS. Variable contains milliseconds value when get
	 * date from DB.
	 */
	// to delete milliseconds when we create object from DB
	private static final String MILLISECONDS = ".0";

	/** The email. Email of user who sent message. */
	private String email;

	/** The type. Message type. */
	private String type;

	/** The message. Message text. */
	private String message;

	/** The date. Date when message sent. */
	private String date;

	/**
	 * Instantiates a new support message.
	 *
	 * @param email
	 *            the user email
	 * @param type
	 *            the message type
	 * @param message
	 *            the message text
	 * @param date
	 *            the date
	 */
	public SupportMessage(String email, String type, String message, String date) {
		this.email = email;
		this.type = type;
		this.message = message;
		this.date = date.replace(MILLISECONDS, "");
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return this.type;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return this.message;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public String getDate() {
		return this.date;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;

		if (o == this)
			return true;

		if (o.getClass() != this.getClass())
			return false;

		SupportMessage temp = (SupportMessage) o;

		if (!this.email.equals(temp.getEmail()))
			return false;

		if (!this.type.equals(temp.getType()))
			return false;

		if (!this.message.equals(temp.getMessage()))
			return false;

		if (!this.date.equals(temp.getDate()))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(this.date.replace(MILLISECONDS, ""), formatter);
		return dateTime.hashCode();
	}
}
