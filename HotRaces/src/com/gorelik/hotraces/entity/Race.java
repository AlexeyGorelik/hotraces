package com.gorelik.hotraces.entity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * The Class Race. Class contains information about race.
 */
public class Race {
	/** The name. Hippodrome name. */
	private String name;

	/** The city. Name of the city where hippodrome is located. */
	private String city;

	/** The country. Name of the country where hippodrome is located. */
	private String country;

	/** The status. Race status. */
	private String status;

	/** The length. Hippodrome lenth. */
	private int length;

	/** The local date. Date when race will be carried out. */
	private LocalDateTime localDate;

	/**
	 * Instantiates a new race.
	 *
	 * @param name
	 *            the race name
	 * @param status
	 *            the race status
	 * @param city
	 *            the city where hipodrome is located
	 * @param country
	 *            the country where hipodrome is located
	 * @param length
	 *            the length of hippodrome
	 */
	public Race(String name, String status, String city, String country, int length) {
		this.name = name;
		this.status = status;
		this.city = city;
		this.country = country;
		this.length = length;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return this.city;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return this.country;
	}

	/**
	 * Gets the length.
	 *
	 * @return the length
	 */
	public int getLength() {
		return this.length;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Sets the date.
	 *
	 * @param date
	 *            the new date
	 */
	public void setDate(LocalDateTime date) {
		this.localDate = date;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public LocalDateTime getDate() {
		return this.localDate;
	}

	/**
	 * Gets the string date.
	 *
	 * @return the string date
	 */
	public String getStringDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return localDate.format(formatter);
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;

		if (o == this)
			return true;

		if (o.getClass() != this.getClass())
			return false;

		Race temp = (Race) o;

		if (!this.name.equals(temp.getName()))
			return false;

		if (!this.city.equals(temp.getCity()))
			return false;

		if (!this.country.equals(temp.getCountry()))
			return false;

		if (!this.status.equals(temp.getStatus()))
			return false;

		if (this.length != temp.getLength())
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return (this.name.hashCode() + this.status.hashCode()) % this.length;
	}
}
