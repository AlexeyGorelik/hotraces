package com.gorelik.hotraces.entity;

/**
 * The Class Message.
 */
public class Message {

	/**
	 * The Constant MAX_SMALL_MESSAGE_LENGTH. Maximum message length to display
	 * as short part of message.
	 */
	private static final int MAX_SMALL_MESSAGE_LENGTH = 50;

	/** The id from. Unique message identifier. */
	private int idFrom;

	/** The name from. User name who sends message. */
	private String nameFrom;

	/** The date. Date when message was sent. */
	private String date;

	/** The text. Text of the message. */
	private String text;

	/**
	 * The conversation id. Unique identifier of conversation between two users.
	 */
	private int convId;

	/** The avatar path. Path to avatar (user image) on the PC. */
	private String avatarPath;

	/**
	 * Instantiates a new message.
	 *
	 * @param idFrom
	 *            the user identifier who sends message
	 * @param nameFrom
	 *            the user name who sends message
	 * @param convId
	 *            the conversation id
	 */
	public Message(int idFrom, String nameFrom, int convId) {
		this.convId = convId;
		this.idFrom = idFrom;
		this.nameFrom = nameFrom;
	}

	/**
	 * Instantiates a new message.
	 *
	 * @param idFrom
	 *            the user identifier who sends message
	 * @param nameFrom
	 *            the user name who sends message
	 * @param date
	 *            the date when user sent the message
	 * @param text
	 *            the text of the message
	 * @param avatarPath
	 *            the avatar path to user profile picture
	 */
	public Message(int idFrom, String nameFrom, String date, String text, String avatarPath) {
		this.idFrom = idFrom;
		this.nameFrom = nameFrom;
		this.date = date;
		this.text = text;
		this.avatarPath = avatarPath;
	}

	/**
	 * Gets the id from.
	 *
	 * @return the id from
	 */
	public int getIdFrom() {
		return this.idFrom;
	}

	/**
	 * Gets the name from.
	 *
	 * @return the name from
	 */
	public String getNameFrom() {
		return this.nameFrom;
	}

	/**
	 * Sets the date.
	 *
	 * @param date
	 *            the new date
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public String getDate() {
		return this.date;
	}

	/**
	 * Sets the text.
	 *
	 * @param text
	 *            the new text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * Gets the cut text message to display short message.
	 *
	 * @return the cut text
	 */
	public String getCutText() {
		if (text.length() > MAX_SMALL_MESSAGE_LENGTH) {
			return this.text.substring(0, MAX_SMALL_MESSAGE_LENGTH) + "...";
		} else {
			return this.text;
		}
	}

	/**
	 * Gets the conv id.
	 *
	 * @return the conv id
	 */
	public int getConvId() {
		return convId;
	}

	/**
	 * Sets the conv id.
	 *
	 * @param convId
	 *            the new conv id
	 */
	public void setConvId(int convId) {
		this.convId = convId;
	}

	/**
	 * Gets the avatar path.
	 *
	 * @return the avatar path
	 */
	public String getAvatarPath() {
		return avatarPath;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;

		if (o == this)
			return true;

		if (o.getClass() != this.getClass())
			return false;

		Message temp = (Message) o;

		if (this.idFrom != temp.getIdFrom())
			return false;

		if (!this.nameFrom.equals(temp.getNameFrom()))
			return false;

		if (!this.date.equals(temp.getDate()))
			return false;

		if (!this.text.equals(temp.getText()))
			return false;

		if (this.convId != temp.getConvId())
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return (this.idFrom + this.nameFrom.hashCode()) % this.convId;
	}
}