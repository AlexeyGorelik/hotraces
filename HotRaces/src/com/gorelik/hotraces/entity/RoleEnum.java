package com.gorelik.hotraces.entity;

/**
 * The Enum RoleEnum. Enum contains all options of user's roles.
 */
public enum RoleEnum {

	/** The admin. Administrator role. */
	ADMIN("Admin"),

	/** The bookmaker. Bookmaker role. */
	BOOKMAKER("Bookmaker"),

	/** The client. Client role. */
	CLIENT("Client");

	/**
	 * Instantiates a new role enum.
	 *
	 * @param role
	 *            the user role
	 */
	private RoleEnum(String role) {
		this.role = role;
	}

	/** The role. String value of roles. */
	private String role;

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public String getRole() {
		return this.role;
	}
}
