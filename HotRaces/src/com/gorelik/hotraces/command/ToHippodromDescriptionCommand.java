package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToHippodromDescriptionCommand. The class provides a process for
 * user.
 */
public class ToHippodromDescriptionCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		request.setAttribute(PageManager.REDIRECT, true);
		return PageManager.HIPPODROM_DESCRIPTION_FULL_PROP + ParamManagerJSP.ANCOR_DELIMETER
				+ request.getParameter(ParamManagerJSP.HIPP_DESCR_NAME);
	}

}
