package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.pageerror.ErrorCodeManager;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToProfileChangeCommand. The class provides a process for user.
 */
public class ToProfileChangeCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute(PageManager.USER);
		if (user == null) {
			request.setAttribute(PageManager.REDIRECT, true);
			return PRGManager.TO_MAIN;
		} else {
			if (request.getParameter(HotRacesErrorCode.ERROR_ATTR) != null) {
				if (!request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString().isEmpty()) {
					request.setAttribute(ParamManagerJSP.CHANGE_ERROR, ErrorCodeManager
							.getErrorMessage(request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString()));
				}
			}
			request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_CHANGE_PROFILE);
			return PageManager.PAGE_PROFILE_CHANGE_PROP;
		}
	}

}
