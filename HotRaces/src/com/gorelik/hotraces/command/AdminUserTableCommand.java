package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.admin.AdminUserTableManager;
import com.gorelik.hotraces.pageerror.ErrorCodeManager;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class AdminUserTableCommand. The class provides a process for
 * administrator.
 */
public class AdminUserTableCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(AdminUserTableCommand.class);

	@Override
	public String execute(HttpServletRequest request) {
		if (request.getParameter(HotRacesErrorCode.ERROR_ATTR) != null) {
			if (!request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString().isEmpty()) {
				request.setAttribute(HotRacesErrorCode.ERROR_USER_TABLE, ErrorCodeManager
						.getErrorMessage(request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString()));
				return PageManager.PAGE_ADMIN_USER_TABLE_PROP;
			}
		}

		AdminUserTableManager adminUserTableMng = new AdminUserTableManager();
		try {
			request.setAttribute(ParamManagerJSP.USER_LIST_PROP, adminUserTableMng.createUserList());
			request.setAttribute(ParamManagerJSP.USER_LIST_EXIST_PROP, true);
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't load user race table. Error code = " + e.getMessage());
			request.setAttribute(HotRacesErrorCode.ERROR_USER_TABLE, ErrorCodeManager.getErrorMessage(e.getMessage()));
		}
		return PageManager.PAGE_ADMIN_USER_TABLE_PROP;
	}

}
