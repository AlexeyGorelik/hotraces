package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class LogoutCommand. The class provides a process for user.
 */
public class LogoutCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		String locale = request.getSession().getAttribute(PageManager.LOCALE).toString();

		request.getSession().setAttribute(PageManager.USER, null);
		request.getSession().invalidate();
		request.setAttribute(PageManager.REDIRECT, true);
		request.getSession().setAttribute(PageManager.LOCALE, locale);
		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_MAIN);
		return PRGManager.TO_MAIN;
	}

}
