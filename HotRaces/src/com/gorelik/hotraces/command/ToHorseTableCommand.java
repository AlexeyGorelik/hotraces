package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.horse.HorseManager;
import com.gorelik.hotraces.pageerror.ErrorCodeManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToHorseTableCommand. The class provides a process for
 * administrator.
 */
public class ToHorseTableCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(ToHorseTableCommand.class);

	@Override
	public String execute(HttpServletRequest request) {
		HorseManager horseMng = new HorseManager();

		try {
			request.setAttribute(ParamManagerJSP.HORSE_LIST, horseMng.findAllHorsesList());
			request.setAttribute(ParamManagerJSP.HORSE_LIST_EXISTS, true);
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't load horse table. Error code = " + e.getMessage());
			request.setAttribute(ParamManagerJSP.HORSE_ERROR, ErrorCodeManager.getErrorMessage(e.getMessage()));
		}
		return PageManager.PAGE_ADMIN_HORSE_TABLE_PROP;
	}

}
