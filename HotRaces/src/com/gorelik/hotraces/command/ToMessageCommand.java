package com.gorelik.hotraces.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.entity.Message;
import com.gorelik.hotraces.entity.RoleEnum;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.message.MessageManager;
import com.gorelik.hotraces.pageerror.ErrorCodeManager;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToMessageCommand. The class provides a process for user.
 */
public class ToMessageCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(ToMessageCommand.class);

	@Override
	public String execute(HttpServletRequest request) {
		if (request.getParameter(HotRacesErrorCode.ERROR_ATTR) != null) {
			if (!request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString().isEmpty()) {
				request.setAttribute(ParamManagerJSP.MESSAGE_ERROR, ErrorCodeManager
						.getErrorMessage(request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString()));
			}
		}

		User user = null;
		List<Message> list = null;
		MessageManager messageMng = new MessageManager();
		try {
			user = (User) request.getSession().getAttribute(PageManager.USER);
			list = messageMng.findMessagesList(Integer.valueOf(user.getUserId()));

			request.setAttribute(ParamManagerJSP.SHOW_MESSAGE_LIST, true);
			request.setAttribute(ParamManagerJSP.MESSAGE_LIST, list);
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't load messages list. Error code = " + e.getMessage());
			request.setAttribute(ParamManagerJSP.PROFILE_ERROR, ErrorCodeManager.getErrorMessage(e.getMessage()));
		}
		RoleEnum role = RoleEnum.valueOf(user.getRole().toUpperCase());
		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_PROFILE_MESSAGES);
		switch (role) {
		case ADMIN:
			return PageManager.PAGE_PROFILE_ADMIN_PROP;
		case BOOKMAKER:
			return PageManager.PAGE_PROFILE_BOOKMAKER_PROP;
		case CLIENT:
			return PageManager.PAGE_PROFILE_CLIENT_PROP;
		default:
			return PageManager.PAGE_MAIN_PROP;
		}
	}

}
