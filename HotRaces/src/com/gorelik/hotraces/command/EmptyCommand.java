package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.resource.PageManager;

/**
 * The Class EmptyCommand. Class that returns when command factory can't
 * determine command.
 */
public class EmptyCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		request.setAttribute(PageManager.REDIRECT, true);
		return PageManager.PAGE_INDEX_FULL_PROP;
	}

}