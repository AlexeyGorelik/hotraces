package com.gorelik.hotraces.command;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.bets.BetsManager;
import com.gorelik.hotraces.logic.bets.MakeBetData;
import com.gorelik.hotraces.logic.race.RaceData;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class MakeBetCommand. The class provides a process for user.
 */
public class MakeBetCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(MakeBetCommand.class);

	/** The bets manager. */
	private BetsManager betsMng = new BetsManager();

	@Override
	public String execute(HttpServletRequest request) {
		String raceName = request.getParameter(ParamManagerJSP.RACE_NAME);
		String raceDate = request.getParameter(ParamManagerJSP.RACE_DATE);
		RaceData raceData = new RaceData(raceName, raceDate);
		String locale = request.getSession().getAttribute(PageManager.LOCALE).toString();
		User user = (User) request.getSession().getAttribute(PageManager.USER);
		int betCount = Integer.valueOf(request.getParameter(ParamManagerJSP.BET_COUNT));

		List<MakeBetData> list = new ArrayList<MakeBetData>();
		for (int i = 1; i <= betCount; i++) {
			if (!request.getParameter(ParamManagerJSP.BET_PREF + i).isEmpty()) {
				MakeBetData temp = new MakeBetData(
						Integer.valueOf(request.getParameter(ParamManagerJSP.HORSE_ID_PREF + i)),
						request.getParameter(ParamManagerJSP.HORSE_NAME_PREF + i),
						BigDecimal.valueOf(Double.valueOf(request.getParameter(ParamManagerJSP.HORSE_COEFF_PREF + i))),
						BigDecimal.valueOf(Double.valueOf(request.getParameter(ParamManagerJSP.BET_PREF + i))),
						Integer.valueOf(request.getParameter(ParamManagerJSP.BET_TYPE_PREF + i)));
				list.add(temp);
			}
		}

		request.setAttribute(PageManager.REDIRECT, true);
		try {
			user = betsMng.makeBet(raceData, locale, list, user);
			request.getSession().setAttribute(PageManager.USER, user);
			return PRGManager.TO_BETS;
		} catch (ManagerException e) {
			LOGGER.error("Error. User can't make bet. Error code = " + e.getMessage());
			return PRGManager.TO_BETS_ERROR + e.getMessage();
		}
	}

	/**
	 * Sets the bets manager.
	 *
	 * @param betMng
	 *            the new bets manager
	 */
	public void setBetsManager(BetsManager betMng) {
		this.betsMng = betMng;
	}
}