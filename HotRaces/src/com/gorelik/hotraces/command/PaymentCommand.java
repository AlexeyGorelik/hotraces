package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.payment.PaymentData;
import com.gorelik.hotraces.logic.payment.PaymentManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;
import com.gorelik.hotraces.validation.PaymentValidator;

/**
 * The Class PaymentCommand. The class provides a process for user.
 */
public class PaymentCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(PaymentCommand.class);

	/** The payment manager. */
	private PaymentManager payManager = new PaymentManager();

	@Override
	public String execute(HttpServletRequest request) {
		PaymentData payData = new PaymentData(request.getParameter(ParamManagerJSP.PAYMENT_CVV_CODE),
				request.getParameter(ParamManagerJSP.PAYMENT_SUM),
				request.getParameter(ParamManagerJSP.PAYMENT_CARD_MONTH),
				request.getParameter(ParamManagerJSP.PAYMENT_CARD_YEAR),
				request.getParameter(ParamManagerJSP.PAYMENT_CARD_NUM1),
				request.getParameter(ParamManagerJSP.PAYMENT_CARD_NUM2),
				request.getParameter(ParamManagerJSP.PAYMENT_CARD_NUM3),
				request.getParameter(ParamManagerJSP.PAYMENT_CARD_NUM4));

		PaymentValidator payValidation = new PaymentValidator();
		payValidation.validate(payData);

		User user = (User) request.getSession().getAttribute(PageManager.USER);

		request.setAttribute(PageManager.REDIRECT, true);
		try {
			request.getSession().setAttribute(PageManager.USER, payManager.pay(payData, user));
			return PRGManager.TO_PAYMENT;
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't make payment. Error code = " + e.getMessage());
			return PRGManager.TO_PAYMENT_ERROR + e.getMessage();
		}
	}

	/**
	 * Sets the payment manager.
	 *
	 * @param payMng
	 *            the new payment manager
	 */
	public void setPaymentManager(PaymentManager payMng) {
		this.payManager = payMng;
	}
}
