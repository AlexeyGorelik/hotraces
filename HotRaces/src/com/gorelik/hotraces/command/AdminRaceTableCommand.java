package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.admin.AdminRaceTableManager;
import com.gorelik.hotraces.pageerror.ErrorCodeManager;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class AdminRaceTableCommand. The class provides a process for
 * administrator.
 */
public class AdminRaceTableCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(AdminRaceTableCommand.class);

	/** The administrator race table manager. */
	private AdminRaceTableManager adminRaceTableManager = new AdminRaceTableManager();

	@Override
	public String execute(HttpServletRequest request) {
		if (request.getParameter(HotRacesErrorCode.ERROR_ATTR) != null) {
			if (!request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString().isEmpty()) {
				request.setAttribute(ParamManagerJSP.RACE_ERROR, ErrorCodeManager
						.getErrorMessage(request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString()));
			}
		}

		try {
			request.setAttribute(ParamManagerJSP.RACE_LIST_EXIST_PROP, true);
			request.setAttribute(ParamManagerJSP.RACE_LIST_PROP, adminRaceTableManager.createAdminRaceTable());
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't load admin race table. Error code = " + e.getMessage());
			request.setAttribute(ParamManagerJSP.RACE_ERROR, ErrorCodeManager.getErrorMessage(e.getMessage()));
		}
		return PageManager.PAGE_ADMIN_RACE_TABLE_PROP;
	}

	/**
	 * Sets the race table manager.
	 *
	 * @param tableMng
	 *            the new race table manager
	 */
	public void setRaceTableManager(AdminRaceTableManager tableMng) {
		this.adminRaceTableManager = tableMng;
	}

}
