package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToAboutUsPageCommand. The class provides a process for
 * administrator.
 */
public class ToAboutUsPageCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_ABOUT_US);
		return PageManager.PAGE_TO_ABOUT_US_PROP;
	}

}
