package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.entity.RoleEnum;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.message.MessageManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToNewMessageCommand. The class provides a process for user.
 */
public class ToNewMessageCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(ToNewMessageCommand.class);

	/** The message manager. */
	private MessageManager messageMng = new MessageManager();

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute(PageManager.USER);
		RoleEnum role = RoleEnum.valueOf(user.getRole().toUpperCase());
		request.setAttribute(ParamManagerJSP.NEW_MESSAGE_PARAM, true);

		try {
			request.setAttribute(ParamManagerJSP.NEW_MESSAGE_USER_LIST_PARAM, messageMng.findUserList());
			switch (role) {
			case ADMIN:
				return PageManager.PAGE_PROFILE_ADMIN_PROP;
			case BOOKMAKER:
				return PageManager.PAGE_PROFILE_BOOKMAKER_PROP;
			case CLIENT:
				return PageManager.PAGE_PROFILE_CLIENT_PROP;
			default:
				return PageManager.PAGE_MAIN_PROP;
			}
		} catch (ManagerException e) {
			LOGGER.error("Can't load messages list with user. Error code = " + e.getMessage());
			request.setAttribute(PageManager.REDIRECT, true);
			return PRGManager.TO_PROFILE_MESSAGES_ERROR + e.getMessage();
		}
	}

	/**
	 * Sets the message mng.
	 *
	 * @param messageMng
	 *            the new message mng
	 */
	public void setMessageMng(MessageManager messageMng) {
		this.messageMng = messageMng;
	}
}