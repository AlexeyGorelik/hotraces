package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.profile.ProfileChangeData;
import com.gorelik.hotraces.logic.profile.ProfileChangeManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;
import com.gorelik.hotraces.validation.ProfileChangeValidator;

/**
 * The Class ProfileChangeCommand. The class provides a process for
 * administrator.
 */
public class ProfileChangeCommand implements ActionCommand {
	/** The profile change manager. */
	private ProfileChangeManager profChangeManager = new ProfileChangeManager();

	@Override
	public String execute(HttpServletRequest request) {
		ProfileChangeData profChangeData = new ProfileChangeData(
				request.getParameter(ParamManagerJSP.CHANGE_FIRST_NAME),
				request.getParameter(ParamManagerJSP.CHANGE_SECOND_NAME),
				request.getParameter(ParamManagerJSP.CHANGE_NICKNAME),
				request.getParameter(ParamManagerJSP.CHANGE_EMAIL));

		User user = (User) request.getSession().getAttribute(PageManager.USER);
		ProfileChangeValidator profChangeValid = new ProfileChangeValidator();
		profChangeValid.checkChangeValidation(profChangeData);

		request.setAttribute(PageManager.REDIRECT, true);
		try {
			request.getSession().setAttribute(PageManager.USER, profChangeManager.changeUserData(user, profChangeData));
			return PRGManager.TO_PROFILE;
		} catch (ManagerException e) {
			return PRGManager.TO_PROFILE_ERROR + e.getMessage();
		}
	}

	/**
	 * Sets the profile change manager.
	 *
	 * @param profMng
	 *            the new profile change manager
	 */
	public void setProfileChangeManager(ProfileChangeManager profMng) {
		this.profChangeManager = profMng;
	}
}
