package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.pageerror.ErrorCodeManager;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToPaymentCommand. The class provides a process for user.
 */
public class ToPaymentCommand implements ActionCommand {
	/**
	 * The Constant MONEY_ADDED. Message that user has successfully added money
	 * on account.
	 */
	private static final String MONEY_ADDED = "Money has successfully added on your account.";

	@Override
	public String execute(HttpServletRequest request) {
		if (request.getParameter(HotRacesErrorCode.ERROR_ATTR) != null) {
			if (!request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString().isEmpty()) {
				request.setAttribute(ParamManagerJSP.TO_PAYMENT_ERROR, ErrorCodeManager
						.getErrorMessage(request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString()));
			} else {
				request.setAttribute(ParamManagerJSP.TO_PAYMENT_MESSAGE, MONEY_ADDED);
			}
		}
		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_PAYMENT);
		return PageManager.PAGE_MY_PAYMENT_PROP;
	}

}
