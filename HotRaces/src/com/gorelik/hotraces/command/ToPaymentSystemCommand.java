package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToPaymentSystemCommand. The class provides a process for user.
 */
public class ToPaymentSystemCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_PAYMENT_SYSTEM);
		return PageManager.PAGE_PAYMENT_SYSTEM_PROP;
	}

}
