package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.horse.HorseManager;
import com.gorelik.hotraces.logic.race.RaceData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToPrevRaceDescriptionCommand. The class provides a process for
 * user.
 */
public class ToPrevRaceDescriptionCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(ToPrevRaceDescriptionCommand.class);

	@Override
	public String execute(HttpServletRequest request) {
		String[] raceDataArr = request.getParameter(ParamManagerJSP.SELECTED_PREV_ROW_VALUE).toString()
				.split(ParamManagerJSP.SELECTED_ROW_DELIMETED);
		RaceData raceData = null;
		try {
			raceData = new RaceData(raceDataArr);
		} catch (ArrayIndexOutOfBoundsException e) {
			request.setAttribute(PageManager.REDIRECT, true);
			return PRGManager.TO_BETS_ERROR + HotRacesErrorCode.EMTY_PREV_RACE_DESCR;
		}
		String locale = request.getSession().getAttribute(PageManager.LOCALE).toString();

		HorseManager horseMng = new HorseManager();
		try {
			request.setAttribute(ParamManagerJSP.HORSE_LIST, horseMng.findPrevRaceData(raceData, locale));
			request.setAttribute(ParamManagerJSP.RACE_NAME_TO_JSP, raceData.getName());
			request.setAttribute(ParamManagerJSP.RACE_DATE_TO_JSP, raceData.getDate());
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't load previous race description. Error code = " + e.getMessage());
			request.setAttribute(PageManager.REDIRECT, true);
			return PRGManager.TO_BETS_ERROR + e.getMessage();
		}
		return PageManager.PREV_RACE_DESCRIPTION_PROP;
	}
}