package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.bets.BetsManager;
import com.gorelik.hotraces.logic.client.ClientManager;
import com.gorelik.hotraces.logic.race.RaceEnum;
import com.gorelik.hotraces.pageerror.ErrorCodeManager;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToBetsCommand. The class provides a process for user.
 */
public class ToBetsCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(ToBetsCommand.class);

	@Override
	public String execute(HttpServletRequest request) {
		if (request.getParameter(HotRacesErrorCode.ERROR_ATTR) != null) {
			if (!request.getParameter(HotRacesErrorCode.ERROR_ATTR).isEmpty()) {
				request.setAttribute(ParamManagerJSP.BET_ERROR,
						ErrorCodeManager.getErrorMessage(request.getParameter(HotRacesErrorCode.ERROR_ATTR)));
			}
		}

		BetsManager betsMng = new BetsManager();
		ClientManager clientMng = new ClientManager();
		try {
			for (RaceEnum temp : RaceEnum.values()) {
				request.setAttribute(temp.getEngValue() + ParamManagerJSP.LIST_PARAM, betsMng.getRacesToBets(temp));
			}
			request.setAttribute(ParamManagerJSP.PREV_RACE_TABLE, clientMng.findPrevRaceList());
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't load race tables to make bet. Error code = " + e.getMessage());
			request.setAttribute(ParamManagerJSP.BET_ERROR,
					ErrorCodeManager.getErrorMessage(request.getParameter(e.getMessage())));
		}
		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_BETS);
		return PageManager.PAGE_TO_BETS_PROP;
	}
}
