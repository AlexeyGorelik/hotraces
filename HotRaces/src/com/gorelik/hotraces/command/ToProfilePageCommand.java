package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.entity.RoleEnum;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.pageerror.ErrorCodeManager;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToProfilePageCommand. The class provides a process for user.
 */
public class ToProfilePageCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		if (request.getParameter(HotRacesErrorCode.ERROR_ATTR) != null) {
			if (!request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString().isEmpty()) {
				request.setAttribute(ParamManagerJSP.PROFILE_ERROR, ErrorCodeManager
						.getErrorMessage(request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString()));
			}
		}
		User user = (User) request.getSession().getAttribute(PageManager.USER);
		String page = null;
		if (user == null) {
			return PageManager.PAGE_MAIN_PROP;
		}
		RoleEnum role = RoleEnum.valueOf(user.getRole().toUpperCase());
		switch (role) {
		case ADMIN:
			page = PageManager.PAGE_PROFILE_ADMIN_PROP;
			break;
		case BOOKMAKER:
			page = PageManager.PAGE_PROFILE_BOOKMAKER_PROP;
			break;
		case CLIENT:
			page = PageManager.PAGE_PROFILE_CLIENT_PROP;
			break;
		default:
			page = PageManager.PAGE_MAIN_PROP;
			break;
		}
		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_PROFILE);
		return page;
	}
}
