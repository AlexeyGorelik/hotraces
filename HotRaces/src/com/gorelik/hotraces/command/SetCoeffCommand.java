package com.gorelik.hotraces.command;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.bookmaker.BookmakerSetCoeffData;
import com.gorelik.hotraces.logic.race.BookmakerRaceManager;
import com.gorelik.hotraces.logic.race.RaceData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class SetCoeffCommand. The class provides a process for bookmaker.
 */
public class SetCoeffCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(SetCoeffCommand.class);

	/** The set coefficient manager. */
	private BookmakerRaceManager bookRaceManager = new BookmakerRaceManager();

	@Override
	public String execute(HttpServletRequest request) {
		String raceName = request.getParameter(ParamManagerJSP.RACE_NAME);
		String raceDate = request.getParameter(ParamManagerJSP.RACE_DATE);
		RaceData raceData = new RaceData(raceName, raceDate);
		String locale = request.getSession().getAttribute(PageManager.LOCALE).toString();
		int horseCount = Integer.valueOf(request.getParameter(ParamManagerJSP.HORSE_COUNT));

		List<BookmakerSetCoeffData> inputList = new ArrayList<BookmakerSetCoeffData>();

		for (int i = 1; i <= horseCount; i++) {
			if (request.getParameter(ParamManagerJSP.COEFF_PREF + i).isEmpty()) {
				return PRGManager.TO_COEFF_TABLE_ERROR + HotRacesErrorCode.BOOKMAKER_EMPTY_COEFF;
			}
			BookmakerSetCoeffData temp = new BookmakerSetCoeffData(
					Integer.valueOf(request.getParameter(ParamManagerJSP.HORSE_ID_PREF + i)),
					request.getParameter(ParamManagerJSP.HORSE_NAME_PREF + i),
					BigDecimal.valueOf(Double.valueOf(request.getParameter(ParamManagerJSP.COEFF_PREF + i))));
			inputList.add(temp);
		}

		request.setAttribute(PageManager.REDIRECT, true);
		try {
			bookRaceManager.setCoeffInRace(raceData, locale, inputList);
			return PRGManager.TO_COEFF_TABLE;
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't set coeff in race. Error code = " + e.getMessage());
			return PRGManager.TO_PROFILE_ERROR + e.getMessage();
		}
	}

	/**
	 * Sets the bookmaker race manager.
	 *
	 * @param setMng
	 *            the new bookmaker race manager
	 */
	public void setRaceManager(BookmakerRaceManager setMng) {
		this.bookRaceManager = setMng;
	}
}
