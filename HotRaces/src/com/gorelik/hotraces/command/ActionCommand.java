package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

/**
 * The Interface ActionCommand. Main interface for "command"-type classes.
 */
public interface ActionCommand {
	/**
	 * Executes the current command and returns a string containing the https
	 * address. Depending on the type of command, the address can be full (for
	 * redirection) or truncated (forwards). For example:
	 * "/jsp/main/hotraces.jsp" - forward, "/HotRaces/jsp/main/hotraces.jsp" -
	 * redirect.
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return adress of jsp-page.
	 */
	public String execute(HttpServletRequest request);
}