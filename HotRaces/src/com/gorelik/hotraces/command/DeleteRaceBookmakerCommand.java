package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.race.BookmakerRaceManager;
import com.gorelik.hotraces.logic.race.RaceData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class DeleteRaceBookmakerCommand. The class provides a process for
 * bookmaker.
 */
public class DeleteRaceBookmakerCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(DeleteRaceBookmakerCommand.class);

	/** The race manager. */
	private BookmakerRaceManager raceMng = new BookmakerRaceManager();

	@Override
	public String execute(HttpServletRequest request) {
		String raceDataArr[] = request.getParameter(ParamManagerJSP.SELECTED_ROW_VALUE)
				.split(ParamManagerJSP.SELECTED_ROW_DELIMETED);
		RaceData raceData = null;
		try {
			raceData = new RaceData(raceDataArr);
		} catch (ArrayIndexOutOfBoundsException e) {
			return PRGManager.TO_PROFILE_ERROR + HotRacesErrorCode.BOOKMAKER_EMPTY_RACE_TO_DELETE;
		}
		String locale = request.getSession().getAttribute(PageManager.LOCALE).toString();

		try {
			raceMng.deleteRace(raceData, locale);
			return PRGManager.TO_PROFILE;
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't delete race in bookmaker profile. Error code = " + e.getMessage());
			return PRGManager.TO_PROFILE_ERROR + e.getMessage();
		}
	}

	/**
	 * Sets the race manager.
	 *
	 * @param raceManager
	 *            the new race manager
	 */
	public void setDeleteManager(BookmakerRaceManager raceManager) {
		this.raceMng = raceManager;
	}
}