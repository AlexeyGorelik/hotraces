package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.message.MessageManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class SendMessageCommand. The class provides a process for user.
 */
public class SendMessageCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(SendMessageCommand.class);

	/** The message manager. */
	private MessageManager messageMng = new MessageManager();

	@Override
	public String execute(HttpServletRequest request) {
		String text = request.getParameter(ParamManagerJSP.MESSAGE_TEXT).toString();
		String userName = request.getParameter(ParamManagerJSP.MESSAGE_USER_TO_NAME).toString();
		User user = (User) request.getSession().getAttribute(PageManager.USER);

		request.setAttribute(PageManager.REDIRECT, true);
		try {
			messageMng.sendMessage(Integer.valueOf(user.getUserId()), userName, text);
			return PRGManager.TO_WRITE_MESSAGE + userName + "#history-with";
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't send message to user. Error code = " + e.getMessage());
			return PRGManager.TO_PROFILE_MESSAGES_ERROR + e.getMessage();
		}
	}

	/**
	 * Sets the message mng.
	 *
	 * @param messageMng
	 *            the new message mng
	 */
	public void setMessageMng(MessageManager messageMng) {
		this.messageMng = messageMng;
	}
}