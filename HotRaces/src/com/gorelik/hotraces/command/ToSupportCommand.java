package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToSupportCommand. The class provides a process for user.
 */
public class ToSupportCommand implements ActionCommand {
	/** The Constant SUCCESS. */
	private static final String SUCCESS = "success";

	/** The Constant ERROR. Tag name on jsp. */
	private static final String ERROR = "error";

	@Override
	public String execute(HttpServletRequest request) {
		if (request.getParameter(SUCCESS) != null) {
			if (!request.getParameter(SUCCESS).toString().isEmpty()) {
				request.setAttribute(SUCCESS, true);
			}
		} else if (request.getParameter(ERROR) != null) {
			if (!request.getParameter(ERROR).toString().isEmpty()) {
				request.setAttribute(ERROR, true);
			}
		}
		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_SUPPORT);
		return PageManager.PAGE_SUPPORT_PROP;
	}

}
