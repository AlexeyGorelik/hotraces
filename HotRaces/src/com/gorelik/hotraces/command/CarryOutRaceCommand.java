package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.race.AdminRaceManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class CarryOutRaceCommand. The class provides a process for
 * administrator.
 */
public class CarryOutRaceCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(CarryOutRaceCommand.class);

	/** The race manager. */
	private AdminRaceManager raceManager = new AdminRaceManager();

	@Override
	public String execute(HttpServletRequest request) {
		String[] raceDataArr = request.getParameter(ParamManagerJSP.SELECTED_ROW_VALUE)
				.split(ParamManagerJSP.SELECTED_ROW_DELIMETED);
		String locale = request.getSession().getAttribute(PageManager.LOCALE).toString();

		request.setAttribute(PageManager.REDIRECT, true);
		try {
			raceManager.carryOutRace(raceDataArr, locale);
			return PRGManager.TO_ADMIN_RACE_TABLE;
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't carry out race. Error code = " + e.getMessage());
			return PRGManager.TO_ADMIN_RACE_TABLE_ERROR + e.getMessage();
		}
	}

	/**
	 * Sets the race manager.
	 *
	 * @param raceMng
	 *            the new race manager
	 */
	public void setRaceManager(AdminRaceManager raceMng) {
		this.raceManager = raceMng;
	}
}