package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.support.SupportManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;

/**
 * The Class ToAdminSupportCommand. The class provides a process for
 * administrator.
 */
public class ToAdminSupportCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		SupportManager suppMng = new SupportManager();
		try {
			request.setAttribute("supportList", suppMng.findSuppMessageList());
			request.setAttribute("suppMsgListExist", true);
			return PageManager.ADMIN_SUPPORT_PROP;
		} catch (ManagerException e) {
			return PRGManager.TO_PROFILE_ERROR + e.getMessage();
		}
	}

}
