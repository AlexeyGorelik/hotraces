package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.resource.PageManager;

/**
 * The Class ProfileCommand. The class provides a process for user.
 */
public class ProfileCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		request.setAttribute(PageManager.REDIRECT, true);
		return PageManager.PAGE_PROFILE_CLIENT_FULL_PROP;
	}

}
