package com.gorelik.hotraces.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.entity.Bet;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.profile.PreviousBetManager;
import com.gorelik.hotraces.pageerror.ErrorCodeManager;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class PreviousBetCommand. The class provides a process for user.
 */
public class PreviousBetCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(PreviousBetCommand.class);

	/** The previous bet manager. */
	private PreviousBetManager prevBetMng = new PreviousBetManager();

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute(PageManager.USER);
		try {
			List<Bet> list = prevBetMng.getPreviousBets(user);
			if (list != null && !list.isEmpty()) {
				request.setAttribute(ParamManagerJSP.PREV_BETS_LIST, list);
			} else {
				request.setAttribute(ParamManagerJSP.PREV_BETS_NO_BETS_BEFORE, true);
			}
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't show previous bets. Error code = " + e.getMessage());
			request.setAttribute(HotRacesErrorCode.ERROR_PROFILE_CLIENT,
					ErrorCodeManager.getErrorMessage(e.getMessage()));
		}
		return PageManager.PAGE_PROFILE_CLIENT_PROP;
	}

	/**
	 * Sets the prev manager.
	 *
	 * @param prevMng
	 *            the new prev manager
	 */
	public void setPrevManager(PreviousBetManager prevMng) {
		this.prevBetMng = prevMng;
	}

}
