package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.race.AdminRaceManager;
import com.gorelik.hotraces.logic.race.RaceData;
import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class CreateRaceCommand. The class provides a process for administrator.
 */
public class CreateRaceCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(CreateRaceCommand.class);

	/** The create manager. Manager that creates race. */
	private AdminRaceManager raceMng = new AdminRaceManager();

	@Override
	public String execute(HttpServletRequest request) {
		String raceName = request.getParameter(ParamManagerJSP.RACE_NAME_PARAM_VALUE);
		String raceDate = request.getParameter(ParamManagerJSP.RACE_DATE_PARAM_VALUE);
		RaceData raceData = new RaceData(raceName, raceDate);
		String[] addedHorsesArr = request.getParameterValues(ParamManagerJSP.ADD_PARAM_VALUE);
		String[] horsesId = new String[addedHorsesArr.length];
		String[] horsesName = new String[addedHorsesArr.length];
		for (int i = 0; i < addedHorsesArr.length; i++) {
			horsesId[i] = request.getParameter(addedHorsesArr[i] + ParamManagerJSP.POSTFIX_ID);
			horsesName[i] = request.getParameter(addedHorsesArr[i] + ParamManagerJSP.POSTFIX_NAME);
		}

		request.setAttribute(PageManager.REDIRECT, true);
		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_ADMIN_RACE_TABLE);
		try {
			raceMng.createRace(horsesId, horsesName, raceData,
					request.getSession().getAttribute(PageManager.LOCALE).toString());
			return PRGManager.TO_ADMIN_RACE_TABLE;
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't create race. Error code = " + e.getMessage());
			return PRGManager.TO_ADMIN_RACE_TABLE_ERROR + e.getMessage();
		}
	}

	/**
	 * Sets the creates the race manager.
	 *
	 * @param createMng
	 *            the new creates the race manager
	 */
	public void setCreateRaceManager(AdminRaceManager raceMng) {
		this.raceMng = raceMng;
	}
}
