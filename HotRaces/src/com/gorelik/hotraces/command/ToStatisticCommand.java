package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.statistic.StatisticManager;
import com.gorelik.hotraces.pageerror.ErrorCodeManager;
import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToStatisticCommand. The class provides a process for administrator.
 */
public class ToStatisticCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		StatisticManager statMng = new StatisticManager();
		try {
			request.setAttribute(ParamManagerJSP.STAT_BETS_TODAY, statMng.findTodayBetsCount());
			request.setAttribute(ParamManagerJSP.STAT_BETS_MONTH, statMng.findMonthBetsCount());
			request.setAttribute(ParamManagerJSP.STAT_INCOME_TODAY, statMng.findTodayIncome());
			request.setAttribute(ParamManagerJSP.STAT_INCOME_MONTH, statMng.findMonthIncome());
			request.setAttribute(ParamManagerJSP.STAT_RACE_TODAY, statMng.findTodayRaceCount());
			request.setAttribute(ParamManagerJSP.STAT_RACE_MONTH, statMng.findMonthRaceCount());
			request.setAttribute(ParamManagerJSP.STAT_USER_COUNT, statMng.findUserThisMonthCount());
			request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_STATISTIC);
		} catch (ManagerException e) {
			request.setAttribute(ParamManagerJSP.STATISTIC_ERROR, ErrorCodeManager.getErrorMessage(e.getMessage()));
		}
		return PageManager.ADMIN_STATISTIC_PROP;
	}
}