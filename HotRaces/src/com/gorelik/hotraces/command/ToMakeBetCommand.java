package com.gorelik.hotraces.command;

import java.time.format.DateTimeParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.bets.BetsManager;
import com.gorelik.hotraces.logic.bets.MakeBetData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToMakeBetCommand. The class provides a process for user.
 */
public class ToMakeBetCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(ToMakeBetCommand.class);

	/** The bets manager. */
	private BetsManager betsMng = new BetsManager();

	@Override
	public String execute(HttpServletRequest request) {
		String locale = request.getSession().getAttribute(PageManager.LOCALE).toString();
		String raceName = request.getParameter(ParamManagerJSP.HIPP_COMMON_NAME).toString();
		String raceDate = request.getParameter(ParamManagerJSP.SELECTED_ROW_VALUE).toString();
		try {
			raceDate = PageManager.formatDate(raceDate, locale);
		} catch (DateTimeParseException e) {
			return PRGManager.TO_BETS_ERROR_FORWARD + HotRacesErrorCode.MAKE_BET_EMPTY_RACE;
		}

		try {
			List<MakeBetData> list = betsMng.findMakeBetData(raceName, raceDate, locale);
			request.setAttribute(ParamManagerJSP.BET_LIST, list);
			request.setAttribute(ParamManagerJSP.BET_COUNT, list.size());

			request.setAttribute(ParamManagerJSP.RACE_NAME_TO_JSP, raceName);
			request.setAttribute(ParamManagerJSP.RACE_DATE_TO_JSP, raceDate);

			return PageManager.PAGE_TO_MAKE_BET_PROP;
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't load make bet data. Error code = " + e.getMessage());
			return PRGManager.TO_BETS_ERROR_FORWARD + e.getMessage();
		}
	}

	/**
	 * Sets the bets manager.
	 *
	 * @param betsMng
	 *            the new bets manager
	 */
	public void setBetsManager(BetsManager betsMng) {
		this.betsMng = betsMng;
	}
}
