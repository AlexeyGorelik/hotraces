package com.gorelik.hotraces.command;

/**
 * The Enumeration ChangeUserStatusCommand. The enumeration provides a process
 * for command.
 */
public enum CommandType {
	LOGIN {
		{
			this.command = new LoginCommand();
		}
	},

	LOCALE {
		{
			this.command = new LocaleCommand();
		}
	},

	LOGOUT {
		{
			this.command = new LogoutCommand();
		}
	},
	REGISTRATION {
		{
			this.command = new RegistrationCommand();
		}
	},
	MYPROFILE {
		{
			this.command = new ProfileCommand();
		}
	},
	PROFILEPREVIOUSBET {
		{
			this.command = new PreviousBetCommand();
		}
	},
	PROFILEMESSAGES {
		{
			this.command = new ToMessageCommand();
		}
	},
	SENDMESSAGE {
		{
			this.command = new SendMessageCommand();
		}
	},
	SENDNEWMESSAGE {
		{
			this.command = new SendNewMessageCommand();
		}
	},
	CHANGEPROFILE {
		{
			this.command = new ProfileChangeCommand();
		}
	},
	UPLOADAVATAR {
		{
			this.command = new UploadAvatarCommand();
		}
	},
	PAYMENT {
		{
			this.command = new PaymentCommand();
		}
	},
	ADMINUSERTABLE {
		{
			this.command = new AdminUserTableCommand();
		}
	},
	ADMINRACETABLE {
		{
			this.command = new AdminRaceTableCommand();
		}
	},
	CARRYOUTRACE {
		{
			this.command = new CarryOutRaceCommand();
		}
	},
	CREATERACE {
		{
			this.command = new CreateRaceCommand();
		}
	},
	EDITRACE {
		{
			this.command = new EditRaceCommand();
		}
	},
	DELETERACE {
		{
			this.command = new AdminDeleteRaceCommand();
		}
	},
	DELETERACEBOOKMAKER {
		{
			this.command = new DeleteRaceBookmakerCommand();
		}
	},
	SENDQUESTION {
		{
			this.command = new SendSuppMessageCommand();
		}
	},
	SETCOEFF {
		{
			this.command = new SetCoeffCommand();
		}
	},
	CHANGEUSERPASSWORD {
		{
			this.command = new ChangeUserPasswordCommand();
		}
	},
	BANUSER {
		{
			this.command = new ChangeUserStatusCommand();
		}
	},
	UNBANUSER {
		{
			this.command = new ChangeUserStatusCommand();
		}
	},
	MAKEBET {
		{
			this.command = new MakeBetCommand();
		}
	},
	TOPROFILE {
		{
			this.command = new ToProfilePageCommand();
		}
	},
	TOLOGIN {
		{
			this.command = new ToLoginPageCommand();
		}
	},
	TOREGISTRATION {
		{
			this.command = new ToRegistrationPageCommand();
		}
	},
	TOBETS {
		{
			this.command = new ToBetsCommand();
		}
	},
	TOMAKEBET {
		{
			this.command = new ToMakeBetCommand();
		}
	},
	TOLIVE {
		{
			this.command = new ToLiveCommand();
		}
	},
	TOCONTACT {
		{
			this.command = new ToContactPageCommand();
		}
	},
	TOABOUTUS {
		{
			this.command = new ToAboutUsPageCommand();
		}
	},
	TOPAYMENT {
		{
			this.command = new ToPaymentCommand();
		}
	},
	TOHORSETABLE {
		{
			this.command = new ToHorseTableCommand();
		}
	},
	TOCREATERACE {
		{
			this.command = new ToCreateRaceCommand();
		}
	},
	TOEDITRACE {
		{
			this.command = new ToEditRaceCommand();
		}
	},
	TOPREVRACEDESCRIPTION {
		{
			this.command = new ToPrevRaceDescriptionCommand();
		}
	},
	TOSETCOEFF {
		{
			this.command = new ToSetCoeffCommand();
		}
	},
	TOCOEFFTABLE {
		{
			this.command = new ToCoeffTableCommand();
		}
	},
	TOWRITEMESSAGE {
		{
			this.command = new ToWriteMessageCommand();
		}
	},
	TONEWMESSAGE {
		{
			this.command = new ToNewMessageCommand();
		}
	},
	TOHIPPODROMDESCRIPTION {
		{
			this.command = new ToHippodromDescriptionCommand();
		}
	},
	TOFAQ {
		{
			this.command = new ToFaqCommand();
		}
	},
	TOCHANGEPROFILE {
		{
			this.command = new ToProfileChangeCommand();
		}
	},
	TOSTATISTIC {
		{
			this.command = new ToStatisticCommand();
		}
	},
	TOPAYMENTSYSTEM {
		{
			this.command = new ToPaymentSystemCommand();
		}
	},
	TOHOWTOPLAY {
		{
			this.command = new ToHowToPlayCommand();
		}
	},
	TOFORGOTPASSWORD {
		{
			this.command = new ToForgotPasswordCommand();
		}
	},
	TOSUPPORT {
		{
			this.command = new ToSupportCommand();
		}
	},
	TOADMINSUPPORT {
		{
			this.command = new ToAdminSupportCommand();
		}
	},
	TOMAIN {
		{
			this.command = new ToMainPageCommand();
		}
	};

	ActionCommand command;

	public ActionCommand getCurrentCommand() {
		return command;
	}
}