package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.race.AdminRaceManager;
import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToCreateRaceCommand. The class provides a process for
 * administrator.
 */
public class ToCreateRaceCommand implements ActionCommand {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(ToCreateRaceCommand.class);

	@Override
	public String execute(HttpServletRequest request) {
		request.setAttribute(ParamManagerJSP.MIN_HORSE_COUNT_ATTR, ParamManagerJSP.MIN_HORSE_COUNT_VAL);
		request.setAttribute(ParamManagerJSP.MAX_HORSE_COUNT_ATTR, ParamManagerJSP.MAX_HORSE_COUNT_VAL);
		AdminRaceManager raceMng = new AdminRaceManager();
		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_CREATE_RACE);
		try {
			request.setAttribute(ParamManagerJSP.HORSE_IN_RACE_LIST, raceMng.findAllHorses());
			request.setAttribute(ParamManagerJSP.HIPPODROME_LIST, raceMng.findAllHippodromes());
			return PageManager.PAGE_ADMIN_CREATE_RACE_PROP;
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't load create race data. Error code = " + e.getMessage());
			return PRGManager.TO_ADMIN_RACE_TABLE_ERROR + e.getMessage();
		}

	}

}