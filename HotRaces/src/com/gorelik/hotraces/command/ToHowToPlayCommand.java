package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToHowToPlayCommand. The class provides a process for user.
 */
public class ToHowToPlayCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_HOW_TO_PLAY);
		return PageManager.PAGE_HOW_TO_PLAY_PROP;
	}

}
