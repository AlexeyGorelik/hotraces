package com.gorelik.hotraces.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.entity.Horse;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.race.AdminRaceManager;
import com.gorelik.hotraces.logic.race.RaceData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToEditRaceCommand. The class provides a process for administrator.
 */
public class ToEditRaceCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(ToEditRaceCommand.class);

	@Override
	public String execute(HttpServletRequest request) {
		String[] raceDataArr = request.getParameter(ParamManagerJSP.SELECTED_ROW_VALUE)
				.split(ParamManagerJSP.SELECTED_ROW_DELIMETED);
		RaceData raceData = null;
		try {
			raceDataArr[1] = PageManager.formatDate(raceDataArr[1],
					request.getSession().getAttribute(PageManager.LOCALE).toString());
			raceData = new RaceData(raceDataArr);
		} catch (IndexOutOfBoundsException e) {
			request.setAttribute(PageManager.REDIRECT, true);
			return PRGManager.TO_ADMIN_RACE_TABLE_ERROR + HotRacesErrorCode.ADMIN_EDIT_EMPTY_RACE;
		}

		AdminRaceManager raceMng = new AdminRaceManager();
		try {
			List<Horse> horseInRaceList = raceMng.createEditHorseList(raceData);
			request.setAttribute(ParamManagerJSP.RACE_NAME_TO_JSP, raceData.getName());
			request.setAttribute(ParamManagerJSP.RACE_DATE_TO_JSP, raceData.getDate());
			request.setAttribute(ParamManagerJSP.HORSE_IN_RACE_LIST, horseInRaceList);

			List<Horse> otherHorseList = raceMng.findAllHorses();
			for (Horse temp : horseInRaceList) {
				otherHorseList.remove(temp);
			}

			request.setAttribute(ParamManagerJSP.EDIT_COMM_HORSE_TO_RACE_LIST, otherHorseList);
			request.setAttribute(ParamManagerJSP.MIN_HORSE_COUNT_ATTR, horseInRaceList.size() + 1);
			request.setAttribute(ParamManagerJSP.MAX_HORSE_COUNT_ATTR, ParamManagerJSP.MAX_HORSE_COUNT_VAL);
			return PageManager.PAGE_ADMIN_EDIT_RACE_PROP;
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't load table to edit race. Error code = " + e.getMessage());
			request.setAttribute(PageManager.REDIRECT, true);
			return PRGManager.TO_ADMIN_RACE_TABLE_ERROR + e.getMessage();
		}
	}

}
