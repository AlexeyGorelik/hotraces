package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class LocaleCommand. The class provides a process for user.
 */
public class LocaleCommand implements ActionCommand {

	/**
	 * The Constant HOT_RACES_SERVLET. Contains first part of the path to jsp
	 * page.
	 */
	private static final String HOT_RACES_SERVLET = "/HotRaces/HotRacesServlet?";

	@Override
	public String execute(HttpServletRequest request) {
		if (request.getSession().getAttribute(ParamManagerJSP.LOCALE_HREF) == null) {
			request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_MAIN);
		}
		request.getSession().setAttribute(PageManager.LOCALE, request.getParameter(PageManager.LOCALE).toString());
		request.setAttribute(PageManager.REDIRECT, true);
		return HOT_RACES_SERVLET + request.getSession().getAttribute(ParamManagerJSP.LOCALE_HREF).toString();
	}

}
