package com.gorelik.hotraces.command;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.entity.SupportMessage;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.support.SupportManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class SendSuppMessageCommand. The class provides a process for user.
 */
public class SendSuppMessageCommand implements ActionCommand {
	/** The Constant CURRENT_TIME_FORMATTER. Format of data in DB. */
	private static final String CURRENT_TIME_FORMATTER = "yyyy-MM-dd HH:mm:ss";

	/** The support manager. */
	SupportManager suppMng = new SupportManager();

	@Override
	public String execute(HttpServletRequest request) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(CURRENT_TIME_FORMATTER);
		SupportMessage suppMessage = new SupportMessage(request.getParameter(ParamManagerJSP.SUPP_EMAIL),
				request.getParameter(ParamManagerJSP.SUPP_QUEST_TYPE), request.getParameter(ParamManagerJSP.SUPP_TEXT),
				LocalDateTime.now().format(formatter));

		request.setAttribute(PageManager.REDIRECT, true);
		try {
			suppMng.sendQuestion(suppMessage);
			return PRGManager.TO_SUPP_SUCCESS;
		} catch (ManagerException e) {
			return PRGManager.TO_SUPP_ERROR;
		}
	}

	/**
	 * Sets the support manager.
	 *
	 * @param suppMng
	 *            the new support manager
	 */
	public void setSupportManager(SupportManager suppMng) {
		this.suppMng = suppMng;
	}

}
