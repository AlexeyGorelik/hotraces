package com.gorelik.hotraces.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.race.AdminRaceManager;
import com.gorelik.hotraces.logic.race.EditRaceData;
import com.gorelik.hotraces.logic.race.RaceData;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class EditRaceCommand. The class provides a process for administrator.
 */
public class EditRaceCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(EditRaceCommand.class);

	/** The edit manager. Manager that edits race. */
	private AdminRaceManager raceMng = new AdminRaceManager();

	@Override
	public String execute(HttpServletRequest request) {
		List<EditRaceData> list = new ArrayList<EditRaceData>(ParamManagerJSP.MAX_HORSE_COUNT_VAL);

		String locale = request.getSession().getAttribute(PageManager.LOCALE).toString();
		String raceName = request.getParameter(ParamManagerJSP.RACE_NAME);
		String raceDate = request.getParameter(ParamManagerJSP.RACE_DATE);
		RaceData raceData = new RaceData(raceName, raceDate);
		int horseBefore = 0; // counter of horses in race before edit to check
								// it in manager
		String action;

		for (int i = 1; i <= ParamManagerJSP.MAX_HORSE_COUNT_VAL; i++) {
			action = request.getParameter(ParamManagerJSP.PREFIX_HORSE + i);
			if (action != null) {
				list.add(new EditRaceData(
						request.getParameter(ParamManagerJSP.PREFIX_HORSE + i + ParamManagerJSP.POSTFIX_ID),
						request.getParameter(ParamManagerJSP.PREFIX_HORSE + i + ParamManagerJSP.POSTFIX_NAME), action));
			} else {
				horseBefore++;
			}
		}

		request.setAttribute(PageManager.REDIRECT, true);
		try {
			raceMng.editRace(list, raceData, locale, horseBefore);
			return PRGManager.TO_ADMIN_RACE_TABLE;
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't edit race. Error code = " + e.getMessage());
			return PRGManager.TO_ADMIN_RACE_TABLE_ERROR + e.getMessage();
		}
	}

	/**
	 * Sets the edits the race manager.
	 *
	 * @param editMng
	 *            the new edits the race manager
	 */
	public void setEditRaceManager(AdminRaceManager raceMng) {
		this.raceMng = raceMng;
	}

}
