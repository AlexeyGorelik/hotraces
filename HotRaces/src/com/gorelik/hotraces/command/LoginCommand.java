package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.login.LoginData;
import com.gorelik.hotraces.logic.login.LoginManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;
import com.gorelik.hotraces.validation.AuthorizationValidator;

/**
 * The Class LoginCommand. The class provides a process for user.
 */
public class LoginCommand implements ActionCommand {

	/** The login manager. */
	private LoginManager logManager = new LoginManager();

	@Override
	public String execute(HttpServletRequest request) {
		String login = request.getParameter(ParamManagerJSP.PARAM_LOGIN);
		String password = request.getParameter(ParamManagerJSP.PARAM_PASSWORD);
		LoginData loginData = new LoginData(login, password);

		AuthorizationValidator authValidator = new AuthorizationValidator();
		authValidator.checkAuthentication(loginData);

		try {
			User user = logManager.login(loginData);
			request.setAttribute(PageManager.REDIRECT, true);
			request.getSession().setAttribute(PageManager.USER, user);
			return PRGManager.TO_MAIN;
		} catch (ManagerException e) {
			request.setAttribute(PageManager.REDIRECT, true);
			return PRGManager.TO_LOGIN_ERROR + e.getMessage();
		}
	}

	/**
	 * Sets the login manager.
	 *
	 * @param logMng
	 *            the new login manager
	 */
	public void setLoginManager(LoginManager logMng) {
		this.logManager = logMng;
	}
}