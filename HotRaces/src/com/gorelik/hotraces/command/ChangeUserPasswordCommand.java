package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.user.UserManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ChangeUserPasswordCommand. The class provides a process for user.
 */
public class ChangeUserPasswordCommand implements ActionCommand {

	private UserManager userMng = new UserManager();

	@Override
	public String execute(HttpServletRequest request) {
		String email = request.getParameter(ParamManagerJSP.FORG_PASS_EMAIL);

		request.setAttribute(PageManager.REDIRECT, true);
		try {
			return PRGManager.TO_FORG_PASSWORD + userMng.changePassword(email);
		} catch (ManagerException e) {
			return PRGManager.TO_FORG_PASSWORD_ERROR + e.getMessage();
		}
	}

}
