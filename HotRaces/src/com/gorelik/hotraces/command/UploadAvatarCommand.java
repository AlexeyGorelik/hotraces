package com.gorelik.hotraces.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.upload.UploadManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class UploadAvatarCommand. The class provides a process for user.
 */
public class UploadAvatarCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(UploadAvatarCommand.class);

	@Override
	public String execute(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute(PageManager.USER);
		UploadManager uplMng = new UploadManager();
		try {
			request.getSession().setAttribute(PageManager.USER, uplMng.uploadAvatar(
					request.getPart(ParamManagerJSP.AVATAR_FILE), user, request.getServletContext().getRealPath("\\")));
			return PRGManager.TO_PROFILE;
		} catch (IOException | ServletException e) {
			LOGGER.error("Error. Can't upload avatar. Some problems with file. " + e.getMessage());
			return PRGManager.TO_PROFILE_ERROR + e.getMessage();
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't upload avatar. Error code = " + e.getMessage());
			return PRGManager.TO_PROFILE_ERROR + e.getMessage();
		}
	}
}