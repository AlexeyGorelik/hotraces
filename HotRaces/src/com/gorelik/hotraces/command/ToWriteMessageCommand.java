package com.gorelik.hotraces.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.entity.Message;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.message.MessageManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToWriteMessageCommand. The class provides a process for user.
 */
public class ToWriteMessageCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(ToWriteMessageCommand.class);

	/** The message manager. */
	private MessageManager messageMng = new MessageManager();

	@Override
	public String execute(HttpServletRequest request) {
		String userTo = request.getParameter(ParamManagerJSP.USER_TO);
		User user = (User) request.getSession().getAttribute(PageManager.USER);

		List<Message> list;
		try {
			list = messageMng.findConversation(Integer.valueOf(user.getUserId()), userTo);
			request.setAttribute(ParamManagerJSP.TO_WRITE_MESSAGE_LIST, list);
			request.setAttribute(ParamManagerJSP.TO_WRITE_MESSAGE_WITH_USER, true);
			request.setAttribute(PageManager.USER, user);
			request.setAttribute(ParamManagerJSP.MESSAGE_USER_TO_NAME, userTo);
			return PRGManager.TO_PROFILE;
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't load dialog with user. Error code = " + e.getMessage());
			request.setAttribute(PageManager.REDIRECT, true);
			return PRGManager.TO_PROFILE_MESSAGES_ERROR + e.getMessage();
		}
	}

	/**
	 * Sets the message mng.
	 *
	 * @param messageMng
	 *            the new message mng
	 */
	public void setMessageMng(MessageManager messageMng) {
		this.messageMng = messageMng;
	}

}
