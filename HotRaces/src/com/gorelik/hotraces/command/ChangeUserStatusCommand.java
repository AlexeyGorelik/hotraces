package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.user.UserManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ChangeUserStatusCommand. The class provides a process for
 * administrator.
 */
public class ChangeUserStatusCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(ChangeUserStatusCommand.class);

	@Override
	public String execute(HttpServletRequest request) {
		UserManager userMng = new UserManager();
		request.setAttribute(PageManager.REDIRECT, true);
		try {
			userMng.changeUserStatus(request.getParameter(ParamManagerJSP.SELECTED_ROW_VALUE),
					request.getParameter(ParamManagerJSP.COMMAND));
			return PRGManager.TO_ADMIN_USER_TABLE;
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't change user status. Error code = " + e.getMessage());
			return PRGManager.TO_ADMIN_USER_TABLE_ERROR + e.getMessage();
		}
	}
}
