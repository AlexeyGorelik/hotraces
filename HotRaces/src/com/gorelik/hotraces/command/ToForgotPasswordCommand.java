package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.pageerror.ErrorCodeManager;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToForgotPasswordCommand. The class provides a process for user.
 */
public class ToForgotPasswordCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		if (request.getParameter(HotRacesErrorCode.ERROR_ATTR) != null) {
			if (!request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString().isEmpty()) {
				request.setAttribute(ParamManagerJSP.FORG_PASS_ERROR, ErrorCodeManager
						.getErrorMessage(request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString()));
			}
		}
		if (request.getParameter(ParamManagerJSP.FORG_PASS_PASSWORD_PARAM) != null) {
			if (!request.getParameter(ParamManagerJSP.FORG_PASS_PASSWORD_PARAM).toString().isEmpty()) {
				request.setAttribute(ParamManagerJSP.FORG_PASS_NEW_PASSWORD,
						request.getParameter(ParamManagerJSP.FORG_PASS_PASSWORD_PARAM).toString());
			}
		}
		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_FORG_PASS);
		return PageManager.PAGE_FORGOT_PASSWORD_PROP;
	}
}