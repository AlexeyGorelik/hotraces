package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.pageerror.ErrorCodeManager;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToLoginPageCommand. The class provides a process for user.
 */
public class ToLoginPageCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		request.getSession().setAttribute(HotRacesErrorCode.ERROR_AUTHENTICATION, null);
		if (request.getParameter(HotRacesErrorCode.ERROR_ATTR) != null) {
			if (!request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString().isEmpty()) {
				request.getSession().setAttribute(HotRacesErrorCode.ERROR_AUTHENTICATION, ErrorCodeManager
						.getErrorMessage(request.getParameter(HotRacesErrorCode.ERROR_ATTR).toString()));
			}
		}
		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_LOGIN);
		request.setAttribute(PageManager.REDIRECT, true);
		return PageManager.PAGE_LOGIN_FULL_PROP + ParamManagerJSP.ANCOR_DELIMETER + ParamManagerJSP.TO_MAIN_ANCOR;
	}
}