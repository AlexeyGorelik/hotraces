package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.race.BookmakerRaceManager;
import com.gorelik.hotraces.pageerror.ErrorCodeManager;
import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToCoeffTableCommand. The class provides a process for bookmaker.
 */
public class ToCoeffTableCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(ToCoeffTableCommand.class);

	/** The table manager. */
	private BookmakerRaceManager raceMng = new BookmakerRaceManager();

	@Override
	public String execute(HttpServletRequest request) {

		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_COEFF_TABLE);
		try {
			request.setAttribute(ParamManagerJSP.PROFILE_RACE_LIST, raceMng.createBookmakerRaceTable());
			request.setAttribute(ParamManagerJSP.PROFILE_TO_COEFF_TABLE_NAME, true);
			return PRGManager.TO_PROFILE;
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't load coeff table. Error code = " + e.getMessage());
			request.setAttribute(ParamManagerJSP.PROFILE_COEFF_ERROR, ErrorCodeManager.getErrorMessage(e.getMessage()));
			return PRGManager.TO_PROFILE_ERROR + e.getMessage();
		}

	}

	/**
	 * Sets the bookmaker race manager.
	 *
	 * @param tableMng
	 *            the new bookmaker race manager
	 */
	public void setTableManager(BookmakerRaceManager raceMng) {
		this.raceMng = raceMng;
	}
}
