package com.gorelik.hotraces.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.bookmaker.BookmakerSetCoeffData;
import com.gorelik.hotraces.logic.race.BookmakerRaceManager;
import com.gorelik.hotraces.logic.race.RaceData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToSetCoeffCommand. The class provides a process for bookmaker.
 */
public class ToSetCoeffCommand implements ActionCommand {
	/** The coefficient manager. */
	private BookmakerRaceManager raceMng = new BookmakerRaceManager();

	@Override
	public String execute(HttpServletRequest request) {
		String[] raceDataArr = request.getParameter(ParamManagerJSP.SELECTED_ROW_VALUE)
				.split(ParamManagerJSP.SELECTED_ROW_DELIMETED);
		RaceData raceData = null;
		try {
			raceDataArr[1] = PageManager.formatDate(raceDataArr[1],
					request.getSession().getAttribute(PageManager.LOCALE).toString());
			raceData = new RaceData(raceDataArr);
		} catch (ArrayIndexOutOfBoundsException e) {
			return PRGManager.TO_PROFILE_ERROR + HotRacesErrorCode.BOOKMAKER_EMPTY_SELECTED_RACE;
		}

		try {
			List<BookmakerSetCoeffData> list = raceMng.findRacesToSetCoeff(raceData);
			request.setAttribute(ParamManagerJSP.SET_COEFF_LIST, list);
			request.setAttribute(ParamManagerJSP.HORSE_COUNT, list.size());
			request.setAttribute(ParamManagerJSP.RACE_NAME_TO_JSP, raceDataArr[0]);
			request.setAttribute(ParamManagerJSP.RACE_DATE_TO_JSP, raceDataArr[1]);
			return PageManager.PAGE_BOOKMAKER_SET_COEFF_PROP;
		} catch (ManagerException e) {
			return PRGManager.TO_PROFILE_ERROR + e.getMessage();
		}
	}

	/**
	 * Sets the bookmaker race manager.
	 *
	 * @param coeffMng
	 *            the new bookmaker race manager
	 */
	public void setCoeffManager(BookmakerRaceManager raceMng) {
		this.raceMng = raceMng;
	}
}