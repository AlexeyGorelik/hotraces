package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.registration.RegistrationData;
import com.gorelik.hotraces.logic.registration.RegistrationManager;
import com.gorelik.hotraces.resource.PRGManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;
import com.gorelik.hotraces.validation.RegistrationValidator;

/**
 * The Class RegistrationCommand. The class provides a process for user.
 */
public class RegistrationCommand implements ActionCommand {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(RegistrationCommand.class);

	/** The registration manager. */
	private RegistrationManager regManager = new RegistrationManager();

	@Override
	public String execute(HttpServletRequest request) {
		RegistrationData regData = new RegistrationData(request.getParameter(ParamManagerJSP.REGISTR_LOGIN),
				request.getParameter(ParamManagerJSP.REGISTR_PASSWORD),
				request.getParameter(ParamManagerJSP.REGISTR_REPEAT_PASSWORD),
				request.getParameter(ParamManagerJSP.REGISTR_EMAIL),
				request.getParameter(ParamManagerJSP.REGISTR_FIRST_NAME),
				request.getParameter(ParamManagerJSP.REGISTR_SECOND_NAME));
		RegistrationValidator regValidator = new RegistrationValidator();
		regValidator.checkRegistration(regData);

		request.setAttribute(PageManager.REDIRECT, true);
		try {
			regManager.registrateUser(regData);
			return PRGManager.TO_MAIN;
		} catch (ManagerException e) {
			LOGGER.error("Error. Can't registrate user in system. Error code = " + e.getMessage());
			return PRGManager.TO_REGISTRATION_ERROR + e.getMessage();
		}
	}

	/**
	 * Sets the registration manager.
	 *
	 * @param regMng
	 *            the new registration manager
	 */
	public void setRegistrationManager(RegistrationManager regMng) {
		this.regManager = regMng;
	}

}
