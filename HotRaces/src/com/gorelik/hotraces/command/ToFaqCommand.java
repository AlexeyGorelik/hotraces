package com.gorelik.hotraces.command;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.resource.LocaleHrefManager;
import com.gorelik.hotraces.resource.PageManager;
import com.gorelik.hotraces.resource.ParamManagerJSP;

/**
 * The Class ToFaqCommand. The class provides a process for user.
 */
public class ToFaqCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		request.setAttribute(PageManager.REDIRECT, true);
		request.getSession().setAttribute(ParamManagerJSP.LOCALE_HREF, LocaleHrefManager.TO_FAQ);
		return PageManager.FAQ_FULL_PROP + ParamManagerJSP.ANCOR_DELIMETER + ParamManagerJSP.FAQ_ANCOR;
	}
}