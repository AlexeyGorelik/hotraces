package com.gorelik.hotraces.pageerror;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.dao.UserDAO;

/**
 * The Class ErrorCodeManager. Class handles error codes.
 */
public class ErrorCodeManager {

	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(UserDAO.class);

	/** The Constant ERROR_CODE_PATH. Path to file with errors. */
	private static final String ERROR_CODE_PATH = "resources.errortypes";

	/** The res bundle. Bundle which connected with file. */
	private static ResourceBundle resBundle = ResourceBundle.getBundle(ERROR_CODE_PATH);

	/**
	 * Gets the error message.
	 *
	 * @param code
	 *            the code
	 * @return the error message
	 */
	public static String getErrorMessage(String code) {
		try {
			return resBundle.getString(code);
		} catch (MissingResourceException e) {
			LOGGER.error("Can't find resource bundle for errors.");
			return "Can't find resource bundle for errors.";
		}
	}
}
