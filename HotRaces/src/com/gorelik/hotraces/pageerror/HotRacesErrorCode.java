package com.gorelik.hotraces.pageerror;

/**
 * The Class HotRacesErrorCode. Class contains all codes of errors in
 * application.
 */
public class HotRacesErrorCode {
	/** The Constant ERROR_ATTR. Name of tag on jsp. */
	// error attributes
	public static final String ERROR_ATTR = "error";

	/** The Constant ERROR_USER_TABLE. Name of tag on jsp. */
	public static final String ERROR_USER_TABLE = "userTableError";

	/** The Constant ERROR_REGISTRATION. Name of tag on jsp. */
	public static final String ERROR_REGISTRATION = "registrationError";

	/** The Constant ERROR_AUTHENTICATION. Name of tag on jsp. */
	public static final String ERROR_AUTHENTICATION = "authenticationError";

	/** The Constant ERROR_PROFILE_CLIENT. Name of tag on jsp. */
	public static final String ERROR_PROFILE_CLIENT = "profileError";

	/** The Constant AUTH_ERR. Name of tag on jsp. */
	// error global type
	public final static String AUTH_ERR = "Authorization error: ";

	// Common
	/**
	 * The Constant DAO_EXCEPTION. Common exception that DAO can't handle
	 * information.
	 */
	public static final String DAO_EXCEPTION = "000";

	/** The Constant LOGIN_IN_USE. Error that login already in use. */
	public static final String LOGIN_IN_USE = "001";

	/** The Constant EMAIL_IN_USE. Error that email already in use. */
	public static final String EMAIL_IN_USE = "002";

	/**
	 * The Constant EMPTY_SELECTED_ROW. Error that user didn't select a table
	 * row.
	 */
	public static final String EMPTY_SELECTED_ROW = "003";

	/** The Constant WRONG_DATE. Error that error of event is wrong. */
	public static final String WRONG_DATE = "004";

	/**
	 * The Constant AUTH_VALID_CLASS_IS_NULL. Error that validator can't handle
	 * information because class not exists.
	 */
	// authorization
	public static final String AUTH_VALID_CLASS_IS_NULL = "100";

	/** The Constant AUTH_INVALID_LOGIN. Error that login is invalid. */
	public static final String AUTH_INVALID_LOGIN = "101";

	/** The Constant AUTH_INVALID_PASSWORD. Error that password is invalid. */
	public static final String AUTH_INVALID_PASSWORD = "102";

	/**
	 * The Constant AUTH_WRONG_LOGIN_OR_PASS. Error that login or password is
	 * wrong (not exists, etc.).
	 */
	public static final String AUTH_WRONG_LOGIN_OR_PASS = "103";

	/**
	 * The Constant REGISTR_VALID_CLASS_IS_NULL. Error that validator can't
	 * handle information because class not exists.
	 */
	// registration
	public static final String REGISTR_VALID_CLASS_IS_NULL = "120";

	/** The Constant REGISTR_INVALID_LOGIN. Error that login is invalid. */
	public static final String REGISTR_INVALID_LOGIN = "121";

	/**
	 * The Constant REGISTR_INVALID_PASSWORD. Error that password is invalid.
	 */
	public static final String REGISTR_INVALID_PASSWORD = "122";

	/**
	 * The Constant REGISTR_INVALID_REPEAT_PASSWORD. Error that repeated
	 * password is invalid.
	 */
	public static final String REGISTR_INVALID_REPEAT_PASSWORD = "123";

	/** The Constant REGISTR_INVALID_EMAIL. Error that email is invalid. */
	public static final String REGISTR_INVALID_EMAIL = "124";

	/**
	 * The Constant REGISTR_INVALID_FIRST_NAME. Error that first name is
	 * invalid.
	 */
	public static final String REGISTR_INVALID_FIRST_NAME = "125";

	/**
	 * The Constant REGISTR_INVALID_SECOND_NAME. Error that second name is
	 * invalid.
	 */
	public static final String REGISTR_INVALID_SECOND_NAME = "126";

	/**
	 * The Constant PAYMENT_VALID_CLASS_IS_NULL. Error that validator can't
	 * handle information because class not exists.
	 */
	// payment
	public static final String PAYMENT_VALID_CLASS_IS_NULL = "150";

	/** The Constant PAYMENT_INVALID_CVV. Error that CVV code is invalid. */
	public static final String PAYMENT_INVALID_CVV = "151";

	/** The Constant PAYMENT_INVALID_SUM. Error that sum is invalid. */
	public static final String PAYMENT_INVALID_SUM = "152";

	/**
	 * The Constant PAYMENT_INVALID_CARD_CODE. Error that card code is invalid.
	 */
	public static final String PAYMENT_INVALID_CARD_CODE = "153";

	/** The Constant PAYMENT_INVALID_YEAR. Error that year is invalid. */
	public static final String PAYMENT_INVALID_YEAR = "154";

	/** The Constant PAYMENT_INVALID_MONTH. Error that month is invalid. */
	public static final String PAYMENT_INVALID_MONTH = "155";

	/**
	 * The Constant PROFILE_CHANGE_VALID_CLASS_IS_NULL. Error that validator
	 * class can't handle information because class not exists.
	 */
	// change profile data
	public static final String PROFILE_CHANGE_VALID_CLASS_IS_NULL = "170";

	/**
	 * The Constant PROFILE_CHANGE_INVALID_LOGIN. Error that login is invalid.
	 */
	public static final String PROFILE_CHANGE_INVALID_LOGIN = "171";

	/**
	 * The Constant PROFILE_CHANGE_INVALID_EMAIL. Error that email is invalid.
	 */
	public static final String PROFILE_CHANGE_INVALID_EMAIL = "172";

	/**
	 * The Constant PROFILE_CHANGE_INVALID_FIRST_NAME. Error that first name is
	 * invalid.
	 */
	public static final String PROFILE_CHANGE_INVALID_FIRST_NAME = "173";

	/**
	 * The Constant PROFILE_CHANGE_INVALID_SECOND_NAME. Error that second name
	 * is invalid.
	 */
	public static final String PROFILE_CHANGE_INVALID_SECOND_NAME = "174";

	/**
	 * The Constant ADMIN_CHANGE_STATUS_CLASS_IS_NULL. Error that validator
	 * class can't handle information because class not exists.
	 */
	// admin
	public static final String ADMIN_CHANGE_STATUS_CLASS_IS_NULL = "200";

	/**
	 * The Constant ADMIN_CHANGE_STATUS_USER_IS_EMPTY. Error that user status is
	 * empty.
	 */
	public static final String ADMIN_CHANGE_STATUS_USER_IS_EMPTY = "201";

	/**
	 * The Constant ADMIN_RACE_ALREADY_CARRIED_OUT. Error that race has already
	 * carried out.
	 */
	// admin race
	public static final String ADMIN_RACE_ALREADY_CARRIED_OUT = "202";

	/**
	 * The Constant ADMIN_RACE_NOT_EQUALS_COUNT. Error that count of horses name
	 * not equals count of horses id.
	 */
	public static final String ADMIN_RACE_NOT_EQUALS_COUNT = "203";

	/**
	 * The Constant ADMIN_RACE_NAME_IS_EMPTY. Error that race name doesn't
	 * exists.
	 */
	public static final String ADMIN_RACE_NAME_IS_EMPTY = "204";

	/**
	 * The Constant ADMIN_RACE_DATE_IS_EMPTY. Error that race date doesn't
	 * exists.
	 */
	public static final String ADMIN_RACE_DATE_IS_EMPTY = "205";

	/** The Constant ADMIN_WRONG_RACE_DATE. Error that race date is wrong. */
	public static final String ADMIN_WRONG_RACE_DATE = "206";

	/**
	 * The Constant ADMIN_WRONG_CARRY_OUT_STATUS. Error that race status is
	 * wrong.
	 */
	public static final String ADMIN_WRONG_CARRY_OUT_STATUS = "207";

	/**
	 * The Constant ADMIN_CREATE_RACE_ONE_HORSE. Error that admin can't create
	 * race with one horse.
	 */
	public static final String ADMIN_CREATE_RACE_ONE_HORSE = "208";

	/**
	 * The Constant ADMIN_CANT_EDIT_ONE_HORSE. Error that user can't edit race
	 * and left one horse.
	 */
	public static final String ADMIN_CANT_EDIT_ONE_HORSE = "209";

	/**
	 * The Constant ADMIN_EDIT_EMPTY_RACE. Error that user can't edit race
	 * becaus of wrong date.
	 */
	public static final String ADMIN_EDIT_EMPTY_RACE = "210";

	/**
	 * The Constant CANT_REAL_DELETE_RACE. Error that DAO can't delete race from
	 * DB.
	 */
	public static final String CANT_REAL_DELETE_RACE = "215";

	/**
	 * The Constant CANT_GET_RACE_STATUS. Error that DAO can't get race status.
	 */
	public static final String CANT_GET_RACE_STATUS = "216";

	/**
	 * The Constant CANT_REAL_DELETE_HORSE. Error that DAO can't delete horse
	 * from race.
	 */
	public static final String CANT_REAL_DELETE_HORSE = "217";

	// bookmaker
	/**
	 * The Constant BOOKMAKER_EMPTY_SELECTED_RACE. Error that bookmaker didn't
	 * select a race.
	 */
	public static final String BOOKMAKER_EMPTY_SELECTED_RACE = "220";

	/**
	 * The Constant BOOKMAKER_EMPTY_COEFF. Error that bookmaker forget to set
	 * coefficient at least for one horse.
	 */
	public static final String BOOKMAKER_EMPTY_COEFF = "221";

	/**
	 * The Constant BOOKMAKER_WRONG_RACE_TYPE. Error that bookmaker tried to
	 * delete race with wrong type.
	 */
	public static final String BOOKMAKER_WRONG_RACE_TYPE = "222";

	/**
	 * The Constant BOOKMAKER_EMPTY_RACE_TO_DELETE. Error that bookmaker tried
	 * to delete empty race.
	 */
	public static final String BOOKMAKER_EMPTY_RACE_TO_DELETE = "223";

	/**
	 * The Constant MAKE_BET_NOT_ENOUGHT_MONEY. Error that user can't make bet
	 * because he doesn't has enough money.
	 */
	public static final String MAKE_BET_NOT_ENOUGHT_MONEY = "301";

	/**
	 * The Constant WRONG_PREMISSION. Error that user can't make bet because he
	 * didn't logged in or his account type is wrong.
	 */
	public static final String WRONG_PREMISSION = "302";

	/** The Constant MAKE_BET_EMPTY_RACE. Error that user didn't select race. */
	public static final String MAKE_BET_EMPTY_RACE = "303";

	/** The Constant BET_DOES_NOT_EXIST. Error that bet doesn't exists. */
	public static final String BET_DOES_NOT_EXIST = "304";

	/**
	 * The Constant EMTY_PREV_RACE_DESCR. Error that user selected wrong race.
	 */
	public static final String EMTY_PREV_RACE_DESCR = "305";

	// race
	/**
	 * The Constant EMPTY_DELETE_RACE_SELECT_VALUE. Error that user tried to
	 * delete race with wrong type.
	 */
	public static final String EMPTY_DELETE_RACE_SELECT_VALUE = "320";

	/**
	 * The Constant SET_COEFF_NOT_OPEN_STATUS. Error that user tried to set
	 * coefficient to race with wrong status.
	 */
	public static final String SET_COEFF_NOT_OPEN_STATUS = "321";

	/**
	 * The Constant EDIT_RACE_WRONG_STATUS. Error that user tried to edit race
	 * with wrong status.
	 */
	public static final String EDIT_RACE_WRONG_STATUS = "322";

	/**
	 * The Constant EQUALS_HORSES_IN_RACE. Error that user can't add or remove
	 * horses from race.
	 */
	public static final String EQUALS_HORSES_IN_RACE = "323";

	// message
	/**
	 * The Constant CONVERSATION_ALREADY_EXISTS. Error that conversation between
	 * users already exists.
	 */
	public static final String CONVERSATION_ALREADY_EXISTS = "362";

	/**
	 * The Constant EMPTY_USER. Error that user didn't select anyone to create
	 * conversation.
	 */
	public static final String EMPTY_USER = "363";

	// upload avatar
	/**
	 * The Constant MESSAGE_IS_EMPTY. Error that user can't send message to
	 * wrong user.
	 */
	public static final String MESSAGE_IS_EMPTY = "340";

	/** The Constant UPLOAD_AVATAR_ERROR. Error that user cant upload avatar. */
	public static final String UPLOAD_AVATAR_ERROR = "360";

	/** The Constant WRONG_EXPANSION. Error that avatar expansion is wrong. */
	public static final String WRONG_EXPANSION = "361";

	// change password
	/**
	 * The Constant CHANGE_PROFILE_USER_DOES_NOT_EXIST. Error that user with
	 * this information doesn't exists.
	 */
	public static final String CHANGE_PROFILE_USER_DOES_NOT_EXIST = "380";

	// ban status
	/**
	 * The Constant BAN_STATUS_ACTION. Error that user can't make action because
	 * of ban profile status.
	 */
	public static final String BAN_STATUS_ACTION = "400";
}