package com.gorelik.hotraces.connection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

/**
 * The Class WrapperConnection. Contains connection to DataBase and methods
 * which work with it (get, commit, rollback, etc.).
 */
public class WrapperConnection {

	/** The Constant LOGGER. Defines logger for handling exceptions. */
	static final Logger LOGGER = Logger.getLogger(WrapperConnection.class);

	/** The connection to DataBase. */
	private Connection connection;

	/**
	 * Instantiates a new wrapper connection.
	 *
	 * @param connect
	 *            the Connection
	 */
	WrapperConnection(Connection connect) {
		this.connection = connect;
	}

	/**
	 * Sets the connection.
	 *
	 * @param conn
	 *            the new connection
	 */
	public void setConnection(Connection conn) {
		this.connection = conn;
	}

	/**
	 * Gets the statement.
	 *
	 * @return the statement
	 * @throws SQLException
	 *             the SQL exception
	 */
	public Statement getStatement() throws SQLException {
		Statement st = null;
		if (connection != null) {
			st = connection.createStatement();
		}
		return st;
	}

	/**
	 * Gets the prepared statement.
	 *
	 * @param query
	 *            the query
	 * @return the prepared statement
	 * @throws SQLException
	 *             the SQL exception
	 */
	public PreparedStatement getPreparedStatement(String query) throws SQLException {
		PreparedStatement st = null;
		if (connection != null) {
			st = connection.prepareStatement(query);
		}
		return st;
	}

	/**
	 * Gets the callable statement.
	 *
	 * @param query
	 *            the query
	 * @return the callable statement
	 * @throws SQLException
	 *             the SQL exception
	 */
	public CallableStatement getCallableStatement(String query) throws SQLException {
		CallableStatement st = null;
		if (connection != null) {
			st = connection.prepareCall(query);
		}
		return st;
	}

	/**
	 * Sets the auto commit.
	 *
	 * @param commit
	 *            the new auto commit
	 * @throws SQLException
	 *             the SQL exception
	 */
	public void setAutoCommit(boolean commit) throws SQLException {
		this.connection.setAutoCommit(commit);
	}

	/**
	 * Commit current connection.
	 *
	 * @throws SQLException
	 *             the SQL exception
	 */
	public void commit() throws SQLException {
		this.connection.commit();
	}

	/**
	 * Rollback current connection.
	 *
	 * @throws SQLException
	 *             the SQL exception
	 */
	public void rollback() throws SQLException {
		this.connection.rollback();
	}

	/**
	 * Sets the default prop.
	 *
	 * @throws SQLException
	 *             the SQL exception
	 */
	public void setDefaultProp() throws SQLException {
		this.connection.setAutoCommit(true);
	}

	/**
	 * Returns connection to ConnectionPool.
	 */
	public void close() {
		ConnectionPool.returnConnection(this);
	}

	/**
	 * Real close connection.
	 */
	void realCloseConnection() {
		try {
			this.connection.close();
		} catch (SQLException e) {
			LOGGER.error("Can't provide real close connection. " + e);
		}
	}

}
