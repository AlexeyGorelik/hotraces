package com.gorelik.hotraces.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.WrapperConnectionException;

/**
 * The Class WrapperConnectionBuilder. Class builds connection to the DataBase.
 */
public class WrapperConnectionBuilder {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(WrapperConnectionBuilder.class);

	/** The Constant dataBasePropPath. Contains path to .properties file. */
	private static String dataBasePropPath = "resources.db";

	/** The Constant DB_URL. Contains key of DB url. */
	private static String dbUrl = "db.url";

	/** The Constant DB_NAME. Contains key of DB name. */
	private static String dbName = "db.name";

	/** The Constant DB_PASSWORD. Contains key of DB password. */
	private static String dbPassword = "db.password";

	/**
	 * Creates the WrapperConnection.
	 *
	 * @return the wrapper connection
	 * @throws WrapperConnectionException
	 *             the wrapper connection exception
	 */
	public WrapperConnection createConnection() throws WrapperConnectionException {
		try {
			ResourceBundle resBundle = ResourceBundle.getBundle(dataBasePropPath);
			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
			String url = resBundle.getString(dbUrl);
			String name = resBundle.getString(dbName);
			String pass = resBundle.getString(dbPassword);
			Connection connection = DriverManager.getConnection(url, name, pass);
			return new WrapperConnection(connection);
		} catch (MissingResourceException e) {
			LOGGER.fatal("Can't create connection. Properties file doesn't exist.");
			throw new RuntimeException("Can't create connection. Properties file doesn't exist.");
		} catch (SQLException e) {
			LOGGER.error("Can't obtain connection.");
			throw new WrapperConnectionException("Can't obtain connection.");
		}
	}

	/**
	 * Sets database property file path.
	 */
	public static void setPropPath(String path) {
		dataBasePropPath = path;
	}

	/**
	 * Sets database url key value.
	 */
	public static void setUrl(String path) {
		dbUrl = path;
	}

	/**
	 * Sets database name key value.
	 */
	public static void setName(String path) {
		dbName = path;
	}

	/**
	 * Sets database password key value.
	 */
	public static void setPassword(String path) {
		dbPassword = path;
	}
}