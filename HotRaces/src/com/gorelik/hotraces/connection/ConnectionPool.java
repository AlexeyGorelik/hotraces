package com.gorelik.hotraces.connection;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.exception.WrapperConnectionException;

/**
 * The Class ConnectionPool (Singleton). Class contains list of connections to
 * DataBase, methods which works with this connections (take, close, etc.).
 */
public class ConnectionPool {

	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class);

	/**
	 * The Constant CONNECTION_NUMBER. Maximum number of connections to
	 * DataBase.
	 */
	private static final int CONNECTION_NUMBER = 10;

	/** The instance. Instance of current singleton-class. */
	private static ConnectionPool instance;

	/** The lock. Locker for multithreading. */
	private static ReentrantLock lock = new ReentrantLock();

	/** The instance created. Variable to check singleton instance status. */
	private static AtomicBoolean instanceCreated = new AtomicBoolean(false);

	/** The connection pool. List of connections. */
	private static ArrayBlockingQueue<WrapperConnection> connectionPool = new ArrayBlockingQueue<WrapperConnection>(
			CONNECTION_NUMBER);

	/**
	 * Instantiates a new connection pool.
	 */
	private ConnectionPool() {
		WrapperConnectionBuilder wrapConnBuilder = new WrapperConnectionBuilder();
		for (int i = 0; i < CONNECTION_NUMBER; i++) {
			try {
				connectionPool.add(wrapConnBuilder.createConnection());
				LOGGER.info("Connection has added to the ConnectionPool.");
			} catch (WrapperConnectionException e) {
				LOGGER.error("Can't create WrapperConnection." + e);
			}
		}

		if (connectionPool.isEmpty()) {
			LOGGER.fatal("Connection pool is empty!");
			throw new RuntimeException("Connection pool is empty.");
		}
	}

	/**
	 * Gets the instance of ConnectionPool.
	 *
	 * @return instance of ConnectionPool
	 */
	public static ConnectionPool getInstance() {
		if (!instanceCreated.get()) {
			lock.lock();
			try {
				if (instance == null) {
					instance = new ConnectionPool();
					instanceCreated.getAndSet(true);
				}
			} finally {
				lock.unlock();
			}
		}
		return instance;
	}

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 */
	public static WrapperConnection getConnection() {
		WrapperConnection wrapConn = null;
		try {
			wrapConn = connectionPool.take();
		} catch (InterruptedException e) {
			LOGGER.error("Can't take Connection from ConnectionPool. " + e);
		}
		return wrapConn;

	}

	/**
	 * Real close pool. Take and close all connections with DataBase.
	 */
	public static void realClosePool() {
		lock.lock();
		for (int i = 0; i < connectionPool.size(); i++) {
			try {
				connectionPool.take().realCloseConnection();
			} catch (InterruptedException e) {
				LOGGER.error("Can't close Connection from ConnectionPool. " + e);
			}
		}

		try {
			Enumeration<Driver> drivers = DriverManager.getDrivers();
			while (drivers.hasMoreElements()) {
				Driver driver = drivers.nextElement();
				DriverManager.deregisterDriver(driver);
			}
			LOGGER.info("Connection pool has closed sudccesfully.");
		} catch (SQLException e) {
			LOGGER.error("DriverManager not found. " + e);
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Return wrapper connection.
	 *
	 * @param wrapConn
	 *            the WrapperConnection
	 */
	static void returnConnection(WrapperConnection wrapConn) {
		try {
			wrapConn.setDefaultProp();
			connectionPool.add(wrapConn);
		} catch (SQLException e) {
			LOGGER.error("Can't set default properties to connection.");
		}
	}
}
