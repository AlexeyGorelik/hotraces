package com.gorelik.hotraces.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.logic.login.LoginData;
import com.gorelik.hotraces.logic.profile.ProfileChangeData;
import com.gorelik.hotraces.logic.registration.RegistrationData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class UserDAO. Class provides some process user.
 */
public class UserDAO extends AbstractDAO {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(UserDAO.class);

	/** The Constant FIND_USER_BY_ID. Query to find user by id. */
	private static final String FIND_USER_BY_ID = "SELECT id, name, firstname, secondname, email, role, status, money, avatarpath FROM user WHERE user.id = ?;";

	/** The Constant CHECK_AUTH. Query to check authentication. */
	private static final String CHECK_AUTH = "{call checkAuthentication (?, ?, ?, ?)}";

	/** The Constant CHECK_REGISTRATION. Query to check registration. */
	private static final String CHECK_REGISTRATION = "{call checkRegistration (?, ?, ?, ?)}";

	/** The Constant GET_USER. Query to get user data. */
	private static final String GET_USER = "SELECT id, name, firstname, secondname, email, role, status, money, avatarpath FROM user WHERE user.name = ?;";

	/** The Constant REGISTR_USER. Query to registrate user. */
	private static final String REGISTR_USER = "INSERT INTO  races.user (`name`, `password`, `email`, `firstname`, `secondname`, `reg_date`, `role`, `money`, `status`) VALUES ( ?, ?, ?, ?, ?, ?, 3, 0.0, 1);";

	// change profile
	/** The Constant CHECK_LOGIN. Query to check user login. */
	private static final String CHECK_LOGIN = "select name from user where user.name = ?;";

	/** The Constant CHECK_EMAIL. Query to check user email. */
	private static final String CHECK_EMAIL = "select email from user where user.email = ?;";

	/** The Constant UPDATE_USER. Query to change user data. */
	private static final String UPDATE_USER = "update user set user.firstname = ?, user.secondname = ?, user.name = ?, user.email =  ? where user.name = ?;";

	/** The Constant UPDATE_AVATAR. Query to change avatar path. */
	private static final String UPDATE_AVATAR = "update user set user.avatarpath = ? where user.name = ?;";

	/**
	 * The Constant GET_ADMIN_USER_TABLE. Query to get user data to admin page.
	 */
	// admin
	private static final String GET_ADMIN_USER_TABLE = "SELECT id, name, firstname, secondname, email, role, status, money FROM user WHERE user.role = 3;";

	/** The Constant CHANGE_USER_STATUS. Query to change user status. */
	private static final String CHANGE_USER_STATUS = "UPDATE user SET user.status = ? WHERE user.id = ?;";

	/** The Constant GET_USER_LIST. Query to get user name list. */
	// message
	private static final String GET_USER_LIST = "SELECT name FROM user;";

	/** The Constant FIND_USER_COUNT. Query to find user count. */
	// admin statistic
	private static final String FIND_USER_COUNT = "select count(id) from user where date_format(reg_date, '%Y%m') = date_format(now(), '%Y%m');";

	/** The Constant CHANGE_PASSWORD. Query to change password. */
	// change password
	private static final String CHANGE_PASSWORD = "UPDATE user SET password = ? WHERE email = ?;";

	/** The Constant FINT_USER_BY_EMAIL. Query to find user by email. */
	private static final String FINT_USER_BY_EMAIL = "SELECT name FROM user WHERE email = ?;";

	/** The Constant CHAR_SEQ. Char sequence to generate password. */
	private static final String CHAR_SEQ = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	// create new password
	/** The Constant ZERO. Zero digit to create random integer. */
	final int ZERO = 0;

	/** The Constant NINE. Nine digit to create random integer. */
	final int NINE = 9;

	/** The Constant SMALL_CHAR_VAL. Char "a" integer value in ascii table. */
	final int SMALL_CHAR_VAL = 96;

	/** The Constant BIG_CHAR_VAL. Char "A" integer value in ascii table. */
	final int BIG_CHAR_VAL = 64;

	/** The Constant FIRST_SEQ_CHAR. Start digit of character sequence. */
	final int FIRST_SEQ_CHAR = 1;

	/** The Constant LAST_SEQ_CHAR. Last digit of character sequence. */
	final int LAST_SEQ_CHAR = 26;

	/**
	 * Login.
	 *
	 * @param loginData
	 *            the login data
	 * @return the user data
	 * @throws DAOException
	 *             the DAO exception
	 */
	public User login(LoginData loginData) throws DAOException {
		CallableStatement checkAuthStatement = null;
		PreparedStatement getUserStatement = null;
		User user = null;

		try {
			checkAuthStatement = wrapConnection.getCallableStatement(CHECK_AUTH);
			checkAuthStatement.setString(1, loginData.getLogin());
			checkAuthStatement.setString(2, loginData.getPassword());
			checkAuthStatement.registerOutParameter(3, java.sql.Types.BOOLEAN);
			checkAuthStatement.registerOutParameter(4, java.sql.Types.BOOLEAN);

			checkAuthStatement.execute();

			if (checkAuthStatement.getBoolean(3) && checkAuthStatement.getBoolean(4)) {
				getUserStatement = wrapConnection.getPreparedStatement(GET_USER);
				getUserStatement.setString(1, loginData.getLogin());
				ResultSet resSet = getUserStatement.executeQuery();
				if (resSet.next()) {
					user = new User(Integer.valueOf(resSet.getString(1)), resSet.getString(2), resSet.getString(3),
							resSet.getString(4), resSet.getString(5), resSet.getString(6), resSet.getString(7),
							BigDecimal.valueOf(resSet.getDouble(8)));
					user.setAvatarPath(resSet.getString(9));
				}
				LOGGER.info("User " + user.getLogin() + " authorizated.");
				return user;
			} else {
				throw new DAOException(HotRacesErrorCode.AUTH_WRONG_LOGIN_OR_PASS);
			}

		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(checkAuthStatement);
			closeStatement(getUserStatement);
		}
	}

	/**
	 * Registrates user.
	 *
	 * @param regData
	 *            the registration data
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void registrateUser(RegistrationData regData) throws DAOException {
		CallableStatement checkRegStatement = null;
		PreparedStatement registrUserStatement = null;
		try {
			checkRegStatement = wrapConnection.getCallableStatement(CHECK_REGISTRATION);
			checkRegStatement.setString(1, regData.getLogin());
			checkRegStatement.setString(2, regData.getEmail());
			checkRegStatement.registerOutParameter(3, java.sql.Types.BOOLEAN);
			checkRegStatement.registerOutParameter(4, java.sql.Types.BOOLEAN);

			checkRegStatement.execute();

			if (checkRegStatement.getBoolean(3) && checkRegStatement.getBoolean(4)) {
				registrUserStatement = wrapConnection.getPreparedStatement(REGISTR_USER);
				registrUserStatement.setString(1, regData.getLogin());
				registrUserStatement.setString(2, regData.getPassword());
				registrUserStatement.setString(3, regData.getEmail());
				registrUserStatement.setString(4, regData.getFirstName());
				registrUserStatement.setString(5, regData.getSecondName());
				registrUserStatement.setTimestamp(6, java.sql.Timestamp.valueOf(LocalDateTime.now()));

				registrUserStatement.execute();
				LOGGER.info("User " + regData.getLogin() + " has created.");
			} else {
				if (!checkRegStatement.getBoolean(3)) {
					throw new DAOException(HotRacesErrorCode.LOGIN_IN_USE);
				}
				if (!checkRegStatement.getBoolean(4)) {
					throw new DAOException(HotRacesErrorCode.EMAIL_IN_USE);
				}
			}
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(checkRegStatement);
			closeStatement(registrUserStatement);
		}
	}

	/**
	 * Change user data.
	 *
	 * @param user
	 *            the user which data to change
	 * @param profChangeData
	 *            the new profile data
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void changeData(User user, ProfileChangeData profChangeData) throws DAOException {
		PreparedStatement checkLoginStatement = null;
		PreparedStatement checkEmailStatement = null;
		PreparedStatement updateUserStatement = null;
		try {
			ResultSet resSet = null;
			if (profChangeData.getLogin() != null && !user.getLogin().equals(profChangeData.getLogin())) {
				checkLoginStatement = wrapConnection.getPreparedStatement(CHECK_LOGIN);
				checkLoginStatement.setString(1, profChangeData.getFirstName());
				resSet = checkLoginStatement.executeQuery();
				if (resSet.next()) {
					throw new DAOException(HotRacesErrorCode.LOGIN_IN_USE);
				}

			}

			if (profChangeData.getEmail() != null && !user.getEmail().equals(profChangeData.getEmail())) {
				checkEmailStatement = wrapConnection.getPreparedStatement(CHECK_EMAIL);
				checkEmailStatement.setString(1, profChangeData.getFirstName());
				resSet = checkEmailStatement.executeQuery();
				if (resSet.next()) {
					throw new DAOException(HotRacesErrorCode.EMAIL_IN_USE);
				}
			}

			updateUserStatement = wrapConnection.getPreparedStatement(UPDATE_USER);
			updateUserStatement.setString(1, profChangeData.getFirstName());
			updateUserStatement.setString(2, profChangeData.getSecondName());
			updateUserStatement.setString(3, profChangeData.getLogin());
			updateUserStatement.setString(4, profChangeData.getEmail());
			updateUserStatement.setString(5, user.getLogin());
			updateUserStatement.executeUpdate();

			user.setFirstName(profChangeData.getFirstName());
			user.setSecondName(profChangeData.getSecondName());
			user.setLogin(profChangeData.getLogin());
			user.setEmail(profChangeData.getEmail());
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(checkLoginStatement);
			closeStatement(checkEmailStatement);
			closeStatement(updateUserStatement);
		}
	}

	/**
	 * Change user avatar.
	 *
	 * @param user
	 *            the user to which change avatar
	 * @param path
	 *            the path to new avatar
	 * @return the user
	 * @throws DAOException
	 *             the DAO exception
	 */
	public User changeAvatar(User user, String path) throws DAOException {
		PreparedStatement updateAvatarStatement = null;
		try {
			updateAvatarStatement = wrapConnection.getPreparedStatement(UPDATE_AVATAR);
			updateAvatarStatement.setString(1, path);
			updateAvatarStatement.setString(2, user.getLogin());
			updateAvatarStatement.executeUpdate();

			user.setAvatarPath(path);
			return user;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(updateAvatarStatement);
		}
	}

	/**
	 * Creates the admin user table.
	 *
	 * @return the list with users
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<User> createAdminUserTable() throws DAOException {
		List<User> list = null;
		PreparedStatement getTableStatement = null;
		try {
			list = new LinkedList<User>();

			getTableStatement = wrapConnection.getPreparedStatement(GET_ADMIN_USER_TABLE);
			ResultSet resSet = getTableStatement.executeQuery();

			while (resSet.next()) {
				User temp = new User(Integer.valueOf(resSet.getString(1)), resSet.getString(2), resSet.getString(3),
						resSet.getString(4), resSet.getString(5), resSet.getString(6), resSet.getString(7));
				list.add(temp);
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(getTableStatement);
		}
	}

	/**
	 * Change user status.
	 *
	 * @param userId
	 *            the user id
	 * @param statusType
	 *            the status type
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void changeUserStatus(String userId, int statusType) throws DAOException {
		PreparedStatement changeStatusStatement = null;
		try {
			changeStatusStatement = wrapConnection.getPreparedStatement(CHANGE_USER_STATUS);
			changeStatusStatement.setInt(1, statusType);
			changeStatusStatement.setString(2, userId);
			changeStatusStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(changeStatusStatement);
		}
	}

	/**
	 * Find list of all user name's.
	 *
	 * @return the list with user names data
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<String> findUserList() throws DAOException {
		PreparedStatement getListStatement = null;
		List<String> list = null;
		try {
			list = new ArrayList<String>();
			getListStatement = wrapConnection.getPreparedStatement(GET_USER_LIST);
			ResultSet userSet = getListStatement.executeQuery();
			while (userSet.next()) {
				list.add(userSet.getString(1));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(getListStatement);
		}
	}

	/**
	 * Find how many users has registrated in this month.
	 *
	 * @return the int value of registrater users
	 * @throws DAOException
	 *             the DAO exception
	 */
	public int findUserThisMonthCount() throws DAOException {
		int count = 0;
		PreparedStatement userCountStatement = null;
		try {
			userCountStatement = wrapConnection.getPreparedStatement(FIND_USER_COUNT);
			ResultSet betSet = userCountStatement.executeQuery();
			if (betSet.next()) {
				return betSet.getInt(1);
			} else {
				return count;
			}
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(userCountStatement);
		}
	}

	/**
	 * Change user password.
	 *
	 * @param email
	 *            the email of user
	 * @return the string value of new password
	 * @throws DAOException
	 *             the DAO exception
	 */
	public String changePassword(String email) throws DAOException {
		CallableStatement changePasswStatement = null;
		PreparedStatement findUserStatement = null;
		try {
			findUserStatement = wrapConnection.getPreparedStatement(FINT_USER_BY_EMAIL);
			findUserStatement.setString(1, email);
			ResultSet result = findUserStatement.executeQuery();
			if (!result.next()) {
				throw new DAOException(HotRacesErrorCode.CHANGE_PROFILE_USER_DOES_NOT_EXIST);
			}
			changePasswStatement = wrapConnection.getCallableStatement(CHANGE_PASSWORD);
			String password = generatePassword(ThreadLocalRandom.current().nextInt(10, 12));
			changePasswStatement.setString(1, password);
			changePasswStatement.setString(2, email);
			changePasswStatement.executeUpdate();
			return password;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(findUserStatement);
			closeStatement(changePasswStatement);
		}
	}

	/**
	 * Generate user password.
	 *
	 * @param len
	 *            the length of new password
	 * @return the string
	 */
	private String generatePassword(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			sb.append(CHAR_SEQ.charAt(ThreadLocalRandom.current().nextInt(0, CHAR_SEQ.length() - 1)));
		}
		sb.append(String.valueOf(ThreadLocalRandom.current().nextInt(ZERO, NINE)));
		sb.append((char) (SMALL_CHAR_VAL + ThreadLocalRandom.current().nextInt(FIRST_SEQ_CHAR, LAST_SEQ_CHAR)));
		sb.append((char) (BIG_CHAR_VAL + ThreadLocalRandom.current().nextInt(FIRST_SEQ_CHAR, LAST_SEQ_CHAR)));
		return sb.toString();
	}

	/**
	 * Find user by id.
	 *
	 * @param id
	 *            the id
	 * @return the user
	 * @throws DAOException
	 *             the DAO exception
	 */
	public User findUserById(int id) throws DAOException {
		PreparedStatement findUserStatement = null;
		User user = null;
		try {
			findUserStatement = wrapConnection.getPreparedStatement(FIND_USER_BY_ID);
			findUserStatement.setInt(1, id);
			ResultSet resSet = findUserStatement.executeQuery();
			if (resSet.next()) {
				user = new User(Integer.valueOf(resSet.getString(1)), resSet.getString(2), resSet.getString(3),
						resSet.getString(4), resSet.getString(5), resSet.getString(6), resSet.getString(7));
			}
			return user;
		} catch (SQLException e) {
			throw new DAOException();
		} finally {
			closeStatement(findUserStatement);
		}
	}
}