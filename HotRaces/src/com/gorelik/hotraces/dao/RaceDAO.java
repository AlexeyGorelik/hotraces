package com.gorelik.hotraces.dao;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.entity.Horse;
import com.gorelik.hotraces.entity.Race;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.logic.bets.BetsRaceData;
import com.gorelik.hotraces.logic.bookmaker.BookmakerSetCoeffData;
import com.gorelik.hotraces.logic.bookmaker.BookmakerTableRaceData;
import com.gorelik.hotraces.logic.race.EditRaceData;
import com.gorelik.hotraces.logic.race.PrevRaceData;
import com.gorelik.hotraces.logic.race.RaceData;
import com.gorelik.hotraces.logic.race.RaceEnum;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class RaceDAO. Class provides some process with races.
 */
public class RaceDAO extends AbstractDAO {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(RaceDAO.class);

	/** The Constant FIND_RACE_STATUS. Query to find race status. */
	// find status
	private static final String FIND_RACE_STATUS = "SELECT status FROM race WHERE race_name = ? AND date = ?;";

	/**
	 * The Constant ADMIN_RACE_TABLE. Query to find races for admin race table.
	 */
	private static final String ADMIN_RACE_TABLE = "SELECT hippodrom.name, race.date, race.status, hippodrom.city, hippodrom.country, hippodrom.length FROM race JOIN hippodrom ON race.race_name = hippodrom.name ORDER BY race.date DESC, FIELD(race.status,'�������','�������','���������','��������');";

	/**
	 * The Constant SELECT_HORSES_FROM_RACE. Query to find all horses in current
	 * race.
	 */
	private static final String SELECT_HORSES_FROM_RACE = "SELECT horse.id, horse.name, horse.color, horse.weight FROM horse JOIN horse_in_race ON horse.id = horse_in_race.horse_id WHERE horse_in_race.horse_race_name = ? AND horse_in_race.horse_race_date = ?;";

	/** The Constant SET_HORSE_PLACE. Query to set horse place in race. */
	private static final String SET_HORSE_PLACE = "UPDATE horse_in_race set horse_in_race.place = ?  where horse_in_race.horse_race_name = ? AND horse_in_race.horse_race_date = ? AND horse_in_race.horse_id = ?;";

	/** The Constant CARRY_OUT_RACE. Query to carry out current race. */
	private static final String CARRY_OUT_RACE = "UPDATE race SET race.status = 4 WHERE race_name = ? and date = ?;";

	/** The Constant ADD_HORSE_IN_RACE. Query to add horse in race. */
	private static final String ADD_HORSE_IN_RACE = "INSERT INTO  races.horse_in_race (`horse_id`, `horse_race_name`, `horse_race_date`) VALUES (?, ?, ?);";

	/**
	 * The Constant UPDATE_HORSE_IN_RACE. Query to change horse status on "Open"
	 * in race.
	 */
	private static final String UPDATE_HORSE_IN_RACE = "UPDATE horse_in_race SET horse_in_race.status = 1, horse_in_race.coeff = null, horse_in_race.place = null WHERE horse_in_race.horse_id = ? AND horse_in_race.horse_race_name = ? AND horse_in_race.horse_race_date = ?;";

	/**
	 * The Constant GET_HORSE_FROM_RACE. Query to get horse status from race.
	 */
	private static final String GET_HORSE_STATUS_FROM_RACE = "SELECT status FROM horse_in_race WHERE horse_in_race.horse_id = ? AND horse_in_race.horse_race_name = ? AND horse_in_race.horse_race_date = ?;";

	/** The Constant CREATE_RACE. Query to create race into DB. */
	private static final String CREATE_RACE = "INSERT INTO race VALUES(?, ?, 2);";

	/** The Constant OPEN_RACE. Query to change race status on "Open". */
	private static final String OPEN_RACE = "UPDATE race SET status = 1 WHERE race.race_name = ? AND race.date = ?;";

	/** The Constant FIND_HIPP_LIST. Query to find all hippodromes. */
	private static final String FIND_HIPP_LIST = "SELECT name FROM races.hippodrom GROUP BY name;";

	/** The Constant CLOSE_RACE. Query to change horse status on "Close". */
	private static final String CLOSE_RACE = "UPDATE race SET status = 2 WHERE race_name = ? and date = ?;";

	/**
	 * The Constant GET_HORSES_FROM_RACE. Query to get horses data from race.
	 */
	private static final String GET_HORSES_FROM_RACE = "SELECT horse_in_race.horse_id, horse.name, horse.color, horse.weight FROM horse_in_race JOIN horse ON horse_in_race.horse_id = horse.id WHERE horse_in_race.horse_id IN (SELECT horse_in_race.horse_id FROM horse_in_race WHERE horse_in_race.horse_race_name = ? AND horse_in_race.horse_race_date = ? AND horse_in_race.status = 1) AND horse_in_race.horse_race_name = ? AND horse_in_race.horse_race_date = ?;";

	/**
	 * The Constant DELETE_HORSE_IN_RACE. Query to change horse status on
	 * "Decline" in race.
	 */
	private static final String DELETE_HORSE_IN_RACE = "UPDATE horse_in_race SET status = 2 WHERE horse_race_name = ? AND horse_race_date = ? AND horse_id = ?;";

	/**
	 * The Constant DECLINE_HORSE. Query (procedure) to decline horse in race.
	 */
	private static final String DECLINE_HORSE = "CALL races.decline_horse(?, ?, ?);";

	/**
	 * The Constant GET_RACES_TO_BOOKMAKER. Query to get all available (with
	 * status "Open" or "Closed" for bookmaker.
	 */
	private static final String GET_RACES_TO_BOOKMAKER = "SELECT race.race_name, race.date, race.status, hippodrom.city, hippodrom.country FROM race JOIN hippodrom ON race.race_name = hippodrom.name WHERE race.status = 1 OR race.status = 2 ORDER BY race.date DESC;";

	/**
	 * The Constant GET_HORSES_TO_SET_COEFF. Query to get horses list to set
	 * coefficient page.
	 */
	private static final String GET_HORSES_TO_SET_COEFF = "SELECT horse.id, horse.name, horse_in_race.coeff FROM horse_in_race JOIN horse ON horse_in_race.horse_id = horse.id WHERE horse_in_race.horse_race_name = ? AND horse_in_race.horse_race_date = ?;";

	/** The Constant UPDATE_HORSE_COEFF. Query to update horse coefficient. */
	private static final String UPDATE_HORSE_COEFF = "UPDATE horse_in_race SET coeff = ? WHERE horse_id = ? AND horse_race_name = ? AND horse_race_date = ?;";

	/** The Constant DECLINE_RACE. Query (procedure) to decline race. */
	// delete race
	private static final String DECLINE_RACE = "CALL races.decline_race(?, ?);";

	/** The Constant DELETE_RACE. QUery to decline race. */
	private static final String DELETE_RACE = "UPDATE race SET race.status = 3 WHERE race.race_name = ? AND race.date = ?;";

	/**
	 * The Constant DELETE_RACE_BOOKMAKER. Query to decline race by bookmaker
	 * role.
	 */
	private static final String DELETE_RACE_BOOKMAKER = "UPDATE race SET race.status = 2 WHERE race.race_name = ? AND race.date = ?;";

	/**
	 * The Constant SELECT_RACE_TO_BETS. Query to select race to make bets on
	 * it.
	 */
	// bets
	private static final String SELECT_RACE_TO_BETS = "SELECT race.date, race.status FROM race WHERE race.race_name = ? AND race.status = 1 ORDER BY race.date DESC;";

	/** The Constant FIND_PREV_RACE_LIST. Query to find previous race list. */
	private static final String FIND_PREV_RACE_LIST = "SELECT race_name, date, status FROM race WHERE status <> 1 AND status <>2 ORDER BY date DESC;";

	/** The Constant TODAY_RACE_COUNT. Query to find today races count. */
	// statistic
	private static final String TODAY_RACE_COUNT = "SELECT count(race_name)  FROM race WHERE date_format(date, '%Y%m%d') = date_format(now(), '%Y%m%d');";

	/** The Constant THIS_MONTH_RACE_COUNT. Query to find month races count. */
	private static final String THIS_MONTH_RACE_COUNT = "SELECT count(race_name)  FROM race WHERE date_format(date, '%Y%m') = date_format(now(), '%Y%m');";

	/**
	 * The Constant MILLISECONDS. Represent milliseconds value when we retrieve
	 * date from DB.
	 */
	private static final String MILLISECONDS = ".0";

	/**
	 * The Constant EMPTY_VALUE. Empty string. Needs to replace milliseconds
	 * value on it to get right result.
	 */
	private static final String EMPTY_VALUE = "";

	/**
	 * Creates the table with all races for admin page.
	 *
	 * @return the list with data about all races
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<Race> createAdminRaceTable() throws DAOException {
		PreparedStatement raceTableStatement = null;
		List<Race> list = null;
		try {
			list = new ArrayList<Race>();
			raceTableStatement = wrapConnection.getPreparedStatement(ADMIN_RACE_TABLE);
			ResultSet resSet = raceTableStatement.executeQuery();
			while (resSet.next()) {
				String stringDate = resSet.getTimestamp(2).toString().replace(MILLISECONDS, EMPTY_VALUE);
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				LocalDateTime dateTime = LocalDateTime.parse(stringDate, formatter);

				Race race = new Race(resSet.getString(1), resSet.getString(3), resSet.getString(4), resSet.getString(5),
						resSet.getInt(6));
				race.setDate(dateTime);
				list.add(race);
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(raceTableStatement);
		}
	}

	/**
	 * Carry out race.
	 *
	 * @param raceData
	 *            the race data of race which needs to carry out
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void carryOutRace(RaceData raceData) throws DAOException {
		PreparedStatement selectHorsesStatement = null;
		PreparedStatement selectPlaceStatement = null;
		PreparedStatement carryOutStatement = null;
		List<Horse> horseList;
		try {
			wrapConnection.setAutoCommit(false);
			selectHorsesStatement = wrapConnection.getPreparedStatement(SELECT_HORSES_FROM_RACE);

			selectHorsesStatement.setString(1, raceData.getName());
			selectHorsesStatement.setTimestamp(2, java.sql.Timestamp.valueOf(raceData.getDate()));

			ResultSet horseSet = selectHorsesStatement.executeQuery();
			horseList = new ArrayList<Horse>();
			while (horseSet.next()) {
				horseList.add(new Horse(horseSet.getInt(1), horseSet.getString(2), horseSet.getString(3),
						horseSet.getInt(4)));
			}

			int horsePlace = 1;
			for (int i = 0; i < horseList.size();) {
				selectPlaceStatement = wrapConnection.getPreparedStatement(SET_HORSE_PLACE);
				selectPlaceStatement.setInt(1, horsePlace);
				selectPlaceStatement.setString(2, raceData.getName());
				selectPlaceStatement.setTimestamp(3, java.sql.Timestamp.valueOf(raceData.getDate()));

				int currentHorse = ThreadLocalRandom.current().nextInt(0, horseList.size());
				selectPlaceStatement.setInt(4, horseList.get(currentHorse).getId());
				selectPlaceStatement.executeUpdate();
				wrapConnection.commit();

				horseList.remove(currentHorse);
				horsePlace++;
			}

			carryOutStatement = wrapConnection.getPreparedStatement(CARRY_OUT_RACE);
			carryOutStatement.setString(1, raceData.getName());
			carryOutStatement.setTimestamp(2, java.sql.Timestamp.valueOf(raceData.getDate()));
			carryOutStatement.executeUpdate();
			wrapConnection.commit();
		} catch (SQLException e) {
			try {
				wrapConnection.rollback();
			} catch (SQLException ex) {
				LOGGER.error("Can't rollback data. " + e);
			}
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			try {
				wrapConnection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Can't set autocommit in connection. " + e);
			}
			closeStatement(selectHorsesStatement);
			closeStatement(selectPlaceStatement);
			closeStatement(carryOutStatement);
		}
	}

	/**
	 * Creates the race.
	 *
	 * @param raceData
	 *            the race data
	 * @param horseMap
	 *            the list with horses in race
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void createRace(RaceData raceData, Map<String, String> horseMap) throws DAOException {
		PreparedStatement createRaceStatement = null;
		PreparedStatement addHorseStatement = null;
		try {
			wrapConnection.setAutoCommit(false);
			createRaceStatement = wrapConnection.getPreparedStatement(CREATE_RACE);
			createRaceStatement.setString(1, raceData.getName());
			createRaceStatement.setTimestamp(2, java.sql.Timestamp.valueOf(raceData.getDate()));
			createRaceStatement.executeUpdate();

			addHorseStatement = wrapConnection.getPreparedStatement(ADD_HORSE_IN_RACE);
			for (Map.Entry<String, String> entry : horseMap.entrySet()) {
				addHorseStatement.setInt(1, Integer.valueOf(entry.getKey()));
				addHorseStatement.setString(2, raceData.getName());
				addHorseStatement.setTimestamp(3, java.sql.Timestamp.valueOf(raceData.getDate()));
				addHorseStatement.executeUpdate();
			}
			wrapConnection.commit();
		} catch (SQLException e) {
			try {
				wrapConnection.rollback();
			} catch (SQLException ex) {
				LOGGER.error("Can't rollback data. " + e);
			}
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			try {
				wrapConnection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Can't set autocommit in connection. " + e);
			}
			closeStatement(createRaceStatement);
			closeStatement(addHorseStatement);
		}
	}

	/**
	 * Adds the horse in race.
	 *
	 * @param horseMap
	 *            the list with horse to add in race
	 * @param raceData
	 *            the race data where to add horses
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void addHorseInRace(Map<String, String> horseMap, RaceData raceData) throws DAOException {
		PreparedStatement prepStGetHorse = null;
		PreparedStatement addHorseStatement = null;
		PreparedStatement updateHorseStatement = null;
		try {
			wrapConnection.setAutoCommit(false);
			prepStGetHorse = wrapConnection.getPreparedStatement(GET_HORSE_STATUS_FROM_RACE);
			addHorseStatement = wrapConnection.getPreparedStatement(ADD_HORSE_IN_RACE);
			updateHorseStatement = wrapConnection.getPreparedStatement(UPDATE_HORSE_IN_RACE);

			for (Map.Entry<String, String> entry : horseMap.entrySet()) {
				prepStGetHorse.setInt(1, Integer.valueOf(entry.getKey()));
				prepStGetHorse.setString(2, raceData.getName());
				prepStGetHorse.setTimestamp(3, java.sql.Timestamp.valueOf(raceData.getDate()));
				ResultSet tempSet = prepStGetHorse.executeQuery();
				if (!tempSet.next()) {
					addHorseStatement.setInt(1, Integer.valueOf(entry.getKey()));
					addHorseStatement.setString(2, raceData.getName());
					addHorseStatement.setTimestamp(3, java.sql.Timestamp.valueOf(raceData.getDate()));
					addHorseStatement.executeUpdate();
					wrapConnection.commit();
				} else {
					updateHorseStatement.setInt(1, Integer.valueOf(entry.getKey()));
					updateHorseStatement.setString(2, raceData.getName());
					updateHorseStatement.setTimestamp(3, java.sql.Timestamp.valueOf(raceData.getDate()));
					updateHorseStatement.executeUpdate();
					wrapConnection.commit();
				}
			}
			wrapConnection.commit();
		} catch (SQLException e) {
			try {
				wrapConnection.rollback();
			} catch (SQLException ex) {
				LOGGER.error("Can't rollback data. " + e);
			}
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			try {
				wrapConnection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Can't set autocommit in connection. " + e);
			}
			closeStatement(prepStGetHorse);
			closeStatement(addHorseStatement);
			closeStatement(updateHorseStatement);
		}
	}

	/**
	 * Find horses in race.
	 *
	 * @param raceData
	 *            the race data where to find horses
	 * @return the list with data about horses in race
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<Horse> findEditHorseList(RaceData raceData) throws DAOException {
		List<Horse> horseList = new LinkedList<Horse>();
		PreparedStatement getHorsesStatement = null;
		try {
			getHorsesStatement = wrapConnection.getPreparedStatement(GET_HORSES_FROM_RACE);
			getHorsesStatement.setString(1, raceData.getName());
			getHorsesStatement.setTimestamp(2, java.sql.Timestamp.valueOf(raceData.getDate()));
			getHorsesStatement.setString(3, raceData.getName());
			getHorsesStatement.setTimestamp(4, java.sql.Timestamp.valueOf(raceData.getDate()));

			ResultSet horseSet = getHorsesStatement.executeQuery();

			while (horseSet.next()) {
				horseList.add(new Horse(horseSet.getInt(1), horseSet.getString(2), horseSet.getString(3),
						horseSet.getInt(4)));
			}
			return horseList;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(getHorsesStatement);
		}
	}

	/**
	 * Edits the race.
	 *
	 * @param list
	 *            the list with edit horses
	 * @param raceData
	 *            the race data which race to edit
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void editRace(List<EditRaceData> list, RaceData raceData) throws DAOException {
		CallableStatement declineHorseStatement = null;
		PreparedStatement deleteHorseStatement = null;
		PreparedStatement getHorseStatusStatement = null;
		PreparedStatement addHorseStatement = null;
		PreparedStatement updateHorseStatement = null;
		PreparedStatement closeRaceStatement = null;
		try {
			wrapConnection.setAutoCommit(false);
			declineHorseStatement = wrapConnection.getCallableStatement(DECLINE_HORSE);
			deleteHorseStatement = wrapConnection.getPreparedStatement(DELETE_HORSE_IN_RACE);
			getHorseStatusStatement = wrapConnection.getPreparedStatement(GET_HORSE_STATUS_FROM_RACE);
			addHorseStatement = wrapConnection.getPreparedStatement(ADD_HORSE_IN_RACE);
			updateHorseStatement = wrapConnection.getPreparedStatement(UPDATE_HORSE_IN_RACE);
			closeRaceStatement = wrapConnection.getPreparedStatement(CLOSE_RACE);

			for (int i = 0; i < list.size(); i++) {
				String str = list.get(i).getAction();
				switch (str) {
				case "delete": {
					declineHorseStatement.setString(1, raceData.getName());
					declineHorseStatement.setTimestamp(2, java.sql.Timestamp.valueOf(raceData.getDate()));
					declineHorseStatement.setInt(3, Integer.valueOf(list.get(i).getHorseId()));
					declineHorseStatement.executeUpdate();

					deleteHorseStatement.setString(1, raceData.getName());
					deleteHorseStatement.setTimestamp(2, java.sql.Timestamp.valueOf(raceData.getDate()));
					deleteHorseStatement.setInt(3, Integer.valueOf(list.get(i).getHorseId()));
					deleteHorseStatement.executeUpdate();

					wrapConnection.commit();
				}
					break;

				case "add": {
					getHorseStatusStatement.setInt(1, Integer.valueOf(list.get(i).getHorseId()));
					getHorseStatusStatement.setString(2, raceData.getName());
					getHorseStatusStatement.setTimestamp(3, java.sql.Timestamp.valueOf(raceData.getDate()));

					ResultSet tempSet = getHorseStatusStatement.executeQuery();
					if (!tempSet.next()) {
						addHorseStatement.setInt(1, Integer.valueOf(list.get(i).getHorseId()));
						addHorseStatement.setString(2, raceData.getName());
						addHorseStatement.setTimestamp(3, java.sql.Timestamp.valueOf(raceData.getDate()));
						addHorseStatement.executeUpdate();
						wrapConnection.commit();
					} else {
						updateHorseStatement.setInt(1, Integer.valueOf(list.get(i).getHorseId()));
						updateHorseStatement.setString(2, raceData.getName());
						updateHorseStatement.setTimestamp(3, java.sql.Timestamp.valueOf(raceData.getDate()));
						updateHorseStatement.executeUpdate();
						wrapConnection.commit();
					}
				}
					break;

				default:
					throw new SQLException(HotRacesErrorCode.EMPTY_SELECTED_ROW);
				}
			}
			closeRaceStatement.setString(1, raceData.getName());
			closeRaceStatement.setTimestamp(2, java.sql.Timestamp.valueOf(raceData.getDate()));
			closeRaceStatement.executeUpdate();
			wrapConnection.commit();
		} catch (SQLException e) {
			try {
				wrapConnection.rollback();
			} catch (SQLException ex) {
				LOGGER.error("Can't rollback data. " + e);
			}
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			try {
				wrapConnection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Can't set autocommit in connection. " + e);
			}
			closeStatement(declineHorseStatement);
			closeStatement(deleteHorseStatement);
			closeStatement(getHorseStatusStatement);
			closeStatement(addHorseStatement);
			closeStatement(updateHorseStatement);
			closeStatement(closeRaceStatement);
		}
	}

	/**
	 * Find table for bookmaker with available races.
	 *
	 * @return the list with race data
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<BookmakerTableRaceData> findBookmakerTable() throws DAOException {
		List<BookmakerTableRaceData> list = null;
		PreparedStatement prepStatement = null;
		try {
			list = new ArrayList<BookmakerTableRaceData>();
			prepStatement = wrapConnection.getPreparedStatement(GET_RACES_TO_BOOKMAKER);
			ResultSet raceSet = prepStatement.executeQuery();
			while (raceSet.next()) {
				String stringDate = raceSet.getTimestamp(2).toString().replace(MILLISECONDS, EMPTY_VALUE);
				list.add(new BookmakerTableRaceData(raceSet.getString(1), stringDate, raceSet.getString(3),
						raceSet.getString(4), raceSet.getString(5)));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(prepStatement);
		}
	}

	/**
	 * Find horses to set coefficient.
	 *
	 * @param raceData
	 *            the race data
	 * @return the list with horses on which set coefficient
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<BookmakerSetCoeffData> findHorsesToSetCoeff(RaceData raceData) throws DAOException {
		List<BookmakerSetCoeffData> list = null;
		PreparedStatement prepStatement = null;
		try {
			list = new ArrayList<BookmakerSetCoeffData>();
			prepStatement = wrapConnection.getPreparedStatement(GET_HORSES_TO_SET_COEFF);
			prepStatement.setString(1, raceData.getName());
			prepStatement.setTimestamp(2, java.sql.Timestamp.valueOf(raceData.getDate()));
			ResultSet resSet = prepStatement.executeQuery();
			while (resSet.next()) {
				list.add(new BookmakerSetCoeffData(resSet.getInt(1), resSet.getString(2), resSet.getBigDecimal(3)));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(prepStatement);
		}
	}

	/**
	 * Sets the coefficient on horses in race.
	 *
	 * @param raceData
	 *            the race data
	 * @param list
	 *            the list of horses and coefficients
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void setCoeffInRace(RaceData raceData, List<BookmakerSetCoeffData> list) throws DAOException {
		PreparedStatement updateCoeffStatement = null;
		PreparedStatement openRaceStatement = null;

		try {
			wrapConnection.setAutoCommit(false);
			updateCoeffStatement = wrapConnection.getPreparedStatement(UPDATE_HORSE_COEFF);
			openRaceStatement = wrapConnection.getPreparedStatement(OPEN_RACE);

			for (int i = 0; i < list.size(); i++) {

				updateCoeffStatement.setBigDecimal(1, list.get(i).getCoeff());
				updateCoeffStatement.setInt(2, list.get(i).getHorseId());
				updateCoeffStatement.setString(3, raceData.getName());
				updateCoeffStatement.setTimestamp(4, java.sql.Timestamp.valueOf(raceData.getDate()));
				updateCoeffStatement.executeUpdate();
				wrapConnection.commit();
			}

			openRaceStatement.setString(1, raceData.getName());
			openRaceStatement.setTimestamp(2, java.sql.Timestamp.valueOf(raceData.getDate()));
			openRaceStatement.executeUpdate();
			wrapConnection.commit();
		} catch (SQLException e) {
			try {
				wrapConnection.rollback();
			} catch (SQLException ex) {
				LOGGER.error("Can't rollback data. " + e);
			}
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			try {
				wrapConnection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Can't set autocommit in connection. " + e);
			}
			closeStatement(updateCoeffStatement);
			closeStatement(openRaceStatement);
		}
	}

	/**
	 * Find race to bets. (Method to test app.)
	 *
	 * @param race
	 *            the race where to find data
	 * @return the list with available races to bets
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<BetsRaceData> findRaceToBets(RaceEnum race) throws DAOException {
		List<BetsRaceData> list = null;
		PreparedStatement prepStatement = null;
		try {
			list = new ArrayList<BetsRaceData>();
			prepStatement = wrapConnection.getPreparedStatement(SELECT_RACE_TO_BETS);
			prepStatement.setString(1, race.getValue());
			ResultSet raceSet = prepStatement.executeQuery();
			while (raceSet.next()) {
				String date = raceSet.getTimestamp(1).toString().replace(MILLISECONDS, EMPTY_VALUE);
				list.add(new BetsRaceData(date, raceSet.getString(2)));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(prepStatement);
		}
	}

	/**
	 * Declines race and all horses in it.
	 *
	 * @param raceData
	 *            the race data
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void deleteRace(RaceData raceData) throws DAOException {
		CallableStatement declineRaceStatement = null;
		PreparedStatement deleteRaceStatement = null;

		try {
			wrapConnection.setAutoCommit(false);
			declineRaceStatement = wrapConnection.getCallableStatement(DECLINE_RACE);
			declineRaceStatement.setString(1, raceData.getName());
			declineRaceStatement.setTimestamp(2, java.sql.Timestamp.valueOf(raceData.getDate()));
			declineRaceStatement.executeUpdate();
			wrapConnection.commit();

			deleteRaceStatement = wrapConnection.getPreparedStatement(DELETE_RACE);
			deleteRaceStatement.setString(1, raceData.getName());
			deleteRaceStatement.setTimestamp(2, java.sql.Timestamp.valueOf(raceData.getDate()));
			deleteRaceStatement.executeUpdate();
			wrapConnection.commit();
		} catch (SQLException e) {
			try {
				wrapConnection.rollback();
			} catch (SQLException ex) {
				LOGGER.error("Can't rollback data. " + e);
			}
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			try {
				wrapConnection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Can't set autocommit in connection. " + e);
			}
			closeStatement(declineRaceStatement);
			closeStatement(deleteRaceStatement);
		}
	}

	/**
	 * Delete race as bookmaker.
	 *
	 * @param raceData
	 *            the race data
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void deleteRaceBookmaker(RaceData raceData) throws DAOException {
		PreparedStatement prepStatement = null;

		try {
			prepStatement = wrapConnection.getPreparedStatement(DELETE_RACE_BOOKMAKER);
			prepStatement.setString(1, raceData.getName());
			prepStatement.setString(2, raceData.getDate());
			prepStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(prepStatement);
		}
	}

	/**
	 * Find race list.
	 *
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<RaceData> findAllHippodromesList() throws DAOException {
		PreparedStatement prepStatement = null;
		List<RaceData> list = null;
		try {
			list = new ArrayList<RaceData>();
			prepStatement = wrapConnection.getPreparedStatement(FIND_HIPP_LIST);
			ResultSet hippSet = prepStatement.executeQuery();
			while (hippSet.next()) {
				list.add(new RaceData(hippSet.getString(1)));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(prepStatement);
		}
	}

	/**
	 * Find previous race list.
	 *
	 * @return the list with all previous races
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<PrevRaceData> findPrevRaceList() throws DAOException {
		List<PrevRaceData> list = null;
		PreparedStatement prepStatement = null;
		try {
			list = new ArrayList<PrevRaceData>();
			prepStatement = wrapConnection.getPreparedStatement(FIND_PREV_RACE_LIST);
			ResultSet prevRaceSet = prepStatement.executeQuery();
			while (prevRaceSet.next()) {
				String stringDate = prevRaceSet.getTimestamp(2).toString().replace(MILLISECONDS, EMPTY_VALUE);
				list.add(new PrevRaceData(prevRaceSet.getString(1), stringDate, prevRaceSet.getString(3)));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		}
	}

	/**
	 * Find today race count.
	 *
	 * @return the int value of race count
	 * @throws DAOException
	 *             the DAO exception
	 */
	public int findTodayRaceCount() throws DAOException {
		int count = 0;
		PreparedStatement prepStatement = null;
		try {
			prepStatement = wrapConnection.getPreparedStatement(TODAY_RACE_COUNT);
			ResultSet betSet = prepStatement.executeQuery();
			if (betSet.next()) {
				return betSet.getInt(1);
			} else {
				return count;
			}
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(prepStatement);
		}
	}

	/**
	 * Find month race count.
	 *
	 * @return the int value of month race count
	 * @throws DAOException
	 *             the DAO exception
	 */
	public int findMonthRaceCount() throws DAOException {
		int count = 0;
		PreparedStatement prepStatement = null;
		try {
			prepStatement = wrapConnection.getPreparedStatement(THIS_MONTH_RACE_COUNT);
			ResultSet betSet = prepStatement.executeQuery();
			if (betSet.next()) {
				return betSet.getInt(1);
			} else {
				return count;
			}
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(prepStatement);
		}
	}

	/**
	 * Gets the race status.
	 *
	 * @param raceData
	 *            the race data
	 * @return the race status
	 * @throws DAOException
	 *             the DAO exception
	 */
	public String getRaceStatus(RaceData raceData) throws DAOException {
		PreparedStatement raceStatusStatement = null;
		try {
			String result = "";
			raceStatusStatement = wrapConnection.getPreparedStatement(FIND_RACE_STATUS);
			raceStatusStatement.setString(1, raceData.getName());
			raceStatusStatement.setString(2, raceData.getDate());
			ResultSet resSet = raceStatusStatement.executeQuery();
			if (resSet.next()) {
				result = resSet.getString(1);
			}
			return result;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.CANT_GET_RACE_STATUS);
		} finally {
			closeStatement(raceStatusStatement);
		}
	}
}