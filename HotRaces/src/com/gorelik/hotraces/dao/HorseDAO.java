package com.gorelik.hotraces.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.gorelik.hotraces.entity.Horse;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.logic.horse.HorseData;
import com.gorelik.hotraces.logic.race.RaceData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class HorseDAO. Class provides some process with horses.
 */
public class HorseDAO extends AbstractDAO {
	/** The Constant GET_HORSES_LIST. Query to find all horses. */
	private static final String GET_HORSES_LIST = "SELECT id, name, color, weight FROM races.horse;";

	/** The Constant FIND_HORSE_DATA. Query to find all data about horse in race. */
	private static final String FIND_HORSE_DATA = "SELECT horse.name, horse_in_race.horse_id, horse_in_race.coeff, horse_in_race.place FROM horse_in_race JOIN horse ON horse_in_race.horse_id = horse.id WHERE horse_in_race.horse_race_name = ? AND horse_in_race.horse_race_date = ?;";

	/** The Constant REAL_DELETE_HORSE. Query to delete horse from race. */
	private static final String REAL_DELETE_HORSE = "DELETE FROM horse_in_race WHERE horse_race_name = ? AND horse_race_date = ? AND horse_id = ?;";

	/** The Constant REAL_INSERT_HORSE. Query to insert horse into race. */
	private static final String REAL_INSERT_HORSE = "INSERT INTO horse_in_race(`horse_race_date`, `horse_race_name`, `horse_id`, `status`) values(?, ?, ?, 1);";

	/**
	 * Find list of all horses in DB.
	 *
	 * @return the list with data about all horses
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<Horse> findAllHorsesList() throws DAOException {
		List<Horse> list = null;
		PreparedStatement prepStatement = null;
		try {
			list = new ArrayList<Horse>();
			prepStatement = wrapConnection.getPreparedStatement(GET_HORSES_LIST);
			ResultSet horseSet = prepStatement.executeQuery();

			while (horseSet.next()) {
				list.add(new Horse(horseSet.getInt(1), horseSet.getString(2), horseSet.getString(3),
						horseSet.getInt(4)));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(prepStatement);
		}
	}

	/**
	 * Find data about horses in previous race.
	 *
	 * @param raceData
	 *            the race data where to find horse
	 * @return the list with data about horses
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<HorseData> findHorsesDataInPrevRace(RaceData raceData) throws DAOException {
		PreparedStatement prepStatement = null;
		List<HorseData> list = null;
		try {
			list = new ArrayList<HorseData>();
			prepStatement = wrapConnection.getPreparedStatement(FIND_HORSE_DATA);
			prepStatement.setString(1, raceData.getName());
			prepStatement.setTimestamp(2, java.sql.Timestamp.valueOf(raceData.getDate()));
			ResultSet horseSet = prepStatement.executeQuery();
			while (horseSet.next()) {
				list.add(new HorseData(horseSet.getString(1), horseSet.getInt(2), horseSet.getBigDecimal(3),
						horseSet.getInt(4)));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(prepStatement);
		}
	}

	/**
	 * Real insert horse in DB.
	 *
	 * @param raceData
	 *            the race data where to insert horse
	 * @param horseId
	 *            the horse unique id
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void realInsertHorse(RaceData raceData, int horseId) throws DAOException {
		PreparedStatement prepStatement = null;
		try {
			prepStatement = wrapConnection.getPreparedStatement(REAL_INSERT_HORSE);
			prepStatement.setString(1, raceData.getDate());
			prepStatement.setString(2, raceData.getName());
			prepStatement.setInt(3, horseId);
			prepStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.CANT_REAL_DELETE_HORSE);
		} finally {
			closeStatement(prepStatement);
		}
	}

	/**
	 * Real delete horse from race.
	 *
	 * @param raceData
	 *            the race data where to insert horse
	 * @param horseId
	 *            the horse unique id
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void realDeleteHorse(RaceData raceData, int horseId) throws DAOException {
		PreparedStatement prepStatement = null;
		try {
			prepStatement = wrapConnection.getPreparedStatement(REAL_DELETE_HORSE);
			prepStatement.setString(1, raceData.getName());
			prepStatement.setString(2, raceData.getDate());
			prepStatement.setInt(3, horseId);
			prepStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.CANT_REAL_DELETE_HORSE);
		} finally {
			closeStatement(prepStatement);
		}
	}
}
