package com.gorelik.hotraces.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.connection.ConnectionPool;
import com.gorelik.hotraces.connection.WrapperConnection;

/**
 * The Class AbstractDAO. The class provides some process with DB.
 */
public abstract class AbstractDAO implements AutoCloseable {

	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(AbstractDAO.class);

	/** The wrapper connection. */
	protected WrapperConnection wrapConnection;

	/**
	 * Instantiates a new abstract DAO.
	 */
	public AbstractDAO() {
		this.wrapConnection = ConnectionPool.getConnection();
	}

	/**
	 * Sets the connection.
	 *
	 * @param conn
	 *            the Connection
	 */
	public void setConnection(Connection conn) {
		this.wrapConnection.setConnection(conn);
	}

	/**
	 * Close statement.
	 *
	 * @param st
	 *            the Statement
	 */
	protected void closeStatement(Statement st) {
		try {
			if (st != null) {
				st.close();
			}
		} catch (SQLException e) {
			LOGGER.error("Can't close statement." + e);
		}
	}

	@Override
	public void close() {
		this.wrapConnection.close();
	}
}
