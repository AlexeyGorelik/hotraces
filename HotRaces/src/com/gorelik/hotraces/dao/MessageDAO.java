package com.gorelik.hotraces.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.entity.Message;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class MessageDAO. Class provides some process with messages.
 */
public class MessageDAO extends AbstractDAO {
	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(MessageDAO.class);

	/**
	 * The Constant GET_CONV_MESSAGES_LIST. 
	 * Query to find all conversation for current user.
	 */
	private static final String GET_CONV_MESSAGES_LIST = "SELECT u.id, c.conv_id, u.name FROM conversation c, user u WHERE CASE WHEN c.user_one = ? THEN c.user_two = u.id WHEN c.user_two = ? THEN c.user_one = u.id END AND (c.user_one = ? OR c.user_two = ?) ORDER BY c.conv_id DESC;";

	/**
	 * The Constant GET_REPLY_MESSAGES_LIST. 
	 * Query to find last message for current user with another user.
	 */
	private static final String GET_REPLY_MESSAGES_LIST = "SELECT conversation_reply.time, conversation_reply.reply FROM conversation_reply WHERE conversation_reply.time = (select max(time) from conversation_reply where conv_id_fk = ?) ORDER BY conversation_reply.time;";

	/**
	 * The Constant GET_USER_CONVERSATION. 
	 * Query to find all messages for current user with another user.
	 */
	private static final String GET_USER_CONVERSATION = "SELECT R.conv_repl_id, R.time, R.reply, U.id, U.name, U.avatarpath FROM user U, conversation_reply R WHERE R.user_id_fk = U.id AND R.conv_id_fk = (SELECT DISTINCT conv_id_fk FROM conversation_reply WHERE conversation_reply.conv_id_fk = (SELECT conv_id FROM conversation WHERE (user_one = ? AND user_two = (SELECT id FROM user WHERE name = ?)) OR (user_one = (SELECT id FROM user WHERE name = ?) AND user_two = ?))) ORDER BY R.conv_repl_id;";

	/** The Constant GET_CONV_ID. Query to find current conversation id. */
	private static final String GET_CONV_ID = "SELECT conv_id FROM races.conversation WHERE (user_one = ? and user_two = (select id from user where name = ?)) or (user_one = (select id from user where name = ?) and user_two = ?);";

	/** The Constant GET_CONV_ID_BY_ID. Query to find conversation by id. */
	private static final String GET_CONV_ID_BY_ID = "SELECT conv_id FROM races.conversation WHERE user_one = ? and user_two = ?;";

	/** The Constant SEND_MESSAGE. Query to send message to another user. */
	private static final String SEND_MESSAGE = "INSERT INTO conversation_reply (`reply`, `user_id_fk`, `time`, `conv_id_fk`) VALUES(?, ?, ?, ?);";

	/** The Constant GET_USER_ID. Query to get user id for conversation. */
	private static final String GET_USER_ID = "SELECT id FROM user WHERE user.name = ?;";

	/**
	 * The Constant CREATE_NEW_CONVERSATION. Query to create new conversation
	 * with another user.
	 */
	private static final String CREATE_NEW_CONVERSATION = "INSERT INTO conversation (`user_one`, `user_two`, `time`) VALUES(?, ?, ?);";

	/**
	 * The Constant MILLISECONDS. Represent milliseconds value when we retrieve
	 * date from DB.
	 */
	private static final String MILLISECONDS = ".0";

	/**
	 * The Constant EMPTY_VALUE. Empty string. Needs to replace milliseconds
	 * value on it to get right result.
	 */
	private static final String EMPTY_VALUE = "";

	/**
	 * Find message list for current user.
	 *
	 * @param userId
	 *            the id of needed user
	 * @return the list with all messages to another users
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<Message> findMessageList(int userId) throws DAOException {
		List<Message> list = null;
		PreparedStatement getConvStatement = null;
		PreparedStatement convReplyStatement = null;
		try {
			list = new ArrayList<Message>();
			getConvStatement = wrapConnection.getPreparedStatement(GET_CONV_MESSAGES_LIST);
			getConvStatement.setInt(1, userId);
			getConvStatement.setInt(2, userId);
			getConvStatement.setInt(3, userId);
			getConvStatement.setInt(4, userId);
			ResultSet messageSet = getConvStatement.executeQuery();
			while (messageSet.next()) {
				list.add(new Message(messageSet.getInt(1), messageSet.getString(3), messageSet.getInt(2)));
			}

			for (int i = 0; i < list.size(); i++) {
				convReplyStatement = wrapConnection.getPreparedStatement(GET_REPLY_MESSAGES_LIST);
				convReplyStatement.setInt(1, list.get(i).getConvId());
				ResultSet tempSet = convReplyStatement.executeQuery();
				if (tempSet.next()) {
					list.get(i).setText(tempSet.getString(2));
					String date = tempSet.getTimestamp(1).toString().replace(MILLISECONDS, EMPTY_VALUE);
					list.get(i).setDate(date);
				}
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(getConvStatement);
			closeStatement(convReplyStatement);
		}
	}

	/**
	 * Find conversation with another user.
	 *
	 * @param userId the needed user id
	 * @param userTo the user to whom user has conversation
	 * @return the list of all messages between two users
	 * @throws DAOException the DAO exception
	 */
	public List<Message> findConversation(int userId, String userTo) throws DAOException {
		List<Message> list = null;
		PreparedStatement getConvStatement = null;
		try {
			list = new ArrayList<Message>();
			getConvStatement = wrapConnection.getPreparedStatement(GET_USER_CONVERSATION);
			getConvStatement.setInt(1, userId);
			getConvStatement.setString(2, userTo);
			getConvStatement.setString(3, userTo);
			getConvStatement.setInt(4, userId);
			ResultSet messageSet = getConvStatement.executeQuery();

			while (messageSet.next()) {
				String date = messageSet.getTimestamp(2).toString().replace(MILLISECONDS, EMPTY_VALUE);
				list.add(new Message(messageSet.getInt(4), messageSet.getString(5), date, messageSet.getString(3),
						messageSet.getString(6)));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(getConvStatement);
		}
	}

	/**
	 * Send message to another user.
	 *
	 * @param userIdFrom the user id who sends message
	 * @param userToName the user to whom sends message
	 * @param text the message text
	 * @throws DAOException the DAO exception
	 */
	public void sendMessage(int userIdFrom, String userToName, String text) throws DAOException {
		PreparedStatement convIdStatement = null;
		PreparedStatement sendMsgStatement = null;
		try {
			convIdStatement = wrapConnection.getPreparedStatement(GET_CONV_ID);
			convIdStatement.setInt(1, userIdFrom);
			convIdStatement.setString(2, userToName);
			convIdStatement.setString(3, userToName);
			convIdStatement.setInt(4, userIdFrom);
			ResultSet resSet = convIdStatement.executeQuery();

			int convId = 0;
			if (resSet.next()) {
				convId = resSet.getInt(1);
			}

			sendMsgStatement = wrapConnection.getPreparedStatement(SEND_MESSAGE);
			sendMsgStatement.setString(1, text);
			sendMsgStatement.setInt(2, userIdFrom);
			sendMsgStatement.setTimestamp(3, java.sql.Timestamp.valueOf(LocalDateTime.now()));
			sendMsgStatement.setInt(4, convId);
			sendMsgStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(convIdStatement);
			closeStatement(sendMsgStatement);
		}
	}

	/**
	 * Send new message to another user. Can't send message to user with which
	 * there is already a conversation.
	 *
	 * @param userIdFrom the user id from
	 * @param userToName the user to name
	 * @param text the text
	 * @throws DAOException the DAO exception
	 */
	public void sendNewMessage(int userIdFrom, String userToName, String text) throws DAOException {
		PreparedStatement convIdStatement = null;
		PreparedStatement userIdStatement = null;
		PreparedStatement createConvStatement = null;
		PreparedStatement findConvStatement = null;
		PreparedStatement sendMsgStatement = null;
		try {
			wrapConnection.setAutoCommit(false);
			convIdStatement = wrapConnection.getPreparedStatement(GET_CONV_ID);
			convIdStatement.setInt(1, userIdFrom);
			convIdStatement.setString(2, userToName);
			convIdStatement.setString(3, userToName);
			convIdStatement.setInt(4, userIdFrom);
			ResultSet tempSet = convIdStatement.executeQuery();
			if (tempSet.next()) {
				throw new DAOException(HotRacesErrorCode.CONVERSATION_ALREADY_EXISTS);
			}

			userIdStatement = wrapConnection.getPreparedStatement(GET_USER_ID);
			userIdStatement.setString(1, userToName);
			ResultSet resSet = userIdStatement.executeQuery();
			int userIdTo = 0;
			if (resSet.next()) {
				userIdTo = resSet.getInt(1);
			}

			createConvStatement = wrapConnection.getPreparedStatement(CREATE_NEW_CONVERSATION);
			createConvStatement.setInt(1, userIdFrom);
			createConvStatement.setInt(2, userIdTo);
			createConvStatement.setTimestamp(3, java.sql.Timestamp.valueOf(LocalDateTime.now()));
			createConvStatement.executeUpdate();
			wrapConnection.commit();

			findConvStatement = wrapConnection.getPreparedStatement(GET_CONV_ID_BY_ID);
			findConvStatement.setInt(1, userIdFrom);
			findConvStatement.setInt(2, userIdTo);
			ResultSet conversSet = findConvStatement.executeQuery();
			wrapConnection.commit();

			int convId = 0;
			if (conversSet.next()) {
				convId = conversSet.getInt(1);
			}

			sendMsgStatement = wrapConnection.getPreparedStatement(SEND_MESSAGE);
			sendMsgStatement.setString(1, text);
			sendMsgStatement.setInt(2, userIdFrom);
			sendMsgStatement.setTimestamp(3, java.sql.Timestamp.valueOf(LocalDateTime.now()));
			sendMsgStatement.setInt(4, convId);
			sendMsgStatement.executeUpdate();
		} catch (SQLException e) {
			try {
				wrapConnection.rollback();
			} catch (SQLException ex) {
				LOGGER.error("Can't rollback data. " + e);
			}
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			try {
				wrapConnection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Can't set autocommit in connection. " + e);
			}
			closeStatement(convIdStatement);
			closeStatement(userIdStatement);
			closeStatement(createConvStatement);
			closeStatement(findConvStatement);
			closeStatement(sendMsgStatement);
		}
	}
}