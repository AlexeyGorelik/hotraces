package com.gorelik.hotraces.dao;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.gorelik.hotraces.entity.Bet;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.logic.bets.MakeBetData;
import com.gorelik.hotraces.logic.race.RaceData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class BetDAO. Class provides some process with Bets.
 */
public class BetDAO extends AbstractDAO {

	/** The Constant LOGGER. Defines logger for handling exceptions. */
	private static final Logger LOGGER = Logger.getLogger(BetDAO.class);

	/** The Constant FIND_USER_RESULT. Query to find all user bets. */
	private static final String FIND_USER_RESULT = "SELECT bet.id, bet.horse_race_name, bet.horse_race_date, horse.name, `bet_type`, `result`, `sum`, bet.bet_date, bet.horse_id FROM bet JOIN horse ON bet.horse_id = horse.id WHERE bet.user_id = ? ORDER BY bet.id DESC;";

	/** The Constant FIND_COEFF. Query to find coefficient for current horse. */
	private static final String FIND_COEFF = "select coeff from horse_in_race where horse_race_name = ? AND horse_race_date = ? AND horse_id = ?;";

	/** The Constant GET_MAKE_BET_DATA. Query to find data for making bet. */
	private static final String GET_MAKE_BET_DATA = "SELECT horse_in_race.horse_id, horse.name, horse_in_race.coeff FROM horse_in_race JOIN horse ON horse_in_race.horse_id = horse.id WHERE horse_in_race.horse_race_name = ? AND horse_in_race.horse_race_date = ?;";

	/** The Constant MAKE_BET. Query to write bet data into DB. */
	private static final String MAKE_BET = "INSERT INTO bet(`user_id`, `bet_type`, `sum`, `bet_date`, `horse_race_name`, `horse_race_date`, `horse_id`) VALUES(?, ?, ?, ?, ?, ?, ?);";

	// admin statistic
	/** The Constant GET_TODAY_BETS_COUNT. Query to find today bet's count. */
	private static final String GET_TODAY_BETS_COUNT = "SELECT count(id) FROM bet WHERE date_format(bet_date, '%Y%m%d') = date_format(now(), '%Y%m%d');";

	/** The Constant GET_MONTH_BETS_COUNT. Query to find month bet's count. */
	private static final String GET_MONTH_BETS_COUNT = "SELECT count(id) FROM bet WHERE date_format(bet_date, '%Y%m') = date_format(now(), '%Y%m');";

	/**
	 * The Constant GET_TODAY_LOSE_INCOME. Query to find today's lose income.
	 */
	private static final String GET_TODAY_LOSE_INCOME = "SELECT sum(sum) FROM bet WHERE date_format(bet_date, '%Y%m%d') = date_format(now(), '%Y%m%d') AND result = 2;";

	/** The Constant GET_TODAY_WIN_INCOME. Query to count today's win income. */
	private static final String GET_TODAY_WIN_INCOME = "SELECT sum(sum) FROM bet WHERE date_format(bet_date, '%Y%m%d') = date_format(now(), '%Y%m%d') AND result = 1 AND bet_type = 1";

	/**
	 * The Constant GET_TODAY_FIRST_THREE_INCOME. Query to find today
	 * "first three" bet income.
	 */
	private static final String GET_TODAY_FIRST_THREE_INCOME = "SELECT ROUND(sum(sum) / 3, 1) FROM bet WHERE date_format(bet_date, '%Y%m%d') = date_format(now(), '%Y%m%d') AND result = 2;";

	/**
	 * The Constant GET_MONTH_LOSE_INCOME. Query to find month's lose income.
	 */
	private static final String GET_MONTH_LOSE_INCOME = "SELECT sum(sum) FROM bet WHERE date_format(bet_date, '%Y%m') = date_format(now(), '%Y%m') AND result = 2;";

	/** The Constant GET_MONTH_WIN_INCOME. Query to find month's win income. */
	private static final String GET_MONTH_WIN_INCOME = "SELECT sum(sum) FROM bet WHERE date_format(bet_date, '%Y%m') = date_format(now(), '%Y%m') AND result = 1 AND bet_type = 1";

	/**
	 * The Constant GET_MONTH_FIRST_THREE_INCOME. Query to find "first three"
	 * bet month's income.
	 */
	private static final String GET_MONTH_FIRST_THREE_INCOME = "SELECT ROUND(sum(sum) / 3, 1) FROM bet WHERE date_format(bet_date, '%Y%m') = date_format(now(), '%Y%m') AND result = 2;";

	/**
	 * The Constant MILLISECONDS. Represent milliseconds value when we retrieve
	 * date from DB.
	 */
	private static final String MILLISECONDS = ".0";

	/**
	 * The Constant EMPTY_VALUE. Empty string. Needs to replace milliseconds
	 * value on it to get right result.
	 */
	private static final String EMPTY_VALUE = "";

	/**
	 * Find list of previous user bets.
	 *
	 * @param user
	 *            the current user
	 * @return the list of user previous bets
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<Bet> findUserResult(User user) throws DAOException {
		List<Bet> list = null;
		PreparedStatement userListStatement = null;
		PreparedStatement coeffStatement = null;
		try {
			userListStatement = wrapConnection.getPreparedStatement(FIND_USER_RESULT);
			userListStatement.setInt(1, Integer.valueOf(user.getUserId()));

			ResultSet resultSet = userListStatement.executeQuery();
			list = new ArrayList<Bet>();

			while (resultSet.next()) {
				String raceDate = resultSet.getTimestamp(3).toString().replace(MILLISECONDS, EMPTY_VALUE);
				String date = resultSet.getTimestamp(8).toString().replace(MILLISECONDS, EMPTY_VALUE);

				Bet bet = new Bet(resultSet.getInt(1), resultSet.getString(2), raceDate, resultSet.getString(4),
						resultSet.getString(5), resultSet.getBigDecimal(7), date, resultSet.getInt(9),
						user.getUserId());
				bet.setResult(resultSet.getString(6));

				coeffStatement = wrapConnection.getPreparedStatement(FIND_COEFF);
				coeffStatement.setString(1, bet.getRaceName());
				coeffStatement.setTimestamp(2, java.sql.Timestamp.valueOf(bet.getRaceDate()));
				coeffStatement.setInt(3, bet.getHorseId());
				ResultSet resSet = coeffStatement.executeQuery();
				if (resSet.next()) {
					bet.setCoeff(resSet.getBigDecimal(1));
				}
				list.add(bet);
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(userListStatement);
			closeStatement(coeffStatement);
		}
	}

	/**
	 * Find list of horses to make bet.
	 *
	 * @param raceData
	 *            the RaceData of current data
	 * @return the list of race data to make bet
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<MakeBetData> findMakeBetList(RaceData raceData) throws DAOException {
		List<MakeBetData> list = null;
		PreparedStatement betDataStatement = null;
		try {
			list = new ArrayList<MakeBetData>();
			betDataStatement = wrapConnection.getPreparedStatement(GET_MAKE_BET_DATA);
			betDataStatement.setString(1, raceData.getName());
			betDataStatement.setTimestamp(2, java.sql.Timestamp.valueOf(raceData.getDate()));
			ResultSet betSet = betDataStatement.executeQuery();
			while (betSet.next()) {
				list.add(new MakeBetData(betSet.getInt(1), betSet.getString(2), betSet.getBigDecimal(3)));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(betDataStatement);
		}
	}

	/**
	 * Make bet.
	 *
	 * @param raceData
	 *            the RaceData of current race
	 * @param list
	 *            the list of user bets
	 * @param user
	 *            the current user
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void makeBet(RaceData raceData, List<MakeBetData> list, User user) throws DAOException {
		PreparedStatement makeBetStatement = null;
		try {
			wrapConnection.setAutoCommit(false);
			makeBetStatement = wrapConnection.getPreparedStatement(MAKE_BET);
			for (int i = 0; i < list.size(); i++) {
				makeBetStatement.setInt(1, Integer.valueOf(user.getUserId()));
				makeBetStatement.setInt(2, list.get(i).getBetType());
				makeBetStatement.setBigDecimal(3, list.get(i).getSum());
				makeBetStatement.setTimestamp(4, java.sql.Timestamp.valueOf(LocalDateTime.now()));
				makeBetStatement.setString(5, raceData.getName());
				makeBetStatement.setTimestamp(6, java.sql.Timestamp.valueOf(raceData.getDate()));
				makeBetStatement.setInt(7, list.get(i).getHorseId());
				makeBetStatement.executeUpdate();
				wrapConnection.commit();
			}
		} catch (SQLException e) {
			try {
				wrapConnection.rollback();
			} catch (SQLException ex) {
				LOGGER.error("Can't rollback data. " + e);
			}
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			try {
				wrapConnection.setAutoCommit(true);
			} catch (SQLException e) {
				LOGGER.error("Can't set autocommit in connection. " + e);
			}
			closeStatement(makeBetStatement);
		}
	}

	/**
	 * Find today bets count for statistic page.
	 *
	 * @return the int number of bets
	 * @throws DAOException
	 *             the DAO exception
	 */
	public int findTodayBetsCount() throws DAOException {
		int count = 0;
		PreparedStatement betsCountStatement = null;
		try {
			betsCountStatement = wrapConnection.getPreparedStatement(GET_TODAY_BETS_COUNT);
			ResultSet betSet = betsCountStatement.executeQuery();
			if (betSet.next()) {
				return betSet.getInt(1);
			} else {
				return count;
			}
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(betsCountStatement);
		}
	}

	/**
	 * Find month bets count for statistic page.
	 *
	 * @return the int count of month bets
	 * @throws DAOException
	 *             the DAO exception
	 */
	public int findMonthBetsCount() throws DAOException {
		int count = 0;
		PreparedStatement betsCountStatement = null;
		try {
			betsCountStatement = wrapConnection.getPreparedStatement(GET_MONTH_BETS_COUNT);
			ResultSet betSet = betsCountStatement.executeQuery();
			if (betSet.next()) {
				return betSet.getInt(1);
			} else {
				return count;
			}
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(betsCountStatement);
		}
	}

	/**
	 * Find today income of all races.
	 *
	 * @return the big decimal sum of today's bets
	 * @throws DAOException
	 *             the DAO exception
	 */
	public BigDecimal findTodayIncome() throws DAOException {
		BigDecimal resultIncome = new BigDecimal(0);
		PreparedStatement winIncomeStatement = null;
		PreparedStatement loseIncomeStatement = null;
		PreparedStatement threeIncomeStatement = null;
		try {
			loseIncomeStatement = wrapConnection.getPreparedStatement(GET_TODAY_LOSE_INCOME);
			ResultSet betSet = loseIncomeStatement.executeQuery();
			if (betSet.next()) {
				if (betSet.getBigDecimal(1) != null) {
					resultIncome = resultIncome.add(betSet.getBigDecimal(1));
				}
			}

			winIncomeStatement = wrapConnection.getPreparedStatement(GET_TODAY_WIN_INCOME);
			betSet = winIncomeStatement.executeQuery();
			if (betSet.next()) {
				if (betSet.getBigDecimal(1) != null) {
					resultIncome = resultIncome.subtract(betSet.getBigDecimal(1));
				}
			}

			threeIncomeStatement = wrapConnection.getPreparedStatement(GET_TODAY_FIRST_THREE_INCOME);
			betSet = threeIncomeStatement.executeQuery();
			if (betSet.next()) {
				if (betSet.getBigDecimal(1) != null) {
					resultIncome = resultIncome.subtract(betSet.getBigDecimal(1));
				}
			}
			return resultIncome;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(winIncomeStatement);
			closeStatement(loseIncomeStatement);
			closeStatement(threeIncomeStatement);
		}
	}

	/**
	 * Find month income of all races.
	 *
	 * @return the big decimal sum of month's mets
	 * @throws DAOException
	 *             the DAO exception
	 */
	public BigDecimal findMonthIncome() throws DAOException {
		BigDecimal resultIncome = new BigDecimal(0);
		PreparedStatement loseIncomeStatement = null;
		PreparedStatement winIncomeStatement = null;
		PreparedStatement threeIncomeStatement = null;
		try {
			loseIncomeStatement = wrapConnection.getPreparedStatement(GET_MONTH_LOSE_INCOME);
			ResultSet betSet = loseIncomeStatement.executeQuery();
			if (betSet.next()) {
				if (betSet.getBigDecimal(1) != null) {
					resultIncome = resultIncome.add(betSet.getBigDecimal(1));
				}
			}

			winIncomeStatement = wrapConnection.getPreparedStatement(GET_MONTH_WIN_INCOME);
			betSet = winIncomeStatement.executeQuery();
			if (betSet.next()) {
				if (betSet.getBigDecimal(1) != null) {
					resultIncome = resultIncome.subtract(betSet.getBigDecimal(1));
				}
			}

			threeIncomeStatement = wrapConnection.getPreparedStatement(GET_MONTH_FIRST_THREE_INCOME);
			betSet = threeIncomeStatement.executeQuery();
			if (betSet.next()) {
				if (betSet.getBigDecimal(1) != null) {
					resultIncome = resultIncome.subtract(betSet.getBigDecimal(1));
				}
			}
			return resultIncome;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(loseIncomeStatement);
			closeStatement(winIncomeStatement);
			closeStatement(threeIncomeStatement);
		}
	}
}