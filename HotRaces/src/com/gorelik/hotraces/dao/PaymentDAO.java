package com.gorelik.hotraces.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.logic.payment.PaymentData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class PaymentDAO. Class provides some process with payment.
 */
public class PaymentDAO extends AbstractDAO {
	/** The Constant MAKE_PAYMENT. Query to create new payment. */
	private final static String MAKE_PAYMENT = "INSERT INTO  races.account (`user_id`, `date`, `sum`) VALUES (?, ?, ?);";

	/**
	 * Make payment. Write data into DB for current user.
	 *
	 * @param payData the pay data with information about payment type, card number, etc.
	 * @param user the current user
	 * @return the current user with new money data
	 * @throws DAOException the DAO exception
	 */
	public User makePayment(PaymentData payData, User user) throws DAOException {
		PreparedStatement makePaymentStatement = null;
		try {
			makePaymentStatement = wrapConnection.getPreparedStatement(MAKE_PAYMENT);
			makePaymentStatement.setInt(1, Integer.valueOf(user.getUserId()));
			makePaymentStatement.setDate(2, java.sql.Date.valueOf(java.time.LocalDate.now()));
			makePaymentStatement.setBigDecimal(3, payData.getSum());
			makePaymentStatement.executeUpdate();
			user.setMoney(user.getMoney().add(payData.getSum()));
			return user;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			super.closeStatement(makePaymentStatement);
		}
	}
}
