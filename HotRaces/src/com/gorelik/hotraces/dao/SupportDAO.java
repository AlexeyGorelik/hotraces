package com.gorelik.hotraces.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.gorelik.hotraces.entity.SupportMessage;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class SupportDAO. Class provides some process with support messages.
 */
public class SupportDAO extends AbstractDAO {
	/**
	 * The Constant SUPPORT_MESSAGE. Query to insert new support message into
	 * DB.
	 */
	private static final String SUPPORT_MESSAGE = "INSERT INTO support (`email`, `type`, `date`, `text`) VALUES(?, ?, ?, ?);";

	/**
	 * The Constant FIND_SUPP_MESSAGE_LIST. Query to find all support messages.
	 */
	private static final String FIND_SUPP_MESSAGE_LIST = "SELECT email, type, text, date FROM support;";

	/**
	 * Send question.
	 *
	 * @param suppMsg
	 *            the support message data
	 * @throws DAOException
	 *             the DAO exception
	 */
	public void sendQuestion(SupportMessage suppMsg) throws DAOException {
		PreparedStatement addMsgStatement = null;
		try {
			addMsgStatement = wrapConnection.getPreparedStatement(SUPPORT_MESSAGE);
			addMsgStatement.setString(1, suppMsg.getEmail());
			addMsgStatement.setString(2, suppMsg.getType());
			addMsgStatement.setTimestamp(3, java.sql.Timestamp.valueOf(suppMsg.getDate()));
			addMsgStatement.setString(4, suppMsg.getMessage());
			addMsgStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(addMsgStatement);
		}
	}

	/**
	 * Find support message list.
	 *
	 * @return the list with all support messages
	 * @throws DAOException
	 *             the DAO exception
	 */
	public List<SupportMessage> findSuppMessageList() throws DAOException {
		PreparedStatement findMsgListStatement = null;
		List<SupportMessage> list = null;
		try {
			list = new ArrayList<SupportMessage>();
			findMsgListStatement = wrapConnection.getPreparedStatement(FIND_SUPP_MESSAGE_LIST);
			ResultSet suppMessResSet = findMsgListStatement.executeQuery();
			while (suppMessResSet.next()) {
				list.add(new SupportMessage(suppMessResSet.getString(1), suppMessResSet.getString(2),
						suppMessResSet.getString(3), suppMessResSet.getTimestamp(4).toString()));
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(HotRacesErrorCode.DAO_EXCEPTION);
		} finally {
			closeStatement(findMsgListStatement);
		}
	}
}
