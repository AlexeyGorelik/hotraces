package com.gorelik.hotraces.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.entity.RoleEnum;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.resource.PageManager;

/**
 * The Class AdminFilter. Class that filter administrator status to redirect.
 */
@WebFilter(urlPatterns = { "/jsp/profile/admin/*" })

public class AdminFilter implements Filter {
	@Override
	public void init(FilterConfig fConfig) throws ServletException {

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		User user = (User) httpRequest.getSession().getAttribute(PageManager.USER);
		if (user == null || !user.getRole().equals(RoleEnum.ADMIN.getRole())) {
			RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/index.jsp");
			dispatcher.forward(request, response);
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {

	}
}
