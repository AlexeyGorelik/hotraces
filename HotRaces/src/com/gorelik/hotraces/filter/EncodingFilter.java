package com.gorelik.hotraces.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 * The Class EncodingFilter. Class that encoding servlet request.
 */
@WebFilter(urlPatterns = { "*" })

public class EncodingFilter implements Filter {
	/** The Constant UTF_8. */
	private static final String UTF_8 = "UTF-8";

	public void init(FilterConfig fConfig) throws ServletException {

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding(UTF_8);
		response.setCharacterEncoding(UTF_8);
		chain.doFilter(request, response);
	}

	public void destroy() {

	}
}
