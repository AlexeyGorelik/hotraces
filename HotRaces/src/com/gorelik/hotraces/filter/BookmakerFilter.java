package com.gorelik.hotraces.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.entity.RoleEnum;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.resource.PageManager;

/**
 * The Class BookmakerFilter. Class that filter bookmaker status to redirect.
 */
@WebFilter(urlPatterns = { "/jsp/profile/bookmaker/*" })

public class BookmakerFilter implements Filter {
	public void init(FilterConfig fConfig) throws ServletException {

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		User user = (User) httpRequest.getSession().getAttribute(PageManager.USER);
		if (user == null || !user.getRole().equals(RoleEnum.BOOKMAKER.getRole())) {
			RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/index.jsp");
			dispatcher.forward(request, response);
		}
		chain.doFilter(request, response);
	}

	public void destroy() {

	}
}