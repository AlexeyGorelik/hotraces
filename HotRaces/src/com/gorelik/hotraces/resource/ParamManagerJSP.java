package com.gorelik.hotraces.resource;

public class ParamManagerJSP {
	// common
	public static final String LOCALE_HREF = "localeHref";
	public static final String ANCOR_DELIMETER = "#";

	// race and bets
	public static final String SELECTED_PREV_ROW_VALUE = "selected-prev-race-row";
	public static final String SELECTED_ROW_VALUE = "selected-row-value";
	public static final String SELECTED_ROW_DELIMETED = ";";
	public static final String ERROR = "error";
	public static final String COMMAND = "command";
	public static final String RACE_NAME = "race-name";
	public static final String RACE_DATE = "race-date";
	public static final String RACE_NAME_TO_JSP = "raceName";
	public static final String RACE_DATE_TO_JSP = "raceDate";
	public static final String PREV_RACE_TABLE = "prevRaceList";
	public static final String BET_ERROR = "betError";
	public static final String LIST_PARAM = "List";

	// admin profile
	public static final String STAT_USER_COUNT = "userCount";
	public static final String STAT_BETS_TODAY = "betsToday";
	public static final String STAT_BETS_MONTH = "betsMonth";
	public static final String STAT_INCOME_TODAY = "incomeToday";
	public static final String STAT_INCOME_MONTH = "incomeMonth";
	public static final String STAT_RACE_TODAY = "raceToday";
	public static final String STAT_RACE_MONTH = "raceMonth";
	public static final String STAT_STAT_PAGE = "adminPageStat";

	// admin statistic
	public static final String STATISTIC_ERROR = "statisticError";

	// admin race table
	public static final String RACE_LIST_PROP = "raceList";
	public static final String RACE_LIST_EXIST_PROP = "raceListExist";
	public static final String RACE_ERROR = "raceError";

	// admin horse table
	public static final String HORSE_LIST_EXISTS = "horseListExist";
	public static final String HORSE_LIST = "horseList";
	public static final String HORSE_ERROR = "horseError";

	// admin user table
	public static final String USER_LIST_PROP = "userList";
	public static final String USER_LIST_EXIST_PROP = "userListExist";

	// admin create race
	public static final String ADD_PARAM_VALUE = "checkbox-horse";
	public static final String RACE_NAME_PARAM_VALUE = "add-race-name";
	public static final String RACE_DATE_PARAM_VALUE = "add-race-date";
	public static final String POSTFIX_ID = "-id";
	public static final String POSTFIX_NAME = "-name";
	public static final String HIPPODROME_LIST = "hippList";

	// admin edit race
	public static final String PREFIX_HORSE = "horse-";
	public static final String EDIT_COMM_HORSE_TO_RACE_LIST = "horseCollList";

	// create and edit command
	public static final String HORSE_IN_RACE_LIST = "horseList";
	public static final String MIN_HORSE_COUNT_ATTR = "minHorseCount";
	public static final String MAX_HORSE_COUNT_ATTR = "maxHorseCount";
	public static final int MIN_HORSE_COUNT_VAL = 1;
	public static final int MAX_HORSE_COUNT_VAL = 8;

	// set coeff and make bet
	public static final String HORSE_ID_PREF = "horse-id-";
	public static final String HORSE_NAME_PREF = "horse-name-";
	public static final String HORSE_COEFF_PREF = "horse-coeff-";
	public static final String RIDER_ID_PREF = "rider-id-";
	public static final String COEFF_PREF = "coeff-";
	public static final String HORSE_COUNT = "horseCount";
	public static final String SET_COEFF_LIST = "setCoeffList";
	public static final String BET_PREF = "bet-";
	public static final String BET_TYPE_PREF = "bet-type-";
	public static final String BET_COUNT = "betCount";
	public static final String BET_LIST = "betList";

	// payment
	public static final String PAYMENT_CVV_CODE = "cvv";
	public static final String PAYMENT_SUM = "sum";
	public static final String PAYMENT_CARD_MONTH = "card-month";
	public static final String PAYMENT_CARD_YEAR = "card-year";
	public static final String PAYMENT_CARD_NUM1 = "card-num1";
	public static final String PAYMENT_CARD_NUM2 = "card-num2";
	public static final String PAYMENT_CARD_NUM3 = "card-num3";
	public static final String PAYMENT_CARD_NUM4 = "card-num4";

	// to payment
	public static final String TO_PAYMENT_ERROR = "errorMessage";
	public static final String TO_PAYMENT_MESSAGE = "paymentMsg";

	// prev bets
	public final static String PREV_BETS_NO_BETS_BEFORE = "noBetsBefore";
	public final static String PREV_BETS_LIST = "prevBetList";

	// profile change
	public static final String CHANGE_ERROR = "changeError";
	public static final String CHANGE_FIRST_NAME = "changeFirstName";
	public static final String CHANGE_SECOND_NAME = "changeSecondName";
	public static final String CHANGE_NICKNAME = "changeNickname";
	public static final String CHANGE_EMAIL = "changeEmail";

	// hippodrome
	public static final String HIPP_DESCR_NAME = "hippName";
	public static final String HIPP_COMMON_NAME = "hippodrom";

	// message
	public static final String USER_TO = "user-to";
	public static final String MESSAGE_ERROR = "messageError";
	public static final String SHOW_MESSAGE_LIST = "showMessagesList";
	public static final String MESSAGE_LIST = "messagesList";
	public static final String MESSAGE_TEXT = "message-text";
	public static final String MESSAGE_USER_TO_NAME = "userToName";
	public static final String PROFILE_ERROR = "profileError";

	// to write message
	public static final String TO_WRITE_MESSAGE_LIST = "messageList";
	public static final String TO_WRITE_MESSAGE_WITH_USER = "messageWithUser";

	// new message comm
	public static final String NEW_MESSAGE_PARAM = "newMessage";
	public static final String NEW_MESSAGE_USER_LIST_PARAM = "allUserList";

	// login
	public static final String PARAM_LOGIN = "login";
	public static final String PARAM_PASSWORD = "password";

	// registration
	public static final String REGISTR_LOGIN = "login";
	public static final String REGISTR_PASSWORD = "pswrd1";
	public static final String REGISTR_REPEAT_PASSWORD = "pswrd2";
	public static final String REGISTR_FIRST_NAME = "firstName";
	public static final String REGISTR_SECOND_NAME = "secondName";
	public static final String REGISTR_EMAIL = "email";

	// login and registration
	public static final String TO_MAIN_ANCOR = "toMain";

	// profile
	public static final String PROFILE_RACE_LIST = "raceList";
	public static final String PROFILE_COEFF_ERROR = "bookmakerError";
	public static final String PROFILE_TO_COEFF_TABLE_NAME = "currentRaces";

	// avatar
	public static final String AVATAR_FILE = "file";
	public static final String AVATAR_UPLOAD_VARIABLE = "uplAvatar";

	// forgot password
	public static final String FORG_PASS_ERROR = "forgPasswError";
	public static final String FORG_PASS_PASSWORD_PARAM = "password";
	public static final String FORG_PASS_NEW_PASSWORD = "newPassword";
	public static final String FORG_PASS_EMAIL = "email";

	// faq
	public static final String FAQ_ANCOR = "faqName";

	// support

	public static final String SUPP_EMAIL = "email";
	public static final String SUPP_QUEST_TYPE = "quest-type";
	public static final String SUPP_TEXT = "text-question";
	public static final String SUPP_ANCOR = "supp-header";
}