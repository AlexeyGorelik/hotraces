package com.gorelik.hotraces.resource;

/**
 * The Class LocaleHrefManager. Class contains constants to determine on which
 * page to go after changing locale.
 */
public class LocaleHrefManager {
	/** The Constant TO_MAIN. Main page. */
	public static final String TO_MAIN = "command=toMain";

	/** The Constant TO_ABOUT_US. "About us" page. */
	public static final String TO_ABOUT_US = "command=toAboutUs";

	/** The Constant TO_ADMIN_USER_TABLE. Admin user table page. */
	public static final String TO_ADMIN_USER_TABLE = "command=adminUserTable";

	/** The Constant TO_ADMIN_USER_TABLE. Admin race table page. */
	public static final String TO_ADMIN_RACE_TABLE = "command=adminRaceTable";

	/** The Constant TO_BETS. Bets page. */
	public static final String TO_BETS = "command=toBets";

	/** The Constant TO_COEFF_TABLE. Coefficient table page. */
	public static final String TO_COEFF_TABLE = "command=toCoeffTable";

	/** The Constant TO_CONTACT. "Contact us" page. */
	public static final String TO_CONTACT = "command=toContact";

	/** The Constant TO_CREATE_RACE. Create race page. */
	public static final String TO_CREATE_RACE = "command=toCreateRace";

	/** The Constant TO_FAQ. "FAQ" page. */
	public static final String TO_FAQ = "command=toFaq";

	/** The Constant TO_FORG_PASS. "Forgot password?" page. */
	public static final String TO_FORG_PASS = "command=toForgotPassword";

	/** The Constant TO_HOW_TO_PLAY. "How to play" page. */
	public static final String TO_HOW_TO_PLAY = "command=toHowToPlay";

	/** The Constant TO_LIVE. "LIVE" page. */
	public static final String TO_LIVE = "command=toLive";

	/** The Constant TO_LOGIN. Login page. */
	public static final String TO_LOGIN = "command=toLogin";

	/** The Constant TO_PROFILE_MESSAGES. Profile messages page. */
	public static final String TO_PROFILE_MESSAGES = "command=profileMessages";

	/** The Constant TO_PAYMENT. Payment page. */
	public static final String TO_PAYMENT = "command=toPayment";

	/** The Constant TO_PAYMENT_SYSTEM. Payment system page. */
	public static final String TO_PAYMENT_SYSTEM = "command=toPaymentSystem";

	/** The Constant TO_CHANGE_PROFILE. Profile page. */
	public static final String TO_CHANGE_PROFILE = "command=toChangeProfile";

	/** The Constant TO_REGISTRATION. Registration page. */
	public static final String TO_REGISTRATION = "command=toregistration";

	/** The Constant TO_PROFILE. Profile page. */
	public static final String TO_PROFILE = "command=toProfile";

	/** The Constant TO_STATISTIC. Statistic page. */
	public static final String TO_STATISTIC = "command=toStatistic";

	/** The Constant TO_SUPPORT. Support page. */
	public static final String TO_SUPPORT = "command=toSupport";

	/**
	 * Instantiates a new locale href manager.
	 */
	private LocaleHrefManager() {

	}
}
