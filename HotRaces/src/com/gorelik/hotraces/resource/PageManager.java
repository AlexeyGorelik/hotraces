package com.gorelik.hotraces.resource;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * The Class PageManager. Class that contains pathes to all pages in app.
 */
public class PageManager {

	/** The Constant REDIRECT. Redirect attribute. */
	public static final String REDIRECT = "redirect";

	/** The Constant LOCALE. Locale attribute. */
	public static final String LOCALE = "locale";

	/** The Constant USER. User attribute. */
	public static final String USER = "user";

	/** The Constant PAGE_INDEX_PROP. Index page forward. */
	public static final String PAGE_INDEX_PROP = "/index.jsp";

	/** The Constant PAGE_INDEX_FULL_PROP. Index page redirect. */
	public static final String PAGE_INDEX_FULL_PROP = "/HotRaces/index.jsp";

	/** The Constant PAGE_MAIN_PROP. Main page forward. */
	public static final String PAGE_MAIN_PROP = "/jsp/main/hotraces.jsp";

	/** The Constant PAGE_MAIN_FULL_PROP. Main page redirect. */
	public static final String PAGE_MAIN_FULL_PROP = "/HotRaces/jsp/main/hotraces.jsp";

	/** The Constant PAGE_LOGIN_PROP. Login page forward. */
	public static final String PAGE_LOGIN_PROP = "/jsp/login/login.jsp";

	/** The Constant PAGE_LOGIN_FULL_PROP. Login page redirect. */
	public static final String PAGE_LOGIN_FULL_PROP = "/HotRaces/jsp/login/login.jsp";

	/** The Constant PAGE_REGISTRATION_PROP. Registration page forward. */
	public static final String PAGE_REGISTRATION_PROP = "/jsp/registration/registration.jsp";

	/** The Constant PAGE_REGISTRATION_FULL_PROP. Registration page redirect. */
	public static final String PAGE_REGISTRATION_FULL_PROP = "/HotRaces/jsp/registration/registration.jsp";

	/** The Constant PAGE_PROFILE_CHANGE_PROP. Profile change page forward. */
	public static final String PAGE_PROFILE_CHANGE_PROP = "/jsp/profile/profilechange/profilechange.jsp";

	/**
	 * The Constant PAGE_PROFILE_CHANGE_FULL_PROP. Profile change page redirect.
	 */
	public static final String PAGE_PROFILE_CHANGE_FULL_PROP = "/HotRaces/jsp/profile/profilechange/profilechange.jsp";

	/** The Constant PAGE_PROFILE_CLIENT_PROP. Profile client page forward. */
	public static final String PAGE_PROFILE_CLIENT_PROP = "/jsp/profile/client/profileClient.jsp";

	/**
	 * The Constant PAGE_PROFILE_CLIENT_FULL_PROP. Profile client page redirect.
	 */
	public static final String PAGE_PROFILE_CLIENT_FULL_PROP = "/HotRaces/jsp/profile/client/profileClient.jsp";

	/** The Constant PAGE_PROFILE_ADMIN_PROP. Profile admin page forward. */
	public static final String PAGE_PROFILE_ADMIN_PROP = "/jsp/profile/admin/profileAdmin.jsp";

	/**
	 * The Constant PAGE_PROFILE_ADMIN_FULL_PROP. Profile admin page redirect.
	 */
	public static final String PAGE_PROFILE_ADMIN_FULL_PROP = "/HotRaces/jsp/profile/admin/profileAdmin.jsp";

	/** The Constant PAGE_MY_PAYMENT_PROP. Client payment page forward. */
	public static final String PAGE_MY_PAYMENT_PROP = "/jsp/profile/client/clientPayment.jsp";

	/** The Constant PAGE_MY_PAYMENT_FULL_PROP. Client payment page redirect. */
	public static final String PAGE_MY_PAYMENT_FULL_PROP = "/HotRaces/jsp/profile/client/clientPayment.jsp";

	/**
	 * The Constant PAGE_PROFILE_BOOKMAKER_PROP. Profile bookmaker page forward.
	 */
	public static final String PAGE_PROFILE_BOOKMAKER_PROP = "/jsp/profile/bookmaker/profileBookmaker.jsp";

	/**
	 * The Constant PAGE_PROFILE_BOOKMAKER_FULL_PROP. Profile bookmaker page
	 * redirect.
	 */
	public static final String PAGE_PROFILE_BOOKMAKER_FULL_PROP = "/HotRaces/jsp/profile/bookmaker/profileBookmaker.jsp";

	/**
	 * The Constant PAGE_BOOKMAKER_SET_COEFF_PROP. Bookmaker set coeff page
	 * forward.
	 */
	public static final String PAGE_BOOKMAKER_SET_COEFF_PROP = "/jsp/profile/bookmaker/bookmakerSetCoeff.jsp";

	/**
	 * The Constant PAGE_BOOKMAKER_SET_COEFF_FULL_PROP. Bookmaker set coeff page
	 * redirect.
	 */
	public static final String PAGE_BOOKMAKER_SET_COEFF_FULL_PROP = "/HotRaces/jsp/profile/bookmaker/bookmakerSetCoeff.jsp";

	/**
	 * The Constant PAGE_ADMIN_USER_TABLE_PROP. Admin user table page forward.
	 */
	public static final String PAGE_ADMIN_USER_TABLE_PROP = "/jsp/profile/admin/adminUserTable.jsp";

	/**
	 * The Constant PAGE_ADMIN_USER_TABLE_FULL_PROP. Admin user table page
	 * redirect.
	 */
	public static final String PAGE_ADMIN_USER_TABLE_FULL_PROP = "/HotRaces/jsp/profile/admin/adminUserTable.jsp";

	/**
	 * The Constant PAGE_ADMIN_RACE_TABLE_PROP. Admin race table page forward.
	 */
	public static final String PAGE_ADMIN_RACE_TABLE_PROP = "/jsp/profile/admin/adminRaceTable.jsp";

	/**
	 * The Constant PAGE_ADMIN_RACE_TABLE_FULL_PROP. Admin race table page
	 * redirect.
	 */
	public static final String PAGE_ADMIN_RACE_TABLE_FULL_PROP = "/HotRaces/jsp/profile/admin/adminRaceTable.jsp";

	/**
	 * The Constant PAGE_ADMIN_HORSE_TABLE_PROP. Admin horse table page forward.
	 */
	public static final String PAGE_ADMIN_HORSE_TABLE_PROP = "/jsp/profile/admin/adminHorseTable.jsp";

	/**
	 * The Constant PAGE_ADMIN_HORSE_TABLE_FULL_PROP. Adminm horse table page
	 * redirect.
	 */
	public static final String PAGE_ADMIN_HORSE_TABLE_FULL_PROP = "/HotRaces/jsp/profile/admin/adminHorseTable.jsp";

	/**
	 * The Constant PAGE_ADMIN_CREATE_RACE_PROP. Admin create race page forward.
	 */
	public static final String PAGE_ADMIN_CREATE_RACE_PROP = "/jsp/profile/admin/adminCreateRace.jsp";

	/**
	 * The Constant PAGE_ADMIN_CREATE_RACE_FULL_PROP. Admin create race page
	 * redirect.
	 */
	public static final String PAGE_ADMIN_CREATE_RACE_FULL_PROP = "/HotRaces/jsp/profile/admin/adminCreateRace.jsp";

	/** The Constant PAGE_ADMIN_EDIT_RACE_PROP. Admin edit race page forward. */
	public static final String PAGE_ADMIN_EDIT_RACE_PROP = "/jsp/profile/admin/adminEditRace.jsp";

	/**
	 * The Constant PAGE_ADMIN_EDIT_RACE_FULL_PROP. Admin edit race page
	 * redirect.
	 */
	public static final String PAGE_ADMIN_EDIT_RACE_FULL_PROP = "/HotRaces/jsp/profile/admin/adminEditRace.jsp";

	/** The Constant PAGE_TO_BETS_PROP. Bets page forward. */
	public static final String PAGE_TO_BETS_PROP = "/jsp/bets/bets.jsp";

	/** The Constant PAGE_TO_BETS_FULL_PROP. Bets page redirect. */
	public static final String PAGE_TO_BETS_FULL_PROP = "/HotRaces/jsp/bets/bets.jsp";

	/** The Constant PAGE_TO_MAKE_BET_PROP. Make bet page forward. */
	public static final String PAGE_TO_MAKE_BET_PROP = "/jsp/profile/client/makeBet.jsp";

	/** The Constant PAGE_TO_MAKE_BET_FULL_PROP. Make bet page redirect. */
	public static final String PAGE_TO_MAKE_BET_FULL_PROP = "/HotRaces/jsp/profile/client/makeBet.jsp";

	/** The Constant PAGE_TO_LIVE_PROP. Live page forward. */
	public static final String PAGE_TO_LIVE_PROP = "/jsp/live/live.jsp";

	/** The Constant PAGE_TO_LIVE_FULL_PROP. Live page redirect. */
	public static final String PAGE_TO_LIVE_FULL_PROP = "/HotRaces/jsp/live/live.jsp";

	/** The Constant PAGE_TO_CONTACT_PROP. Contact us page forward. */
	public static final String PAGE_TO_CONTACT_PROP = "/jsp/contact/contact.jsp";

	/** The Constant PAGE_TO_CONTACT_FULL_PROP. Contact us page redirect. */
	public static final String PAGE_TO_CONTACT_FULL_PROP = "/HotRaces/jsp/contact/contact.jsp";

	/** The Constant PAGE_TO_ABOUT_US_PROP. About us page forward. */
	public static final String PAGE_TO_ABOUT_US_PROP = "/jsp/contact/aboutUs.jsp";

	/** The Constant PAGE_TO_ABOUT_US_FULL_PROP. About us page redirect. */
	public static final String PAGE_TO_ABOUT_US_FULL_PROP = "/HotRaces/jsp/contact/aboutUs.jsp";

	/**
	 * The Constant PREV_RACE_DESCRIPTION_PROP. Previous race description page
	 * forward.
	 */
	public static final String PREV_RACE_DESCRIPTION_PROP = "/jsp/bets/prevRaceDescription.jsp";

	/**
	 * The Constant PREV_RACE_DESCRIPTION_FULL_PROP. Previous race description
	 * page redirect.
	 */
	public static final String PREV_RACE_DESCRIPTION_FULL_PROP = "/HotRaces/jsp/bets/prevRaceDescription.jsp";

	/**
	 * The Constant HIPPODROM_DESCRIPTION_PROP. Hippodromes description page
	 * forward.
	 */
	public static final String HIPPODROM_DESCRIPTION_PROP = "/jsp/hippodroms/hippodromsDesctiprion.jsp";

	/**
	 * The Constant HIPPODROM_DESCRIPTION_FULL_PROP. Hippodromes description
	 * page redirect.
	 */
	public static final String HIPPODROM_DESCRIPTION_FULL_PROP = "/HotRaces/jsp/hippodroms/hippodromsDesctiprion.jsp";

	/** The Constant FAQ_PROP. FAQ page forward. */
	public static final String FAQ_PROP = "/jsp/faq/faq.jsp";

	/** The Constant FAQ_FULL_PROP. FAQ page redirect. */
	public static final String FAQ_FULL_PROP = "/HotRaces/jsp/faq/faq.jsp";

	/** The Constant PAGE_PAYMENT_SYSTEM_PROP. Payment system page forward. */
	public static final String PAGE_PAYMENT_SYSTEM_PROP = "/jsp/help/paymentSystem.jsp";

	/**
	 * The Constant PAGE_PAYMENT_SYSTEM_FULL_PROP. Payment system page redirect.
	 */
	public static final String PAGE_PAYMENT_SYSTEM_FULL_PROP = "/HotRaces/jsp/help/paymentSystem.jsp";

	/** The Constant PAGE_HOW_TO_PLAY_PROP. How to play page forward. */
	public static final String PAGE_HOW_TO_PLAY_PROP = "/jsp/help/howToPlay.jsp";

	/** The Constant PAGE_HOW_TO_PLAY_FULL_PROP. How to play page redirect. */
	public static final String PAGE_HOW_TO_PLAY_FULL_PROP = "/HotRaces/jsp/help/howToPlay.jsp";

	/** The Constant PAGE_FORGOT_PASSWORD_PROP. Forgot password page forward. */
	public static final String PAGE_FORGOT_PASSWORD_PROP = "/jsp/help/forgotPassword.jsp";

	/**
	 * The Constant PAGE_FORGOT_PASSWORD_FULL_PROP. Forgot password page
	 * redirect.
	 */
	public static final String PAGE_FORGOT_PASSWORD_FULL_PROP = "/HotRaces/jsp/help/forgotPassword.jsp";

	/** The Constant PAGE_SUPPORT_PROP. Support page forward. */
	public static final String PAGE_SUPPORT_PROP = "/jsp/help/support.jsp";

	/** The Constant PAGE_SUPPORT_FULL_PROP. Support page redirect. */
	public static final String PAGE_SUPPORT_FULL_PROP = "/HotRaces/jsp/help/support.jsp";

	/** The Constant ADMIN_STATISTIC_PROP. Statistic page forward. */
	public static final String ADMIN_STATISTIC_PROP = "/jsp/profile/admin/statisticAdmin.jsp";

	/** The Constant ADMIN_STATISTIC_FULL_PROP. Statistic page redirect. */
	public static final String ADMIN_STATISTIC_FULL_PROP = "/HotRaces/jsp/profile/admin/statisticAdmin.jsp";

	/** The Constant ADMIN_SUPPORT_PROP. Admin support page forward. */
	public static final String ADMIN_SUPPORT_PROP = "/jsp/profile/admin/adminSupport.jsp";

	/** The Constant ADMIN_SUPPORT_FULL_PROP. Admin support page redirect. */
	public static final String ADMIN_SUPPORT_FULL_PROP = "/HotRaces/jsp/profile/admin/adminSupport.jsp";

	/**
	 * Instantiates a new page manager.
	 */
	private PageManager() {

	}

	/**
	 * Format date.
	 *
	 * @param date
	 *            the date
	 * @param locale
	 *            the locale
	 * @return the string
	 */
	public static String formatDate(String date, String locale) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = null;

		DateTimeFormatter formatterRu = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		DateTimeFormatter formatterEn = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm");

		switch (locale) {
		case "ru_RU":
			dateTime = LocalDateTime.parse(date, formatterRu);
			break;
		case "en_US":
			dateTime = LocalDateTime.parse(date, formatterEn);
			break;
		default:
			dateTime = LocalDateTime.parse(date, formatterRu);
			break;
		}

		return dateTime.format(formatter).toString();
	}
}