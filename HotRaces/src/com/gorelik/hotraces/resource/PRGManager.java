package com.gorelik.hotraces.resource;

/**
 * The Class PRGManager. Class contains string value of URL to
 * PRG(Post-Redirect-Get) pattern.
 */
public class PRGManager {

	/** The Constant TO_MAIN. Main page. */
	public static final String TO_MAIN = "/HotRaces/HotRacesServlet?command=toMain";

	/** The Constant TO_LOGIN. Login page. */
	public static final String TO_LOGIN = "/HotRaces/HotRacesServlet?command=toLogin";

	/** The Constant TO_LOGIN_ERROR. Login page with error. */
	public static final String TO_LOGIN_ERROR = "/HotRaces/HotRacesServlet?command=toLogin&error=";

	/** The Constant TO_REGISTRATION_ERROR. Registration page with error. */
	public static final String TO_REGISTRATION_ERROR = "/HotRaces/HotRacesServlet?command=toregistration&error=";

	/** The Constant TO_PROFILE. Profile page. */
	public static final String TO_PROFILE = "/HotRacesServlet?command=toProfile";

	/** The Constant TO_PROFILE_ERROR. Profile page with error. */
	public static final String TO_PROFILE_ERROR = "/HotRacesServlet?command=toProfile&error=";

	/** The Constant TO_ADMIN_RACE_TABLE. Admin race table. */
	public static final String TO_ADMIN_RACE_TABLE = "/HotRaces/HotRacesServlet?command=adminRaceTable";

	/** The Constant TO_ADMIN_RACE_TABLE_ERROR. Admin race table with error. */
	public static final String TO_ADMIN_RACE_TABLE_ERROR = "/HotRaces/HotRacesServlet?command=adminRaceTable&error=";

	/** The Constant TO_ADMIN_USER_TABLE. Admin user table. */
	public static final String TO_ADMIN_USER_TABLE = "/HotRaces/HotRacesServlet?command=adminUserTable";

	/** The Constant TO_ADMIN_USER_TABLE_ERROR. Admin user table with error. */
	public static final String TO_ADMIN_USER_TABLE_ERROR = "/HotRaces/HotRacesServlet?command=adminUserTable&error=";

	/** The Constant TO_FORG_PASSWORD. Forgot password page. */
	public static final String TO_FORG_PASSWORD = "/HotRaces/HotRacesServlet?command=toForgotPassword&password=";

	/** The Constant TO_FORG_PASSWORD_ERROR. Forgot password page with error. */
	public static final String TO_FORG_PASSWORD_ERROR = "/HotRaces/HotRacesServlet?command=toForgotPassword&error=";

	/** The Constant TO_BETS. Bets page. */
	public static final String TO_BETS = "/HotRaces/HotRacesServlet?command=toBets";

	/** The Constant TO_BETS_ERROR. Bets page with error. */
	public static final String TO_BETS_ERROR = "/HotRaces/HotRacesServlet?command=toBets&error=";

	/** The Constant TO_BETS_FORWARD. Bets page. */
	public static final String TO_BETS_FORWARD = "/HotRacesServlet?command=toBets";

	/** The Constant TO_BETS_ERROR_FORWARD. Bets page with error. */
	public static final String TO_BETS_ERROR_FORWARD = "/HotRacesServlet?command=toBets&error=";

	/** The Constant TO_PAYMENT. Payment page. */
	public static final String TO_PAYMENT = "/HotRaces/HotRacesServlet?command=toPayment";

	/** The Constant TO_PAYMENT_ERROR. Payment page with error. */
	public static final String TO_PAYMENT_ERROR = "/HotRaces/HotRacesServlet?command=toPayment&error=";

	/** The Constant TO_PROFILE_MESSAGES. Messages page. */
	public static final String TO_PROFILE_MESSAGES = "/HotRaces/HotRacesServlet?command=profileMessages";

	/** The Constant TO_PROFILE_MESSAGES_ERROR. Messages page with error. */
	public static final String TO_PROFILE_MESSAGES_ERROR = "/HotRaces/HotRacesServlet?command=profileMessages&error=";

	/** The Constant TO_COEFF_TABLE. Coefficient page. */
	public static final String TO_COEFF_TABLE = "/HotRaces/HotRacesServlet?command=toCoeffTable";

	/** The Constant TO_COEFF_TABLE_ERROR. Coefficient page with error. */
	public static final String TO_COEFF_TABLE_ERROR = "/HotRaces/HotRacesServlet?command=toCoeffTable&error=";

	/** The Constant TO_WRITE_MESSAGE. Write message page. */
	public static final String TO_WRITE_MESSAGE = "/HotRaces/HotRacesServlet?command=toWriteMessage&user-to=";

	/** The Constant TO_SUPP_SUCCESS. Support page with successful action. */
	public static final String TO_SUPP_SUCCESS = "/HotRaces/HotRacesServlet?command=toSupport&success=true";

	/** The Constant TO_SUPP_ERROR. Support page with unsuccessful action. */
	public static final String TO_SUPP_ERROR = "/HotRaces/HotRacesServlet?command=toSupport&error=true";

	/**
	 * Instantiates a new PRG manager.
	 */
	private PRGManager() {

	}
}
