package com.gorelik.hotraces.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.gorelik.hotraces.logic.profile.ProfileChangeData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class ProfileChangeValidator. Class that handles profile change data.
 */
public class ProfileChangeValidator {

	/** The Constant LOGIN_REGEXPR. Regular expression for login. */
	private static final String LOGIN_REGEXPR = "[a-zA-Z\\d_]{3,16}";

	/** The Constant EMAIL_REGEXPR. Regular expression for email. */
	private static final String EMAIL_REGEXPR = "^[-\\w.]+@([A-z0-9][-A-z0-9]+\\.)+[A-z]{2,4}$";

	/**
	 * The Constant FIRST_AND_SECOND_NAME_REGEXPR. Regular expression for first
	 * and second name.
	 */
	private static final String FIRST_AND_SECOND_NAME_REGEXPR = "[�-��-�a-zA-Z]{3,16}";

	/**
	 * Check change validation.
	 *
	 * @param profData
	 *            the profile data
	 */
	public void checkChangeValidation(ProfileChangeData profData) {
		checkLogin(profData.getLogin(), profData);
		checkEmail(profData.getEmail(), profData);
		checkFirstName(profData.getFirstName(), profData);
		checkSecondName(profData.getSecondName(), profData);
		if (profData.getValidation() == null) {
			profData.setValidation(true);
		}
	}

	/**
	 * Check login.
	 *
	 * @param login
	 *            the login
	 * @param profData
	 *            the prof data
	 */
	private void checkLogin(String login, ProfileChangeData profData) {
		if (login.isEmpty()) {
			return;
		}
		Pattern parserPattern = Pattern.compile(LOGIN_REGEXPR);
		Matcher matcher = parserPattern.matcher(login);
		if (!(matcher.find() && matcher.group().equals(login))) {
			profData.setErrorMessage(HotRacesErrorCode.PROFILE_CHANGE_INVALID_LOGIN);
			profData.setValidation(false);
		}
	}

	/**
	 * Check email.
	 *
	 * @param email
	 *            the email
	 * @param profData
	 *            the prof data
	 */
	private void checkEmail(String email, ProfileChangeData profData) {
		Pattern parserPattern = Pattern.compile(EMAIL_REGEXPR);
		Matcher matcher = parserPattern.matcher(email);
		if (!(matcher.find() && matcher.group().equals(email))) {
			profData.setErrorMessage(HotRacesErrorCode.PROFILE_CHANGE_INVALID_EMAIL);
			profData.setValidation(false);
		}
	}

	/**
	 * Check first name.
	 *
	 * @param name
	 *            the name
	 * @param profData
	 *            the prof data
	 */
	private void checkFirstName(String name, ProfileChangeData profData) {
		if (name.isEmpty()) {
			return;
		}
		Pattern parserPattern = Pattern.compile(FIRST_AND_SECOND_NAME_REGEXPR);
		Matcher matcher = parserPattern.matcher(name);
		if (!(matcher.find() && matcher.group().equals(name))) {
			profData.setErrorMessage(HotRacesErrorCode.PROFILE_CHANGE_INVALID_FIRST_NAME);
			profData.setValidation(false);
		}
	}

	/**
	 * Check second name.
	 *
	 * @param name
	 *            the name
	 * @param profData
	 *            the prof data
	 */
	private void checkSecondName(String name, ProfileChangeData profData) {
		if (name.isEmpty()) {
			return;
		}
		Pattern parserPattern = Pattern.compile(FIRST_AND_SECOND_NAME_REGEXPR);
		Matcher matcher = parserPattern.matcher(name);
		if (!(matcher.find() && matcher.group().equals(name))) {
			profData.setErrorMessage(HotRacesErrorCode.PROFILE_CHANGE_INVALID_SECOND_NAME);
			profData.setValidation(false);
		}
	}
}
