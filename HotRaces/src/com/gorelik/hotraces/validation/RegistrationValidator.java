package com.gorelik.hotraces.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.gorelik.hotraces.logic.registration.RegistrationData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class RegistrationValidator. Class that handles registration data.
 */
public class RegistrationValidator {

	/** The Constant NAME_REGEXPR. Regular expression for name. */
	private static final String NAME_REGEXPR = "[a-zA-Z]{3,16}";

	/** The Constant LOGIN_REGEXPR. Regular expression for login. */
	private static final String LOGIN_REGEXPR = "[a-zA-Z\\d_]{3,16}";

	/** The Constant PASSWORD_REGEXPR. Regular expression for password. */
	private static final String PASSWORD_REGEXPR = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[\\w]{6,}$";

	/** The Constant EMAIL_REGEXPR. Regular expression for email. */
	private static final String EMAIL_REGEXPR = "^[-\\w.]+@([A-z0-9][-A-z0-9]+\\.)+[A-z]{2,4}$";

	/**
	 * Check registration.
	 *
	 * @param regData
	 *            the registration data
	 */
	public void checkRegistration(RegistrationData regData) {
		checkFirstName(regData.getFirstName(), regData);
		checkSecondName(regData.getSecondName(), regData);
		checkLogin(regData.getLogin(), regData);
		checkPassword(regData.getPassword(), regData.getRepeatPassword(), regData);
		checkEmail(regData.getEmail(), regData);
		if (regData.getValidation() == null) {
			regData.setValidation(true);
		}
	}

	/**
	 * Check first name.
	 *
	 * @param firstName
	 *            the first name
	 * @param regData
	 *            the registration data
	 */
	private void checkFirstName(String firstName, RegistrationData regData) {
		Pattern parserPattern = Pattern.compile(NAME_REGEXPR);
		Matcher matcher = parserPattern.matcher(firstName);
		if (!(matcher.find() && matcher.group().equals(firstName))) {
			regData.setErrorMessage(HotRacesErrorCode.REGISTR_INVALID_LOGIN);
			regData.setValidation(false);
		}
	}

	/**
	 * Check second name.
	 *
	 * @param secondName
	 *            the second name
	 * @param regData
	 *            the registration data
	 */
	private void checkSecondName(String secondName, RegistrationData regData) {
		Pattern parserPattern = Pattern.compile(NAME_REGEXPR);
		Matcher matcher = parserPattern.matcher(secondName);
		if (!(matcher.find() && matcher.group().equals(secondName))) {
			regData.setErrorMessage(HotRacesErrorCode.REGISTR_INVALID_LOGIN);
			regData.setValidation(false);
		}
	}

	/**
	 * Check login.
	 *
	 * @param login
	 *            the login
	 * @param regData
	 *            the registration data
	 */
	private void checkLogin(String login, RegistrationData regData) {
		Pattern parserPattern = Pattern.compile(LOGIN_REGEXPR);
		Matcher matcher = parserPattern.matcher(login);
		if (!(matcher.find() && matcher.group().equals(login))) {
			regData.setErrorMessage(HotRacesErrorCode.REGISTR_INVALID_LOGIN);
			regData.setValidation(false);
		}
	}

	/**
	 * Check password.
	 *
	 * @param password1
	 *            the password
	 * @param password2
	 *            the repoeated password
	 * @param regData
	 *            the registration data
	 */
	private void checkPassword(String password1, String password2, RegistrationData regData) {
		Pattern parserPattern = Pattern.compile(PASSWORD_REGEXPR);
		Matcher matcher = parserPattern.matcher(password1);
		if (!(matcher.find() && matcher.group().equals(password1))) {
			regData.setErrorMessage(HotRacesErrorCode.REGISTR_INVALID_PASSWORD);
			regData.setValidation(false);
		}
		if (!password1.equals(password2)) {
			regData.setErrorMessage(HotRacesErrorCode.REGISTR_INVALID_REPEAT_PASSWORD);
			regData.setValidation(false);
		}
	}

	/**
	 * Check email.
	 *
	 * @param email
	 *            the email
	 * @param regData
	 *            the registration data
	 */
	private void checkEmail(String email, RegistrationData regData) {
		Pattern parserPattern = Pattern.compile(EMAIL_REGEXPR);
		Matcher matcher = parserPattern.matcher(email);
		if (!(matcher.find() && matcher.group().equals(email))) {
			regData.setErrorMessage(HotRacesErrorCode.REGISTR_INVALID_EMAIL);
			regData.setValidation(false);
		}
	}
}
