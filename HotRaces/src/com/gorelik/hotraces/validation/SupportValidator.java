package com.gorelik.hotraces.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.gorelik.hotraces.entity.SupportMessage;

/**
 * The Class SupportValidator. Class that handles support message data.
 */
public class SupportValidator {

	/** The Constant EMAIL_REGEXPR. Regular expression for email. */
	private static final String EMAIL_REGEXPR = "^[-\\w.]+@([A-z0-9][-A-z0-9]+\\.)+[A-z]{2,4}$";

	/** The Constant TEXT_REGEXPR. Regular expression for message. */
	private static final String TEXT_REGEXPR = "[A-Za-z�-��-�\\d\\p{P}\\s]{10,}$";

	/** The Constant MIN_TYPE_VALUE. Regular expression for type value. */
	private static final int MIN_TYPE_VALUE = 1;

	/** The Constant MAX_TYPE_VALUE. Regular expression for type value. */
	private static final int MAX_TYPE_VALUE = 5;

	/**
	 * Check support message.
	 *
	 * @param suppMessage
	 *            the support message
	 * @return true, if successful
	 */
	public boolean checkSuppMessage(SupportMessage suppMessage) {
		if (!checkMessage(suppMessage.getMessage())) {
			return false;
		}
		if (!checkEmail(suppMessage.getEmail())) {
			return false;
		}
		if (!checkType(Integer.valueOf(suppMessage.getType()))) {
			return false;
		}

		return true;
	}

	/**
	 * Check email.
	 *
	 * @param email
	 *            the email
	 * @return true, if successful
	 */
	public boolean checkEmail(String email) {
		Pattern parserPattern = Pattern.compile(EMAIL_REGEXPR);
		Matcher matcher = parserPattern.matcher(email);
		if (!(matcher.find() && matcher.group().equals(email))) {
			return false;
		}
		return true;

	}

	/**
	 * Check message.
	 *
	 * @param message
	 *            the message
	 * @return true, if successful
	 */
	public boolean checkMessage(String message) {
		if (message.isEmpty()) {
			return false;
		}
		Pattern parserPattern = Pattern.compile(TEXT_REGEXPR);
		Matcher matcher = parserPattern.matcher(message);
		if (!(matcher.find() && matcher.group().equals(message))) {
			return false;
		}
		return true;
	}

	/**
	 * Check type.
	 *
	 * @param type
	 *            the type
	 * @return true, if successful
	 */
	public boolean checkType(int type) {
		return (type >= MIN_TYPE_VALUE && type <= MAX_TYPE_VALUE) ? true : false;
	}
}