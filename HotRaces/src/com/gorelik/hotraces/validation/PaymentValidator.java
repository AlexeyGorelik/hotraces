package com.gorelik.hotraces.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.gorelik.hotraces.logic.payment.PaymentData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class PaymentValidator. Class that handles payment values.
 */
public class PaymentValidator {

	/** The Constant CARD_CODE_REGEXRP. Regular expression for card code. */
	private final static String CARD_CODE_REGEXRP = "^[0-9]{4}$";

	/** The Constant CVV_REGEXRP. Regular expression for CVV code. */
	private final static String CVV_REGEXRP = "^[0-9]{3}$";

	/** The Constant SUM_REGEXPR. Regular expression for sum. */
	private final static String SUM_REGEXPR = "^[-+]?[0-9]*[\\.]?[0-9](?:[eE][-+]?[0-9]+)?$";

	/**
	 * The Constant MONTH_AND_YEAR_REGEXRP. Regular expression for month and
	 * year.
	 */
	private final static String MONTH_AND_YEAR_REGEXRP = "^[0-9]{2}$";

	/**
	 * Validate.
	 *
	 * @param payData
	 *            the pay data
	 */
	public void validate(PaymentData payData) {
		checkCvv(payData.getCvv(), payData);
		checkSum(payData.getStringSum(), payData);
		checkCode(payData.getCardCodeArray(), payData);
		checkYear(payData.getYear(), payData);
		checkMonth(payData.getMonth(), payData);
		if (payData.getValidation() == null) {
			payData.setValidation(true);
		}
	}

	/**
	 * Check cvv.
	 *
	 * @param cvv
	 *            the cvv
	 * @param payData
	 *            the pay data
	 */
	private void checkCvv(String cvv, PaymentData payData) {
		Pattern parserPattern = Pattern.compile(CVV_REGEXRP);
		Matcher matcher = parserPattern.matcher(cvv);
		if (!(matcher.find() && matcher.group().equals(cvv))) {
			payData.setErrorMessage(HotRacesErrorCode.PAYMENT_INVALID_CARD_CODE);
			payData.setValidation(false);
		}
	}

	/**
	 * Check sum.
	 *
	 * @param sum
	 *            the sum
	 * @param payData
	 *            the pay data
	 */
	private void checkSum(String sum, PaymentData payData) {
		Pattern parserPattern = Pattern.compile(SUM_REGEXPR);
		Matcher matcher = parserPattern.matcher(sum);
		if (!(matcher.find() && matcher.group().equals(sum))) {
			payData.setErrorMessage(HotRacesErrorCode.PAYMENT_INVALID_SUM);
			payData.setValidation(false);
		}
	}

	/**
	 * Check code.
	 *
	 * @param code
	 *            the code
	 * @param payData
	 *            the pay data
	 */
	private void checkCode(String[] code, PaymentData payData) {
		Pattern parserPattern = Pattern.compile(CARD_CODE_REGEXRP);

		for (int i = 0; i < code.length; i++) {
			Matcher matcher = parserPattern.matcher(code[i]);
			if (!(matcher.find() && matcher.group().equals(code[i]))) {
				payData.setErrorMessage(HotRacesErrorCode.PAYMENT_INVALID_CARD_CODE);
				payData.setValidation(false);
				return;
			}
		}
	}

	/**
	 * Check year.
	 *
	 * @param year
	 *            the year
	 * @param payData
	 *            the pay data
	 */
	private void checkYear(String year, PaymentData payData) {
		Pattern parserPattern = Pattern.compile(MONTH_AND_YEAR_REGEXRP);
		Matcher matcher = parserPattern.matcher(year);
		if (!(matcher.find() && matcher.group().equals(year))) {
			payData.setErrorMessage(HotRacesErrorCode.PAYMENT_INVALID_YEAR);
			payData.setValidation(false);
		}
	}

	/**
	 * Check month.
	 *
	 * @param month
	 *            the month
	 * @param payData
	 *            the pay data
	 */
	private void checkMonth(String month, PaymentData payData) {
		Pattern parserPattern = Pattern.compile(MONTH_AND_YEAR_REGEXRP);
		Matcher matcher = parserPattern.matcher(month);
		if (!(matcher.find() && matcher.group().equals(month))) {
			payData.setErrorMessage(HotRacesErrorCode.PAYMENT_INVALID_MONTH);
			payData.setValidation(false);
			return;
		}
		if (month.charAt(0) != '0') {
			if (Integer.valueOf(month) > 12) {
				payData.setErrorMessage(HotRacesErrorCode.PAYMENT_INVALID_MONTH);
				payData.setValidation(false);
			}
		}
	}

}
