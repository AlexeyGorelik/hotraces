package com.gorelik.hotraces.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.gorelik.hotraces.logic.login.LoginData;
import com.gorelik.hotraces.pageerror.HotRacesErrorCode;

/**
 * The Class AuthorizationValidator. Class that handles auhorization data.
 */
public class AuthorizationValidator {

	/** The Constant LOGIN_REGEXPR. Regular expression for login value. */
	private static final String LOGIN_REGEXPR = "[a-zA-Z\\d_]{3,16}";

	/** The Constant PASSWORD_REGEXPR. Regular expression for password value. */
	private static final String PASSWORD_REGEXPR = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[\\w]{6,}$";

	/**
	 * Check authentication.
	 *
	 * @param loginData
	 *            the login data
	 */
	public void checkAuthentication(LoginData loginData) {
		checkLogin(loginData.getLogin(), loginData);
		checkPassword(loginData.getPassword(), loginData);
		if (loginData.getValidation() == null) {
			loginData.setValidation(true);
		}
	}

	/**
	 * Check login.
	 *
	 * @param login
	 *            the login
	 * @param loginData
	 *            the login data
	 */
	private void checkLogin(String login, LoginData loginData) {
		Pattern parserPattern = Pattern.compile(LOGIN_REGEXPR);
		Matcher matcher = parserPattern.matcher(login);
		if (!(matcher.find() && matcher.group().equals(login))) {
			loginData.setErrorMessage(HotRacesErrorCode.AUTH_INVALID_LOGIN);
			loginData.setValidation(false);
		}
	}

	/**
	 * Check password.
	 *
	 * @param password
	 *            the password
	 * @param loginData
	 *            the login data
	 */
	private void checkPassword(String password, LoginData loginData) {
		Pattern parserPattern = Pattern.compile(PASSWORD_REGEXPR);
		Matcher matcher = parserPattern.matcher(password);
		if (!(matcher.find() && matcher.group().equals(password))) {
			loginData.setErrorMessage(HotRacesErrorCode.AUTH_INVALID_PASSWORD);
			loginData.setValidation(false);
		}
	}
}
