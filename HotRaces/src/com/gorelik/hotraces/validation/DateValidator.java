package com.gorelik.hotraces.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class DateValidator. Class that handles date value.
 */
public class DateValidator {

	/**
	 * The Constant DATE_REGEXPR. Regular expression for years, months and days.
	 */
	private static final String DATE_REGEXPR = "[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])"; // YYYY-MM-DD

	/**
	 * The Constant TIME_REGEXPR. Regular expression for hours, minutes and
	 * seconds.
	 */
	private static final String TIME_REGEXPR = "^([0-1]\\d|2[0-3])(:[0-5]\\d){2}$"; // HH:MM:SS

	/**
	 * The Constant CUT_TIME_REGEXPR. Regular expression for hours and minutes.
	 */
	private static final String CUT_TIME_REGEXPR = "^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$"; // HH:MM

	/** The Constant DATE_DELIMETER. Delimeter for date to DB. */
	private static final String DATE_DELIMETER = " ";

	/** The Constant MAX_LENGTH_COUNT. Right array length of input date. */
	private static final int MAX_LENGTH_COUNT = 2;

	/**
	 * Check full date.
	 *
	 * @param fullDate
	 *            the full date
	 * @return true, if successful
	 */
	public boolean checkFullDate(String fullDate) {
		String dateArr[] = fullDate.split(DATE_DELIMETER);
		if (dateArr.length != MAX_LENGTH_COUNT) {
			return false;
		}
		Pattern datePattern = Pattern.compile(DATE_REGEXPR);
		Matcher matcher = datePattern.matcher(dateArr[0]);
		if (!(matcher.find() && matcher.group().equals(dateArr[0]))) {
			return false;
		}

		Pattern timePattern = Pattern.compile(TIME_REGEXPR);
		matcher = timePattern.matcher(dateArr[1]);
		if (!(matcher.find() && matcher.group().equals(dateArr[1]))) {
			return false;
		}
		return true;
	}

	/**
	 * Check cut date.
	 *
	 * @param cutDate
	 *            the cut date
	 * @return true, if successful
	 */
	public boolean checkCutDate(String cutDate) {
		String dateArr[] = cutDate.split(DATE_DELIMETER);
		if (dateArr.length != MAX_LENGTH_COUNT) {
			return false;
		}
		Pattern datePattern = Pattern.compile(DATE_REGEXPR);
		Matcher matcher = datePattern.matcher(dateArr[0]);
		if (!(matcher.find() && matcher.group().equals(dateArr[0]))) {
			return false;
		}

		Pattern timePattern = Pattern.compile(CUT_TIME_REGEXPR);
		matcher = timePattern.matcher(dateArr[1]);
		if (!(matcher.find() && matcher.group().equals(dateArr[1]))) {
			return false;
		}
		return true;
	}
}
