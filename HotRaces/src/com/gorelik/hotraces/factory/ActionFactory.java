package com.gorelik.hotraces.factory;

import javax.servlet.http.HttpServletRequest;

import com.gorelik.hotraces.command.ActionCommand;
import com.gorelik.hotraces.command.CommandType;
import com.gorelik.hotraces.command.EmptyCommand;

/**
 * A factory for creating Action objects. Class that creates command.
 */
public class ActionFactory {

	/**
	 * Define command and return command variable.
	 *
	 * @param request
	 *            the request
	 * @return the action command
	 */
	public ActionCommand defineCommand(HttpServletRequest request) {
		ActionCommand current = new EmptyCommand();
		String action = request.getParameter("command");
		try {
			CommandType currentEnum = CommandType.valueOf(action.toUpperCase());
			current = currentEnum.getCurrentCommand();
		} catch (IllegalArgumentException e) {
			current = new EmptyCommand();
		}
		return current;
	}
}