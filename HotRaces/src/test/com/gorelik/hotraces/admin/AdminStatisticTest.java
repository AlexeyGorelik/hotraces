package test.com.gorelik.hotraces.admin;

import java.math.BigDecimal;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.connection.ConnectionPool;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.statistic.StatisticManager;

public class AdminStatisticTest {
	@BeforeClass
	public static void initVariables() {
		ConnectionPool.getInstance();
	}

	@Test
	public void testTodayBetsCount() {
		int expected = 25;
		try {
			Assert.assertEquals(expected, new StatisticManager().findTodayBetsCount());
		} catch (ManagerException e) {
			Assert.fail();
		}
	}

	@Test
	public void testMonthBetsCount() {
		int expected = 25;
		try {
			Assert.assertEquals(expected, new StatisticManager().findMonthBetsCount());
		} catch (ManagerException e) {
			Assert.fail();
		}
	}

	@Test
	public void testTodayIncome() {
		BigDecimal expected = new BigDecimal("-17.3");
		try {
			Assert.assertEquals(expected, new StatisticManager().findTodayIncome());
		} catch (ManagerException e) {
			Assert.fail();
		}
	}

	@Test
	public void testMonthIncome() {
		BigDecimal expected = new BigDecimal("-17.3");
		try {
			Assert.assertEquals(expected, new StatisticManager().findMonthIncome());
		} catch (ManagerException e) {
			Assert.fail();
		}
	}

	@Test
	public void testTodayRaceCount() {
		int expected = 0;
		try {
			Assert.assertEquals(expected, new StatisticManager().findTodayRaceCount());
		} catch (ManagerException e) {
			Assert.fail();
		}
	}

	@Test
	public void testMonthRaceCount() {
		int expected = 0;
		try {
			Assert.assertEquals(expected, new StatisticManager().findUserThisMonthCount());
		} catch (ManagerException e) {
			Assert.fail();
		}
	}

	@AfterClass
	public static void destroy() {
		ConnectionPool.realClosePool();
	}
}
