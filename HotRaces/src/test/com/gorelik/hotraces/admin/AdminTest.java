package test.com.gorelik.hotraces.admin;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.command.ChangeUserStatusCommand;
import com.gorelik.hotraces.connection.ConnectionPool;
import com.gorelik.hotraces.entity.Horse;
import com.gorelik.hotraces.entity.Race;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.ManagerException;
import com.gorelik.hotraces.logic.admin.AdminRaceTableManager;
import com.gorelik.hotraces.logic.admin.AdminUserTableManager;
import com.gorelik.hotraces.logic.horse.HorseManager;

public class AdminTest {
	private static HttpServletRequest request;

	@BeforeClass
	public static void initVariables() {
		ConnectionPool.getInstance();
	}

	@Before
	public void initTempVariables() {
		request = mock(HttpServletRequest.class);
	}

	@Test
	public void testHorseTableCount() {
		int expected = 13;
		try {
			HorseManager horseMng = new HorseManager();
			List<Horse> list = horseMng.findAllHorsesList();
			Assert.assertEquals(expected, list.size());
		} catch (ManagerException e) {
			Assert.fail();
		}
	}

	@Test
	public void testUserTableCommand() {
		int expected = 3;
		try {
			AdminUserTableManager userMng = new AdminUserTableManager();
			List<User> list = userMng.createUserList();
			Assert.assertEquals(expected, list.size());
		} catch (ManagerException e) {
			Assert.fail();
		}
	}

	@Test
	public void testChangeUserStatus() {
		String userId = "3";
		String banCommand = "banUser";
		String unbanCommand = "unbanUser";

		when(request.getParameter("command")).thenReturn(banCommand);
		when(request.getParameter("selected-row-value")).thenReturn(userId);
		ChangeUserStatusCommand changeCommand = new ChangeUserStatusCommand();
		Assert.assertEquals("/HotRaces/HotRacesServlet?command=adminUserTable", changeCommand.execute(request));
		when(request.getParameter("command")).thenReturn(unbanCommand);
		changeCommand.execute(request);
	}

	@Test
	public void testRaceTableCommand() {
		int expected = 9;
		try {
			AdminRaceTableManager raceMng = new AdminRaceTableManager();
			List<Race> list = raceMng.createAdminRaceTable();
			Assert.assertEquals(expected, list.size());
		} catch (ManagerException e) {
			Assert.fail();
		}
	}
}