package test.com.gorelik.hotraces.bets;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import com.gorelik.hotraces.command.MakeBetCommand;
import com.gorelik.hotraces.command.PreviousBetCommand;
import com.gorelik.hotraces.command.ToMakeBetCommand;
import com.gorelik.hotraces.entity.Bet;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.logic.bets.BetsManager;
import com.gorelik.hotraces.logic.bets.MakeBetData;
import com.gorelik.hotraces.logic.profile.PreviousBetManager;

public class BetsCommandTest {
	private static HttpSession session;
	private static HttpServletRequest request;

	@Before
	public void initTempVariables() {
		session = mock(HttpSession.class);
		request = mock(HttpServletRequest.class);
	}

	@Test
	public void testToMakeBetCommand() throws Exception {
		String expected = "/jsp/profile/client/makeBet.jsp";

		ToMakeBetCommand betCommand = new ToMakeBetCommand();
		BetsManager betMng = mock(BetsManager.class);
		betCommand.setBetsManager(betMng);

		when(request.getSession()).thenReturn(session);
		when(request.getSession().getAttribute("locale")).thenReturn("ru_RU");
		when(request.getParameter("selected-row-value")).thenReturn("10-05-2017 15:15");
		when(request.getParameter("locale")).thenReturn("ru_RU");
		when(request.getParameter("hippodrom")).thenReturn("������");

		when(betMng.findMakeBetData(any(String.class), any(String.class), any(String.class)))
				.thenReturn(new ArrayList<MakeBetData>());

		Assert.assertEquals(expected, betCommand.execute(request));
	}

	@Test
	public void testMakeBetCommand() {
		String expected = "/HotRaces/HotRacesServlet?command=toBets";

		MakeBetCommand betCommand = new MakeBetCommand();
		BetsManager betMng = mock(BetsManager.class);
		User user = mock(User.class);
		betCommand.setBetsManager(betMng);

		when(request.getSession()).thenReturn(session);
		when(request.getSession().getAttribute("locale")).thenReturn("ru_RU");
		when(request.getSession().getAttribute("user")).thenReturn(user);

		when(request.getParameter("race-name")).thenReturn("������");
		when(request.getParameter("race-date")).thenReturn("2017-05-10 16:25:00");
		when(request.getParameter("betCount")).thenReturn("0");

		Assert.assertEquals(expected, betCommand.execute(request));
	}

	@Test
	public void testPreviousBetsCommand() throws Exception {
		String expected = "/jsp/profile/client/profileClient.jsp";

		PreviousBetCommand prevCommand = new PreviousBetCommand();
		PreviousBetManager prevMng = mock(PreviousBetManager.class);
		prevCommand.setPrevManager(prevMng);

		Bet bet = mock(Bet.class);
		List<Bet> list = new ArrayList<Bet>();
		list.add(bet);
		User user = mock(User.class);

		when(prevMng.getPreviousBets(user)).thenReturn(list);
		when(request.getSession()).thenReturn(session);
		when(request.getSession().getAttribute("user")).thenReturn(user);

		Assert.assertEquals(expected, prevCommand.execute(request));
	}
}
