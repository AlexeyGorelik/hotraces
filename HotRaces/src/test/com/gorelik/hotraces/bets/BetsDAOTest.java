package test.com.gorelik.hotraces.bets;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.connection.ConnectionPool;
import com.gorelik.hotraces.dao.BetDAO;
import com.gorelik.hotraces.entity.Bet;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.logic.bets.MakeBetData;
import com.gorelik.hotraces.logic.race.RaceData;

public class BetsDAOTest {
	private static final String DELETE_BET = "DELETE FROM bet WHERE horse_race_name = ? AND horse_race_date = ?;";

	@BeforeClass
	public static void initBeforeClass() {
		ConnectionPool.getInstance();
	}

	@Test
	public void testFindMakeBetList() throws Exception {
		int expected = 4;

		String raceName = "������";
		String raceDate = "2017-05-20 09:00:00";

		RaceData raceData = new RaceData(raceName, raceDate);
		BetDAO betDao = new BetDAO();
		List<MakeBetData> actual = betDao.findMakeBetList(raceData);

		betDao.close();

		Assert.assertEquals(expected, actual.size());
	}

	@Test
	public void testMakeBetCommand() throws Exception {
		Integer expected = 1;

		String raceName = "������";
		String raceDate = "2017-02-02 03:03:00";
		User user = new User(3, "orangeMood", "�������", "���������", "orange@mood.com", "Client", "�������");

		RaceData raceData = new RaceData(raceName, raceDate);

		BetDAO betDao = new BetDAO();

		List<MakeBetData> betList = new ArrayList<MakeBetData>();
		betList.add(new MakeBetData(1, "����", new BigDecimal(2.3), new BigDecimal(1), 1));

		betDao.makeBet(raceData, betList, user);

		betDao.close();

		PreparedStatement deleteStatement = ConnectionPool.getConnection().getPreparedStatement(DELETE_BET);
		deleteStatement.setString(1, raceName);
		deleteStatement.setTimestamp(2, java.sql.Timestamp.valueOf(raceDate));
		Integer actual = deleteStatement.executeUpdate();
		deleteStatement.close();

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void getPrevBetsCommand() throws Exception {
		Integer expected = 9;

		User user = new User(3, "orangeMood", "�������", "���������", "orange@mood.com", "Client", "�������");

		BetDAO betDao = new BetDAO();
		List<Bet> resultList = betDao.findUserResult(user);
		Integer actual = resultList.size();
		betDao.close();

		Assert.assertEquals(expected, actual);
	}

	@AfterClass
	public static void destroy() {
		ConnectionPool.realClosePool();
	}
}
