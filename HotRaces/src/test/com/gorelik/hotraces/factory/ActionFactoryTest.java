package test.com.gorelik.hotraces.factory;

import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.command.EmptyCommand;
import com.gorelik.hotraces.command.ToMainPageCommand;
import com.gorelik.hotraces.factory.ActionFactory;

public class ActionFactoryTest {
	private static ActionFactory actionFactory;

	@BeforeClass
	public static void initVariables() {
		actionFactory = new ActionFactory();
	}

	@Test
	public void testRightCommand() {
		HttpServletRequest request = mock(HttpServletRequest.class);
		when(request.getParameter("command")).thenReturn("toMain");
		Assert.assertTrue(actionFactory.defineCommand(request).getClass() == new ToMainPageCommand().getClass());
	}

	@Test
	public void testWrongCommand() {
		HttpServletRequest request = mock(HttpServletRequest.class);
		when(request.getParameter("command")).thenReturn("wrongCommand");
		Assert.assertTrue(actionFactory.defineCommand(request).getClass() == new EmptyCommand().getClass());
	}
}