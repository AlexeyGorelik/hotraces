package test.com.gorelik.hotraces.payment;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.gorelik.hotraces.command.PaymentCommand;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.logic.payment.PaymentData;
import com.gorelik.hotraces.logic.payment.PaymentManager;

public class PaymentCommandTest {
	private static HttpSession session;
	private static HttpServletRequest request;

	@Before
	public void initTempVariables() {
		session = mock(HttpSession.class);
		request = mock(HttpServletRequest.class);
	}

	@Test
	public void testPaymentCommand() throws Exception {
		String expected = "/HotRaces/HotRacesServlet?command=toPayment";

		String cvv = "232";
		String sum = "1.0";
		String cardMonth = "01";
		String cardYear = "15";
		String cardNum1 = "1234";
		String cardNum2 = "2345";
		String cardNum3 = "3456";
		String cardNum4 = "4567";

		PaymentCommand payCommand = new PaymentCommand();
		PaymentManager payMng = mock(PaymentManager.class);
		User user = mock(User.class);

		when(payMng.pay(any(PaymentData.class), any(User.class))).thenReturn(user);
		payCommand.setPaymentManager(payMng);

		when(request.getSession()).thenReturn(session);
		when(request.getSession().getAttribute("user")).thenReturn(user);
		when(request.getParameter("cvv")).thenReturn(cvv);
		when(request.getParameter("sum")).thenReturn(sum);
		when(request.getParameter("card-month")).thenReturn(cardMonth);
		when(request.getParameter("card-year")).thenReturn(cardYear);
		when(request.getParameter("card-num1")).thenReturn(cardNum1);
		when(request.getParameter("card-num2")).thenReturn(cardNum2);
		when(request.getParameter("card-num3")).thenReturn(cardNum3);
		when(request.getParameter("card-num4")).thenReturn(cardNum4);

		Assert.assertEquals(expected, payCommand.execute(request));
	}
}
