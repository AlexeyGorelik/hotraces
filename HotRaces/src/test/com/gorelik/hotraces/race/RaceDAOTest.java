package test.com.gorelik.hotraces.race;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.connection.ConnectionPool;
import com.gorelik.hotraces.dao.HorseDAO;
import com.gorelik.hotraces.dao.RaceDAO;
import com.gorelik.hotraces.entity.Horse;
import com.gorelik.hotraces.logic.race.EditRaceData;
import com.gorelik.hotraces.logic.race.RaceData;


public class RaceDAOTest 
{
	@BeforeClass
	public static void initBeforeClass()
	{
		ConnectionPool.getInstance();
	}
	
	@Test
	public void createRace() throws Exception
	{
		RaceData raceData = new RaceData("������", "2017-05-07 16:48:55");
		Map<String, String> horseMap = new HashMap<String, String>();
		horseMap.put("1", "����");
		horseMap.put("2", "����");
		
		RaceDAO raceDao = new RaceDAO();
		raceDao.createRace(raceData, horseMap);
		raceDao.close();
		
		ConnectionPool.getConnection().getPreparedStatement("DELETE FROM horse_in_race WHERE horse_race_date = '2017-05-07 13:48:55';").executeUpdate();
		ConnectionPool.getConnection().getPreparedStatement("DELETE FROM race WHERE date = '2017-05-07 13:48:55';").executeUpdate();
	}
	
	@Test
	public void testAdminRaceTable() throws Exception
	{
		int expected = 10;
		RaceDAO raceDao = new RaceDAO();
		Assert.assertEquals(expected, raceDao.createAdminRaceTable().size());
		raceDao.close();
	}
	
	@Test
	public void testCarryOutRace() throws Exception
	{
		String expected = "���������";
		String raceName = "������";
		String raceDate = "2017-05-06 11:33:11";
		String dbRaceDate = "2017-05-06 08:33:11";
		String actual = "";
		
		//create temp race without horses
		RaceData raceData = new RaceData(raceName, raceDate);
		Map<String, String> horseMap = new HashMap<String, String>();
		
		RaceDAO raceDao = new RaceDAO();
		raceDao.createRace(raceData, horseMap);
		
		raceDao.carryOutRace(raceData);
		
		//find race status
		RaceData statusData = new RaceData(raceName, dbRaceDate);
		actual = raceDao.getRaceStatus(statusData);
		
		//delete created race
		RaceData deleteRaceData = new RaceData(raceName, dbRaceDate);
		raceDao.deleteRaceFromDB(deleteRaceData);
		raceDao.close();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testAdminDeleteRace() throws Exception
	{
		String expected = "��������";
		String raceName = "������";
		String raceDate = "2017-05-06 11:33:11";
		String dbRaceDate = "2017-05-06 08:33:11";
		String actual = "";
		
		//create temp race without horses
		RaceData raceData = new RaceData(raceName, raceDate);
		Map<String, String> horseMap = new HashMap<String, String>();
		
		RaceDAO raceDao = new RaceDAO();
		raceDao.createRace(raceData, horseMap);
		
		raceDao.deleteRace(raceData);
		
		//find race status
		RaceData statusData = new RaceData(raceName, dbRaceDate);
		actual = raceDao.getRaceStatus(statusData);
		
		//delete created race
		RaceData deleteRaceData = new RaceData(raceName, dbRaceDate);
		raceDao.deleteRaceFromDB(deleteRaceData);
		raceDao.close();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testEditRace() throws Exception
	{
		int actual = 3;
		RaceData raceData = new RaceData("��������", "2017-05-08 05:05:05");
		RaceData dbRaceData = new RaceData("��������", "2017-05-08 02:05:05");
		
		Map<String, String> horseMap = new HashMap<String, String>();
		horseMap.put("1", "����");
		horseMap.put("2", "����");
		
		RaceDAO raceDao = new RaceDAO();
		raceDao.createRace(raceData, horseMap);
		
		List<EditRaceData> editList = new ArrayList<EditRaceData>();
		editList.add(new EditRaceData("2", "����", "delete"));
		editList.add(new EditRaceData("3", "�����", "add"));
		editList.add(new EditRaceData("4", "�����", "add"));
		raceDao.editRace(editList, raceData);
		
		List<Horse> newHorseList = raceDao.findEditHorseList(raceData);
		
		HorseDAO horseDao = new HorseDAO();
		horseDao.realDeleteHorse(dbRaceData, 1);
		horseDao.realDeleteHorse(dbRaceData, 2);
		horseDao.realDeleteHorse(dbRaceData, 3);
		horseDao.realDeleteHorse(dbRaceData, 4);
		raceDao.deleteRaceFromDB(dbRaceData);
		
		horseDao.close();
		raceDao.close();
		
		Assert.assertEquals(newHorseList.size(), actual);
	}
	
	@AfterClass
	public static void destroy()
	{
		ConnectionPool.realClosePool();
	}
}
