package test.com.gorelik.hotraces.race;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import com.gorelik.hotraces.command.AdminDeleteRaceCommand;
import com.gorelik.hotraces.command.AdminRaceTableCommand;
import com.gorelik.hotraces.command.CarryOutRaceCommand;
import com.gorelik.hotraces.command.CreateRaceCommand;
import com.gorelik.hotraces.command.EditRaceCommand;
import com.gorelik.hotraces.connection.ConnectionPool;
import com.gorelik.hotraces.logic.admin.AdminRaceTableManager;
import com.gorelik.hotraces.logic.race.AdminRaceManager;
import com.gorelik.hotraces.logic.race.EditRaceData;
import com.gorelik.hotraces.logic.race.RaceData;
import com.gorelik.hotraces.resource.PageManager;

public class RaceCommandTest {
	private static HttpSession session;
	private static HttpServletRequest request;

	@BeforeClass
	public static void initVariables() {
		ConnectionPool.getInstance();
	}

	@Before
	public void initTempVariables() {
		session = mock(HttpSession.class);
		request = mock(HttpServletRequest.class);
	}

	@Test
	public void testCreateRace() throws Exception {

		CreateRaceCommand createCommand = new CreateRaceCommand();
		AdminRaceManager raceMng = mock(AdminRaceManager.class);

		Mockito.doNothing().when(raceMng).createRace((any(String[].class)), (any(String[].class)),
				(any(RaceData.class)), (any(String.class)));
		createCommand.setCreateRaceManager(raceMng);

		String expected = "/HotRaces/HotRacesServlet?command=adminRaceTable";

		when(request.getParameter("add-race-name")).thenReturn("������");
		when(request.getParameter("add-race-date")).thenReturn("2017-05-04 03:03");
		when(request.getSession()).thenReturn(session);
		when(request.getSession().getAttribute("locale")).thenReturn("ru_RU");

		when(request.getParameterValues("checkbox-horse")).thenReturn(new String[] { "horse-1", "horse-2" });
		when(request.getParameter("horse-1-id")).thenReturn("1");
		when(request.getParameter("horse-1-name")).thenReturn("����");
		when(request.getParameter("horse-2-id")).thenReturn("2");
		when(request.getParameter("horse-2-name")).thenReturn("����");

		Assert.assertEquals(expected, createCommand.execute(request));
	}

	@Test
	public void testAdminRaceTableCommand() throws Exception {
		String expected = "/jsp/profile/admin/adminRaceTable.jsp";

		AdminRaceTableCommand tableCommand = new AdminRaceTableCommand();
		AdminRaceTableManager tableMng = mock(AdminRaceTableManager.class);

		when(tableMng.createAdminRaceTable()).thenReturn(null);
		when(request.getParameter("error")).thenReturn(null);

		tableCommand.setRaceTableManager(tableMng);

		Assert.assertEquals(expected, tableCommand.execute(request));
	}

	@Test
	public void testAdminCarryOutRaceCommand() throws Exception {
		String raceName = "������";
		String raceDate = "2017-11-11 11:11:11";
		String raceStatus = "�������";

		String expected = "/HotRaces/HotRacesServlet?command=adminRaceTable";

		CarryOutRaceCommand carryOutCommand = new CarryOutRaceCommand();
		AdminRaceManager raceMng = mock(AdminRaceManager.class);

		Mockito.doNothing().when(raceMng).carryOutRace(any(String[].class), any(String.class));
		when(request.getParameter("selected-row-value")).thenReturn(raceName + ";" + raceDate + ";" + raceStatus);
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("locale")).thenReturn("ru_RU");

		carryOutCommand.setRaceManager(raceMng);

		Assert.assertEquals(expected, carryOutCommand.execute(request));
	}

	@Test
	public void testAdminDeleteRaceCommand() throws Exception {
		String raceName = "������";
		String raceDate = "2017-11-11 11:11:11";
		String raceStatus = "�������";
		String expected = "/HotRaces/HotRacesServlet?command=adminRaceTable";

		AdminDeleteRaceCommand deleteCommand = new AdminDeleteRaceCommand();
		AdminRaceManager raceMng = mock(AdminRaceManager.class);

		Mockito.doNothing().when(raceMng).deleteRace(any(String[].class), any(String.class));
		when(request.getParameter("selected-row-value")).thenReturn(raceName + ";" + raceDate + ";" + raceStatus);
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("locale")).thenReturn("ru_RU");

		deleteCommand.setDeleteRaceManager(raceMng);

		Assert.assertEquals(expected, deleteCommand.execute(request));
	}

	@Test
	public void testEditRaceCommand() throws Exception {
		String raceName = "������";
		String raceDate = "2017-05-07 23:00:00";
		String expected = "/HotRaces/HotRacesServlet?command=adminRaceTable";

		when(request.getSession()).thenReturn(session);
		when(request.getSession().getAttribute(PageManager.LOCALE)).thenReturn("ru_RU");
		when(request.getParameter("race-name")).thenReturn(raceName);
		when(request.getParameter("race-date")).thenReturn(raceDate);

		when(request.getParameter("horse-1")).thenReturn("delete");
		when(request.getParameter("horse-1-id")).thenReturn("1");
		when(request.getParameter("horse-1-name")).thenReturn("����");

		when(request.getParameter("horse-3")).thenReturn("add");
		when(request.getParameter("horse-3-id")).thenReturn("2");
		when(request.getParameter("horse-3-name")).thenReturn("����");

		when(request.getParameter("horse-2")).thenReturn(null);
		when(request.getParameter("horse-4")).thenReturn(null);
		when(request.getParameter("horse-5")).thenReturn(null);
		when(request.getParameter("horse-6")).thenReturn(null);
		when(request.getParameter("horse-7")).thenReturn(null);
		when(request.getParameter("horse-8")).thenReturn(null);

		EditRaceCommand editCommand = new EditRaceCommand();
		AdminRaceManager editMng = mock(AdminRaceManager.class);
		Mockito.doNothing().when(editMng).editRace(Matchers.anyListOf(EditRaceData.class), any(RaceData.class),
				any(String.class), any(Integer.class));
		editCommand.setEditRaceManager(editMng);

		Assert.assertEquals(expected, editCommand.execute(request));
	}
}
