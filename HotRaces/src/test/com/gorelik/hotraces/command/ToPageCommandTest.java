package test.com.gorelik.hotraces.command;

import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.command.ToMainPageCommand;
import com.gorelik.hotraces.factory.ActionFactory;

public class ToPageCommandTest {
	private static ActionFactory actFactory;
	private static HttpSession session;
	private static HttpServletRequest request;

	@BeforeClass
	public static void initVariables() {
		actFactory = new ActionFactory();
		request = mock(HttpServletRequest.class);
		request.getSession();
	}

	@Before
	public void resetVariables() {
		session = mock(HttpSession.class);
	}

	@Test
	public void testToMainPageCommand() {
		when(request.getSession()).thenReturn(session);
		Assert.assertEquals(new ToMainPageCommand().execute(request), "/index.jsp");
	}

	@Test
	public void testToAboutUsPageCommand() {
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("command")).thenReturn("toAboutUs");
		Assert.assertEquals(actFactory.defineCommand(request).execute(request), "/jsp/contact/aboutUs.jsp");
	}

	@Test
	public void testToConactPageCommand() {
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("command")).thenReturn("toContact");
		Assert.assertEquals(actFactory.defineCommand(request).execute(request), "/jsp/contact/contact.jsp");
	}

	@Test
	public void testToFaqPageCommand() {
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("command")).thenReturn("toFaq");
		Assert.assertEquals(actFactory.defineCommand(request).execute(request), "/HotRaces/jsp/faq/faq.jsp#faqName");
	}

	@Test
	public void testToForgotPasswordPageCommand() {
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("command")).thenReturn("toForgotPassword");
		Assert.assertEquals(actFactory.defineCommand(request).execute(request), "/jsp/help/forgotPassword.jsp");
	}

	@Test
	public void testToHowToPlayPageCommand() {
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("command")).thenReturn("toHowToPlay");
		Assert.assertEquals(actFactory.defineCommand(request).execute(request), "/jsp/help/howToPlay.jsp");
	}

	@Test
	public void testToLivePageCommand() {
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("command")).thenReturn("toLive");
		Assert.assertEquals(actFactory.defineCommand(request).execute(request), "/jsp/live/live.jsp");
	}

	@Test
	public void testToLoginPageCommand() {
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("command")).thenReturn("toLogin");
		Assert.assertEquals(actFactory.defineCommand(request).execute(request), "/HotRaces/jsp/login/login.jsp#toMain");
	}

	@Test
	public void testToPaymentPageCommand() {
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("command")).thenReturn("toPayment");
		Assert.assertEquals(actFactory.defineCommand(request).execute(request),
				"/jsp/profile/client/clientPayment.jsp");
	}

	@Test
	public void testToPaymentSystemPageCommand() {
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("command")).thenReturn("toPaymentSystem");
		Assert.assertEquals(actFactory.defineCommand(request).execute(request), "/jsp/help/paymentSystem.jsp");
	}

	@Test
	public void testToRegistrationPageCommand() {
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("command")).thenReturn("toRegistration");
		Assert.assertEquals(actFactory.defineCommand(request).execute(request),
				"/HotRaces/jsp/registration/registration.jsp#toMain");
	}

	@Test
	public void testToSupportPageCommand() {
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("command")).thenReturn("toSupport");
		Assert.assertEquals(actFactory.defineCommand(request).execute(request), "/jsp/help/support.jsp");
	}
}
