package test.com.gorelik.hotraces.user;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.gorelik.hotraces.command.LoginCommand;
import com.gorelik.hotraces.command.ProfileChangeCommand;
import com.gorelik.hotraces.command.RegistrationCommand;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.logic.login.LoginData;
import com.gorelik.hotraces.logic.login.LoginManager;
import com.gorelik.hotraces.logic.profile.ProfileChangeData;
import com.gorelik.hotraces.logic.profile.ProfileChangeManager;
import com.gorelik.hotraces.logic.registration.RegistrationData;
import com.gorelik.hotraces.logic.registration.RegistrationManager;

public class UserCommandTest {
	private static HttpSession session;
	private static HttpServletRequest request;

	@Before
	public void initTempVariables() {
		session = mock(HttpSession.class);
		request = mock(HttpServletRequest.class);
	}

	@Test
	public void testLoginCommand() throws Exception {
		LoginCommand loginCommand = new LoginCommand();
		User user = mock(User.class);

		LoginManager loginMng = mock(LoginManager.class);
		when(loginMng.login(any(LoginData.class))).thenReturn(user);
		loginCommand.setLoginManager(loginMng);

		String expected = "/HotRaces/HotRacesServlet?command=toMain";
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("login")).thenReturn("orangeMood");
		when(request.getParameter("password")).thenReturn("qwerT1");

		Assert.assertEquals(expected, loginCommand.execute(request));
	}

	@Test
	public void testRegistrationCommand() throws Exception {
		RegistrationCommand registrationCommand = new RegistrationCommand();

		RegistrationManager registrationMng = mock(RegistrationManager.class);
		Mockito.doNothing().when(registrationMng).registrateUser(any(RegistrationData.class));
		registrationCommand.setRegistrationManager(registrationMng);

		String expected = "/HotRaces/HotRacesServlet?command=toMain";
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("login")).thenReturn("orangeMood");
		when(request.getParameter("pswrd1")).thenReturn("qwerT1");
		when(request.getParameter("pswrd2")).thenReturn("qwerT1");
		when(request.getParameter("email")).thenReturn("someemail@gmail.com");
		when(request.getParameter("firstName")).thenReturn("����");
		when(request.getParameter("secondName")).thenReturn("�������");

		Assert.assertEquals(expected, registrationCommand.execute(request));
	}

	@Test
	public void twstProfileChangeCommand() throws Exception {
		String expected = "/HotRacesServlet?command=toProfile";
		ProfileChangeCommand profCommand = new ProfileChangeCommand();
		ProfileChangeManager profMng = mock(ProfileChangeManager.class);
		User user = mock(User.class);

		when(profMng.changeUserData(any(User.class), any(ProfileChangeData.class))).thenReturn(user);
		profCommand.setProfileChangeManager(profMng);

		when(request.getSession()).thenReturn(session);
		when(request.getSession().getAttribute("user")).thenReturn(user);
		when(request.getParameter("changeFirstName")).thenReturn("FirstName");
		when(request.getParameter("changeSecondName")).thenReturn("SecondName");
		when(request.getParameter("changeNickname")).thenReturn("nickname");
		when(request.getParameter("changeEmail")).thenReturn("Email");

		Assert.assertEquals(expected, profCommand.execute(request));
	}

	@AfterClass
	public static void destroy() {
		session = null;
		request = null;
	}
}
