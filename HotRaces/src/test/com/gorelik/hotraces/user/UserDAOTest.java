package test.com.gorelik.hotraces.user;

import java.math.BigDecimal;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.connection.ConnectionPool;
import com.gorelik.hotraces.dao.UserDAO;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.exception.DAOException;
import com.gorelik.hotraces.logic.login.LoginData;
import com.gorelik.hotraces.logic.profile.ProfileChangeData;
import com.gorelik.hotraces.logic.registration.RegistrationData;

public class UserDAOTest {
	@BeforeClass
	public static void initBeforeClass() {
		ConnectionPool.getInstance();
	}

	@Test
	public void testRightLogin() throws Exception {
		User expected = new User(3, "orangeMood", "�������", "���������", "orange@mood.com", "Client", "�������",
				BigDecimal.valueOf(168.9D));
		LoginData logData = new LoginData("orangeMood", "qwerT1");
		UserDAO userDao = new UserDAO();
		Assert.assertEquals(expected, userDao.login(logData));
		userDao.close();
	}

	@Test(expected = DAOException.class)
	public void testWrongLogin() throws DAOException {
		LoginData logData = new LoginData("wrongLogin", "qwerT1");
		UserDAO userDao = new UserDAO();
		userDao.login(logData);
		userDao.close();
	}

	@Test(expected = DAOException.class)
	public void testWrongLoginPassword() throws DAOException {
		LoginData logData = new LoginData("orangeMood", "qwerT11");
		UserDAO userDao = new UserDAO();
		userDao.login(logData);
		userDao.close();
	}

	@Test
	public void registrateNewUser() throws Exception {
		RegistrationData regData = new RegistrationData("login", "passworD1", "passworD1", "testemail@mail.ru",
				"firstname", "secondname");

		UserDAO userDao = new UserDAO();
		userDao.registrateUser(regData);
		userDao.close();
		ConnectionPool.getConnection().getPreparedStatement("DELETE FROM races.user WHERE name = 'login';")
				.executeUpdate();
	}

	// In this test login and email are already exist in DB
	@Test(expected = DAOException.class)
	public void registrateExistsUser() throws DAOException {
		RegistrationData regData = new RegistrationData("orangeMood", "passworD1", "passworD1", "orange@mood.com",
				"firstname", "secondname");
		UserDAO userDao = new UserDAO();
		userDao.registrateUser(regData);
		userDao.close();
	}

	@Test
	public void testChangeUserData() throws Exception {
		ProfileChangeData previousProfData = new ProfileChangeData("�������", "���������", "orangeMood",
				"orange@mood.com");
		ProfileChangeData profData = new ProfileChangeData("�������", "�������", "orangeMood", "orange@mood.com");

		User userToChange = new User(3, "orangeMood", "�������", "���������", "orange@mood.com", "Client", "�������");
		User expected = new User(3, "orangeMood", "�������", "�������", "orange@mood.com", "Client", "�������");

		UserDAO userDao = new UserDAO();
		userDao.changeData(userToChange, profData);

		User actual = userDao.findUserById(3);

		Assert.assertEquals(expected, actual);

		userDao.changeData(expected, previousProfData);
		userDao.close();
	}

	@Test(expected = DAOException.class)
	public void testChangeWrongUserData() throws Exception {
		ProfileChangeData profData = new ProfileChangeData("�������", "���������", "Administrator", "orange@mood.com");
		User userToChange = new User(3, "orangeMood", "�������", "���������", "orange@mood.com", "Client", "�������");

		UserDAO userDao = new UserDAO();
		userDao.changeData(userToChange, profData);

		userDao.close();
	}

	@AfterClass
	public static void destroy() {
		ConnectionPool.realClosePool();
	}
}
