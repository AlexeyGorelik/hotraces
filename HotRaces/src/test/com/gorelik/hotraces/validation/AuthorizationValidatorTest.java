package test.com.gorelik.hotraces.validation;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.logic.login.LoginData;
import com.gorelik.hotraces.validation.AuthorizationValidator;

public class AuthorizationValidatorTest {
	private static LoginData rightLoginData;
	private static LoginData wrongLoginData;
	private static LoginData wrongPasswordData;

	@BeforeClass
	public static void initVariables() {
		rightLoginData = new LoginData("ValeraCrutch", "DMB2001foreva");
		wrongLoginData = new LoginData("NY", "DMB2001foreva");
		wrongPasswordData = new LoginData("ValeraCrutch", "password");
	}

	@Test
	public void testRightLogin() {
		AuthorizationValidator authValid = new AuthorizationValidator();
		authValid.checkAuthentication(rightLoginData);
		Assert.assertTrue(rightLoginData.getValidation());
	}

	@Test
	public void testWrongLogin() {
		AuthorizationValidator authValid = new AuthorizationValidator();
		authValid.checkAuthentication(wrongLoginData);
		Assert.assertFalse(wrongLoginData.getValidation());
	}

	@Test
	public void testWrongPassword() {
		AuthorizationValidator authValid = new AuthorizationValidator();
		authValid.checkAuthentication(wrongPasswordData);
		Assert.assertFalse(wrongPasswordData.getValidation());
	}
}
