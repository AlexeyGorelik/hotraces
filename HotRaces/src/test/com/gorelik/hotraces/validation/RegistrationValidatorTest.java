package test.com.gorelik.hotraces.validation;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.logic.registration.RegistrationData;
import com.gorelik.hotraces.validation.RegistrationValidator;

public class RegistrationValidatorTest {
	private static RegistrationValidator dateValid;

	private static RegistrationData rightData;
	private static RegistrationData wrongFirstNameData;
	private static RegistrationData wrongSecondNameData;
	private static RegistrationData wrongLoginData;
	private static RegistrationData wrongEmailData;
	private static RegistrationData wrongPasswordData;
	private static RegistrationData wrongRepeatPasswordData;

	@BeforeClass
	public static void initVariables() {
		dateValid = new RegistrationValidator();

		rightData = new RegistrationData("Login1", "qwerT1", "qwerT1", "dimaoleg@gmail.com", "First", "Second");
		wrongFirstNameData = new RegistrationData("Login1", "qwerT1", "qwerT1", "dimaoleg@gmail.com", "1", "Second");
		wrongSecondNameData = new RegistrationData("Login1", "qwerT1", "qwerT1", "dimaoleg@gmail.com", "First", "2");
		wrongLoginData = new RegistrationData("Lo", "qwerT1", "qwerT1", "dimaoleg@gmail.com", "First", "Second");
		wrongEmailData = new RegistrationData("Login1", "qwerT1", "qwerT1", "gmail.com", "First", "Second");
		wrongPasswordData = new RegistrationData("Login1", "qwerT", "qwerT1", "dimaoleg@gmail.com", "First", "Second");
		wrongRepeatPasswordData = new RegistrationData("Login1", "qwerT1", "qwer", "dimaoleg@gmail.com", "First",
				"Second");
	}

	@Test
	public void testRightData() {
		dateValid.checkRegistration(rightData);
		Assert.assertTrue(rightData.getValidation());
	}

	@Test
	public void testWrongFirstNameData() {
		dateValid.checkRegistration(wrongFirstNameData);
		Assert.assertFalse(wrongFirstNameData.getValidation());
	}

	@Test
	public void testWrongSecondNameData() {
		dateValid.checkRegistration(wrongSecondNameData);
		Assert.assertFalse(wrongSecondNameData.getValidation());
	}

	@Test
	public void testWrongLoginData() {
		dateValid.checkRegistration(wrongLoginData);
		Assert.assertFalse(wrongLoginData.getValidation());
	}

	@Test
	public void testWrongEmailData() {
		dateValid.checkRegistration(wrongEmailData);
		Assert.assertFalse(wrongEmailData.getValidation());
	}

	@Test
	public void testWrongPasswordData() {
		dateValid.checkRegistration(wrongPasswordData);
		Assert.assertFalse(wrongPasswordData.getValidation());
	}

	@Test
	public void testWrongRepeatPasswordData() {
		dateValid.checkRegistration(wrongRepeatPasswordData);
		Assert.assertFalse(wrongRepeatPasswordData.getValidation());
	}
}
