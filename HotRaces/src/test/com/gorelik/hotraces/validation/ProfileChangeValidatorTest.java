package test.com.gorelik.hotraces.validation;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.logic.profile.ProfileChangeData;
import com.gorelik.hotraces.validation.ProfileChangeValidator;

public class ProfileChangeValidatorTest {
	private static ProfileChangeValidator changeValidator;

	private static ProfileChangeData rightData;
	private static ProfileChangeData wrongFirstNameData;
	private static ProfileChangeData wrongSecondNameData;
	private static ProfileChangeData wrongLoginData;
	private static ProfileChangeData wrongEmailData;

	@BeforeClass
	public static void initVariables() {
		changeValidator = new ProfileChangeValidator();

		rightData = new ProfileChangeData("Alexey", "Gorelik", "Login", "testemail@gmail.com");
		wrongFirstNameData = new ProfileChangeData("Alex1", "Gorelik", "Login", "testemail@gmail.com");
		wrongSecondNameData = new ProfileChangeData("Alexey", "Go1", "Login", "testemail@gmail.com");
		wrongLoginData = new ProfileChangeData("Alexey", "Gorelik", "wl", "testemail@gmail.com");
		wrongEmailData = new ProfileChangeData("Alexey", "Gorelik", "Login", "testemailgmail.com");
	}

	@Test
	public void testRightData() {
		changeValidator.checkChangeValidation(rightData);
		Assert.assertTrue(rightData.getValidation());
	}

	@Test
	public void testWrongFirstNameData() {
		changeValidator.checkChangeValidation(wrongFirstNameData);
		Assert.assertFalse(wrongFirstNameData.getValidation());
	}

	@Test
	public void testWrongSecondNameData() {
		changeValidator.checkChangeValidation(wrongSecondNameData);
		Assert.assertFalse(wrongSecondNameData.getValidation());
	}

	@Test
	public void testWrongLoginNameData() {
		changeValidator.checkChangeValidation(wrongLoginData);
		Assert.assertFalse(wrongLoginData.getValidation());
	}

	@Test
	public void testWrongEmailNameData() {
		changeValidator.checkChangeValidation(wrongEmailData);
		Assert.assertFalse(wrongEmailData.getValidation());
	}
}
