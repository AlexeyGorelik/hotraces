package test.com.gorelik.hotraces.validation;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.logic.payment.PaymentData;
import com.gorelik.hotraces.validation.PaymentValidator;

public class PaymentValidationTest {
	private static PaymentValidator paymendValid;
	private static PaymentData rightData;
	private static PaymentData wrongCvvData;
	private static PaymentData wrongSumData;
	private static PaymentData wrongMonthData;
	private static PaymentData wrongYearData;
	private static PaymentData wrongCardCodeData;

	@BeforeClass
	public static void initVariables() {
		paymendValid = new PaymentValidator();

		rightData = new PaymentData("123", "52.5", "02", "23", "3214", "3211", "3211", "8328");
		wrongCvvData = new PaymentData("CVV", "52.5", "02", "23", "3214", "3211", "3211", "8328");
		wrongSumData = new PaymentData("412", "52.R", "02", "23", "3214", "3211", "3211", "8328");
		wrongMonthData = new PaymentData("412", "52.5", "13", "23", "3214", "3211", "3211", "8328");
		wrongYearData = new PaymentData("412", "52.5", "02", "AS", "3214", "3211", "3211", "8328");
		wrongCardCodeData = new PaymentData("412", "52.5", "02", "23", "CARD", "CODE", "3211", "8328");
	}

	@Test
	public void testRightData() {
		paymendValid.validate(rightData);
		Assert.assertTrue(rightData.getValidation());
	}

	@Test
	public void testWrongCvvData() {
		paymendValid.validate(wrongCvvData);
		Assert.assertFalse(wrongCvvData.getValidation());
	}

	@Test
	public void testWrongSumData() {
		paymendValid.validate(wrongSumData);
		Assert.assertFalse(wrongSumData.getValidation());
	}

	@Test
	public void testWrongMonthData() {
		paymendValid.validate(wrongMonthData);
		Assert.assertFalse(wrongMonthData.getValidation());
	}

	@Test
	public void testWrongYearData() {
		paymendValid.validate(wrongYearData);
		Assert.assertFalse(wrongYearData.getValidation());
	}

	@Test
	public void testWrongCardCodeData() {
		paymendValid.validate(wrongCardCodeData);
		Assert.assertFalse(wrongCardCodeData.getValidation());
	}
}
