package test.com.gorelik.hotraces.validation;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.validation.DateValidator;

public class DateValidatorTest {
	private static String fullDate = "2017-04-03 16:00:00";
	private static String cutDate = "2017-04-03 16:00";
	private static String wrongFullDate = "2017-04-03 ads";
	private static String wrongCutDate = "2017-04-03 qwwr";

	private static DateValidator dateValid;

	@BeforeClass
	public static void initVariables() {
		dateValid = new DateValidator();
	}

	@Test
	public void testFullDate() {
		Assert.assertTrue(dateValid.checkFullDate(fullDate));
	}

	@Test
	public void testCutDate() {
		Assert.assertTrue(dateValid.checkCutDate(cutDate));
	}

	@Test
	public void testWrongFullDate() {
		Assert.assertFalse(dateValid.checkFullDate(wrongFullDate));
	}

	@Test
	public void testWrongCutDate() {
		Assert.assertFalse(dateValid.checkFullDate(wrongCutDate));
	}
}
