package test.com.gorelik.hotraces.validation;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.entity.SupportMessage;
import com.gorelik.hotraces.validation.SupportValidator;

public class SupportValidatorTest {
	private static SupportValidator suppValid;

	private static SupportMessage rightSuppMsg;
	private static SupportMessage wrongEmailSuppMsg;
	private static SupportMessage wrongTypeSuppMsg;
	private static SupportMessage wrongTextSuppMsg;

	private static String rightEmail = "email@mail.ru";
	private static String wrongEmail = "emailrambler.ru@";

	private static String rightType = "3";
	private static String wrongType = "24";

	private static String rightMessage = "Simple right message";
	private static String wrongMessage = "Wrong";

	private static String date = "2017-04-04 11:11:11";

	@BeforeClass
	public static void initVariables() {
		suppValid = new SupportValidator();

		rightSuppMsg = new SupportMessage(rightEmail, rightType, rightMessage, date);
		wrongEmailSuppMsg = new SupportMessage(wrongEmail, rightType, rightMessage, date);
		wrongTypeSuppMsg = new SupportMessage(rightEmail, wrongType, rightMessage, date);
		wrongTextSuppMsg = new SupportMessage(rightEmail, rightType, wrongMessage, date);
	}

	@Test
	public void testRightData() {
		Assert.assertTrue(suppValid.checkSuppMessage(rightSuppMsg));
	}

	@Test
	public void testWrongEmailData() {
		Assert.assertFalse(suppValid.checkSuppMessage(wrongEmailSuppMsg));
	}

	@Test
	public void testWrongTypeData() {
		Assert.assertFalse(suppValid.checkSuppMessage(wrongTypeSuppMsg));
	}

	@Test
	public void testMessageData() {
		Assert.assertFalse(suppValid.checkSuppMessage(wrongTextSuppMsg));
	}
}
