package test.com.gorelik.hotraces.message;

import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.gorelik.hotraces.command.SendMessageCommand;
import com.gorelik.hotraces.command.SendNewMessageCommand;
import com.gorelik.hotraces.command.ToNewMessageCommand;
import com.gorelik.hotraces.command.ToWriteMessageCommand;
import com.gorelik.hotraces.entity.User;
import com.gorelik.hotraces.logic.message.MessageManager;

public class MessageCommandTest {
	private static HttpSession session;
	private static HttpServletRequest request;
	private MessageManager messageMng;

	@Before
	public void initTempVariables() {
		session = mock(HttpSession.class);
		request = mock(HttpServletRequest.class);
		messageMng = mock(MessageManager.class);
	}

	@Test
	public void testToNewMessageCommand() throws Exception {
		String expected = "/jsp/profile/client/profileClient.jsp";

		ToNewMessageCommand messageCommand = new ToNewMessageCommand();
		messageCommand.setMessageMng(messageMng);

		User user = mock(User.class);

		when(request.getSession()).thenReturn(session);
		when(request.getSession().getAttribute("user")).thenReturn(user);
		when(user.getRole()).thenReturn("Client");
		when(messageMng.findUserList()).thenReturn(null);

		Assert.assertEquals(expected, messageCommand.execute(request));
	}

	@Test
	public void testToWriteMEssageCommand() {
		String expected = "/HotRacesServlet?command=toProfile";

		ToWriteMessageCommand messageCommand = new ToWriteMessageCommand();
		messageCommand.setMessageMng(messageMng);

		User user = mock(User.class);

		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("user")).thenReturn(user);
		when(request.getParameter("user-to")).thenReturn("Ivan22");
		when(user.getUserId()).thenReturn(3);

		Assert.assertEquals(expected, messageCommand.execute(request));
	}

	@Test
	public void testSendNewMessageCommand() throws Exception {
		String expected = "/HotRaces/HotRacesServlet?command=profileMessages";

		SendNewMessageCommand messageCommand = new SendNewMessageCommand();
		messageCommand.setMessageMng(messageMng);

		User user = mock(User.class);

		when(request.getSession()).thenReturn(session);
		when(request.getSession().getAttribute("user")).thenReturn(user);
		Mockito.doNothing().when(messageMng).sendMessage(any(Integer.class), any(String.class), any(String.class));
		when(request.getParameter("userToName")).thenReturn("Ivan22");
		when(request.getParameter("message-text")).thenReturn("Some message");

		Assert.assertEquals(expected, messageCommand.execute(request));
	}

	@Test
	public void testSendMessageCommand() throws Exception {
		String expected = "/HotRaces/HotRacesServlet?command=toWriteMessage&user-to=Ivan22#history-with";

		SendMessageCommand messageCommand = new SendMessageCommand();
		messageCommand.setMessageMng(messageMng);

		User user = mock(User.class);

		when(request.getSession()).thenReturn(session);
		when(request.getSession().getAttribute("user")).thenReturn(user);
		Mockito.doNothing().when(messageMng).sendMessage(any(Integer.class), any(String.class), any(String.class));
		when(request.getParameter("userToName")).thenReturn("Ivan22");
		when(request.getParameter("message-text")).thenReturn("Some message");

		Assert.assertEquals(expected, messageCommand.execute(request));
	}
}
