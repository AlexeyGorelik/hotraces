package test.com.gorelik.hotraces.message;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import java.util.Properties;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.ext.mysql.MySqlMetadataHandler;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.connection.ConnectionPool;
import com.gorelik.hotraces.dao.MessageDAO;
import com.gorelik.hotraces.dao.UserDAO;
import com.gorelik.hotraces.entity.Message;

public class MessageDAOTest {
	private static final String DB_PROPERTIES = "resources/db.properties";
	private static final String KEY_URL = "db.url";
	private static final String KEY_USER = "db.name";
	private static final String KEY_PASSWORD = "db.password";
	private static final String SCHEMA_NAME = "races";

	private static final String XML_CONVERSATION = "test/resources/conversationDefault.xml";
	private static final String XML_CONVERSATION_REPLY = "test/resources/conversationReplyDefault.xml";

	static IDatabaseConnection connection;

	private static IDataSet beforeConvData;
	private static IDataSet beforeConvReplyData;

	private static FlatXmlDataSetBuilder builder;

	@BeforeClass
	public static void init() throws Exception {
		ConnectionPool.getInstance();

		Properties prop = new Properties();
		prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(DB_PROPERTIES));
		Connection conn = DriverManager.getConnection(prop.getProperty(KEY_URL), prop.getProperty(KEY_USER),
				prop.getProperty(KEY_PASSWORD));

		connection = new DatabaseConnection(conn, SCHEMA_NAME);

		DatabaseConfig config = connection.getConfig();
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
		config.setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());

		builder = new FlatXmlDataSetBuilder();
		builder.setColumnSensing(true);

		beforeConvData = buildDataSet(XML_CONVERSATION);
		beforeConvReplyData = buildDataSet(XML_CONVERSATION_REPLY);

		ConnectionPool.getInstance();

		DatabaseOperation.DELETE_ALL.execute(connection, beforeConvReplyData);
		DatabaseOperation.DELETE_ALL.execute(connection, beforeConvData);

		DatabaseOperation.INSERT.execute(connection, beforeConvData);
		DatabaseOperation.INSERT.execute(connection, beforeConvReplyData);
	}

	static IDataSet buildDataSet(String path) throws DataSetException {
		return builder.build(Thread.currentThread().getContextClassLoader().getResourceAsStream(path));
	}

	@Before
	public void initBeforeVar() throws Exception {
		DatabaseOperation.DELETE_ALL.execute(connection, beforeConvReplyData);
		DatabaseOperation.DELETE_ALL.execute(connection, beforeConvData);

		DatabaseOperation.INSERT.execute(connection, beforeConvData);
		DatabaseOperation.INSERT.execute(connection, beforeConvReplyData);
	}

	@Test
	public void testToNewMessage() throws Exception {
		Integer expected = 5;

		UserDAO userDao = new UserDAO();
		List<String> list = userDao.findUserList();
		Integer actual = list.size();
		userDao.close();

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testToMessageCommand() throws Exception {
		Integer expected = 2;

		MessageDAO messageDao = new MessageDAO();
		List<Message> list = messageDao.findMessageList(3);
		Integer actual = list.size();
		messageDao.close();

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testSendNewMessage() throws Exception {
		Integer expected = 2;

		MessageDAO messageDao = new MessageDAO();
		messageDao.sendNewMessage(6, "Ivan22", "test message");

		List<Message> list = messageDao.findMessageList(3);
		Integer actual = list.size();
		messageDao.close();

		Assert.assertEquals(expected, actual);
	}

	@AfterClass
	public static void destroy() throws Exception {
		DatabaseOperation.DELETE_ALL.execute(connection, beforeConvReplyData);
		DatabaseOperation.DELETE_ALL.execute(connection, beforeConvData);

		DatabaseOperation.INSERT.execute(connection, beforeConvData);
		DatabaseOperation.INSERT.execute(connection, beforeConvReplyData);

		ConnectionPool.realClosePool();
	}
}
