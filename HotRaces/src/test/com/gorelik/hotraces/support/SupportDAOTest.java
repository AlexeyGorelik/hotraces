package test.com.gorelik.hotraces.support;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.dbunit.Assertion;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.ext.mysql.MySqlMetadataHandler;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.connection.ConnectionPool;
import com.gorelik.hotraces.dao.SupportDAO;
import com.gorelik.hotraces.entity.SupportMessage;
import com.gorelik.hotraces.exception.DAOException;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class SupportDAOTest {
	private static final String DB_PROPERTIES = "resources/db.properties";
	private static final String KEY_URL = "db.url";
	private static final String KEY_USER = "db.name";
	private static final String KEY_PASSWORD = "db.password";
	private static final String SCHEMA_NAME = "races";
	private static final String SUPPORT_TABLE = "support";

	private static final String XML_SUPPORT_DATA = "test/resources/supportDefault.xml";
	private static final String XML_SUPPORT_NEW_MSG = "test/resources/supportNewMessage.xml";

	static IDatabaseConnection connection;
	static IDataSet beforeClassData;

	private static FlatXmlDataSetBuilder builder;

	IDataSet beforeData;

	@BeforeClass
	public static void init() throws SQLException, IOException, DatabaseUnitException {

		Properties prop = new Properties();
		prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(DB_PROPERTIES));
		Connection conn = DriverManager.getConnection(prop.getProperty(KEY_URL), prop.getProperty(KEY_USER),
				prop.getProperty(KEY_PASSWORD));

		connection = new DatabaseConnection(conn, SCHEMA_NAME);

		DatabaseConfig config = connection.getConfig();
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
		config.setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());

		builder = new FlatXmlDataSetBuilder();
		builder.setColumnSensing(true);

		beforeClassData = buildDataSet(XML_SUPPORT_DATA);

		ConnectionPool.getInstance();

		DatabaseOperation.CLEAN_INSERT.execute(connection, beforeClassData);
	}

	@Before
	public void initTempVariables() throws Exception {
		beforeData = buildDataSet(XML_SUPPORT_DATA);

		DatabaseOperation.CLEAN_INSERT.execute(connection, beforeData);
	}

	@After
	public void tearDown() throws Exception {
		DatabaseOperation.DELETE_ALL.execute(connection, beforeData);
	}

	@AfterClass
	public static void restoreValues() throws Exception {
		DatabaseOperation.INSERT.execute(connection, beforeClassData);
	}

	static IDataSet buildDataSet(String path) throws DataSetException {
		return builder.build(Thread.currentThread().getContextClassLoader().getResourceAsStream(path));
	}

	@Test
	public void checkNewMessageCommand() throws Exception {
		String[] ignoreCols = { "id" };

		String email = "testMessage@gmail.com";
		String type = "5";
		String message = "New message for test.";
		String date = "2017-05-05 13:33:49";

		SupportMessage suppMsg = new SupportMessage(email, type, message, date);

		SupportDAO suppDao = new SupportDAO();
		suppDao.sendQuestion(suppMsg);
		suppDao.close();

		IDataSet expectedDataSet = buildDataSet(XML_SUPPORT_NEW_MSG);

		ITable expectedTable = expectedDataSet.getTable(SUPPORT_TABLE);
		ITable actualTable = connection.createTable(SUPPORT_TABLE);

		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignoreCols);
	}

	/**
	 * Note that you have to add expected time according to YOUR UTC-time Time
	 * stores in database with UTC = 0
	 */
	@Test
	public void testMessageList() throws DAOException {
		String[] messages = {
				"������������, ��� ����� ���������� ������? ���� �������� ����� �����, �� �� ���� ��� ����� ��� ��������. ������� ������� �� �����.",
				"������������. ��� ���� �����������. ��� ������ ����� ��� ��. ������ �� �� ���� ���������� ������ ������ ���������? ���� ����� �� ������ ��� ����� �� �������� ����." };
		List<SupportMessage> expected = new ArrayList<SupportMessage>();

		expected.add(new SupportMessage("testemail@mail.ru", "������", messages[0], "2017-05-01 08:06:52"));
		expected.add(
				new SupportMessage("lovethedesign@gmail.com", "����� �������", messages[1], "2017-05-01 10:40:24"));

		SupportDAO suppDao = new SupportDAO();
		List<SupportMessage> actual = suppDao.findSuppMessageList();
		suppDao.close();

		assertThat(actual, is(expected));
	}
}