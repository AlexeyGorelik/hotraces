package test.com.gorelik.hotraces.support;

import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.gorelik.hotraces.command.SendSuppMessageCommand;
import com.gorelik.hotraces.entity.SupportMessage;
import com.gorelik.hotraces.logic.support.SupportManager;

public class SupportCommandTest {
	private static HttpServletRequest request;

	@Before
	public void initTempVariables() throws Exception {
		request = mock(HttpServletRequest.class);
	}

	@Test
	public void testNewSuppMessage() throws Exception {
		SendSuppMessageCommand suppMsgCommand = new SendSuppMessageCommand();
		String expected = "/HotRaces/HotRacesServlet?command=toSupport&success=true";

		when(request.getParameter("email")).thenReturn("testemail@mail.ru");
		when(request.getParameter("quest-type")).thenReturn("1");
		when(request.getParameter("text-question")).thenReturn("�������� ������. �������� �������.");

		SupportManager suppMng = mock(SupportManager.class);
		Mockito.doNothing().when(suppMng).sendQuestion(any(SupportMessage.class));
		suppMsgCommand.setSupportManager(suppMng);

		Assert.assertEquals(expected, suppMsgCommand.execute(request));
	}
}
