package test.com.gorelik.hotraces.bookmaker;

import static org.mockito.Mockito.*;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import com.gorelik.hotraces.command.DeleteRaceBookmakerCommand;
import com.gorelik.hotraces.command.SetCoeffCommand;
import com.gorelik.hotraces.command.ToSetCoeffCommand;
import com.gorelik.hotraces.logic.bookmaker.BookmakerSetCoeffData;
import com.gorelik.hotraces.logic.race.BookmakerRaceManager;
import com.gorelik.hotraces.logic.race.RaceData;

public class BookmakerCommandTest {
	private static HttpSession session;
	private static HttpServletRequest request;

	@Before
	public void initTempVariables() {
		session = mock(HttpSession.class);
		request = mock(HttpServletRequest.class);
	}

	@Test
	public void deleteRaceBookmaker() throws Exception {
		String expected = "/HotRacesServlet?command=toProfile";

		DeleteRaceBookmakerCommand deleteCommand = new DeleteRaceBookmakerCommand();
		BookmakerRaceManager raceMng = mock(BookmakerRaceManager.class);
		deleteCommand.setDeleteManager(raceMng);

		Mockito.doNothing().when(raceMng).deleteRace(any(RaceData.class), any(String.class));
		when(request.getParameter("selected-row-value")).thenReturn("��������;2017-05-09 12:12:12;�������");
		when(request.getSession()).thenReturn(session);
		when(request.getSession().getAttribute("locale")).thenReturn("ru_RU");

		Assert.assertEquals(expected, deleteCommand.execute(request));
	}

	@Test
	public void testToSetCoeffCommand() throws Exception {
		String expected = "/jsp/profile/bookmaker/bookmakerSetCoeff.jsp";

		ToSetCoeffCommand setCommand = new ToSetCoeffCommand();
		BookmakerRaceManager raceMng = mock(BookmakerRaceManager.class);
		setCommand.setCoeffManager(raceMng);

		when(raceMng.findRacesToSetCoeff(any(RaceData.class))).thenReturn(new ArrayList<BookmakerSetCoeffData>());
		when(request.getParameter("selected-row-value")).thenReturn("��������;09-05-2017 16:40;�������");
		when(request.getSession()).thenReturn(session);
		when(request.getSession().getAttribute("locale")).thenReturn("ru_RU");

		Assert.assertEquals(expected, setCommand.execute(request));
	}

	@Test
	public void testSetCoeffCommand() throws Exception {
		String expected = "/HotRaces/HotRacesServlet?command=toCoeffTable";

		SetCoeffCommand setCommand = new SetCoeffCommand();
		BookmakerRaceManager raceMng = mock(BookmakerRaceManager.class);
		setCommand.setRaceManager(raceMng);

		when(request.getSession()).thenReturn(session);
		when(request.getSession().getAttribute("locale")).thenReturn("ru_RU");
		when(request.getParameter("race-name")).thenReturn("��������");
		when(request.getParameter("race-date")).thenReturn("2017-05-09 19:50:00");
		when(request.getParameter("horseCount")).thenReturn("0");

		Mockito.doNothing().when(raceMng).setCoeffInRace(any(RaceData.class), any(String.class),
				Matchers.anyListOf(BookmakerSetCoeffData.class));

		Assert.assertEquals(expected, setCommand.execute(request));
	}
}