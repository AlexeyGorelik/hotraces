package test.com.gorelik.hotraces.bookmaker;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gorelik.hotraces.connection.ConnectionPool;
import com.gorelik.hotraces.dao.HorseDAO;
import com.gorelik.hotraces.dao.RaceDAO;
import com.gorelik.hotraces.logic.bookmaker.BookmakerTableRaceData;
import com.gorelik.hotraces.logic.race.RaceData;

public class BookmakerDAOTest 
{
	//�������� �����
	@BeforeClass
    public static void init() throws Exception
    {
        ConnectionPool.getInstance();
    }
	
	@Test
	public void toSetCoeff() throws Exception
	{
		int expected = 4;
		
		RaceDAO raceDao = new RaceDAO();
		List<BookmakerTableRaceData> actual = raceDao.findBookmakerTable();
		raceDao.close();
		
		Assert.assertEquals(expected, actual.size());
	}
	
	@Test
	public void testCarryOutRace() throws Exception
	{
		String expected = "���������";
		String raceName = "������";
		String raceDate = "2017-05-07 16:48:55";
		String dbRaceDate = "2017-05-07 13:48:55";
		
		RaceData raceData = new RaceData(raceName, raceDate);
		RaceData dbRaceData = new RaceData(raceName, dbRaceDate);
		Map<String, String> horseMap = new HashMap<String, String>();
		horseMap.put("1", "����");
		horseMap.put("2", "����");
		
		RaceDAO raceDao = new RaceDAO();
		raceDao.createRace(raceData, horseMap);
		
		raceDao.carryOutRace(raceData);
		
		String actual = raceDao.getRaceStatus(dbRaceData);
		
		raceDao.deleteRaceFromDB(dbRaceData);
		raceDao.close();
		
		HorseDAO horseDao = new HorseDAO();
		horseDao.realDeleteHorse(dbRaceData, 1);
		horseDao.realDeleteHorse(dbRaceData, 2);
		horseDao.close();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testDeleteRaceBookmaker() throws Exception
	{
		String expected = "�������";
		String raceName = "������";
		String raceDate = "2017-05-09 16:48:55";
		String dbRaceDate = "2017-05-09 13:48:55";
		
		RaceData raceData = new RaceData(raceName, raceDate);
		RaceData dbRaceData = new RaceData(raceName, dbRaceDate);
		Map<String, String> horseMap = new HashMap<String, String>();
		horseMap.put("1", "����");
		horseMap.put("2", "����");
		
		RaceDAO raceDao = new RaceDAO();
		raceDao.createRace(raceData, horseMap);
		
		raceDao.deleteRaceBookmaker(raceData);
		
		String actual = raceDao.getRaceStatus(dbRaceData);
		
		raceDao.deleteRaceFromDB(dbRaceData);
		raceDao.close();
		
		HorseDAO horseDao = new HorseDAO();
		horseDao.realDeleteHorse(dbRaceData, 1);
		horseDao.realDeleteHorse(dbRaceData, 2);
		horseDao.close();
		
		Assert.assertEquals(expected, actual);
	}
	
	@AfterClass
	public static void restoreValues() throws Exception
	{
		 ConnectionPool.realClosePool();
	}
}
